<?php
    $relationID = EconomyRelation::findRelationID(Yii::app()->user->economyID, Yii::app()->user->economyID);
    $relationRuleList = Rule::getEconomyRelationRules($relationID);
    $economyRuleList = Rule::getEconomyRules(Yii::app()->user->economyID);
    $totalRuleList = array_merge($relationRuleList, $economyRuleList);
?>

<div class="modal fade" id="petition_for_rule_change" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">

            <!-- Petition Form -->
            <form action="/index.php/government/create-rule-petition" method="post">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title">Petition For Rule Change</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
						<label for="Message_ruleID" class="control-label">Rule</label>
		                <select class="chosen form-control" name="Petition[ruleID]" type="text" id="Message_ruleID" required>
                            <option value=""></option>
		                    <?php
		                        foreach ($totalRuleList as $rule)
		                        {
                                    $name = $rule->getRuleName();
                                    $type = $rule->getRuleType();

                                    if ($type == RuleType::MIN_VALUE || 
                                        $type == RuleType::MAX_VALUE ||
                                        $type == RuleType::TAXATION_FROM_PURCHASER ||
                                        $type == RuleType::TAXATION_FROM_PURCHASER_GROUP ||
                                        $type == RuleType::TAXATION_OF_ENTITY_WEALTH)
                                    {
                                        echo '<option value="' . $rule->getRuleID() . '">';
                                        echo $rule->getRuleName() . ' : $' . $rule->getRuleValue();
                                        echo '</option>';
                                    }
		                        }
		                    ?>
		                </select>
                    </div>
                    <div class="form-group">
		                <label for="Petition_ruleValue" class="control-label">New Value</label>
                        <div class="input-group">
                            <span class="input-group-addon"><?php echo Economy::MONETARY_SYMBOL; ?></span>
    		                <input class="form-control money" name= "Petition[ruleValue]" type="text" id="Petition_ruleValue" required>
                        </div>
		            </div>
                </div>
                <div class="modal-footer">
                    <div class="btn-group btn-group-justified">
                        <div class="btn-group">
                            <button class="btn btn-default" type="button" data-dismiss="modal">Cancel</button>
                        </div>
                        <div class="btn-group">
                            <button class="btn btn-primary" type="submit">Submit</button>
                        </div>
                    </div>
                </div>
            </form>

        </div>
    </div>
</div>
