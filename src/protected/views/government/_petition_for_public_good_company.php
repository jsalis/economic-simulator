<?php
	$workerList = Economy::getEntitiesInEconomy(Yii::app()->user->economyID, EntityType::ENTITY_TYPE_WORKER_NAME);
?>

<div class="modal fade" id="petition_for_public_good_company" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">

            <!-- Petition Form -->
            <form action="/index.php/government/create-public-good-petition" method="post">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title">Petition For Public Good Company</h4>
                </div>
                <div class="modal-body">
					<div class="form-group">
						<label for="Petition_workerID" class="control-label">Receiver</label>
		                <select class="chosen form-control" name= "Petition[workerID]" type="text" id="Petition_workerID" required>
                            <option value=""></option>
		                    <?php
		                        for ($i = 0; $i < sizeof($workerList); $i++)
		                        {
		                            echo '<option value="' . $workerList[$i]->getEntityID() . '">';
		                            echo $workerList[$i]->getEntityName();
		                            echo '</option>';
		                        }
		                    ?>
		                </select>
		            </div>
                </div>
                <div class="modal-footer">
                    <div class="btn-group btn-group-justified">
                        <div class="btn-group">
                            <button class="btn btn-default" type="button" data-dismiss="modal">Cancel</button>
                        </div>
                        <div class="btn-group">
                            <button class="btn btn-primary" type="submit">Submit</button>
                        </div>
                    </div>
                </div>
            </form>

        </div>
    </div>
</div>
