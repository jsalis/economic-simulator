<?php
	$companyList = Economy::getEntitiesInEconomy(Yii::app()->user->economyID, EntityType::ENTITY_TYPE_COMPANY_NAME);
    $workerList = Economy::getEntitiesInEconomy(Yii::app()->user->economyID, EntityType::ENTITY_TYPE_WORKER_NAME);
?>

<div class="modal fade" id="petition_for_money_transfer" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">

            <!-- Petition Form -->
            <form action="/index.php/government/create-money-transfer-petition" method="post">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title">Petition For Money Transfer</h4>
                </div>
                <div class="modal-body">
					<div class="form-group">
						<label for="Petition_entityID" class="control-label">Receiver</label>
		                <select class="chosen form-control" name= "Petition[entityID]" type="text" id="Petition_entityID" required>
                            <option value=""></option>
		                    <?php
                                echo '<optgroup label="Company">';
		                        for ($i = 0; $i < sizeof($companyList); $i++)
		                        {
		                            echo '<option value="' . $companyList[$i]->getEntityID() . '">';
		                            echo $companyList[$i]->getEntityName();
		                            echo '</option>';
		                        }
                                echo '</optgroup>';

                                echo '<optgroup label="Worker">';
                                for ($i = 0; $i < sizeof($workerList); $i ++)
                                {
                                    echo '<option value="' . $workerList[$i]->getEntityID() . '">';
                                    echo $workerList[$i]->getEntityName();
                                    echo '</option>';
                                }
                                echo '</optgroup>';
		                    ?>
		                </select>
                    </div>
                    <div class="form-group">
		                <label for="Petition_money" class="control-label">Payment</label>
                        <div class="input-group">
                            <span class="input-group-addon"><?php echo Economy::MONETARY_SYMBOL; ?></span>
                            <input class="form-control money" name= "Petition[money]" type="text" id="Petition_money" required>
                        </div>
		            </div>
                </div>
                <div class="modal-footer">
                    <div class="btn-group btn-group-justified">
                        <div class="btn-group">
                            <button class="btn btn-default" type="button" data-dismiss="modal">Cancel</button>
                        </div>
                        <div class="btn-group">
                            <button class="btn btn-primary" type="submit">Submit</button>
                        </div>
                    </div>
                </div>
            </form>

        </div>
    </div>
</div>
