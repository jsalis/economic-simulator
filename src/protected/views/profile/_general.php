<?php
	$this->renderPartial('/profile/_transfer-money');
?>

<!-- General Panel -->
<div class="panel panel-default">
	<form action="/index.php/<?php echo Yii::app()->user->roleType; ?>/update-name" method="post">
	  	<div class="panel-body">
	  		<div class="form-group">
	    		<div class="pull-right">
	    			<?php
	    				if ($entity->getEntityType() == EntityType::ENTITY_TYPE_COMPANY_NAME && 
	    					$entity->getEntityID() == Yii::app()->user->roleID)
	    				{
	    					echo '<button class="btn btn-default btn-sm btn-edit" type="button">';
	    					echo '&nbsp;<span class="glyphicon glyphicon-pencil"></span>&nbsp;';
	    					echo '</button>';
	    				}
	    			?>
	    		</div>
	    		<span class="lead static-input" name="Entity_name"><?php echo $entity->getEntityName(); ?></span>
	    		<input type="hidden" name="Entity[id]" value="<?php echo $entity->getEntityID(); ?>">
	    		<input name="Entity[name]" id="Entity_name" type="text" class="form-control hide" placeholder="Name" required>
	    		<p class="text-muted"><?php echo $entity->getEntityType(); ?></p>
	    	</div>
    		<span class="lead"><span class="text-success"><?php echo Economy::MONETARY_SYMBOL . $entity->getEntityBalance(); ?></span></span>
    		<p class="text-muted">Balance</p>
	    	<?php
	    		if ($entity->getEntityType() == EntityType::ENTITY_TYPE_WORKER_NAME)
	    		{
	    			echo '<span class="lead">';
	    			echo ($entity->hasEnoughHealth()) ? 'Healthy' : '<span class="text-danger">Sick</span>';
	    			echo '</span>';
	    			echo '<p class="text-muted">Status</p>';

	    			$healthTip = 'Fill your health by consuming food items or you will be unable to work the next day.';
	    			$healthDecimal = min($entity->getEntityHealth() / $entity->getEntityHealthNecessity(), 1);
	    			$healthPercent = round($healthDecimal * 100, 2);
	    			
	    			echo '<div class="progress" style="margin-top: 20px; margin-bottom: 0;" data-toggle="tooltip" data-placement="bottom" title="' . $healthTip . '">';
	    			echo '<div class="progress-bar progress-bar-primary" role="progressbar" style="width: ' . $healthPercent . '%" aria-valuenow="' . $healthPercent . '" aria-valuemin="0" aria-valuemax="100">';
	    			echo $healthPercent . '%';
	    			echo '</div>';
	    			echo '</div>';
	    			echo '<p class="text-center text-muted" style="margin-bottom: 0">Health</p>';
	    		}
	    		if ($entity->getEntityType() == EntityType::ENTITY_TYPE_COMPANY_NAME)
	    		{
	    			echo '<span class="lead">' . $entity->getCompanyType() . '</span>';
	    			echo '<p class="text-muted">Company Type</p>';
	    		}
	    	?>
	    	<div class="form-buttons hide">
	    		<div class="btn-group btn-group-justified">
	    			<div class="btn-group">
			    		<button class="btn btn-default btn-cancel" type="button">Cancel</button>
			    	</div>
			    	<div class="btn-group">
			    		<button class="btn btn-primary" type="submit">Save</button>
			    	</div>
		    	</div>
	    	</div>
	  	</div>
  	</form>
</div>

<!-- Button Bar -->
<div class="form-group btn-group btn-group-justified">
	<?php
		if ($entity->getEntityType() != EntityType::ENTITY_TYPE_GOVERNMENT_NAME &&
			$viewer->getEntityType() != EntityType::ENTITY_TYPE_ACCESSOR_NAME)
		{
			echo CHtml::link('Transfer Money', null, array(
				'class' => 'btn btn-sm btn-default',
				'data-toggle' => 'modal',
				'data-target' => '#transfer_money',
			));
		}
	?>
</div>
