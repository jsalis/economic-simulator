<?php
	$error = $model->getError(EntityController::ERROR_KEY);
	$viewer = Entity::model()->findByPk(Yii::app()->user->roleID);
?>

<div class="col-sm-3">
	<?php
		$this->renderPartial('/profile/_general', array(
			'entity' => $entity,
			'viewer' => $viewer,
		));
	?>
</div>
<div class="col-sm-9">

	<div class="alert alert-warning alert-dismissable <?php if (!isset($error)) echo 'hide'; ?>">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		<?php echo $error; ?>
	</div>

	<div class="panel panel-default">
	  	<div class="panel-body">
	  		<!-- Tab List -->
		    <ul class="nav nav-tabs">
		        <?php
		        	if ($entity->getEntityType() == EntityType::ENTITY_TYPE_WORKER_NAME)
		        	{
		        		echo "<li class='active'>";
		            	echo "<a href='#inventory' data-toggle='tab'>Inventory</a>";
		        		echo "</li>";
		        	}

		        	if ($entity->getEntityType() == EntityType::ENTITY_TYPE_COMPANY_NAME)
		        	{
		        		echo "<li class='active'>";
		            	echo "<a href='#products' data-toggle='tab'>Products</a>";
		        		echo "</li>";
		        	}

		        	if ($entity->getEntityType() == EntityType::ENTITY_TYPE_WORKER_NAME || 
		        		$entity->getEntityType() == EntityType::ENTITY_TYPE_COMPANY_NAME)
		        	{
		        		echo "<li>";
		            	echo "<a href='#capital' data-toggle='tab'>Capital</a>";
		        		echo "</li>";
		        	}

		        	if ($entity->getEntityType() == EntityType::ENTITY_TYPE_COMPANY_NAME)
		        	{
		        		echo "<li>";
		            	echo "<a href='#employees' data-toggle='tab'>Employees</a>";
		        		echo "</li>";
		        	}

		        	if ($entity->getEntityType() == EntityType::ENTITY_TYPE_GOVERNMENT_NAME)
		        	{
		        		echo "<li class='active'>";
		            	echo "<a href='#petitions' data-toggle='tab'>Petitions</a>";
		        		echo "</li>";
		        	}

		        	if (Yii::app()->user->roleType == EntityType::ENTITY_TYPE_ACCESSOR_NAME)
		        	{
		        		echo "<li>";
		            	echo "<a href='#rules' data-toggle='tab'>Rules</a>";
		        		echo "</li>";
		        	}
		        ?>
		    </ul>
		</div>
		<div class="panel-body">
		    <!-- Tab content -->
			<div class="tab-content">
				<?php
					if ($entity->getEntityType() == EntityType::ENTITY_TYPE_WORKER_NAME)
		        	{
						$this->renderPartial('/profile/_inventory', array(
							'entity' => $entity,
						));
					}

					if ($entity->getEntityType() == EntityType::ENTITY_TYPE_COMPANY_NAME)
					{
						$this->renderPartial('/profile/_product', array(
							'entity' => $entity,
							'viewer' => $viewer,
						));
					}

		        	if ($entity->getEntityType() == EntityType::ENTITY_TYPE_WORKER_NAME || 
		        		$entity->getEntityType() == EntityType::ENTITY_TYPE_COMPANY_NAME)
		        	{
						$this->renderPartial('/profile/_capital', array(
							'entity' => $entity,
							'viewer' => $viewer,
						));
					}

					if ($entity->getEntityType() == EntityType::ENTITY_TYPE_COMPANY_NAME)
					{
						$this->renderPartial('/profile/_employee', array(
							'entity' => $entity,
							'viewer' => $viewer,
						));
					}

					if ($entity->getEntityType() == EntityType::ENTITY_TYPE_GOVERNMENT_NAME)
					{
						$this->renderPartial('/profile/_petition', array(
							'entity' => $entity,
							'viewer' => $viewer,
						));
					}

					if (Yii::app()->user->roleType == EntityType::ENTITY_TYPE_ACCESSOR_NAME)
					{
						echo '<div class="tab-pane" id="rules">';
						$this->renderPartial('/teacher/_rules', array(
							'ruleList' => $entity->getEntityRuleList(),
						));
						echo '</div>';
					}
				?>
			</div>
		</div>
	</div>

</div>
