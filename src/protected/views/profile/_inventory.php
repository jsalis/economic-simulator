<?php
	$entityHasItems = EntityHasItem::findByEntityID($entity->getEntityID(), ItemType::ITEM_TYPE_NONFOOD_NAME);
?>

<!-- Inventory Panel -->
<div class="tab-pane in active" id="inventory">
    <div class="row form-group hide">
        <div class="col-xs-6 col-md-4">
            <div class="btn-group">
                <!-- Buttons -->
            </div>
        </div>
    </div>
	<table class="table-hover" data-filter="#filter" data-filter-text-only="true">
        <thead>
            <tr class="<?php if (sizeof($entityHasItems) == 0) echo 'hide'; ?>">
                <th style="white-space: nowrap">Item</th>
                <th style="white-space: nowrap">Type</th>
                <th data-sort-ignore="true">Description</th>
                <th style="white-space: nowrap">Creator</th>
                <th style="white-space: nowrap">Date Purchased</th>
            </tr>
        </thead>
        <tbody>
            <?php
                for ($i = 0; $i < sizeof($entityHasItems); $i++)
                {
                    $item = $entityHasItems[$i]->getItem();
                	$type = $item->getItemType();
                	$creator = $item->entityCreator->getEntityName();

                    echo "<tr>";
                    echo "<td>" . $item->getItemName() . "</td>";
                    echo "<td style='white-space: nowrap'>" . $type . "</td>";
                    echo "<td>" . $item->getItemDescription() . "</td>";
                    echo "<td>" . $creator . "</td>";
                    echo "<td style='white-space: nowrap'>" . $entityHasItems[$i]->getDatePurchased()->format('M d') . "</td>";
                    echo "</tr>";
                }
                if (sizeof($entityHasItems) == 0)
                {
                    echo "<tr><td><em>No items found.</em></td></tr>";
                }
            ?>
        </tbody>
    </table>
</div>
