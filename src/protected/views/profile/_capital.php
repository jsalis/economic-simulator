<?php
	$entityHasItems = $entity->getEntityHasItems();
    $rentItemList = $entity->getRentItemList();
    $totalItemList = array_merge($entityHasItems, $rentItemList);

    if ($entity->getEntityType() == EntityType::ENTITY_TYPE_WORKER_NAME ||
        $entity->getEntityType() == EntityType::ENTITY_TYPE_COMPANY_NAME)
    {
        $this->renderPartial('/worker/_create-rent-offer', array(
            'entityHasItems' => $entity->getEntityHasItems(ItemType::ITEM_TYPE_LAND_NAME),
            'entity' => $entity,
        ));
    }
?>

<!-- Capital Panel -->
<div class="tab-pane" id="capital">
    <div class="row form-group">
        <div class="col-xs-6 col-md-4">
            <div class="btn-group">
                <?php
                    if ($viewer->getEntityType() == EntityType::ENTITY_TYPE_WORKER_NAME ||
                        $viewer->getEntityType() == EntityType::ENTITY_TYPE_COMPANY_NAME)
                    {
                        echo '<a class="btn btn-sm btn-primary"  data-toggle="modal" data-target="#give_for_rent">Create Rent Offer</a>';
                    }
                ?>
            </div>
        </div>
    </div>
  	<div>
        <table class="table-hover" data-filter="#filter" data-filter-text-only="true">
            <thead>
                <tr class="<?php if (sizeof($totalItemList) == 0) echo 'hide'; ?>">
                    <th style="white-space: nowrap">Capital</th>
                    <th style="white-space: nowrap">Type</th>
                    <th data-sort-ignore="true">Description</th>
                    <th style="white-space: nowrap">Creator</th>
                    <th style="white-space: nowrap">Ownership</th>
                </tr>
            </thead>
            <tbody>
                <?php
                    for ($i = 0; $i < sizeof($totalItemList); $i++)
                    {
                        $ownership = ($totalItemList[$i] instanceof EntityHasItem) ? 'Owned' : 'Rented';

                        $item = $totalItemList[$i]->getItem();
                        $type = $item->getItemType();
                        $maker = $item->entityCreator->getEntityName();

                        if ($type == ItemType::ITEM_TYPE_DEGREE_NAME || 
                            $type == ItemType::ITEM_TYPE_MACHINE_NAME || 
                            $type == ItemType::ITEM_TYPE_LAND_NAME)
                        {
                            echo "<tr>";
                            echo "<td>" . $item->getItemName() . "</td>";
                            echo "<td>" . $type . "</td>";
                            echo "<td>" . $item->getItemDescription() . "</td>";
                            echo "<td>" . $maker . "</td>";
                            echo "<td>" . $ownership . "</td>";
                            echo "</tr>";
                        }
                    }
                    if (sizeof($totalItemList) == 0)
                    {
                        echo "<tr><td><em>No capital found.</em></td></tr>";
                    }
                ?>
            </tbody>
        </table>
  	</div>
</div>
