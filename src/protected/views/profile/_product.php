<?php
    $viewerType = $viewer->getEntityType();
    $companyType = $entity->getCompanyType();
    $economyRelationList = EconomyRelation::findByEconomyFromID(Yii::app()->user->economyID);

    $maxFoodPrice = Rule::findEconomyRule(Yii::app()->user->economyID, RuleType::MAX_VALUE, 'Food Price')->getRuleValue();
    $minFoodPrice = Rule::findEconomyRule(Yii::app()->user->economyID, RuleType::MIN_VALUE, 'Food Price')->getRuleValue();

    $maxNonFoodPrice = Rule::findEconomyRule(Yii::app()->user->economyID, RuleType::MAX_VALUE, 'Non-Food Price')->getRuleValue();
    $minNonFoodPrice = Rule::findEconomyRule(Yii::app()->user->economyID, RuleType::MIN_VALUE, 'Non-Food Price')->getRuleValue();

    $scriptData = array();

    if ($companyType == Company::COMPANY_TYPE_PUBLIC_GOOD)
    {
        $accessorID = Economy::getAccessorID(Yii::app()->user->economyID);
        $itemList = Item::getItemsByCreator($accessorID, ItemType::ITEM_TYPE_PUBLIC_GOOD_NAME);   
    }
    else
    {
        $itemList = $entity->getCompanyItemList();
        
        $this->renderPartial('/company/_create-product', array(
            'companyType' => $companyType,
            'economyRelationList' => $economyRelationList,
        ));
        $this->renderPartial('/company/_update-product', array(
            'companyType' => $companyType,
            'economyRelationList' => $economyRelationList,
        ));
        
        for ($i = 0; $i < sizeof($itemList); $i++)
        {
            $itemID = $itemList[$i]->getItemID();
            $scriptData[$itemID] = array(
                'name' => $itemList[$i]->getItemName(),
                'description' => $itemList[$i]->getItemDescription(),
                'price' => $itemList[$i]->getPrice(),
                'export' => Economy::getEconomyNameByID($itemList[$i]->getExportEconomy()),
            );
        }
    }
?>

<script>
    var productData = <?php echo json_encode($scriptData); ?>;
</script>

<!-- Company Product Panel -->
<div class="tab-pane in active" id="products">
    <div class="row form-group">
        <div class="col-xs-6 col-md-4 <?php if ($viewerType != EntityType::ENTITY_TYPE_COMPANY_NAME) echo 'hide'; ?>">
            <a class="btn btn-sm btn-primary <?php if ($companyType == Company::COMPANY_TYPE_PUBLIC_GOOD) echo 'hide'; ?>" data-toggle="modal" data-target="#create_product">Create Product</a> 
        </div>
    </div>
	<div>
        <table class="table-hover" data-filter="#filter" data-filter-text-only="true">
            <thead>
                <tr class="<?php if (sizeof($itemList) == 0) echo 'hide'; ?>">
                <?php
                if ($companyType != Company::COMPANY_TYPE_PUBLIC_GOOD)
                {
                    echo '<th style="white-space: nowrap">Product</th>';
                    echo '<th style="white-space: nowrap">Type</th>';
                    echo '<th data-sort-ignore="true">Description</th>';
                    echo '<th style="white-space: nowrap">Price</th>';
                    echo '<th style="white-space: nowrap">Stock</th>';
                    echo '<th style="white-space: nowrap">Sold</th>';
                    // echo '<th style="white-space: nowrap">Perished</th>';
                    echo '<th style="white-space: nowrap">Export</th>';
                }
                else
                {
                    echo '<th>Public Good</th>';
                    echo '<th data-sort-ignore="true">Description</th>';
                }
                ?>
                </tr>
            </thead>
            <tbody>
            	<?php
            		for ($i = 0; $i < count($itemList); $i++)
            		{
                        if ($companyType != Company::COMPANY_TYPE_PUBLIC_GOOD)
                        {
                            $id = $itemList[$i]->getItemID();
                            $economyName = Economy::getEconomyNameByID($itemList[$i]->getExportEconomy());

                            $price = $itemList[$i]->getPrice();
                            $validPrice = true;

                            if (($price < $minFoodPrice || $price > $maxFoodPrice) && $itemList[$i]->getItemType() == ItemType::ITEM_TYPE_FOOD_NAME)
                            {
                                $validPrice = false;
                            }

                            if (($price < $minNonFoodPrice || $price > $maxNonFoodPrice) && $itemList[$i]->getItemType() == ItemType::ITEM_TYPE_NONFOOD_NAME)
                            {
                                $validPrice = false;
                            }

                			echo "<tr id='$id' data-toggle='modal' data-target='#update_product' style='cursor: pointer'>";
                            echo "<td>" . $itemList[$i]->getItemName() . "</td>";
                			echo "<td>" . $itemList[$i]->getItemType() . "</td>";
                            echo "<td>" . $itemList[$i]->getItemDescription() . "</td>";

                            if ($validPrice)
                            {
                                echo "<td data-type='numeric' data-value='" . $price . "'><span class='text-success'>" . Economy::MONETARY_SYMBOL . $price . "</span></td>";
                            }
                            else
                            {
                                echo "<td data-type='numeric' data-value='" . $price . "'><span class='text-danger' data-toggle='tooltip' data-placement='bottom' title='Price is not within the valid range and must be adjusted.'>" . Economy::MONETARY_SYMBOL . $price . "</span></td>";
                            }

                            echo "<td data-type='numeric'>" . $itemList[$i]->getItemStock() . "</td>";
                            echo "<td data-type='numeric'>" . $itemList[$i]->getUnitsSold() . "</td>";
                            // echo "<td data-type='numeric'>" . $itemList[$i]->getUnitsPerished() . "</td>";
                            echo "<td>" . $economyName . "</td>";
                			echo "</tr>";
                        }
                        else
                        {
                            echo "<tr>";
                            echo "<td id='name'>" . $itemList[$i]->getItemName() . "</td>";
                            echo "<td id='description'>" . $itemList[$i]->getItemDescription() . "</td>";
                            echo "<td id='monetary_start'>" . $itemList[$i]->getBenefitStart(Entity::ENTITY_ATTRIBUTE_MONETARY) . "</td>";
                            echo "<td id='monetary_decrease_rate'>" . $itemList[$i]->getBenefitDecreaseRate(Entity::ENTITY_ATTRIBUTE_MONETARY) . "</td>";
                            echo "<td id='efficiency_start'>" . $itemList[$i]->getBenefitStart(Entity::ENTITY_ATTRIBUTE_EFFICIENCY) . "</td>";
                            echo "<td id='efficiency_decrease_rate'>" . $itemList[$i]->getBenefitDecreaseRate(Entity::ENTITY_ATTRIBUTE_EFFICIENCY) . "</td>";
                            echo "<td id='health_start'>" . $itemList[$i]->getBenefitStart(Entity::ENTITY_ATTRIBUTE_HEALTH) . "</td>";
                            echo "<td id='health_decrease_rate'>" . $itemList[$i]->getBenefitDecreaseRate(Entity::ENTITY_ATTRIBUTE_HEALTH) . "</td>";
                            echo "<td id='type'>" . $itemList[$i]->getBenefitReceiver() . "</td>";
                            echo "</tr>";
                        }
            		}
            		if (count($itemList) == 0)
            		{
            			echo "<tr><td><em>No products found.</em></td></tr>";
            		}
            	?>
            </tbody>
        </table>
  	</div>
</div>

<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/site/product.js"></script>
