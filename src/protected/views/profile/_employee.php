<?php
	$employeeList = $entity->getCurrentEmployees();

    if ($viewer->getEntityType() == EntityType::ENTITY_TYPE_COMPANY_NAME)
    {
        $this->renderPartial('/company/_create-job-offer', array(
            'entity' => $entity,
        ));
    }
?>

<!-- Company Employee Panel -->
<div class="tab-pane" id="employees">
    <div class="row form-group">
        <div class="col-xs-6 col-md-4 <?php if ($viewer->getEntityType() != EntityType::ENTITY_TYPE_COMPANY_NAME) echo 'hide'; ?>">
            <a class="btn btn-sm btn-primary" data-toggle="modal" data-target="#create_job_offer">Create Job Offer</a> 
        </div>
    </div>
	<div class="scroller-large">
        <table class="table-hover" data-filter="#filter" data-filter-text-only="true">
            <thead>
                <tr class="<?php if (sizeof($employeeList) == 0) echo 'hide'; ?>">
                    <th data-sort-ignore="true">Employee Name</th>
                </tr>
            </thead>
            <tbody>
                <?php
                    for ($i = 0; $i < sizeof($employeeList); $i++)
                    {
                        $id = $employeeList[$i]->getEntityID();

                        echo "<tr>";
                        echo "<td>" . $employeeList[$i]->getEntityName() . "</td>";
                        echo "</tr>";
                    }
                    if (sizeof($employeeList) == 0)
                    {
                        echo "<tr><td><em>No employees found.</em></td></tr>";
                    }
                ?>
            </tbody>
        </table>
  	</div>
</div>
