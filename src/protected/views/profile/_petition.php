<?php
    if ($viewer->getEntityType() == EntityType::ENTITY_TYPE_GOVERNMENT_NAME)
    {
        $userID = Yii::app()->user->getId();
        $economyID = Yii::app()->user->economyID;
        $worker = Worker::model()->user($userID)->economy($economyID)->find();
    }

    $petitionList = $entity->getPetitions();

    if ($viewer->getEntityType() == EntityType::ENTITY_TYPE_GOVERNMENT_NAME)
    {
        $this->renderPartial('/government/_petition_for_rule_change', array(
            'entity' => $entity,
        ));
        $this->renderPartial('/government/_petition_for_public_good_company', array(
            'entity' => $entity,
        ));
        $this->renderPartial('/government/_petition_for_money_transfer', array(
            'entity' => $entity,
        ));
    }
?>

<!-- Government Petition Panel -->
<div class="tab-pane in active" id="petitions">
    <div class="row form-group <?php if ($viewer->getEntityType() != EntityType::ENTITY_TYPE_GOVERNMENT_NAME) echo 'hide'; ?>">
        <div class="col-xs-12">
            <div class="btn-group">
                <a class="btn btn-sm btn-default" data-toggle="modal" data-target="#petition_for_rule_change">Petition For Rule Change</a> 
                <a class="btn btn-sm btn-default" data-toggle="modal" data-target="#petition_for_public_good_company">Petition For Public Good Company</a> 
                <a class="btn btn-sm btn-default" data-toggle="modal" data-target="#petition_for_money_transfer">Petition For Money Transfer</a>
            </div>
        </div>
    </div>
	<div>
        <table class="table-hover" data-filter="#filter" data-filter-text-only="true">
            <thead>
                <tr class="<?php if (sizeof($petitionList) == 0) echo 'hide'; ?>">
                    <th data-sort-ignore="true">Petition</th>
                    <th data-type="numeric" style="white-space: nowrap">Date Created</th>
                    <th style="white-space: nowrap">Votes For</th>
                    <th style="white-space: nowrap">Votes Against</th>
                    <th style="white-space: nowrap">Status</th>
                </tr>
            </thead>
            <tbody>
                <?php
                    for ($i = 0; $i < sizeof($petitionList); $i++)
                    {
                        $petitionID = $petitionList[$i]->getMessage()->getMessageID();
                        $petitionTitle = $petitionList[$i]->getMessage()->findByKey(Message::MESSAGE_KEY_CONTENT);
                        $dateReceived = $petitionList[$i]->getDateReceived();
                        $voteData = $entity->getVoteCountForPetition($petitionList[$i]->getEntitySenderID(), $petitionID);

                        $isClosed = $petitionList[$i]->getMessageReplied();
                        $hasVoted = true;

                        if ($viewer->getEntityType() == EntityType::ENTITY_TYPE_GOVERNMENT_NAME)
                        {
                            $hasVoted = $entity->hasVotedForPetition($petitionList[$i]->getEntitySenderID(), $petitionID, $worker->getEntityID());
                        }

                        echo '<tr>';
                        echo '<td>' . $petitionTitle . '</td>';
                        echo '<td style="white-space: nowrap" data-value="' . $dateReceived->getTimestamp() . '">' . $dateReceived->format('M d, g:i A') . '</td>';
                        echo '<td>' . $voteData['voteAgreeCount'] . '</td>';
                        echo '<td>' . ($voteData['voteCount'] - $voteData['voteAgreeCount']) . '</td>';

                        echo '<td class="fit">';
                        if ($isClosed)
                        {
                            $status = $petitionList[$i]->getMessage()->findByKey(Message::MESSAGE_KEY_VOTE_RESPONSE);
                            echo ($status == Government::VOTE_YES) ? '<span class="text-primary">Passed</span>' : '<span class="text-danger">Declined</span>';   
                        }
                        else if (!$hasVoted && $viewer->getEntityType() == EntityType::ENTITY_TYPE_GOVERNMENT_NAME)
                        {
                            echo '<div class="btn-group">';

                            // Overrides inline issue with bootstrap button groups in tables.
                            $style = 'float: none; display: inline-block;';

                            echo CHtml::beginForm('/index.php/government/vote-petition', 'post', array('style' => $style));
                            echo '<input type="hidden" name="Vote[petitionCreatorID]" value="' . $petitionList[$i]->getEntitySenderID() . '">';
                            echo '<input type="hidden" name="Vote[petitionID]" value="' . $petitionList[$i]->getMessage()->getMessageID() . '">';
                            echo '<input type="hidden" name="Vote[response]" value="' . Government::VOTE_YES . '">';
                            echo CHtml::submitButton('YES', array(
                                'class' => 'btn btn-sm btn-primary',
                            ));
                            echo '</form>';

                            echo CHtml::beginForm('/index.php/government/vote-petition', 'post', array('style' => $style));
                            echo '<input type="hidden" name="Vote[petitionCreatorID]" value="' . $petitionList[$i]->getEntitySenderID() . '">';
                            echo '<input type="hidden" name="Vote[petitionID]" value="' . $petitionList[$i]->getMessage()->getMessageID() . '">';
                            echo '<input type="hidden" name="Vote[response]" value="' . Government::VOTE_NO . '">';
                            echo CHtml::submitButton('NO', array(
                                'class' => 'btn btn-sm btn-default',
                            ));
                            echo '</form>';

                            echo '</div>';
                        }
                        else
                        {
                            echo 'Pending';
                        }
                        echo '</td>';

                        echo '</tr>';
                    }
                    if (sizeof($petitionList) == 0)
                    {
                        echo '<tr><td><em>No petitions found.</em></td></tr>';
                    }
                ?>
            </tbody>
        </table>
  	</div>
</div>
