<?php
    $form = new TransferMoneyForm;
?>

<div class="modal fade" id="transfer_money" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">

            <?php
                $widget = $this->beginWidget('CActiveForm', array(
                    'action' => $this->createUrl(strtolower(Yii::app()->user->roleType) . '/transferMoney'),
                    'enableAjaxValidation' => true,
                    'enableClientValidation' => true,
                    'clientOptions' => array(
                        'validateOnSubmit' => true,
                        'validateOnChange' => true,
                        'validateOnType' => false,
                    ),
                ));
            ?>
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Transfer Money</h4>
                </div>
                <div class="modal-body">

                    <div class="row">
                        <div class="col-sm-6 form-group">
                            <?php
                                echo $widget->label($form, 'receiver_id');

                                $criteria = array('condition' => 't.entity_id != ' . Yii::app()->user->roleID);
                                
                                $workerList = Worker::model()->economy(Yii::app()->user->economyID)->findAll($criteria);
                                $companyList = Company::model()->economy(Yii::app()->user->economyID)->findAll($criteria);

                                $receiverList = array_merge($workerList, $companyList);
                                $receiverData = CHtml::listData($receiverList, 'entity_id', 'entity', 'entityType.entity_type');

                                echo $widget->dropDownList($form, 'receiver_id', $receiverData, array(
                                    'class' => 'chosen form-control',
                                    'empty' => '',
                                ));

                                echo $widget->error($form, 'receiver_id');
                            ?>
                        </div>
                        <div class="col-sm-6 form-group">
                            <?php
                                echo $widget->label($form, 'amount');

                                echo '<div class="input-group">';
                                echo '<span class="input-group-addon">' . Economy::MONETARY_SYMBOL . '</span>';

                                echo $widget->textField($form, 'amount', array(
                                    'class' => 'form-control money',
                                ));

                                echo '</div>';

                                echo $widget->error($form, 'amount');
                            ?>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <div class="btn-group btn-group-justified">
                        <div class="btn-group">
                            <button class="btn btn-default" type="button" data-dismiss="modal">Cancel</button>
                        </div>
                        <div class="btn-group">
                            <?php echo CHtml::submitButton('Submit', array('class' => 'btn btn-primary')); ?>
                        </div>
                    </div>
                </div>
            <?php $this->endWidget(); ?>

        </div>
    </div>
</div>
