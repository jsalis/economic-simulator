<?php
    $viewerType = Yii::app()->user->roleType;

    $message = $entityHasMessage->getMessage();
    $messageType = $message->getMessageType();

    $isClosed = $messageType == MessageType::MESSAGE_TYPE_ACCEPT_JOB_OFFER_NAME || $messageType == MessageType::MESSAGE_TYPE_REJECT_JOB_OFFER_NAME;
    $isPublic = $entityHasMessage->entityReceiver->getEntityType() == EntityType::ENTITY_TYPE_PUBLIC_MESSENGER_NAME;
    $canReply = $viewerType == EntityType::ENTITY_TYPE_WORKER_NAME || !$isPublic;

    $companyName = $entityHasMessage->entitySender->getEntityName();

    $content = $message->findByKey(Message::MESSAGE_KEY_CONTENT);
    $itemID = $message->getRootMessage()->findByKey(Message::MESSAGE_KEY_ITEM_ID);
    $payment = $message->findByKey(Message::MESSAGE_KEY_MONEY);

    if ($messageType == MessageType::MESSAGE_TYPE_REVISE_JOB_OFFER_NAME)
    {
        $previousOffer = $message->getPreviousMessage()->findByKey(Message::MESSAGE_KEY_MONEY);
    }

    if ($messageType == MessageType::MESSAGE_TYPE_ACCEPT_JOB_OFFER_NAME)
    {
        $unitsProduced = $message->findByKey(Message::MESSAGE_KEY_UNITS);
    }

    $scriptData = array(
        'messageType' => array(
            'accept' => MessageType::MESSAGE_TYPE_ACCEPT_JOB_OFFER_NAME,
            'decline' => MessageType::MESSAGE_TYPE_REJECT_JOB_OFFER_NAME,
            'revise' => MessageType::MESSAGE_TYPE_REVISE_JOB_OFFER_NAME,
        ),
        'messageContent' => array(
            'accept' => $model->getEntityName() . ' has accepted the job offer.',
            'decline' => $model->getEntityName() . ' has declined the job offer.',
            'revise' => $model->getEntityName() . ' has revised the job offer.',
        ),
    );
?>

<script>
    var data = <?php echo json_encode($scriptData); ?>;
</script>

<?php
    $form = new JobOfferForm('reply');

    $form->previous_message_id = $message->getMessageID();
    $form->receiver_id = $entityHasMessage->getEntitySenderID();
    $form->payment = $payment;

    $widget = $this->beginWidget('CActiveForm', array(
        'action' => $this->createUrl(strtolower($viewerType) . '/replyJobOffer'),
        'enableAjaxValidation' => true,
        'enableClientValidation' => true,
        'clientOptions' => array(
            'validateOnSubmit' => true,
            'validateOnChange' => true,
            'validateOnType' => false,
        ),
    ));

    echo $widget->hiddenField($form, 'previous_message_id');
    echo $widget->hiddenField($form, 'message_type');
    echo $widget->hiddenField($form, 'receiver_id');
    echo $widget->hiddenField($form, 'content');
?>
    <div class="form-group">
        <p class="lead"><?php if ($isPublic) echo $companyName; else echo $content; ?></p>
    </div>
    <div class="row form-group">
        <label class="col-sm-4 control-label">Product</label>
        <div class="col-sm-8">
            <p class="form-control-static"><?php echo Item::getItemNameByID($itemID); ?></p>
        </div>
    </div>
    <div class="row form-group <?php if ($messageType != MessageType::MESSAGE_TYPE_ACCEPT_JOB_OFFER_NAME) echo 'hide'; ?>">
        <label class="col-sm-4 control-label">Units Produced</label>
        <div class="col-sm-8">
            <p class="form-control-static"><?php if (isset($unitsProduced)) echo $unitsProduced; ?></p>
        </div>
    </div>
    <div class="row form-group <?php if ($messageType != MessageType::MESSAGE_TYPE_REVISE_JOB_OFFER_NAME) echo 'hide'; ?>">
        <label class="col-sm-4 control-label">Previous Offer</label>
        <div class="col-sm-8">
            <p class="form-control-static text-success"><?php if (isset($previousOffer)) echo Economy::MONETARY_SYMBOL . $previousOffer; ?></p>
        </div>
    </div>
    <div class="row form-group">
        <div class="<?php if ($isPublic) echo 'col-sm-12'; else echo 'col-sm-6'; ?>">
            <?php
                echo $widget->label($form, 'payment');

                echo '<div class="input-group">';
                echo '<span class="input-group-addon">' . Economy::MONETARY_SYMBOL . '</span>';
                echo $widget->textField($form, 'payment', array(
                    'class' => 'form-control revise-trigger money',
                    'readonly' => ($isClosed || !$canReply),
                ));
                echo '</div>';

                echo $widget->error($form, 'payment');
            ?>
        </div>
    </div>
    <div class="<?php if ($isClosed || !$canReply) echo 'hide'; ?>">
        <div class="main-buttons btn-group-justified <?php if (!$isPublic) echo 'btn-group'; ?>">
            <div class="btn-group button-accept">
                <button class="btn btn-primary" type="button">Accept</button>
            </div>
            <div class="btn-group button-decline <?php if ($isPublic) echo 'hide'; ?>">
                <button class="btn btn-default" type="button">Decline</button>
            </div>
            <div class="btn-group button-edit">
                <button class="btn btn-default" type="button">Edit</button>
            </div>
        </div>
        <div class="revise-buttons btn-group btn-group-justified">
            <div class="btn-group button-cancel">
                <button class="btn btn-default" type="button">Cancel</button>
            </div>
            <div class="btn-group button-revise">
                <button class="btn btn-primary" type="button">Revise</button>
            </div>
        </div>
    </div>
<?php $this->endWidget(); ?>
