<?php
	$error = $model->getError(MessageFilter::ERROR_KEY);
	$viewerType = $model->getEntityType();
    $notificationList = $model->getRecentMessageList();
    $chatList = $model->getChatMessages();
    $isChat = false;
    
    if (isset($entityHasMessage))
    {
    	$isChat = $entityHasMessage->getMessage()->getMessageType() == MessageType::MESSAGE_TYPE_CHAT_NAME;
    }
?>

<div class="col-sm-12">
	<h3>Messages</h3>
</div>
<div class="col-sm-4 col-md-3">

	<!-- Message List Panel -->
	<div class="panel panel-default">
	  	<div class="panel-body">
	  		<!-- Tab List -->
		    <ul class="nav nav-tabs nav-justified">
		    	<li class="<?php if (!$isChat) echo 'active'; ?>">
		            <a href="#notifications" data-toggle="tab">Notifications</a>
		        </li>
		        <li class="<?php if ($isChat) echo 'active'; ?>">
		            <a href="#chat" data-toggle="tab">Chat</a>
		        </li>
		    </ul>
		</div>
	    <!-- Tab content -->
		<div class="list-group tab-content scroller-large">
			<div class="tab-pane <?php if (!$isChat) echo 'in active'; ?>" id="notifications">
				<?php
				 	for ($i = sizeof($notificationList) - 1; $i >= 0 ; $i--)
				 	{
				 		if (!$notificationList[$i]->getMessageReplied() || !$notificationList[$i]->getMessageChecked())
						{
					 		echo "<a class='list-group-item form-submit'>";
					 		echo "<form action='/index.php/" . $model->getEntityType() . "/display-message' method='post'>";
					 		echo "<input type='hidden' name='EntityHasMessage[id]' value='" . $notificationList[$i]->getEntityMessageID() . "'>";
					 		echo "<h4 class='list-group-item-heading'>";
					 		echo Entity::getEntityNameByID($notificationList[$i]->getEntitySenderID());
					 		if (!$notificationList[$i]->getMessageChecked())
					 		{
					 			echo "<span class='pull-right label label-primary'>New</span>";
					 		}
					 		echo "</h4>";
					 		echo "<p class='list-group-item-text'>";
					 		echo $notificationList[$i]->getMessage()->getMessageType();
					 		echo "<span class='pull-right text-muted'>" . $notificationList[$i]->getDateReceived()->format('g:i A') . "</span>";
					 		echo "</p>";
					 		echo "</form>";
					 		echo "</a>";
					 	}
				 	}
				 	if (sizeof($notificationList) == 0)
					{
						echo "<div class='list-group-item'><em>No notifications.</em></div>";
					}
				?>
	        </div>
	        <div class="tab-pane <?php if ($isChat) echo 'in active'; ?>" id="chat">
	        	<div class="list-group-item">
		    		<a class="btn btn-default btn-block" href="/index.php/<?php echo $viewerType; ?>/display-message">New Message</a>
		    	</div>
	            <?php
				 	for ($i = sizeof($chatList) - 1; $i >= 0; $i--)
				 	{
				 		if (!$chatList[$i]->getMessageReplied())
						{
					 		echo "<a class='list-group-item form-submit'>";
					 		echo "<form action='/index.php/" . $model->getEntityType() . "/display-message' method='post'>";
					 		echo "<input type='hidden' name='EntityHasMessage[id]' value='" . $chatList[$i]->getEntityMessageID() . "'>";
					 		echo "<h4 class='list-group-item-heading'>";
					 		echo Entity::getEntityNameByID($chatList[$i]->getEntitySenderID());
					 		if (!$chatList[$i]->getMessageChecked())
					 		{
					 			echo "<span class='pull-right label label-primary'>New</span>";
					 		}
					 		echo "</h4>";
					 		echo "<p class='list-group-item-text'>";

					 		$type = Entity::getEntityTypeByEntityID($chatList[$i]->getEntitySenderID());
					 		if ($type == EntityType::ENTITY_TYPE_ACCESSOR_NAME)
					 		{
					 			echo UserType::USER_TYPE_TEACHER_NAME;
					 		}
					 		else
					 		{
					 			echo $type;
					 		}
					 		echo "<span class='pull-right text-muted'>" . $chatList[$i]->getDateReceived()->format('M d, g:i A') . "</span>";
					 		echo "</p>";
					 		echo "</form>";
					 		echo "</a>";
					 	}
					}
					if (sizeof($chatList) == 0)
					{
						echo "<div class='list-group-item'><em>No chat messages.</em></div>";
					}
				?>
	        </div>
	    </div>
	</div>

</div>
<div class="col-sm-8 col-md-5">

	<div class="alert alert-warning alert-dismissable <?php if (!isset($error)) echo 'hide'; ?>">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		<?php echo $error; ?>
	</div>

	<!-- Message Content Panel -->
	<?php
		if (isset($entityHasMessage))
		{
			if ($isChat)
			{
				$this->renderPartial('/messaging/_chat', array(
					'model' => $model,
					'entityHasMessage' => $entityHasMessage
				));
			}
			else
			{
				$this->renderPartial('/messaging/_notification', array(
					'model' => $model,
					'entityHasMessage' => $entityHasMessage
				));
			}
		}
		else
		{
			$this->renderPartial('/messaging/_new-message', array(
				'model' => $model,
			));
		}
	?>

</div>
