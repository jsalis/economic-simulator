<form action="<?php echo $this->createUrl('company/close-job-offer'); ?>" method="post">
	<input type="hidden" name="JobOfferForm[message_id]" value="<?php echo $entityHasMessage->getMessage()->getMessageID(); ?>">
	<input type="hidden" name="JobOfferForm[entity_has_message_id]" value="<?php echo $entityHasMessage->getEntityMessageID(); ?>">

	<button class="btn btn-primary col-sm-12">Close Job Offer</button>
</form>