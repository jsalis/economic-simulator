<?php
    $message = $entityHasMessage->getMessage();
    $messageType = $message->getMessageType();

    $content = $message->findByKey(Message::MESSAGE_KEY_CONTENT);

    $label = $value = '';
    $isMonetary = false;
    if ($messageType == MessageType::MESSAGE_TYPE_MONETARY_TRANSFER_NAME)
    {
        $label = 'Amount';
        $value = Economy::MONETARY_SYMBOL . abs($message->findByKey(Message::MESSAGE_KEY_MONEY));
        $isMonetary = true;
    }
    else if ($messageType == MessageType::MESSAGE_TYPE_HEALTH_TRANSFER_NAME)
    {
        $label = 'Amount';
        $value = $message->findByKey(Message::MESSAGE_KEY_HEALTH);
    }
    else if ($messageType == MessageType::MESSAGE_TYPE_ITEM_TRANSFER_NAME)
    {
        $label = 'Item';
        $value = Item::getItemNameByID($message->findByKey(Message::MESSAGE_KEY_ITEM_ID));
    }
?>

<div class="form-group">
    <p class="lead"><?php echo $content; ?></p>
</div>
<div class="row form-group">
    <label class="col-sm-4 control-label"><?php echo $label; ?></label>
    <div class="col-sm-8">
        <p class="form-control-static <?php if ($isMonetary) echo 'text-success'; ?>"><?php echo $value; ?></p>
    </div>
</div>
