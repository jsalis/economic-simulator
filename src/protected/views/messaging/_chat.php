
<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title"><?php echo $entityHasMessage->entitySender->getEntityName(); ?></h3>
	</div>
	<div class="panel-body">
		<div class="scroller-small">
			<?php
				$message = $entityHasMessage->getMessage();
		    	$messageHistory = $message->getMessageHistory();
		    	$messageHistory = array_reverse($messageHistory);

		    	for ($i = 0; $i < sizeof($messageHistory); $i++)
		    	{
		    		$messageID = $messageHistory[$i]->getMessageID();
		    		$senderID = EntityHasMessage::model()->message($messageID)->find()->getEntitySenderID();

		    		if ($senderID == Yii::app()->user->roleID)
		    		{
		    			echo "<div class='pull-right'>";
		    			echo "<p class='text-right text-primary' style='width: 300px; margin-right: 10px'>";
		    		}
		    		else
		    		{
		    			echo "<div class='pull-left'>";
		    			echo "<p class='text-left text-muted' style='width: 300px; margin-left: 10px'>";
		    		}
		    		echo "<strong>" . $messageHistory[$i]->findByKey(Message::MESSAGE_KEY_CONTENT) . "</strong>";
		    		echo "</p>";
		    		echo "</div>";
		    	}
			?>
		</div>
	</div>
	<div class="panel-body">

  		<!-- Chat Form -->
  		<form action="/index.php/<?php echo $model->getEntityType(); ?>/send-message" method="post">
			<input type="hidden" name="Message[append][<?php echo Message::MESSAGE_KEY_PREVIOUS_MESSAGE; ?>]" value="<?php echo $message->getMessageID(); ?>">
			<input type="hidden" name="Message[entityID]" value="<?php echo $entityHasMessage->getEntitySenderID(); ?>">
			<input type="hidden" name="Message[messageType]" value="<?php echo MessageType::MESSAGE_TYPE_CHAT_NAME; ?>">
			<div class="form-group">
				<textarea name="Message[append][<?php echo Message::MESSAGE_KEY_CONTENT; ?>]" class="form-control" id="Message_content" rows="3" placeholder="Write a reply..." required></textarea>
			</div>
	        <button class="btn btn-primary" type="submit">Send</button>
		</form>

  	</div>
</div>
