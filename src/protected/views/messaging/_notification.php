
<div class="panel panel-default">
	<div class="panel-body">
		<?php
			$message = $entityHasMessage->getMessage();
			$type = $message->getMessageType();

	    	if ($type == MessageType::MESSAGE_TYPE_SEND_JOB_OFFER_NAME ||
	    		$type == MessageType::MESSAGE_TYPE_ACCEPT_JOB_OFFER_NAME ||
	    		$type == MessageType::MESSAGE_TYPE_REJECT_JOB_OFFER_NAME ||
	    		$type == MessageType::MESSAGE_TYPE_REVISE_JOB_OFFER_NAME)
	    	{
				$this->renderPartial('/messaging/_job-offer', array(
					'model' => $model,
					'entityHasMessage' => $entityHasMessage
				));
	    	}
	    	else if ($type == MessageType::MESSAGE_TYPE_SEND_RENT_OFFER ||
	    		$type == MessageType::MESSAGE_TYPE_ACCEPT_RENT_OFFER ||
	    		$type == MessageType::MESSAGE_TYPE_REJECT_RENT_OFFER ||
	    		$type == MessageType::MESSAGE_TYPE_REVISE_RENT_OFFER)
	    	{
				$this->renderPartial('/messaging/_rent-offer', array(
					'model' => $model,
					'entityHasMessage' => $entityHasMessage
				));
	    	}
	    	else if ($type == MessageType::MESSAGE_TYPE_MONETARY_TRANSFER_NAME || 
	    			 $type == MessageType::MESSAGE_TYPE_HEALTH_TRANSFER_NAME || 
	    			 $type == MessageType::MESSAGE_TYPE_ITEM_TRANSFER_NAME)
	    	{
	    		$this->renderPartial('/messaging/_transfer', array(
					'model' => $model,
					'entityHasMessage' => $entityHasMessage,
				));
	    	}
		?>
  	</div>
</div>

<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/site/notification.js"></script>
