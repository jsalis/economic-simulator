
<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">New Message</h3>
    </div>
    <div class="panel-body">
        <form action="/index.php/<?php echo $model->getEntityType(); ?>/send-message" method="post">
            <div class="form-group">
                <input type="hidden" name="Message[messageType]" value="<?php echo MessageType::MESSAGE_TYPE_CHAT_NAME; ?>">
                <label for="Message_entityID" class="control-label">To</label>
                <select class="chosen form-control" name= "Message[entityID]" type="text" id="Message_entityID" required>
                    <option value=""></option>
                    <?php
                        // Teacher option list
                        $entityList = Economy::getEntitiesInEconomy(Yii::app()->user->economyID, EntityType::ENTITY_TYPE_ACCESSOR_NAME);
                        echo '<optgroup label="Teacher">';
                        for ($i = 0; $i < sizeof($entityList); $i++)
                        {
                            if ($entityList[$i]->getEntityID() != Yii::app()->user->roleID)
                            {
                                echo '<option value="' . $entityList[$i]->getEntityID() . '">';
                                echo $entityList[$i]->getEntityName();
                                echo '</option>';
                            }
                        }
                        echo '</optgroup>';

                        // Worker option list
                        $entityList = Economy::getEntitiesInEconomy(Yii::app()->user->economyID, EntityType::ENTITY_TYPE_WORKER_NAME);
                        echo '<optgroup label="Worker">';
                        for ($i = 0; $i < sizeof($entityList); $i++)
                        {
                            if ($entityList[$i]->getEntityID() != Yii::app()->user->roleID)
                            {
                                echo '<option value="' . $entityList[$i]->getEntityID() . '">';
                                echo $entityList[$i]->getEntityName();
                                echo '</option>';
                            }
                        }
                        echo '</optgroup>';

                        // Company option list
                        $entityList = Economy::getEntitiesInEconomy(Yii::app()->user->economyID, EntityType::ENTITY_TYPE_COMPANY_NAME);
                        echo '<optgroup label="Company">';
                        for ($i = 0; $i < sizeof($entityList); $i++)
                        {
                            if ($entityList[$i]->getEntityID() != Yii::app()->user->roleID)
                            {
                                echo '<option value="' . $entityList[$i]->getEntityID() . '">';
                                echo $entityList[$i]->getEntityName();
                                echo '</option>';
                            }
                        }
                        echo '</optgroup>';
                    ?>
                </select>
            </div>
            <div class="form-group">
                <textarea name="Message[append][<?php echo Message::MESSAGE_KEY_CONTENT; ?>]" class="form-control" id="Message_content" rows="3" placeholder="Write a message..." required></textarea>
            </div>
            <button class="btn btn-primary" type="submit">Send</button>
        </form>
    </div>
</div>
