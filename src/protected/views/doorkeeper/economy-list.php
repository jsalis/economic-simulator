<?php
    if (!isset($economyList))
    {
        $economyList = array();
    }

    if (!isset($form))
    {
        $form = 'select_economy';
    }
?>

<div class="panel panel-body">
    <!-- Nav List -->
    <ul class="tab-list">
        <li class="<?php
            if ($form == 'select_economy') echo 'active';
            ?>">
            <a href="#select_economy" data-toggle="tab">Select Economy</a>
        </li>
        <li class="<?php 
            if (Yii::app()->user->type == 'student') echo 'hide';
            else if ($form == 'create_economy') echo 'active'; 
            ?>">
            <a href="#create_economy" data-toggle="tab">Create Economy</a>
        </li>
    </ul><br>

    <!-- Tab content -->
    <div class="tab-content">

        <div class="tab-pane fade <?php if ($form == 'select_economy') echo 'in active'; ?>" id="select_economy">
            <div class="list-group scroller-large">
                <?php
                    for ($i = 0; $i < sizeof($economyList); $i++)
                    {
                        echo '<a href="/index.php/' . Yii::app()->user->type . '/select-economy?id=' . $economyList[$i]->getEconomyID() . '" class="list-group-item';
                        if (Yii::app()->user->economyID == $economyList[$i]->getEconomyID())
                        {
                            echo ' active';
                        }
                        echo '">';
                        if ($economyList[$i]->isClosed())
                        {
                            echo '<span class="badge">Closed</span>';
                        }
                        echo '<p class="list-group-item-heading lead">' . $economyList[$i]->getEconomyName() . '</p>';
                        echo '<p class="list-group-item-text">' . $economyList[$i]->getEconomyDescription() . '</p>';
                        echo '</a>';
                    }
                ?>
            </div>
            <div class="form-signin alert alert-warning <?php if (sizeof($economyList) > 0) echo 'hide'; ?>">
                No available economies found.
                <?php
                    if (Yii::app()->user->type == 'student')
                    {
                        echo ' To get started, a teacher must add you to an economy.';
                    }
                    else if (Yii::app()->user->type == 'teacher')
                    {
                        echo ' Create an economy to get started.';
                    }
                ?>
            </div>
            <form action="/index.php/doorkeeper/logout" method="post">
                <button class="btn btn-lg btn-default btn-block" type="submit">Log Out</button>
            </form>
        </div> <!-- /select_economy -->

        <div class="tab-pane fade <?php if ($form == 'create_economy') echo 'in active'; ?>" id="create_economy">
            <form action="/index.php/teacher/create-economy" method="post">
                <div class="form-group">
                    <input name="Economy[name]" id="Economy_name" type="text" class="form-control" placeholder="Economy Name" required>
                    <textarea rows="3" style="resize: none;" name="Economy[description]" id="Economy_description" type="text" class="form-control" placeholder="Economy Description" required></textarea>
                </div>
                <button class="btn btn-lg btn-primary btn-block" type="submit">Create Economy</button>
            </form> 
        </div> <!-- /create_economy -->

    </div> <!-- /tab-content -->
</div>
