<?php
    $errorMessage = '';
    if (isset($model) && $model->hasErrors())
    {
        $error = $model->getErrors();
        foreach ($error as $key => $value)
        {
            $errorMessage .= $model->getError($key) . ' ';
        }
    }
?>

<h1>Economic Simulator<br><small></small></h1>

<div class="panel panel-body">
    <!-- Nav tabs -->
    <ul class="tab-list">
        <li class="<?php if ($form == 'login') echo 'active'; ?>"><a href="#login" data-toggle="tab">Log In</a></li>
        <li class="<?php if ($form == 'signup') echo 'active'; ?>"><a href="#signup" data-toggle="tab">Create Account</a></li>
    </ul><br>

    <!-- Tab content -->
    <div class="tab-content">

        <div class="tab-pane fade <?php if ($form == 'login') echo 'in active'; ?>" id="login">
            <form action="/index.php/doorkeeper/login" method="post">
                <h3>Please log in</h3>
                <div class="form-group">
                    <input name="DoorKeeper[emailAddress]" id="DoorKeeper_emailAddress" type="text" class="form-control" placeholder="Email" autofocus required>
                    <input name="DoorKeeper[password]" id="DoorKeeper_password" type="password" class="form-control" placeholder="Password" required>
                </div>
                <div class="form-group pull-right">
                    <label for="DoorKeeper_rememberMe">Remember me?</label>
                    <input class="switch" name="DoorKeeper[rememberMe]" id="DoorKeeper_rememberMe" value="1" type="checkbox" data-on-text="YES" data-off-text="NO">
                </div>
                <button class="btn btn-lg btn-primary btn-block" type="submit">Log In</button>
            </form> 
        </div> <!-- /login -->

        <div class="tab-pane fade <?php if ($form == 'signup') echo 'in active'; ?>" id="signup">
            <form action="/index.php/doorkeeper/signup" method="post">
                <h3>I am a...</h3>
                <div class="form-group btn-group btn-group-justified" data-toggle="buttons">
                    <label class="radio-student btn btn-default active">
                        <input type="radio" name="DoorKeeper[userType]" id="DoorKeeper_student" value="Student" checked> Student
                    </label>
                    <label class="radio-teacher btn btn-default">
                        <input type="radio" name="DoorKeeper[userType]" id="DoorKeeper_teacher" value="Teacher"> Teacher
                    </label>
                </div>
                <div class="form-group">
                    <input name="DoorKeeper[firstName]" id="DoorKeeper_firstName" type="text" class="form-control" placeholder="First Name" required>
                    <input name="DoorKeeper[lastName]" id="DoorKeeper_lastName" type="text" class="form-control" placeholder="Last Name" required>
                    <input name="DoorKeeper[emailAddress]" id="DoorKeeper_emailAddress" type="text" class="form-control" placeholder="Email" required>
                    <input name="DoorKeeper[cellPhone]" id="DoorKeeper_cellPhone" type="text" class="form-control phone" placeholder="Phone" required>
                    <input name="DoorKeeper[password]" id="DoorKeeper_password" type="password" class="form-control" placeholder="Password" required>
                    <input name="DoorKeeper[passwordRepeat]" id="DoorKeeper_passwordRepeat" type="password" class="form-control" placeholder="Confirm Password" required>
                </div>
                <button class="btn btn-lg btn-primary btn-block" type="submit">Create Account</button>
            </form> 
        </div> <!-- /signup -->

    </div> <!-- /tab-content -->
</div>

<div class="alert-error <?php if ($errorMessage == '') echo 'hide'; ?>">
    <?php echo $errorMessage; ?>
</div>


<div class="panel panel-body">
    <a class="btn btn-default col-lg-12" href="https://bitbucket.org/jsalis/economic-simulator/wiki/Home">
        Check out the wiki
    </a>
</div>