<?php
	$tab = (isset($tab)) ? $tab : 'account';

	// Construct error message
	$errorMessage = '';
    if (isset($model) && $model->hasErrors())
    {
        $error = $model->getErrors();
        foreach ($error as $key => $value)
        {
            $errorMessage .= $model->getError($key) . ' ';
        }
    }
?>

<div class="col-sm-12">
	<h3>Account Settings</h3>
</div>
<div class="col-sm-6 col-md-5">

	<div class="alert alert-danger alert-dismissable <?php if ($errorMessage == '') echo 'hide'; ?>">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		<?php echo $errorMessage; ?>
	</div>

	<div class="panel panel-default">
	  	<div class="panel-body">
	  		<!-- Tab List -->
		    <ul class="nav nav-tabs">
		    	<li class="<?php if ($tab == 'account') echo 'active'; ?>">
		            <a href="#account" data-toggle="tab">Account Details</a>
		        </li>
		        <li class="<?php if ($tab == 'password') echo 'active'; ?>">
		            <a href="#password" data-toggle="tab">Change Password</a>
		        </li>
		    </ul>
		</div>
		<div class="panel-body">
		    <!-- Tab content -->
			<div class="tab-content">
				<div class="tab-pane <?php if ($tab == 'account') echo 'active in'; ?>" id="account">
					<form class="form-horizontal" action="/index.php/<?php echo Yii::app()->user->type; ?>/update-user" method="post">
                        <div class="form-group">
                            <label class="col-sm-4 control-label">Email</label>
                            <div class="col-sm-8">
                            	<p class="form-control-static"><?php echo $model->getUserEmailAddress(); ?></p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4 control-label">Account Type</label>
                            <div class="col-sm-8">
                            	<p class="form-control-static"><?php echo $model->getUserType(); ?></p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="User_firstName" class="col-sm-4 control-label">First Name</label>
                            <div class="col-sm-8">
                            	<input name="User[firstName]" type="text" id="User_firstName" class="form-control" value="<?php echo $model->getUserFirstName(); ?>" required>
                        	</div>
                        </div>
                        <div class="form-group">
                            <label for="User_lastName" class="col-sm-4 control-label">Last Name</label>
                            <div class="col-sm-8">
                            	<input name="User[lastName]" type="text" id="User_lastName" class="form-control" value="<?php echo $model->getUserLastName(); ?>" required>
                        	</div>
                        </div>
                        <div class="form-group">
                            <label for="User_cellPhone" class="col-sm-4 control-label">Phone</label>
                            <div class="col-sm-8">
                            	<input name="User[cellPhone]" type="text" id="User_cellPhone" class="form-control phone" value="<?php echo $model->getUserCellPhone(); ?>" required>
                        	</div>
                        </div>
                        <div class="form-group">
                        	<div class="col-sm-4"></div>
                        	<div class="col-sm-8 form-buttons">
					    		<div class="btn-group btn-group-justified">
					    			<div class="btn-group">
							    		<button class="btn btn-default btn-cancel" type="button">Cancel</button>
							    	</div>
							    	<div class="btn-group">
							    		<button class="btn btn-primary" type="submit">Save</button>
							    	</div>
						    	</div>
					    	</div>
                        </div>
	                </form>
				</div>
				<div class="tab-pane <?php if ($tab == 'password') echo 'active in'; ?>" id="password">
					<form class="form-horizontal" action="/index.php/<?php echo Yii::app()->user->type; ?>/update-password" method="post">
						<div class="form-group">
                            <label for="User_currentPassword" class="col-sm-4 control-label">Current Password</label>
                            <div class="col-sm-8">
                            	<input name="User[currentPassword]" type="password" id="User_currentPassword" class="form-control" required>
                        	</div>
                        </div>
                        <div class="form-group">
                            <label for="User_newPassword" class="col-sm-4 control-label">New Password</label>
                            <div class="col-sm-8">
                            	<input name="User[newPassword]" type="password" id="User_newPassword" class="form-control" required>
                        	</div>
                        </div>
                        <div class="form-group">
                            <label for="User_repeatPassword" class="col-sm-4 control-label">Repeat Password</label>
                            <div class="col-sm-8">
                            	<input name="User[repeatPassword]" type="password" id="User_repeatPassword" class="form-control" required>
                        	</div>
                        </div>
                        <div class="pull-right">
					    	<button class="btn btn-primary" type="submit">Submit</button>
				    	</div>
					</form>
				</div>
			</div>
		</div>
	</div>

</div>
