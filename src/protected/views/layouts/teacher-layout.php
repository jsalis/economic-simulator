<?php  	
	$baseUrl = Yii::app()->request->baseUrl;
	$entityType = strtolower(Yii::app()->user->roleType);
	$entityRole = Entity::model()->findByPk(Yii::app()->user->roleID);
	$unreadMessageCount = $entityRole->getUnreadMessageCount();

	Yii::app()->clientScript->registerCoreScript('jquery');
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="language" content="en" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
		
		<!-- Title -->
		<title><?php echo CHtml::encode($this->pageTitle); ?></title>

		<!-- CSS -->
		<link rel="stylesheet" type="text/css" href="<?php echo $baseUrl; ?>/less/global.min.css">
		<link rel="stylesheet" type="text/css" href="<?php echo $baseUrl; ?>/css/animate.min.css">
		<link rel="stylesheet" type="text/css" href="<?php echo $baseUrl; ?>/css/footable.core.css">
        
        <!-- Javascript -->
        <script type="text/javascript" src="<?php echo $baseUrl; ?>/js/jquery.maskedinput.js"></script>
        <script type="text/javascript" src="<?php echo $baseUrl; ?>/js/jquery.floatThead.min.js"></script>
        <script type="text/javascript" src="<?php echo $baseUrl; ?>/js/bootstrap.min.js"></script>
		<script type="text/javascript" src="<?php echo $baseUrl; ?>/js/bootstrap-datetimepicker.min.js"></script>
		<script type="text/javascript" src="<?php echo $baseUrl; ?>/js/bootstrap-switch.min.js"></script>
		<script type="text/javascript" src="<?php echo $baseUrl; ?>/js/chosen.jquery.js"></script>
		<script type="text/javascript" src="<?php echo $baseUrl; ?>/js/footable.js"></script>
		<script type="text/javascript" src="<?php echo $baseUrl; ?>/js/footable.filter.js"></script>
		<script type="text/javascript" src="<?php echo $baseUrl; ?>/js/footable.sort.js"></script>
		<script type="text/javascript" src="<?php echo $baseUrl; ?>/js/site/global.js"></script>
	</head>

    <body>
		<div id="content-container" class="content-container">
			<nav class="sidebar sidebar-transition">
			    <ul class="sidebar-nav">	
			        <li class="sidebar-header alert-error" id="close-menu">
			        	<a>
			        		<span class="fava"><i class="fa fa-times"></i></span><span class="desc">Close</span>
			        	</a>
					</li>
					<li>
						<h3 style="padding-top: 20px"> Current Role :</h3>
		                <div class="accordion" id="accordion1">
						  	<div class="accordion-group">
						    	<div class="accordion-heading">
						      		<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#collapseOne">
						        		<?php echo $entityRole->getEntityName() . ' '; ?><b class="caret"></b>
						      		</a>
						    	</div>
						    	<div id="collapseOne" class="accordion-body collapse">
						      		<div class="accordion-inner">
										<ul>
											<?php 
												$entityList = $this->getUserEntityList();

												for ($i = 0; $i < sizeof($entityList); $i++)
												{
													if ($entityList[$i]->getEntityID() != Yii::app()->user->roleID)
													{
														echo '<li><a href="/index.php/' . Yii::app()->user->type . '/select-role?id=' . $entityList[$i]->getEntityID() . '">';
														echo $entityList[$i]->getEntityName();
														echo '</a></li>';
													}
												}
												if (sizeof($entityList) <= 1)
												{
													echo '<li><h4>No other roles</h4></li>';
												}
											?>
										</ul>
						      		</div>
						    	</div>
						  	</div>
						</div>
	                </li>
                	<li class="dropdown">
		                <div class="accordion" id="accordion3">
						  	<div class="accordion-group">
						    	<div class="accordion-heading">
						      		<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion3" href="#collapseThree">
						        		<span class="fava"><i class="fa fa-cogs"></i></span> <span class="desc">Settings <b class="caret"></b></span>
						      		</a>
						    	</div>
						    	<div id="collapseThree" class="accordion-body collapse">
						      		<div class="accordion-inner">
										<ul class="sidebar-nav-submenu">
											<li><a href="/index.php/teacher/student-settings">Students</a></li>
											<li><a href="/index.php/teacher/election-settings">Election</a></li>
											<li><a href="/index.php/teacher/capital-settings">Capital</a></li>
											<li><a href="/index.php/teacher/public-good-settings">Public Goods</a></li>
											<li><a href="/index.php/teacher/rule-settings">Economy Rules</a></li>
											<li><a href="/index.php/teacher/trade-settings">Economic Trade</a></li>
											<li><a href="/index.php/teacher/economy-statistics">Statistics</a></li>
										</ul>
						      		</div>
						    	</div>
						  	</div>
						</div>
					</li> <!-- /.dropdown -->
	                <li class="<?php if ($entityType == EntityType::ENTITY_TYPE_GOVERNMENT_NAME) echo 'hide'; ?>">
		                <div class="accordion" id="accordion2">
						  	<div class="accordion-group">
						    	<div class="accordion-heading">
						      		<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseTwo">
						        		<span class="fava"><i class="fa fa-credit-card"></i></span> <span class="desc">Markets <b class="caret"></b></span>
						      		</a>
						    	</div>
						    	<div id="collapseTwo" class="accordion-body collapse">
						      		<div class="accordion-inner">
										<ul class="sidebar-nav-submenu">
											<li><a href="/index.php/<?php echo $entityType; ?>/labor-market">Labor</a></li>
											<li><a href="/index.php/<?php echo $entityType; ?>/product-market">Product</a></li>
											<li><a href="/index.php/<?php echo $entityType; ?>/capital-market">Capital</a></li>
											<li><a href="/index.php/<?php echo $entityType; ?>/rent-market">Rent</a></li>
										</ul>
						      		</div>
						    	</div>
						  	</div>
						</div>
					</li>
	                <li>
	                    <a href="/index.php/<?php echo $entityType; ?>/forum"><span class="fava"><i class="fa fa-weixin"></i></span> <span class="desc">Forum</span></a>
	                </li>
	                <li>
	                    <a href="/index.php/<?php echo $entityType; ?>/"><span class="fava"><i class="fa fa-user"></i></span> <span class="desc">Profile</span></a>
	                </li>
	                <li>
                		<a href="/index.php/<?php echo $entityType; ?>/display-message"><span class="fava"><i class="fa fa-comment"></i></span> <span class="desc">Messages</span>
                    		<?php 
                    			if ($unreadMessageCount > 0)
                    			{
                    				echo '<span class="badge">' . $unreadMessageCount . '</span>';
                    			}
                    		?>
                    	</a>
	                </li>
					<li>
						<a href="/index.php/student/account-settings"><span class="fava"><i class="fa fa-cog"></i></span> <span class="desc">Account Settings</span></a>
					</li>
					<li>
						<a href="/index.php/" ><span class="fava"><i class="fa fa-exchange"></i></span> <span class="desc">Select Economy</span></a>
					</li>
					<li>
						<a href="/index.php/doorkeeper/logout"><span class="fava"><i class="fa fa-sign-out"></i></span> <span class="desc">Log Out</span></a>
					</li>
	            </ul>
	        </nav>
	        <!-- /#sidebar-wrapper -->

	        <!-- Page Content -->
			<div class="pusher">

				<div class="content">
		        	<!-- Fixed navbar -->
					<nav class="navbar navbar-default">
						<ul class="nav navbar-nav">
							<li><a href="#" id="menu-toggle" class="fava"><i class="fa fa-bars"></i></a></li>
			                <li>
			                	<a href="/index.php/">
									<span data-toggle="tooltip" data-placement="bottom" title="Switch Economy">
										<strong style="font-size:20px"><?php echo Economy::getEconomyNameByID(Yii::app()->user->economyID); ?></strong>
									</span>
								</a>
							</li>
						</ul>
						<ul class="nav navbar-nav" style="float: right">
							<li><a><strong><?php echo Yii::app()->user->name . ' '; ?></strong></a></li>
							<li><a href="/index.php/doorkeeper/logout"><span class="fava"><i class="fa fa-sign-out"></i></span> <span>Log Out</span></a></li>
		            	</ul>
					</nav> <!-- /.navbar -->
					<div class="main clearfix">

			            <div style="overflow: none; height: calc(100%- 60px); margin-top: 60px;">
			                <?php echo $content; ?>
			            </div>
					</div><!-- /main -->
					<!-- </div>/content-inner -->
				</div><!-- /content -->
			</div><!-- /pusher -->

			<div class="pusher-overlay"></div>
		</div><!-- /content-container -->
	</body>
</html>
