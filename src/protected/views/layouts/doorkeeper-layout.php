<?php
	$baseUrl = Yii::app()->request->baseUrl;

	Yii::app()->clientScript->registerCoreScript('jquery');
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="language" content="en" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

		<!-- Title -->
		<title><?php echo Yii::app()->name; ?></title>

		<!-- CSS -->
		<link rel="stylesheet" type="text/css" href="<?php echo $baseUrl; ?>/less/doorkeeper.min.css">
		<link rel="stylesheet" type="text/css" href="<?php echo $baseUrl; ?>/css/animate.min.css">
		
		<!-- Javascript -->
		<script type="text/javascript" src="<?php echo $baseUrl; ?>/js/jquery.maskedinput.js"></script>
		<script type="text/javascript" src="<?php echo $baseUrl; ?>/js/bootstrap.min.js"></script>
		<script type="text/javascript" src="<?php echo $baseUrl; ?>/js/bootstrap-datetimepicker.min.js"></script>
		<script type="text/javascript" src="<?php echo $baseUrl; ?>/js/bootstrap-switch.min.js"></script>
		<script type="text/javascript" src="<?php echo $baseUrl; ?>/js/site/login.js"></script>
		<script type="text/javascript" src="<?php echo $baseUrl; ?>/js/site/global.js"></script>
	</head>

	<body>
		<div class="doorkeeper animated slideInDown">
			<?php echo $content; ?>
		</div>
	</body>	
</html>
