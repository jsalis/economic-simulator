<?php  	
	$baseUrl = Yii::app()->request->baseUrl;
	$entityType = strtolower(Yii::app()->user->roleType);
	$entityRole = Entity::model()->findByPk(Yii::app()->user->roleID);
	$unreadMessageCount = $entityRole->getUnreadMessageCount();

	Yii::app()->clientScript->registerCoreScript('jquery');
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="language" content="en" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
		
		<!-- Title -->
		<title><?php echo CHtml::encode($this->pageTitle); ?></title>

		<!-- CSS -->
		<link rel="stylesheet" type="text/css" href="<?php echo $baseUrl; ?>/less/global.min.css">
		<link rel="stylesheet" type="text/css" href="<?php echo $baseUrl; ?>/css/animate.min.css">
		<link rel="stylesheet" type="text/css" href="<?php echo $baseUrl; ?>/css/footable.core.css">
        
        <!-- Javascript -->
        <script type="text/javascript" src="<?php echo $baseUrl; ?>/js/jquery.maskedinput.js"></script>
        <script type="text/javascript" src="<?php echo $baseUrl; ?>/js/jquery.floatThead.min.js"></script>
        <script type="text/javascript" src="<?php echo $baseUrl; ?>/js/bootstrap.min.js"></script>
		<script type="text/javascript" src="<?php echo $baseUrl; ?>/js/bootstrap-datetimepicker.min.js"></script>
		<script type="text/javascript" src="<?php echo $baseUrl; ?>/js/bootstrap-switch.min.js"></script>
		<script type="text/javascript" src="<?php echo $baseUrl; ?>/js/chosen.jquery.js"></script>
		<script type="text/javascript" src="<?php echo $baseUrl; ?>/js/footable.js"></script>
		<script type="text/javascript" src="<?php echo $baseUrl; ?>/js/footable.filter.js"></script>
		<script type="text/javascript" src="<?php echo $baseUrl; ?>/js/footable.sort.js"></script>
		<script type="text/javascript" src="<?php echo $baseUrl; ?>/js/site/global.js"></script>
	</head>

    <body>
    	<!-- Fixed navbar -->
		<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
			<div class="container">

				<div class="navbar-header">
                    <!-- If screen size is too small for the entire navbar, condense it -->
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button> <!-- /navbar-toggle -->
					<a href="/index.php/">
						<span class="navbar-brand" data-toggle="tooltip" data-placement="bottom" title="Switch Economy">
							<?php echo Economy::getEconomyNameByID(Yii::app()->user->economyID); ?>
						</span>
					</a>
				</div> <!-- /navbar-header -->

				<div class="navbar-collapse collapse">
					<ul class="nav navbar-nav">
						<p class="navbar-text hidden-xs">Current Role :</p>
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">
								<?php echo $entityRole->getEntityName() . ' '; ?><b class="caret"></b>
							</a>
							<ul class="dropdown-menu">
								<?php 
									$entityList = $this->getUserEntityList();
									for ($i = 0; $i < sizeof($entityList); $i++)
									{
										if ($entityList[$i]->getEntityID() != Yii::app()->user->roleID)
										{
											echo '<li><a href="/index.php/' . Yii::app()->user->type . '/select-role?id=' . $entityList[$i]->getEntityID() . '">';
											echo $entityList[$i]->getEntityName();
											echo '</a></li>';
										}
									}
									if (sizeof($entityList) <= 1)
									{
										echo '<li class="dropdown-header">No other roles</li>';
									}
								?>
							</ul> <!-- /.dropdown-menu -->
						</li>
					</ul>

                    <ul class="nav navbar-nav navbar-right">
                    	<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">Settings <b class="caret"></b></a>
							<ul class="dropdown-menu">
								<li><a href="/index.php/<?php echo Yii::app()->user->type; ?>/student-settings">Students</a></li>
								<li><a href="/index.php/<?php echo Yii::app()->user->type; ?>/election-settings">Election</a></li>
								<li><a href="/index.php/<?php echo Yii::app()->user->type; ?>/capital-settings">Capital</a></li>
								<li><a href="/index.php/<?php echo Yii::app()->user->type; ?>/public-good-settings">Public Goods</a></li>
								<li><a href="/index.php/<?php echo Yii::app()->user->type; ?>/rule-settings">Economy Rules</a></li>
								<li><a href="/index.php/<?php echo Yii::app()->user->type; ?>/trade-settings">Economic Trade</a></li>
								<li><a href="/index.php/<?php echo Yii::app()->user->type; ?>/economy-statistics">Statistics</a></li>
							</ul> <!-- /.dropdown-menu -->
						</li> <!-- /.dropdown -->
                    	<li class="dropdown <?php if ($entityType == EntityType::ENTITY_TYPE_GOVERNMENT_NAME) echo 'hide'; ?>">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">Markets <b class="caret"></b></a>
							<ul class="dropdown-menu">
								<li><a href="/index.php/<?php echo $entityType; ?>/labor-market">Labor</a></li>
								<li><a href="/index.php/<?php echo $entityType; ?>/product-market">Product</a></li>
								<li><a href="/index.php/<?php echo $entityType; ?>/capital-market">Capital</a></li>
								<li><a href="/index.php/<?php echo $entityType; ?>/rent-market">Rent</a></li>
							</ul>
						</li>
						<li><a href="/index.php/<?php echo $entityType; ?>/forum">Forum</a></li>
                    	<li>
                    		<a href="/index.php/<?php echo $entityType; ?>/display-message">Messages
	                    		<?php 
	                    			if ($unreadMessageCount > 0)
	                    			{
	                    				echo '<span class="badge">' . $unreadMessageCount . '</span>';
	                    			}
	                    		?>
	                    	</a>
	                    </li>
                        <li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown"><strong><?php echo Yii::app()->user->name . ' '; ?></strong><b class="caret"></b></a>
							<ul class="dropdown-menu">
								<li><a href="/index.php/<?php echo Yii::app()->user->type; ?>/account-settings">Account Settings</a></li>
								<li><a href="/index.php/doorkeeper/logout">Log Out</a></li>
							</ul> <!-- /.dropdown-menu -->
						</li> <!-- /.dropdown -->
                    </ul> <!-- /.nav .navbar-nav .navbar-right -->
				</div> <!-- /.navbar-collapse -->

			</div> <!-- /.container -->
		</nav> <!-- /.navbar -->
        
		<div class="container margin-small">
            <div class="row row-offcanvas row-offcanvas-left" style="margin-top:85px; margin-bottom:25px;">
                <?php echo $content; ?>
            </div>
		</div> <!-- /.container -->

	</body>
</html>
