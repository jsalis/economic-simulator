<?php
	$form = new JobOfferForm('create');

	$publicMessengerID = Economy::model()->getPublicMessengerID(Yii::app()->user->economyID);

    if ($entity->getCompanyType() == Company::COMPANY_TYPE_PUBLIC_GOOD)
    {
	    $accessorID = Economy::model()->getAccessorID(Yii::app()->user->economyID);
        $productList = Item::model()->type(ItemType::ITEM_TYPE_PUBLIC_GOOD_NAME)->getItemsByCreator($accessorID); 
	}
	else
	{
		$productList = $entity->getCompanyItemList();
	}
?>

<div class="modal fade" id="create_job_offer" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">

            <?php
                $widget = $this->beginWidget('CActiveForm', array(
                    'action' => $this->createUrl('company/createJobOffer'),
                    'enableAjaxValidation' => true,
                    'enableClientValidation' => true,
                    'clientOptions' => array(
                        'validateOnSubmit' => true,
                        'validateOnChange' => true,
                        'validateOnType' => false,
                    ),
                ));

                echo $widget->hiddenField($form, 'content');
            ?>
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Create Job Offer</h4>
                </div>
                <div class="modal-body">

					<div class="form-group">
						<?php
                            echo $widget->label($form, 'receiver_id');
                        ?>
		                <select class="chosen form-control" name="JobOfferForm[receiver_id]" id="JobOfferForm_receiver_id">
		                    <option value="<?php echo $publicMessengerID; ?>">Labor Market</option>
		                    <?php
		                        $workerList = Worker::model()->economy(Yii::app()->user->economyID)->findAll();

		                        echo '<optgroup label="Worker">';
		                        foreach ($workerList as $worker)
		                        {
		                            echo '<option value="' . $worker->getEntityID() . '">';
		                            echo $worker->getEntityName();
		                            echo '</option>';
		                        }
		                        echo '</optgroup>';
		                    ?>
		                </select>
		                <?php
		                	echo $widget->error($form, 'receiver_id');
		                ?>
		            </div>
		            <div class="row">
		                <div class="col-sm-6 form-group">
		                	<?php
	                            $productData = CHtml::listData($productList, 'item_id', 'item');

	                            echo $widget->label($form, 'item_id');
	                            echo $widget->dropDownList($form, 'item_id', $productData, array(
	                                'class' => 'chosen form-control',
	                                'empty' => '',
	                            ));
	                            echo $widget->error($form, 'item_id');
	                        ?>
			            </div>
			            <div class="col-sm-6 form-group">
				            <?php
		                        echo $widget->label($form, 'payment');

		                        echo '<div class="input-group">';
		                        echo '<span class="input-group-addon">' . Economy::MONETARY_SYMBOL . '</span>';
		                        echo $widget->textField($form, 'payment', array(
		                            'class' => 'form-control money',
		                        ));
		                        echo '</div>';

		                        echo $widget->error($form, 'payment');
		                    ?>
			            </div>
			        </div>

                </div>
                <div class="modal-footer">
                    <div class="btn-group btn-group-justified">
                        <div class="btn-group">
                            <button class="btn btn-default" type="button" data-dismiss="modal">Cancel</button>
                        </div>
                        <div class="btn-group">
                            <?php echo CHtml::submitButton('Submit', array('class' => 'btn btn-primary')); ?>
                        </div>
                    </div>
                </div>
            <?php $this->endWidget(); ?>

        </div>
    </div>
</div>
