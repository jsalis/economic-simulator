<?php
    /**
     * Render Parameters
     * 
     * @param String                    $companyType
     * @param array<EconomyRelation>    $economyRelationList
     */
    
    $form = new ProductForm('update');
    $form->type = $companyType;

    $economyList = array();
    foreach ($economyRelationList as $relation)
    {
        $economyList[] = $relation->getEconomyTo();
    }
?>

<div class="modal fade" id="update_product" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">

            <?php
                $widget = $this->beginWidget('CActiveForm', array(
                    'id' => 'update_product_form',
                    'action' => $this->createUrl('company/updateProduct'),
                    'enableAjaxValidation' => true,
                    'enableClientValidation' => true,
                    'clientOptions' => array(
                        'validateOnSubmit' => true,
                        'validateOnChange' => true,
                        'validateOnType' => false,
                    ),
                ));

                echo $widget->hiddenField($form, 'item_id');
                echo $widget->hiddenField($form, 'type');
            ?>
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Update Product</h4>
                </div>
                <div class="modal-body">

                    <div class="form-group">
                        <?php
                            echo $widget->label($form, 'name');
                            echo $widget->textField($form, 'name', array(
                                'class' => 'form-control',
                            ));
                            echo $widget->error($form, 'name');
                        ?>
                    </div>
                    <div class="form-group">
                        <?php
                            echo $widget->label($form, 'description');
                            echo $widget->textArea($form, 'description', array(
                                'class' => 'form-control',
                                'style' => 'resize: none;',
                                'rows' => '3',
                            ));
                            echo $widget->error($form, 'description');
                        ?>
                    </div>
                    <div class="row">
                        <div class="col-sm-6 form-group">
                            <?php
                                $data = CHtml::listData($economyList, 'economy_id', 'economy');

                                echo $widget->label($form, 'economy_id');
                                echo $widget->dropDownList($form, 'economy_id', $data, array(
                                    'class' => 'chosen form-control',
                                ));
                                echo $widget->error($form, 'economy_id');
                            ?>
                        </div>
                        <div class="col-sm-6 form-group">
                            <?php
                                echo $widget->label($form, 'price');

                                echo '<div class="input-group">';
                                echo '<span class="input-group-addon">' . Economy::MONETARY_SYMBOL . '</span>';
                                echo $widget->textField($form, 'price', array(
                                    'class' => 'form-control money',
                                ));
                                echo '</div>';

                                echo $widget->error($form, 'price');
                            ?>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <div class="btn-group btn-group-justified">
                        <div class="btn-group">
                            <button class="btn btn-default" type="button" data-dismiss="modal">Cancel</button>
                        </div>
                        <div class="btn-group">
                            <?php echo CHtml::submitButton('Submit', array('class' => 'btn btn-primary')); ?>
                        </div>
                    </div>
                </div>
            <?php $this->endWidget(); ?>

        </div>
    </div>
</div>
