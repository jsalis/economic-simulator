<?php
	$error = $model->getError(MessageFilter::ERROR_KEY);
	$viewerType = Yii::app()->user->roleType;
	

	$totalItemList = array();

	$maxFoodPrice = Rule::findEconomyRule(Yii::app()->user->economyID, RuleType::MAX_VALUE, 'Food Price')->getRuleValue();
	$minFoodPrice = Rule::findEconomyRule(Yii::app()->user->economyID, RuleType::MIN_VALUE, 'Food Price')->getRuleValue();
	$foodItemList = Item::model()->type(ItemType::ITEM_TYPE_FOOD_NAME)->available()->today()->findByEconomyMarketItem(Yii::app()->user->economyID);

	foreach ($foodItemList as $item)
	{
		$price = $item->getPrice();
		if ($price >= $minFoodPrice && $price <= $maxFoodPrice)
		{
			$totalItemList[] = $item;
		}
	}

	$maxNonFoodPrice = Rule::findEconomyRule(Yii::app()->user->economyID, RuleType::MAX_VALUE, 'Non-Food Price')->getRuleValue();
	$minNonFoodPrice = Rule::findEconomyRule(Yii::app()->user->economyID, RuleType::MIN_VALUE, 'Non-Food Price')->getRuleValue();
	$nonFoodItemList = Item::model()->type(ItemType::ITEM_TYPE_NONFOOD_NAME)->available()->findByEconomyMarketItem(Yii::app()->user->economyID);

	foreach ($nonFoodItemList as $item)
	{
		$price = $item->getPrice();
		if ($price >= $minNonFoodPrice && $price <= $maxNonFoodPrice)
		{
			$totalItemList[] = $item;
		}
	}


	$showDemandSurvey = false;
	if ($viewerType == EntityType::ENTITY_TYPE_WORKER_NAME)
	{
		$canSend = Economy::canPerformAction(Yii::app()->user->economyID, 'worker/sendDemandSurvey');
		$showDemandSurvey = $canSend && !$model->hasSentDemandSurvey();
		$this->renderPartial('/worker/_demand-survey', array(
	        'model' => $model,
	    ));
	}
?>

<div class="col-sm-12">
	<h3>Product Market</h3>
</div>
<div class="col-sm-12">
	<div class="alert alert-warning alert-dismissable <?php if (!isset($error)) echo 'hide'; ?>">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		<?php echo $error; ?>
	</div>

	<!-- Product Market Panel -->
	<div class="row">
		<?php
			for ($i = 0; $i < 4; $i++)
			{
				echo '<div class="col-sm-3">';
				for ($j = $i; $j < sizeof($totalItemList); $j += 4)
				{
					echo '<div class="panel panel-default panel-body">';
					$this->renderPartial('/market/_market-item', array(
						'item' => $totalItemList[$j],
						'showDemandSurvey' => $showDemandSurvey,
					));
					echo '</div>';
				}
				echo '</div>';
			}
			if (sizeof($totalItemList) == 0)
			{
				echo '<div class="col-sm-3"><div class="panel panel-default panel-body">';
				echo '<em>No products available.</em>';
				echo '</div></div>';
			}
		?>
	</div>
</div>
