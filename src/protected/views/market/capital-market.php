<?php
	$error = $model->getError(MessageFilter::ERROR_KEY);
	$viewerType = Yii::app()->user->roleType;

	$landItemList = Item::model()->type(ItemType::ITEM_TYPE_LAND_NAME)->available()->findByEconomyMarketItem(Yii::app()->user->economyID);
	$machineItemList = Item::model()->type(ItemType::ITEM_TYPE_MACHINE_NAME)->available()->findByEconomyMarketItem(Yii::app()->user->economyID);
	$totalItemList = array_merge($landItemList, $machineItemList);
?>

<div class="col-sm-12">
	<h3>Capital Market</h3>
</div>
<div class="col-sm-12">
	<div class="alert alert-warning alert-dismissable <?php if (!isset($error)) echo 'hide'; ?>">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		<?php echo $error; ?>
	</div>

	<!-- Capital Market Panel -->
	<div class="row">
		<?php
			for ($i = 0; $i < 4; $i++)
			{
				echo '<div class="col-sm-3">';
				for ($j = $i; $j < sizeof($totalItemList); $j += 4)
				{
					echo '<div class="panel panel-default panel-body">';
					$this->renderPartial('/market/_market-item', array(
						'item' => $totalItemList[$j],
					));
					echo '</div>';
				}
				echo '</div>';
			}
			if (sizeof($totalItemList) == 0)
			{
				echo '<div class="col-sm-3"><div class="panel panel-default panel-body">';
				echo '<em>No capital available.</em>';
				echo '</div></div>';
			}
		?>
	</div>
</div>
