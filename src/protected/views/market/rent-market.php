<?php
	$viewerType = Yii::app()->user->roleType;
	$publicMessengerID = Economy::model()->getPublicMessengerID(Yii::app()->user->economyID);

	$rentOfferList = EntityHasMessage::model()
		->messageType(MessageType::MESSAGE_TYPE_SEND_RENT_OFFER)
		->receiver($publicMessengerID)
		->replied(false)
		->findAll();
?>

<div class="col-sm-12">
	<h3>Rent Market</h3>
</div>
<div class="col-sm-12">

	<!-- Rent Market Panel -->
    <div class="row">
		<?php
			for ($i = 0; $i < 4; $i++)
			{
				echo '<div class="col-sm-3">';
				for ($j = $i; $j < sizeof($rentOfferList); $j += 4)
				{
					echo '<div class="panel panel-default panel-body">';
					$this->renderPartial('/messaging/_rent-offer', array(
						'model' => $model,
						'entityHasMessage' => $rentOfferList[$j],
					));
					echo '</div>';
				}
				echo '</div>';
			}
			if (sizeof($rentOfferList) == 0)
			{
				echo '<div class="col-sm-3"><div class="panel panel-default panel-body">';
				echo '<em>No rent offers found.</em>';
				echo '</div></div>';
			}
		?>
	</div>
</div>

<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/site/notification.js"></script>
