<?php
	$viewerType = Yii::app()->user->roleType;

	$entityCreatorName = $item->entityCreator->getEntityName();
    $entityCreatorType = $item->entityCreator->getEntityType();
    $itemType = $item->getItemType();
	$stock = $item->getItemStockInEconomy(Yii::app()->user->economyID);
    $efficiency = $item->getEfficiency();

    $salesTax = Economy::getEconomySalesTax(Yii::app()->user->economyID);

    $isProduct = $itemType == ItemType::ITEM_TYPE_FOOD_NAME || $itemType == ItemType::ITEM_TYPE_NONFOOD_NAME;
    $isCapital = $itemType == ItemType::ITEM_TYPE_LAND_NAME || $itemType == ItemType::ITEM_TYPE_MACHINE_NAME;

    $canPurchase = true;
    if ($isProduct && $viewerType == EntityType::ENTITY_TYPE_COMPANY_NAME)
    {
        $canPurchase = false;
    }
    if ($viewerType == EntityType::ENTITY_TYPE_ACCESSOR_NAME ||
        $viewerType == EntityType::ENTITY_TYPE_GOVERNMENT_NAME)
    {
        $canPurchase = false;
    }
?>

<form action="/index.php/<?php echo $viewerType; ?>/purchase-item" method="post">
    <input type="hidden" name="Item[economyID]" id="Item_economyID" value="<?php echo Yii::app()->user->economyID; ?>">
	<input type="hidden" name="Item[id]" id="Item_id" value="<?php echo $item->getItemID(); ?>">
    <div class="form-group">
        <p class="lead">
        	<strong><?php echo $item->getItemName(); ?></strong><br>
        	<small><?php echo $item->getItemDescription(); ?></small>
        </p>
    </div>
    <div class="row <?php if ($entityCreatorType != EntityType::ENTITY_TYPE_COMPANY_NAME) echo 'hide'; ?>">
        <label class="col-sm-5 control-label">Company</label>
        <div class="col-sm-7">
            <p class="form-control-static"><?php echo $entityCreatorName; ?></p>
        </div>
    </div>
    <div class="row">
        <label class="col-sm-5 control-label">Type</label>
        <div class="col-sm-7">
            <p class="form-control-static"><?php echo $itemType; ?></p>
        </div>
    </div>
    <div class="row">
        <label class="col-sm-5 control-label">Stock</label>
        <div class="col-sm-7">
            <p class="form-control-static"><?php echo $stock; ?></p>
        </div>
    </div>
    <div class="row <?php if (empty($efficiency)) echo 'hide'; ?>">
        <label class="col-sm-5 control-label">Efficiency</label>
        <div class="col-sm-7">
            <p class="form-control-static"><?php echo $efficiency; ?></p>
        </div>
    </div>
    <div class="row">
        <label class="col-sm-5 control-label">Price</label>
        <div class="col-sm-7">
            <p class="form-control-static text-success"><?php echo Economy::MONETARY_SYMBOL . $item->getPrice(); ?></p>
        </div>
    </div>
    <div class="row form-group">
        <label class="col-sm-5 control-label">Sales Tax</label>
        <div class="col-sm-7">
            <p class="form-control-static text-success"><?php echo Economy::MONETARY_SYMBOL . $salesTax; ?></p>
        </div>
    </div>
    <div class="<?php if (!$canPurchase) echo 'hide'; ?>">
        <div class="<?php if ($stock != 0) echo 'hide'; ?>">
            <button class="btn btn-default btn-block" disabled>Out of Stock</button>
        </div>
        <div class="<?php if ($stock == 0) echo 'hide'; ?>">
            <?php
                if (!empty($showDemandSurvey))
                {
                    echo '<button class="btn btn-primary btn-block" type="button" data-toggle="modal" data-target="#demand_survey">Purchase</button>';
                }
                else
                {
                    echo '<button class="btn btn-primary btn-block" type="submit">Purchase</button>';
                }
            ?>
        </div>
    </div>
</form>
