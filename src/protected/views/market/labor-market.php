<?php
    $error = $model->getError(MessageFilter::ERROR_KEY);
	$viewerType = Yii::app()->user->roleType;
	$publicMessengerID = Economy::model()->getPublicMessengerID(Yii::app()->user->economyID);

	$jobOfferList = EntityHasMessage::model()
		->messageType(MessageType::MESSAGE_TYPE_SEND_JOB_OFFER_NAME)
		->receiver($publicMessengerID)
		->replied(false)
		->findAll();

	if ($viewerType == EntityType::ENTITY_TYPE_COMPANY_NAME)
	{
		$this->renderPartial('/company/_create-job-offer', array(
	        'entity' => $model,
	    ));
	}
?>

<div class="col-sm-12">
	<h3>Labor Market</h3>
</div>
<div class="col-sm-12">
	<div class="alert alert-warning alert-dismissable <?php if (!isset($error)) echo 'hide'; ?>">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		<?php echo $error; ?>
	</div>

	<!-- Labor Market Panel -->
	<div class="row">
		<div class="col-sm-12">
			<div class="row form-group <?php if ($viewerType != EntityType::ENTITY_TYPE_COMPANY_NAME) echo 'hide'; ?>">
	            <div class="col-sm-4 col-md-3">
	                <a class="btn btn-block btn-primary" data-toggle="modal" data-target="#create_job_offer">Create Job Offer</a> 
	            </div>
	        </div>
	    </div>
		<?php
			for ($i = 0; $i < 4; $i++)
			{
				echo '<div class="col-sm-3">';
				for ($j = $i; $j < sizeof($jobOfferList); $j += 4)
				{
					echo '<div class="panel panel-default panel-body">';

					if (Yii::app()->user->roleID == $jobOfferList[$j]->entitySender->getEntityID())
					{
						$this->renderPartial('/messaging/_close_job_offer', array(
							'model' => $model,
							'entityHasMessage' => $jobOfferList[$j]
						));
					}

					$this->renderPartial('/messaging/_job-offer', array(
						'model' => $model,
						'entityHasMessage' => $jobOfferList[$j],
					));
					echo '</div>';
				}
				echo '</div>';
			}
			if (sizeof($jobOfferList) == 0)
			{
				echo '<div class="col-sm-3"><div class="panel panel-default panel-body">';
				echo '<em>No jobs offers found.</em>';
				echo '</div></div>';
			}
		?>
	</div>
</div>

<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/site/notification.js"></script>
