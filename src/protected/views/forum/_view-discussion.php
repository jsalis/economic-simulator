<?php
	$form = new DiscussionForm('reply');
    $form->previous_message_id = $entityHasMessage->message->message_id;

    $publicMessengerID = Economy::model()->getPublicMessengerID(Yii::app()->user->economyID);

    $replyList = EntityHasMessage::model()
        ->messageType(MessageType::MESSAGE_TYPE_CHAT_NAME)
        ->receiver($publicMessengerID)
        ->previousMessage($entityHasMessage->message->message_id)
        ->findAll();

    array_unshift($replyList, $entityHasMessage);
?>

<div class="modal fade" id="view_discussion" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            <?php
                $widget = $this->beginWidget('CActiveForm', array(
                    'action' => $this->createUrl('replyDiscussion'),
                    'enableAjaxValidation' => false,
                    'enableClientValidation' => true,
                    'clientOptions' => array(
                        'validateOnSubmit' => true,
                        'validateOnChange' => true,
                        'validateOnType' => true,
                    ),
                ));
                echo $widget->hiddenField($form, 'previous_message_id');
            ?>
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"><?php echo $entityHasMessage->message->findByKey(Message::MESSAGE_KEY_SUBJECT) ?></h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <?php
                            foreach ($replyList as $reply)
                            {
                                $date = $reply->getDateReceived();

                                $isRootMessage = ($reply->message->message_id == $entityHasMessage->message->message_id);
                                $panelClass = ($isRootMessage) ? 'panel-success' : 'panel-default';

                                echo "<div class='panel $panelClass'>";

                                echo '<div class="panel-heading">';
                                echo '<h3 class="panel-title">' . $reply->entitySender->entity . '<span class="pull-right" style="font-size: 0.7em">' . $date->format('M d, g:i A') . '</span></h3>';
                                echo '</div>';

                                echo '<div class="panel-body">';
                                echo $reply->message->findByKey(Message::MESSAGE_KEY_CONTENT);
                                echo '</div>';

                                echo '</div>';
                            }
                        ?>
                    </div>
		            <div class="form-group">
			            <?php
	                        echo $widget->label($form, 'content');
	                        echo $widget->textArea($form, 'content', array(
                                'class' => 'form-control',
                                'style' => 'resize: none;',
                                'rows' => '3',
                            ));
	                        echo $widget->error($form, 'content');
	                    ?>
		            </div>
                    <div class="form-group">
                        <?php echo CHtml::submitButton('Submit', array('class' => 'btn btn-primary')); ?>
                    </div>
                </div>
            <?php $this->endWidget(); ?>

        </div>
    </div>
</div>
