<?php
	$publicMessengerID = Economy::model()->getPublicMessengerID(Yii::app()->user->economyID);

	$discussionList = EntityHasMessage::model()
		->messageType(MessageType::MESSAGE_TYPE_CHAT_NAME)
		->receiver($publicMessengerID)
		->findAll();

	$discussionList = array_reverse($discussionList);

	$this->renderPartial('/forum/_new-discussion');
?>

<div id="discussion_container"></div>

<div class="tab-pane" id="discussion">
	<div class="row form-group">
		<div class="col-xs-12">
			<div class="btn-group">
				<a class="btn btn-sm btn-primary" data-toggle="modal" data-target="#new_discussion">New Discussion</a> 
			</div>
		</div>
	</div>
	<table class="table-hover" data-filter="#filter" data-filter-text-only="true">
		<thead>
			<tr class="<?php if (count($discussionList) == 0) echo 'hide'; ?>">
				<th class="fit" data-sort-ignore="true"></th>
				<th class="fit" data-type="numeric">Date</th>
				<th class="fit">From</th>
				<th class="expand" data-sort-ignore="true">Subject</th>
			</tr>
		</thead>
		<tbody>
			<?php
				foreach ($discussionList as $discussion)
				{
					$previous = $discussion->message->findByKey(Message::MESSAGE_KEY_PREVIOUS_MESSAGE);

					if (empty($previous))
					{
						$id = $discussion->message->message_id;
						$date = $discussion->getDateReceived();

						echo '<tr>';

						echo '<td>';
						$success = 'js:function(html) {
							$("#discussion_container").html(html);
							$("#view_discussion").modal("show");
						}';
						echo CHtml::ajaxLink('View',
                            array('viewDiscussion', 'id' => $id),
                            array('success' => $success),
                            array('class' => 'btn btn-xs btn-default')
                        );
                        echo '</td>';

						echo '<td class="fit" data-value="' . $date->getTimestamp() . '">' . $date->format('M d, g:i A') . '</td>';
						echo '<td class="fit">' . $discussion->entitySender->entity . '</td>';
						echo '<td>' . $discussion->message->findByKey(Message::MESSAGE_KEY_SUBJECT) . '</td>';
						
						echo '</tr>';
					}
				}
				if (count($discussionList) == 0)
				{
					echo '<tr><td><em>No discussions found.</em></td></tr>';
				}
			?>
		</tbody>
	</table>
</div>
