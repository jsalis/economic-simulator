<?php
	$tab = 'petitions';
    $error = $model->getError(MessageFilter::ERROR_KEY);
    $viewer = Entity::model()->findByPk(Yii::app()->user->roleID);
    $government = Government::model()->economy(Yii::app()->user->economyID)->find();
?>

<div class="col-sm-12">
	<h3>Forum</h3>
	<div class="alert alert-warning alert-dismissable <?php if (!isset($error)) echo 'hide'; ?>">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		<?php echo $error; ?>
	</div>
</div>
<div class="col-md-12">
	<div class="panel panel-default">
		<div class="panel-body">
	  		<!-- Tab List -->
		    <ul class="nav nav-tabs">
		    	<li class="<?php if ($tab == 'petitions') echo 'active'; ?>">
		            <a href='#petitions' data-toggle='tab'>Petitions</a>
		        </li>
		    	<li class="<?php if ($tab == 'election') echo 'active'; ?>">
		            <a href='#election' data-toggle='tab'>Election</a>
		        </li>
		        <li class="<?php if ($tab == 'discussion') echo 'active'; ?>">
		            <a href='#discussion' data-toggle='tab'>Discussion Board</a>
		        </li>
		    </ul>
		</div>
		<div class="panel-body">
			<!-- Tab content -->
			<div class="tab-content">
				<?php
					$this->renderPartial('/profile/_petition', array(
						'entity' => $government,
						'viewer' => $viewer,
					));
			        $this->renderPartial('/forum/_election', array(
			        	'model' => $model
			        ));
			        $this->renderPartial('/forum/_discussion', array(
			        	'model' => $model
			        ));
				?>
			</div>
		</div>
	</div>
</div>
