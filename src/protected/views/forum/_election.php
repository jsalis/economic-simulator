<?php
    $viewerType = Yii::app()->user->roleType;
    $electionInProgress = Economy::isElectionInProgress(Yii::app()->user->economyID);
	$messageList = ($electionInProgress) ? Economy::getCandidateList(Yii::app()->user->economyID) : array();

	$canVote = ($viewerType == EntityType::ENTITY_TYPE_WORKER_NAME) ? !$model->hasVoted() : false;
?>

<div class="tab-pane row" id="election">
	<?php
		for ($i = 0; $i < sizeof($messageList); $i++)
		{
			$message = $messageList[$i]['entityHasMessage']->getMessage();
			echo '<div class="col-sm-3"><div class="panel panel-default panel-body">';
			$this->renderPartial('/forum/_vote', array(
				'canVote' => $canVote,
				'entityHasMessage' => $messageList[$i]['entityHasMessage'],
				'name' => $messageList[$i]['name'],
				'emailAddress' => $messageList[$i]['emailAddress'],
			));
			echo '</div></div>';	
		}
		if (!$electionInProgress)
		{
			echo '<div class="col-sm-12"><em>Election not in progress.</em></div>';
		}
		else if (sizeof($messageList) == 0)
		{
			echo '<div class="col-sm-12"><em>No nominations found.</em></div>';
		}
	?>
</div>
