<?php
	$form = new DiscussionForm('create');
?>

<div class="modal fade" id="new_discussion" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">

            <?php
                $widget = $this->beginWidget('CActiveForm', array(
                    'action' => $this->createUrl('createDiscussion'),
                    'enableAjaxValidation' => false,
                    'enableClientValidation' => true,
                    'clientOptions' => array(
                        'validateOnSubmit' => true,
                        'validateOnChange' => true,
                        'validateOnType' => true,
                    ),
                ));
            ?>
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">New Discussion</h4>
                </div>
                <div class="modal-body">

                	<div class="row">
			            <div class="col-sm-12 form-group">
				            <?php
		                        echo $widget->label($form, 'subject');
		                        echo $widget->textField($form, 'subject', array(
		                            'class' => 'form-control',
		                        ));
		                        echo $widget->error($form, 'subject');
		                    ?>
			            </div>
			        </div>
		            <div class="row">
			            <div class="col-sm-12 form-group">
				            <?php
		                        echo $widget->label($form, 'content');
		                        echo $widget->textArea($form, 'content', array(
	                                'class' => 'form-control',
	                                'style' => 'resize: none;',
	                                'rows' => '4',
	                            ));
		                        echo $widget->error($form, 'content');
		                    ?>
			            </div>
			        </div>

                </div>
                <div class="modal-footer">
                    <div class="btn-group btn-group-justified">
                        <div class="btn-group">
                            <button class="btn btn-default" type="button" data-dismiss="modal">Cancel</button>
                        </div>
                        <div class="btn-group">
                            <?php echo CHtml::submitButton('Submit', array('class' => 'btn btn-primary')); ?>
                        </div>
                    </div>
                </div>
            <?php $this->endWidget(); ?>

        </div>
    </div>
</div>
