<?php
    $viewerType = Yii::app()->user->roleType;
    $message = $entityHasMessage->getMessage();
    $messageID = $message->getMessageID();
    $receiverID = $entityHasMessage->getEntityReceiverID();
    $entityID = $message->findByKey(Message::MESSAGE_KEY_ENTITY_ID);
    $messageContent = Entity::getEntityNameByID(Yii::app()->user->roleID) . ' voted for ' . Entity::getEntityNameByID($entityID);
?>

<form action="/index.php/<?php echo $viewerType; ?>/send-message" method="post">
    <input type="hidden" name="Message[entityID]" id="Message_entityID" value="<?php echo $receiverID; ?>">
    <input type="hidden" name="Message[messageType]" value="<?php echo MessageType::MESSAGE_TYPE_VOTE_FOR_CANDIDATE; ?>">
    <input type="hidden" name="Message[append][<?php echo Message::MESSAGE_KEY_PREVIOUS_MESSAGE; ?>]" value="<?php echo $messageID; ?>">
    <input type="hidden" name="Message[append][<?php echo Message::MESSAGE_KEY_ENTITY_ID; ?>]" value="<?php echo $entityID; ?>">
    <input type="hidden" name="Message[append][<?php echo Message::MESSAGE_KEY_CONTENT; ?>]" value="<?php echo $messageContent; ?>">
    <div class="form-group">
        <p class="lead">
        	<strong><?php echo $name; ?></strong><br>
        	<small><?php echo $emailAddress; ?></small>
        </p>
    </div>
    <div class="form-group">
        <div>
            <button class="btn btn-primary btn-block" type="submit" <?php if (!$canVote) echo 'disabled'; ?>>Vote</button>
        </div>
    </div>
</form>
