
<div class="modal fade" id="give_company" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">

            <!-- Give Company Form -->
            <form id="give_company_form" action="/index.php/accessor/give-company" method="post">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 id="give_company_title" class="modal-title">Give Company</h4>
                </div>
                <div class="modal-body">

                    <input type="hidden" id="Entity_userEmail" name="Entity[userEmail]" required>
                    <div class="row">
                        <div class="col-sm-6 form-group">
                            <label for="Entity_companyType" class="control-label">Company Type</label>
                            <select name="Entity[companyType]" id="Entity_companyType" type="text" class="chosen form-control" required>
                                <option value=""></option>
                                <?php
                                    echo '<option value="' . Company::COMPANY_TYPE_FOOD . '">' . Company::COMPANY_TYPE_FOOD . '</option>';
                                    echo '<option value="' . Company::COMPANY_TYPE_NON_FOOD . '">' . Company::COMPANY_TYPE_NON_FOOD . '</option>';
                                    // echo '<option value="' . Company::COMPANY_TYPE_PUBLIC_GOOD . '">' . Company::COMPANY_TYPE_PUBLIC_GOOD . '</option>';
                                ?>
                            </select>
                        </div>
                        <div class="col-sm-6 form-group">                            
                            
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <div class="btn-group btn-group-justified">
                        <div class="btn-group">
                            <button class="btn btn-default" type="button" data-dismiss="modal">Cancel</button>
                        </div>
                        <div class="btn-group">
                            <button class="btn btn-primary" type="submit">Submit</button>
                        </div>
                    </div>
                </div>
            </form>

        </div>
    </div>
</div>
