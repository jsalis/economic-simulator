
<div class="modal fade" id="view_entities" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <!-- View Entities -->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 id="view_entity_title" class="modal-title">All In Economy</h4>
            </div>
            <div class="modal-body">

                <table id="view_entity_table" class="table-hover" data-filter="#filter" data-filter-text-only="true">
                    <thead>
                        <tr class="<?php if (sizeof($entityList) == 0) echo 'hide'; ?>">
                            <th style="white-space: nowrap">Name</th>
                            <th style="white-space: nowrap">Type</th>
                            <th style="white-space: nowrap">Balance</th>
                            <th style="white-space: nowrap">Efficiency</th>
                            <th style="white-space: nowrap">Health</th>
                            <th style="white-space: nowrap">Status</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            for ($i = 0; $i < sizeof($entityList); $i++)
                            {
                                $id = $entityList[$i]->getEntityID();
                                $type = $entityList[$i]->getEntityType();
                                $balance = $entityList[$i]->getEntityBalance();

                                if ($type == EntityType::ENTITY_TYPE_ACCESSOR_NAME) continue;
                                if ($type == EntityType::ENTITY_TYPE_PUBLIC_MESSENGER_NAME) continue;

                                echo "<tr id='$id' class='form-submit'>";

                                // Entity name column
                                echo "<td>" . $entityList[$i]->getEntityName() . "</td>";

                                // Entity type column
                                echo "<td>" . $type . "</td>";

                                // Money column
                                echo "<td data-type='numeric' data-value='" . $balance . "'><span class='text-success'>" . Economy::MONETARY_SYMBOL . $balance . "</span></td>";
                                
                                // Efficiency column
                                echo "<td data-type='numeric'>";
                                if ($type != EntityType::ENTITY_TYPE_GOVERNMENT_NAME)
                                {
                                    echo $entityList[$i]->getEntityEfficiency();
                                }
                                echo "</td>";
                                
                                // Health column
                                echo "<td data-type='numeric'>";
                                if ($type == EntityType::ENTITY_TYPE_WORKER_NAME)
                                {
                                    echo $entityList[$i]->getEntityHealth();
                                }
                                echo "</td>";

                                // Status column
                                echo "<td>";
                                if ($type == EntityType::ENTITY_TYPE_WORKER_NAME)
                                {
                                    echo ($entityList[$i]->hasEnoughHealth()) ? "Healthy" : "Sick";
                                }
                                echo "</td>";

                                // Display profile form
                                echo "<form action='/index.php/accessor/display-profile' method='post'>";
                                echo "<input type='hidden' name='Entity[id]' value='" . $id . "'>";
                                echo "</form>";
                                    
                                echo "</tr>";
                            }
                            if (sizeof($entityList) == 0)
                            {
                                echo "<tr><td><em>No students found.</em></td></tr>";
                            }
                        ?>
                    </tbody>
                </table>

            </div>

        </div>
    </div>
</div>
