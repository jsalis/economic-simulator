
<div class="modal fade" id="update_capital" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">

            <!-- Update Capital Form -->
            <form id="update_capital_form" action="/index.php/accessor/update-modifier" method="post">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title">Update Capital</h4>
                </div>
                <div class="modal-body">

                    <div class="form-group">
                        <label for="Item_name" class="control-label">Name</label>
                        <input name="Item[name]" type="text" id="Item_name" class="form-control" required>
                        <input name="Item[id]" type="hidden" id="Item_id" required>
                    </div>
                    <div class="form-group">
                        <label for="Item_type" class="control-label">Type</label>
                        <select name="Item[type]" id="Item_type" type="text" class="chosen form-control" required>
                            <option value=""></option>
                            <?php
                                echo '<option value="' . ItemType::ITEM_TYPE_LAND_NAME . '">' . ItemType::ITEM_TYPE_LAND_NAME . '</option>';
                                echo '<option value="' . ItemType::ITEM_TYPE_MACHINE_NAME . '">' . ItemType::ITEM_TYPE_MACHINE_NAME . '</option>';
                                echo '<option value="' . ItemType::ITEM_TYPE_DEGREE_NAME . '">' . ItemType::ITEM_TYPE_DEGREE_NAME . '</option>';
                            ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="Item_description" class="control-label">Description</label>
                        <textarea rows="3" style="resize: none;" name="Item[description]" id="Item_description" class="form-control" required></textarea>
                    </div>
                    <div class="row">
                        <div class="col-sm-6 form-group">
                            <label for="Item_efficiency" class="control-label">Efficiency</label>
                            <input name="Item[efficiency]" type="text" id="Item_efficiency" class="form-control money" required>
                        </div>
                        <div class="col-sm-6 form-group">
                            <label for="Item_price" class="control-label">Price</label>
                            <div class="input-group">
                                <span class="input-group-addon"><?php echo Economy::MONETARY_SYMBOL; ?></span>
                                <input name="Item[price]" type="text" id="Item_price" class="form-control money" required>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <div class="btn-group btn-group-justified">
                        <div class="btn-group">
                            <button class="btn btn-default" type="button" data-dismiss="modal">Cancel</button>
                        </div>
                        <div class="btn-group">
                            <button class="btn btn-primary" type="submit">Save</button>
                        </div>
                    </div>
                </div>
            </form>

        </div>
    </div>
</div>
