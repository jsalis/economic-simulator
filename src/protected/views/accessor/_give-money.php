
<div class="modal fade" id="give_money" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">

            <!-- Give Money Form -->
            <form id="give_money_form" action="/index.php/accessor/give-money" method="post">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title">Give Money</h4>
                </div>
                <div class="modal-body">

                    <div class="row">
                        <div class="col-sm-6 form-group">
                            <label for="Entity_type" class="control-label">Receiver</label>
                            <select name="Entity[type]" type="text" id="Entity_type" class="chosen form-control" required>
                                <?php
                                    echo '<option value=""></option>';
                                    echo '<option value="' . EntityType::ENTITY_TYPE_WORKER_NAME . '">' . EntityType::ENTITY_TYPE_WORKER_NAME . '</option>';
                                    echo '<option value="' . EntityType::ENTITY_TYPE_COMPANY_NAME . '">' . EntityType::ENTITY_TYPE_COMPANY_NAME . '</option>';
                                    echo '<option value="' . EntityType::ENTITY_TYPE_GOVERNMENT_NAME . '">' . EntityType::ENTITY_TYPE_GOVERNMENT_NAME . '</option>';
                                ?>
                            </select>
                        </div>
                        <div class="col-sm-6 form-group">
                            <label for="Entity_money" class="control-label">Money</label>
                            <div class="input-group">
                                <span class="input-group-addon"><?php echo Economy::MONETARY_SYMBOL; ?></span>
                                <input name="Entity[money]" type="text" id="Entity_money" class="form-control money" required>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <div class="btn-group btn-group-justified">
                        <div class="btn-group">
                            <button class="btn btn-default" type="button" data-dismiss="modal">Cancel</button>
                        </div>
                        <div class="btn-group">
                            <button class="btn btn-primary" type="submit">Submit</button>
                        </div>
                    </div>
                </div>
            </form>

        </div>
    </div>
</div>
