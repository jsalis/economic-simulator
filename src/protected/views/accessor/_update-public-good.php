<?php
    $entityTypeList = EntityType::model()->findAll();
?>

<div class="modal fade" id="update_public_good" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">

            <!-- Update Public Good Form -->
            <form id="update_public_good_form" action="/index.php/accessor/update-public-good" method="post">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title">Update Public Good</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="Public_Good_name_update" class="control-label">Name</label>
                        <input name="Item[name]" type="text" id="Public_Good_name_update" class="form-control" required>
                        <input name="Item[id]" type="hidden" id="Public_Good_id_update" required>
                    </div>
                    <div class="form-group">
                        <label for="Public_Good_description_update" class="control-label">Description</label>
                        <textarea rows="3" style="resize: none;" name="Item[description]" id="Public_Good_description_update" class="form-control" required></textarea>
                    </div>
                    <div class="row">
                        <div class="modal-header">
                            <h4 class="modal-title">Monetary Benefit</h4>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6 form-group">
                            <label for="Public_Good_monetary_start_update" class="control-label">Start</label>
                            <input name="Item[monetary][start]" type="text" id="Public_Good_monetary_start_update" class="form-control money" required>
                        </div>
                        <div class="col-sm-6 form-group">
                            <label for="Public_Good_monetary_decrease_rate_update" class="control-label">Decrease rate</label>
                            <input name="Item[monetary][decrease_rate]" type="text" id="Public_Good_monetary_decrease_rate_update" class="form-control money" required>
                        </div>
                    </div>
                    <div class="row">
                        <div class="modal-header">
                            <h4 class="modal-title">Efficiency Benefit</h4>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6 form-group">
                            <label for="Public_Good_efficiency_start_update" class="control-label">Start</label>
                            <input name="Item[efficiency][start]" type="text" id="Public_Good_efficiency_start_update" class="form-control money" required>
                        </div>
                        <div class="col-sm-6 form-group">
                            <label for="Public_Good_efficiency_decrease_rate_update" class="control-label">Decrease rate</label>
                            <input name="Item[efficiency][decrease_rate]" type="text" id="Public_Good_efficiency_decrease_rate_update" class="form-control money" required>
                        </div>
                    </div>
                    <div class="row">
                        <div class="modal-header">
                            <h4 class="modal-title">Health Benefit</h4>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6 form-group">
                            <label for="Public_Good_health_start_update" class="control-label">Start</label>
                            <input name="Item[health][start]" type="text" id="Public_Good_health_start_update" class="form-control money" required>
                        </div>
                        <div class="col-sm-6 form-group">
                            <label for="Public_Good_health_decrease_rate_update" class="control-label">Decrease rate</label>
                            <input name="Item[health][decrease_rate]" type="text" id="Public_Good_health_decrease_rate_update" class="form-control money" required>
                        </div>
                    </div>
                    <div class="row">
                        <div class="modal-header">
                            <h4 class="modal-title">Benefit Receiver</h4>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6 form-group">
                            <label for="Item_entity_type" class="control-label">Benefit Receiver</label>
                            <select name="Item[entityType]" type="text" id="Item_entity_type" class="chosen form-control" required>
                                <?php
                                    echo '<option value=""></option>';
                                    echo '<option value="' . EntityType::ENTITY_TYPE_WORKER_NAME . '">' . EntityType::ENTITY_TYPE_WORKER_NAME . '</option>';
                                    echo '<option value="' . EntityType::ENTITY_TYPE_COMPANY_NAME . '">' . EntityType::ENTITY_TYPE_COMPANY_NAME . '</option>';
                                    echo '<option value="' . EntityType::ENTITY_TYPE_PUBLIC_MESSENGER_NAME . '">Economy</option>';
                                ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="btn-group btn-group-justified">
                        <div class="btn-group">
                            <button class="btn btn-default" type="button" data-dismiss="modal">Cancel</button>
                        </div>
                        <div class="btn-group">
                            <button class="btn btn-primary" type="submit">Save</button>
                        </div>
                    </div>
                </div>
            </form>

        </div>
    </div>
</div>
