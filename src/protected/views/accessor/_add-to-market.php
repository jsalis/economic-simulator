
<div class="modal fade" id="add_to_market" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">

            <!-- Add To Market Form -->
            <form action="/index.php/accessor/add-item-to-market" method="post">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title">Add To Market</h4>
                </div>
                <div class="modal-body">

                    <div class="row">
                        <div class="col-sm-6 form-group">
                            <label for="Item_id" class="control-label">Item</label>
                            <select name="Item[id]" id="Item_id" type="text" class="chosen form-control" required>
                                <option value=""></option>
                                <?php
                                    for ($i = 0; $i < sizeof($itemList); $i++)
                                    {
                                        $id = $itemList[$i]->getItemID();
                                        echo '<option value="' . $id . '">';
                                        echo Item::getItemNameByID($id);
                                        echo '</option>';
                                    }
                                ?>
                            </select>
                        </div>
                        <div class="col-sm-6 form-group">
                            <label for="Item_quantity" class="control-label">Quantity</label>
                            <input name="Item[quantity]" type="number" id="Item_quantity" class="form-control" required>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <div class="btn-group btn-group-justified">
                        <div class="btn-group">
                            <button class="btn btn-default" type="button" data-dismiss="modal">Cancel</button>
                        </div>
                        <div class="btn-group">
                            <button class="btn btn-primary" type="submit">Submit</button>
                        </div>
                    </div>
                </div>
            </form>

        </div>
    </div>
</div>
