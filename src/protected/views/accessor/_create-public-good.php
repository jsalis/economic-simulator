
<div class="modal fade" id="create_public_good" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">

            <!-- Create Public Good Form -->
            <form action="/index.php/accessor/create-public-good" method="post">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title">Create Public Good</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="Item_name" class="control-label">Name</label>
                        <input name="Item[name]" type="text" id="Item_name" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label for="Item_description" class="control-label">Description</label>
                        <textarea rows="3" style="resize: none;" name="Item[description]" id="Item_description" class="form-control" required></textarea>
                    </div>
                    <div class="form-group">
                        <label for="Item_entity_type" class="control-label">Benefit Receiver</label>
                        <select name="Item[entityType]" type="text" id="Item_entity_type" class="chosen form-control" required>
                            <?php
                                echo '<option value=""></option>';
                                echo '<option value="' . EntityType::ENTITY_TYPE_WORKER_NAME . '">' . EntityType::ENTITY_TYPE_WORKER_NAME . '</option>';
                                echo '<option value="' . EntityType::ENTITY_TYPE_COMPANY_NAME . '">' . EntityType::ENTITY_TYPE_COMPANY_NAME . '</option>';
                                echo '<option value="' . EntityType::ENTITY_TYPE_PUBLIC_MESSENGER_NAME . '">Economy</option>';
                            ?>
                        </select>
                    </div>
                    <div class="row col-xs-12">
                        <span class="text-muted">Monetary Benefit</span>
                    </div>
                    <div class="row">
                        <div class="col-sm-6 form-group">
                            <label for="Item_monetary_start" class="control-label">Start</label>
                            <input name="Item[monetary][start]" type="text" id="Item_monetary_start" class="form-control money" required>
                        </div>
                        <div class="col-sm-6 form-group">
                            <label for="Item_monetary_decrease_rate" class="control-label">Decrease Rate</label>
                            <input name="Item[monetary][decrease_rate]" type="text" id="Item_monetary_decrease_rate" class="form-control money" required>
                        </div>
                    </div>
                    <div class="row col-xs-12">
                        <span class="text-muted">Efficiency Benefit</span>
                    </div>
                    <div class="row">
                        <div class="col-sm-6 form-group">
                            <label for="Item_efficiency_start" class="control-label">Start</label>
                            <input name="Item[efficiency][start]" type="text" id="Item_efficiency_start" class="form-control money" required>
                        </div>
                        <div class="col-sm-6 form-group">
                            <label for="Item_efficiency_decrease_rate" class="control-label">Decrease Rate</label>
                            <input name="Item[efficiency][decrease_rate]" type="text" id="Item_efficiency_decrease_rate" class="form-control money" required>
                        </div>
                    </div>
                    <div class="row col-xs-12">
                        <span class="text-muted">Health Benefit</span>
                    </div>
                    <div class="row">
                        <div class="col-sm-6 form-group">
                            <label for="Item_health_start" class="control-label">Start</label>
                            <input name="Item[health][start]" type="text" id="Item_health_start" class="form-control money" required>
                        </div>
                        <div class="col-sm-6 form-group">
                            <label for="Item_health_decrease_rate" class="control-label">Decrease Rate</label>
                            <input name="Item[health][decrease_rate]" type="text" id="Item_health_decrease_rate" class="form-control money" required>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <div class="btn-group btn-group-justified">
                        <div class="btn-group">
                            <button class="btn btn-default" type="button" data-dismiss="modal">Cancel</button>
                        </div>
                        <div class="btn-group">
                            <button class="btn btn-primary" type="submit">Submit</button>
                        </div>
                    </div>
                </div>
            </form>

        </div>
    </div>
</div>
