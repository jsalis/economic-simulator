
<div class="modal fade" id="give_health" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">

            <!-- Give Health Form -->
            <form id="give_health_form" action="/index.php/accessor/give-health" method="post">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title">Give Health To Workers</h4>
                </div>
                <div class="modal-body">

                    <input type="hidden" id="Entity_userEmail" name="Entity[userEmail]" required>
                    <div class="row">
                        <div class="col-sm-6 form-group">
                            <label for="Entity_health" class="control-label">Health</label>
                            <input name="Entity[health]" type="number" id="Entity_health" class="form-control" required>
                        </div>
                        <div class="col-sm-6 form-group">
                            
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <div class="btn-group btn-group-justified">
                        <div class="btn-group">
                            <button class="btn btn-default" type="button" data-dismiss="modal">Cancel</button>
                        </div>
                        <div class="btn-group">
                            <button class="btn btn-primary" type="submit">Submit</button>
                        </div>
                    </div>
                </div>
            </form>

        </div>
    </div>
</div>
