<?php
    $maxQuantity = 4;
    $numberList = array(
        1 => 'First',
        2 => 'Second',
        3 => 'Third',
        4 => 'Fourth',
        5 => 'Fifth',
        6 => 'Sixth',
    );
?>

<div class="modal fade" id="demand_survey" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            <!-- Demand Survey Form -->
            <form action="/index.php/worker/send-demand-survey" method="post">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title">Product Demand Survey</h4>
                    <p>Please indicate how much you are willing to pay for each type of product. You must complete this survey before purchasing a product.</p>
                </div>
                <div class="modal-body">

                    <div class="row">
                        <div class="col-sm-2 form-group">
                            <label class="control-label"></label>
                        </div>
                        <div class="col-sm-5 form-group">
                            <label class="control-label"><?php echo ItemType::ITEM_TYPE_FOOD_NAME; ?></label>
                        </div>
                        <div class="col-sm-5 form-group">
                            <label class="control-label"><?php echo ItemType::ITEM_TYPE_NONFOOD_NAME; ?></label>
                        </div>
                    </div>
                    <?php
                        for ($i = 0; $i < $maxQuantity; $i++)
                        {
                            echo "<div class='row'>";

                            $quantity = $i + 1;
                            echo "<div class='col-sm-2 form-group'>";
                            echo $numberList[$quantity];
                            echo "</div>";

                            $index = $i * 2;
                            echo "<div class='col-sm-5 form-group'>";
                            echo "<div class='input-group'>";
                            echo "<span class='input-group-addon'>" . Economy::MONETARY_SYMBOL . "</span>";
                            echo "<input name='Item[$index][price]' type='text' class='form-control money' required>";
                            echo "</div>";
                            echo "<input name='Item[$index][itemType]' type='hidden' value='" . ItemType::ITEM_TYPE_FOOD_NAME . "'>";
                            echo "<input name='Item[$index][quantity]' type='hidden' value='$quantity'>";
                            echo "</div>";

                            $index = ($i * 2) + 1;
                            echo "<div class='col-sm-5 form-group'>";
                            echo "<div class='input-group'>";
                            echo "<span class='input-group-addon'>" . Economy::MONETARY_SYMBOL . "</span>";
                            echo "<input name='Item[$index][price]' type='text' class='form-control money' required>";
                            echo "</div>";
                            echo "<input name='Item[$index][itemType]' type='hidden' value='" . ItemType::ITEM_TYPE_NONFOOD_NAME . "'>";
                            echo "<input name='Item[$index][quantity]' type='hidden' value='$quantity'>";
                            echo "</div>";

                            echo "</div>";
                        }
                    ?>

                </div>
                <div class="modal-footer">
                    <div class="btn-group btn-group-justified">
                        <div class="btn-group">
                            <button class="btn btn-default" type="button" data-dismiss="modal">Cancel</button>
                        </div>
                        <div class="btn-group">
                            <button class="btn btn-primary" type="submit">Submit</button>
                        </div>
                    </div>
                </div>
            </form>

        </div>
    </div>
</div>
