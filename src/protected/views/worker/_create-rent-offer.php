<?php
    $form = new RentOfferForm('create');
?>

<div class="modal fade" id="give_for_rent" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">

            <?php
                $widget = $this->beginWidget('CActiveForm', array(
                    'action' => $this->createUrl(strtolower(Yii::app()->user->roleType) . '/createRentOffer'),
                    'enableAjaxValidation' => true,
                    'enableClientValidation' => true,
                    'clientOptions' => array(
                        'validateOnSubmit' => true,
                        'validateOnChange' => true,
                        'validateOnType' => false,
                    ),
                ));
            ?>
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Create Rent Offer</h4>
                </div>
                <div class="modal-body">

                    <div class="row">
                        <div class="col-sm-6 form-group">
                            <?php
                                echo $widget->label($form, 'item_id');
                            ?>
                            <select class="chosen form-control" name= "RentOfferForm[item_id]" id="RentOfferForm_item_id">
                                <option value=""></option>
                                <?php
                                    foreach ($entityHasItems as $entityHasItem)
                                    {
                                        if (!EntityRentEntityHasItem::isRentInProgress($entityHasItem->getID()))
                                        {
                                            $item = $entityHasItem->getItem();
                                            echo '<option value="' . $entityHasItem->getID() . '">';
                                            echo $item->getItemName();
                                            echo '</option>';
                                        }
                                    }
                                ?>
                            </select>
                            <?php
                                echo $widget->error($form, 'item_id');
                            ?>
                        </div>
                        <div class="col-sm-6 form-group">
                            <?php
                                echo $widget->label($form, 'price');

                                echo '<div class="input-group">';
                                echo '<span class="input-group-addon">' . Economy::MONETARY_SYMBOL . '</span>';
                                echo $widget->textField($form, 'price', array(
                                    'class' => 'form-control money',
                                ));
                                echo '</div>';

                                echo $widget->error($form, 'price');
                            ?>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <div class="btn-group btn-group-justified">
                        <div class="btn-group">
                            <button class="btn btn-default" type="button" data-dismiss="modal">Cancel</button>
                        </div>
                        <div class="btn-group">
                            <?php echo CHtml::submitButton('Submit', array('class' => 'btn btn-primary')); ?>
                        </div>
                    </div>
                </div>
            <?php $this->endWidget(); ?>

        </div>
    </div>
</div>
