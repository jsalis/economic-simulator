<?php
	$economy = Economy::model()->findByPk(Yii::app()->user->economyID);
	$hideTabs = (Yii::app()->user->economyID == Economy::ECONOMY_DEFAULT_ID) ? 'hide' : '';
	$tab = (isset($tab)) ? $tab : 'students';

	// Construct error message
	$errorMessage = '';
    if (isset($model) && $model->hasErrors())
    {
        $error = $model->getErrors();
        foreach ($error as $key => $value)
        {
            $errorMessage .= $model->getError($key) . ' ';
        }
    }
?>

<div class="col-sm-12">
	<h3>Economy Settings</h3>
	<div class="alert alert-danger alert-dismissable <?php if ($errorMessage == '') echo 'hide'; ?>">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		<b>Error : </b><?php echo $errorMessage; ?>
	</div>
</div>
<div class="col-sm-3">
	<?php
		$this->renderPartial('/teacher/_general', array(
			'model' => $model,
			'economy' => $economy,
		));
	?>
</div>
<div class="col-sm-9">

	<div class="panel panel-default">
	  	<div class="panel-body">
	  		<!-- Tab List -->
		    <ul class="nav nav-tabs">
		    	<li class="<?php echo $hideTabs; if ($tab == 'students') echo 'active'; ?>">
		            <a href="/index.php/<?php echo Yii::app()->user->type; ?>/student-settings">Students</a>
		        </li>
		        <li class="<?php echo $hideTabs; if ($tab == 'election') echo 'active'; ?>">
		            <a href="/index.php/<?php echo Yii::app()->user->type; ?>/election-settings">Election</a>
		        </li>
		        <li class="<?php echo $hideTabs; if ($tab == 'capital') echo 'active'; ?>">
		            <a href="/index.php/<?php echo Yii::app()->user->type; ?>/capital-settings">Capital</a>
		        </li>
		        <li class="<?php echo $hideTabs; if ($tab == 'public-goods') echo 'active'; ?>">
		            <a href="/index.php/<?php echo Yii::app()->user->type; ?>/public-good-settings">Public Goods</a>
		        </li>
		        <li class="<?php if ($tab == 'rules') echo 'active'; ?>">
		            <a href="/index.php/<?php echo Yii::app()->user->type; ?>/rule-settings">Economy Rules</a>
		        </li>
		        <li class="<?php echo $hideTabs; if ($tab == 'trade') echo 'active'; ?>">
		            <a href="/index.php/<?php echo Yii::app()->user->type; ?>/trade-settings">Economic Trade</a>
		        </li>
		        <li class="<?php echo $hideTabs; if ($tab == 'statistics') echo 'active'; ?>">
		            <a href="/index.php/<?php echo Yii::app()->user->type; ?>/economy-statistics">Statistics</a>
		        </li>
		    </ul>
		</div>
		<div class="panel-body">
		    <!-- Tab content -->
			<div class="tab-content">
				<?php
					if ($economy->getEconomyID() != Economy::ECONOMY_DEFAULT_ID)
					{
						if ($tab == 'students')
						{
							$this->renderPartial('/teacher/_students', array(
								'model' => $model,
								'accessor' => $accessor,
							));
						}
						else if ($tab == 'election')
						{
							$this->renderPartial('/teacher/_election', array(
								'model' => $model,
								'accessor' => $accessor,
							));
						}
						else if ($tab == 'capital')
						{
							$this->renderPartial('/teacher/_capital', array(
								'model' => $model,
								'accessor' => $accessor,
							));
						}
						else if ($tab == 'trade')
						{
							$this->renderPartial('/teacher/_trade', array(
								'model' => $model,
								'economy' => $economy,
							));
						}
						else if ($tab == 'public-goods')
						{
							$this->renderPartial('/teacher/_public-goods', array(
								'model' => $model,
								'accessor' => $accessor,
							));
						}
						else if ($tab == 'statistics')
						{
							$this->renderPartial('/teacher/_statistics', array(
								'model' => $model,
								'accessor' => $accessor,
							));
						}
					}

					if ($tab == 'rules')
					{
						$this->renderPartial('/teacher/_rules', array(
							'model' => $model,
							'ruleList' => $economy->getRuleList(),
						));
					}
				?>
			</div>
		</div>
	</div>

</div>
