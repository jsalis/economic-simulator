<?php
    $isNomination = Economy::isNominationInProgress(Yii::app()->user->economyID);
    $isElection = Economy::isElectionInProgress(Yii::app()->user->economyID);
    $isElectionEnded = Economy::isElectionEnded(Yii::app()->user->economyID);
    $candidateList = Economy::getCandidateList(Yii::app()->user->economyID);
?>

<!-- Election Panel -->
<div id="election">
    <div class="row form-group">
        <div class="col-xs-6">
            <div class="btn-group">
                <a class="btn btn-sm btn-default" href="/index.php/accessor/start-nomination" <?php if ($isNomination || $isElection) echo 'disabled'; ?>>Start Nomination</a>
                <a class="btn btn-sm btn-default" href="/index.php/accessor/start-election" <?php if (!$isNomination || $isElectionEnded) echo 'disabled'; ?>>Start Election</a>
                <a class="btn btn-sm btn-default" href="/index.php/accessor/end-election" <?php if (!$isElection || $isNomination) echo 'disabled'; ?>>End Election</a>
            </div>
        </div>
        <div class="col-xs-6">
            <p class="lead text-right">
                <?php
                    if ($isNomination)
                    {
                        echo 'Nomination in progress.';
                    }
                    if ($isElection)
                    {
                        echo 'Election in progress.';
                    }
                    if ($isElectionEnded)
                    {
                        echo 'Election has ended.';
                    }
                ?>
            </p>
        </div>
    </div>
    <div>
        <table class="table-hover" data-filter="#filter" data-filter-text-only="true">
            <thead>
                <tr class="<?php if (sizeof($candidateList) == 0) echo 'hide'; ?>">
                    <th>Name</th>
                    <th data-sort-ignore="true">Email</th>
                    <th>Votes</th>
                </tr>
            </thead>
            <tbody>
                <?php
                    for ($i = 0; $i < sizeof($candidateList); $i++)
                    {
                        echo "<tr>";
                        echo "<td>" . $candidateList[$i]['name'] . "</td>";
                        echo "<td>" . $candidateList[$i]['emailAddress'] . "</td>";
                        echo "<td>" . $candidateList[$i]['voteCount'] . "</td>";
                        echo "</tr>";
                    }
                    if (sizeof($candidateList) == 0)
                    {
                        echo "<tr><td><em>No candidates found.</em></td></tr>";
                    }
                ?>
            </tbody>
        </table>
    </div>
</div>
