<?php
    $publicGoodList = $accessor->getPublicGoodList();

    $this->renderPartial('/accessor/_create-public-good', array(
        'model' => $model,
    ));
    $this->renderPartial('/accessor/_update-public-good', array(
        'model' => $model,
    ));

    $scriptData = array();
    for ($i = 0; $i < sizeof($publicGoodList); $i++)
    {
        $itemID = $publicGoodList[$i]->getItemID();
        $scriptData[$itemID] = array(
            'name' => $publicGoodList[$i]->getItemName(),
            'description' => $publicGoodList[$i]->getItemDescription(),
        );
    }
?>

<script>
    var publicGoodData = <?php echo json_encode($scriptData); ?>;
</script>

<!-- Public Goods Panel -->
<div id="public_goods">
    <div class="row form-group">
        <div class="col-xs-12">
            <div class="btn-group">
                <a class="btn btn-sm btn-primary" data-toggle="modal" data-target="#create_public_good">Create Public Good</a>
            </div>
        </div>
    </div>
	<div>
        <table class="table-hover" data-filter="#filter" data-filter-text-only="true">
            <thead>
                <tr class="<?php if (sizeof($publicGoodList) == 0) echo 'hide'; ?>">
                    <th>Public Good</th>
                    <th data-sort-ignore="true">Description</th>
                </tr>
            </thead>
            <tbody>
            	<?php
            		for ($i = 0; $i < sizeof($publicGoodList); $i++)
            		{
                        $id = $publicGoodList[$i]->getItemID();

            			echo "<tr id='$id' data-toggle='modal' data-target='#update_public_good'>";
                        echo "<td id='name'>" . $publicGoodList[$i]->getItemName() . "</td>";
                        echo "<td id='description'>" . $publicGoodList[$i]->getItemDescription() . "</td>";
                        echo "<td id='monetary_start'>" . $publicGoodList[$i]->getBenefitStart(Entity::ENTITY_ATTRIBUTE_MONETARY) . "</td>";
                        echo "<td id='monetary_decrease_rate'>" . $publicGoodList[$i]->getBenefitDecreaseRate(Entity::ENTITY_ATTRIBUTE_MONETARY) . "</td>";
                        echo "<td id='efficiency_start'>" . $publicGoodList[$i]->getBenefitStart(Entity::ENTITY_ATTRIBUTE_EFFICIENCY) . "</td>";
                        echo "<td id='efficiency_decrease_rate'>" . $publicGoodList[$i]->getBenefitDecreaseRate(Entity::ENTITY_ATTRIBUTE_EFFICIENCY) . "</td>";
                        echo "<td id='health_start'>" . $publicGoodList[$i]->getBenefitStart(Entity::ENTITY_ATTRIBUTE_HEALTH) . "</td>";
                        echo "<td id='health_decrease_rate'>" . $publicGoodList[$i]->getBenefitDecreaseRate(Entity::ENTITY_ATTRIBUTE_HEALTH) . "</td>";
                        echo "<td id='type'>" . $publicGoodList[$i]->getBenefitReceiver() . "</td>";
            			echo "</tr>";
            		}
            		if (sizeof($publicGoodList) == 0)
            		{
            			echo "<tr><td><em>No public goods found.</em></td></tr>";
            		}
            	?>
            </tbody>
        </table>
  	</div>
</div>

<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/site/public-good.js"></script>
