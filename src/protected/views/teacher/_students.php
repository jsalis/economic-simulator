<?php
    $studentList = $model->getStudentsInEconomy(Yii::app()->user->economyID);
    $isNomination = Economy::isNominationInProgress(Yii::app()->user->economyID);

    $entityList = $accessor->getEntitiesInEconomy();

    $scriptData = array();
    foreach ($studentList as $student)
    {
        $userID = $student->getUserID();
        $scriptData[$userID] = array();
        $scriptData[$userID]['name'] = $student->getUserFirstName() . ' ' . $student->getUserLastName();
        $scriptData[$userID]['email'] = $student->getUserEmailAddress();
    }

    foreach ($entityList as $entity)
    {
        $entityID = $entity->getEntityID();

        foreach ($entity->userHasEntities as $userHasEntity)
        {
            $userID = $userHasEntity->user_id;

            $scriptData[$userID][$entityID] = array(
                'name' => $entity->getEntityName(),
            );
        }
    }

    $this->renderPartial('/accessor/_view-entities', array(
        'model' => $model,
        'entityList' => $entityList,
    ));

    $this->renderPartial('/accessor/_give-company', array(
        'model' => $model,
    ));

    $this->renderPartial('/accessor/_give-money', array(
        'model' => $model,
    ));

    $this->renderPartial('/accessor/_give-health', array(
        'model' => $model,
    ));
?>

<script>
    var studentData = <?php echo json_encode($scriptData); ?>;
</script>

<!-- Students Panel -->
<div id="students">
    <div class="row form-group">
        <div class="col-xs-6 col-md-4">
            <div class="btn-group">
                <a class="btn btn-sm btn-default" data-toggle="modal" data-target="#view_entities">View All</a>
                <a class="btn btn-sm btn-default" data-toggle="modal" data-target="#give_money">Give Money</a>
                <a class="btn btn-sm btn-default" data-toggle="modal" data-target="#give_health">Give Health</a>
            </div>
        </div>
        <div class="col-xs-6 col-md-4 pull-right">
            <form action="/index.php/teacher/add-student-to-economy" method="post">
                <div class="input-group">
                    <input name= "Student[emailAddress]" type="text" class="form-control input-sm" id="Student_emailAddress" placeholder="Enter an email address" required>
                    <span class="input-group-btn">
                        <button class="btn btn-sm btn-primary" type="submit">&nbsp;<span class="glyphicon glyphicon-plus"></span>&nbsp;</button>
                    </span>
                </div>
            </form>
        </div>
    </div>
    <div>
        <table class="table-hover" data-filter="#filter" data-filter-text-only="true">
            <thead>
                <tr class="<?php if (sizeof($studentList) == 0) echo 'hide'; ?>">
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th data-sort-ignore="true">Email</th>
                    <th data-sort-ignore="true">Phone</th>
                    <th data-sort-ignore="true"></th>
                </tr>
            </thead>
            <tbody>
                <?php
                    for ($i = 0; $i < sizeof($studentList); $i++)
                    {
                        $id = $studentList[$i]->getUserID();

                        echo "<tr id='$id' data-toggle='modal' data-target='#view_entities' style='cursor: pointer'>";
                        echo "<td>" . $studentList[$i]->getUserFirstName() . "</td>";
                        echo "<td>" . $studentList[$i]->getUserLastName() . "</td>";
                        echo "<td>" . $studentList[$i]->getUserEmailAddress() . "</td>";
                        echo "<td>" . $studentList[$i]->getUserCellPhone() . "</td>";

                        echo "<td class='text-right'>";
                        echo "<div class='dropdown'>";

                        echo "<button class='btn btn-xs btn-default dropdown-toggle' type='button' data-toggle='dropdown'>";
                        echo "&nbsp;<span class='glyphicon glyphicon-cog'></span>&nbsp;";
                        echo "</button>";

                        echo "<ul class='dropdown-menu dropdown-menu-right' role='menu'>";
                        echo "<li><a id='$id' class='option' tabindex='-1' data-toggle='modal' data-target='#give_company'>Give Company</a></li>";
                        if ($isNomination)
                        {
                            echo "<li><a class='option form-submit' tabindex='-1'>Nominate";
                            echo "<form action='/index.php/accessor/add-candidate' method='post'>";
                            echo "<input type='hidden' name='User[id]' value='$id' required>";
                            echo "</form>";
                            echo "</a></li>";
                        }
                        echo "</ul>";

                        echo "</div>";
                        echo "</td>";

                        echo "</tr>";
                    }
                    if (sizeof($studentList) == 0)
                    {
                        echo "<tr><td><em>No students found.</em></td></tr>";
                    }
                ?>
            </tbody>
        </table>
    </div>
</div>

<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/site/students.js"></script>
