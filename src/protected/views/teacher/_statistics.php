
<!-- Statistics Panel -->
<div id="statistics">
	<div class="form-group">
	    <form action="/index.php/<?php echo Yii::app()->user->type; ?>/entered-economy" method="post">
	    	<button class="btn btn-sm btn-default download" type="submit">Entered Economy</button>
	    </form>
	</div>
	<div class="form-group">
	    <form action="/index.php/<?php echo Yii::app()->user->type; ?>/entered-labor-market" method="post">
	    	<button class="btn btn-sm btn-default download" type="submit">Entered Labor Market</button>
	    </form>
	</div>
	<div class="form-group">
	    <form action="/index.php/<?php echo Yii::app()->user->type; ?>/entities-in-economy" method="post">
	    	<button class="btn btn-sm btn-default download" type="submit">Get All Entities</button>
	    </form>
	</div>
	<div class="form-group">
	    <form action="/index.php/<?php echo Yii::app()->user->type; ?>/items-in-economy" method="post">
	    	<button class="btn btn-sm btn-default download" type="submit">Get All Items</button>
	    </form>
	</div>
	<div class="form-group">
	    <form action="/index.php/<?php echo Yii::app()->user->type; ?>/workers-with-jobs" method="post">
	    	<button class="btn btn-sm btn-default download" type="submit">Workers With Jobs</button>
	    </form>
	</div>
	<div class="form-group">
	    <form action="/index.php/<?php echo Yii::app()->user->type; ?>/unfilled-job-offers" method="post">
	    	<button class="btn btn-sm btn-default download" type="submit">Unfilled Job Offers</button>
	    </form>
	</div>
	<div class="form-group">
	    <form action="/index.php/<?php echo Yii::app()->user->type; ?>/willingness-to-pay" method="post">
	    	<button class="btn btn-sm btn-default download" type="submit">Willingness To Pay</button>
	    </form>
	</div>
	<div class="form-group">
	    <form action="/index.php/<?php echo Yii::app()->user->type; ?>/item-and-production" method="post">
	    	<button class="btn btn-sm btn-default download" type="submit">Item And Production</button>
	    </form>
	</div>
	<div class="form-group">
	    <form action="/index.php/<?php echo Yii::app()->user->type; ?>/purchased-market-items" method="post">
	    	<button class="btn btn-sm btn-default download" type="submit">Purchased Market Items</button>
	    </form>
	</div>
	<div class="form-group">
	    <form action="/index.php/<?php echo Yii::app()->user->type; ?>/available-market-items" method="post">
	    	<button class="btn btn-sm btn-default download" type="submit">Available Market Items</button>
	    </form>
	</div>
	<div class="form-group">
	    <form action="/index.php/<?php echo Yii::app()->user->type; ?>/daily-data" method="post">
	    	<button class="btn btn-sm btn-default download" type="submit">Daily Data</button>
	    </form>
	</div>
</div>
