<?php
    // Construct creation date
    if (is_null($economy->getEconomyCreationDate()))
    {
    	$creationDate = '<em>none</em>';
    }
    else
    {
    	$date = DateTime::createFromFormat(Yii::app()->params->dbDateFormat, $economy->getEconomyCreationDate());
		$creationDate = $date->format('F j, Y');
    }

	// Construct end date
	if (is_null($economy->getEconomyEndDate()))
	{
		$endDate = '<em>none</em>';
	}
	else
	{
		$date = DateTime::createFromFormat(Yii::app()->params->dbDateFormat, $economy->getEconomyEndDate());
		$endDate = $date->format('F j, Y');
	}
?>

<!-- General Panel -->
<div class="panel panel-default">
	<form action="/index.php/<?php echo Yii::app()->user->type; ?>/update-economy" method="post">
	  	<div class="panel-body">
	  		<div class="form-group">
	    		<div class="pull-right">
	    			<button class="btn btn-default btn-sm btn-edit" type="button">&nbsp;<span class="glyphicon glyphicon-pencil"></span>&nbsp;</button>
	    		</div>
	    		<span class="lead static-input" name="Economy_name"><?php echo $economy->getEconomyName(); ?></span>
	    		<input name="Economy[name]" id="Economy_name" type="text" class="form-control hide" placeholder="Economy Name" required>
	    	</div>
	    	<div class="form-group">
	    		<strong>Description</strong><br />
	    		<span class="static-input small" name="Economy_description"><?php echo $economy->getEconomyDescription(); ?></span>
	    		<textarea rows="3" style="resize: none;" name="Economy[description]" id="Economy_description" type="text" class="form-control hide" required></textarea>
	    	</div>
	    	<div class="form-group">
	    		<strong>Creation Date</strong>
	    		<div class="pull-right small"><?php echo $creationDate; ?></div>
	    	</div>
	    	<div class="form-group">
	    		<strong>End Date</strong>
		    	<div class="pull-right small"><?php echo $endDate; ?></div>
	    	</div>
	    	<div class="form-buttons hide">
	    		<div class="btn-group btn-group-justified">
	    			<div class="btn-group">
			    		<button class="btn btn-default btn-cancel" type="button">Cancel</button>
			    	</div>
			    	<div class="btn-group">
			    		<button class="btn btn-primary" type="submit">Save</button>
			    	</div>
		    	</div>
	    	</div>
	  	</div>
    </form>
</div>
