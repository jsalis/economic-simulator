<?php
    if (Yii::app()->user->type == 'super' || Yii::app()->user->type == 'admin')
    {
        $economyList = Economy::model()->findAll();
    }
    else
    {
        $economyList = $model->getEconomyList();
    }
?>

<!-- Economic Trade Panel -->
<div id="trade">
    <form action="/index.php/<?php echo Yii::app()->user->type; ?>/update-economy-relations" method="post">
        <div class="row form-group">
            <div class="col-xs-6 col-md-4 pull-right">
                <div class="btn-group btn-group-justified">
                    <div class="btn-group">
                        <button class="btn btn-sm btn-default btn-cancel" type="button">Cancel</button>
                    </div>
                    <div class="btn-group">
                        <button class="btn btn-sm btn-primary" type="submit">Save</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="scroller-large">
            <table class="table-hover fix-header" data-filter="#filter" data-filter-text-only="true" data-sort="false">
                <thead>
                    <tr class="<?php if (sizeof($economyList) <= 1) echo 'hide'; ?>">
                        <th>Trade Flow</th>
                        <th>Description</th>
                        <th>State</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        for ($i = 0; $i < sizeof($economyList); $i++)
                        {
                            $id = $economyList[$i]->getEconomyID();

                            if ($id != Yii::app()->user->economyID && $id != Economy::ECONOMY_DEFAULT_ID)
                            {
                                echo "<tr>";
                                echo "<td><strong>" . $economy->getEconomyName() . ' <span class="glyphicon glyphicon-arrow-right"></span> ' . $economyList[$i]->getEconomyName() . "</strong></td>";

                                echo "<td>" . $economyList[$i]->getEconomyDescription() . "</td>";

                                echo "<td>";
                                echo "<input type='hidden' name='Economy[relation][$id]' value='0'>";
                                echo "<input class='switch' name='Economy[relation][$id]' id='Economy_relation_$id' value='1' type='checkbox' ";
                                if (EconomyRelation::hasRelation(Yii::app()->user->economyID, $id))
                                {
                                    echo "checked";
                                }
                                echo "></td></tr>";
                            }
                        }
                        if (sizeof($economyList) == 1)
                        {
                            echo '<tr><td><em>No other economies found.</em></td></tr>';
                        }
                    ?>
                </tbody>
            </table>
        </div>
    </form>
</div>
