<?php
    $modifierList = $accessor->getModifierList();
    $workerList = Worker::model()->economy(Yii::app()->user->economyID)->findAll();

    $this->renderPartial('/accessor/_create-capital', array(
        'model' => $model,
    ));

    $this->renderPartial('/accessor/_update-capital', array(
        'model' => $model,
    ));

    $this->renderPartial('/accessor/_add-to-market', array(
        'model' => $model,
        'itemList' => $modifierList,
    ));
    
    $this->renderPartial('/accessor/_give-item', array(
        'model' => $model,
        'itemList' => $modifierList,
        'entityList' => $workerList,
    ));

    $scriptData = array();
    for ($i = 0; $i < sizeof($modifierList); $i++)
    {
        $itemID = $modifierList[$i]->getItemID();
        $scriptData[$itemID] = array(
            'name' => $modifierList[$i]->getItemName(),
            'type' => $modifierList[$i]->getItemType(),
            'description' => $modifierList[$i]->getItemDescription(),
            'efficiency' => $modifierList[$i]->getEfficiency(),
            'price' => $modifierList[$i]->getPrice(),
        );
    }
?>

<script>
    var capitalData = <?php echo json_encode($scriptData); ?>;
</script>

<!-- Capital Panel -->
<div id="capital">
    <div class="row form-group">
        <div class="col-xs-12">
            <div class="btn-group">
                <a class="btn btn-sm btn-primary" data-toggle="modal" data-target="#create_capital">Create Capital</a>
                <a class="btn btn-sm btn-default" data-toggle="modal" data-target="#add_to_market">Add To Market</a>
                <a class="btn btn-sm btn-default" data-toggle="modal" data-target="#give_item">Give To Worker</a>
            </div>
        </div>
    </div>
	<div>
        <table class="table-hover" data-filter="#filter" data-filter-text-only="true">
            <thead>
                <tr class="<?php if (sizeof($modifierList) == 0) echo 'hide'; ?>">
                    <th>Name</th>
                    <th>Type</th>
                    <th data-sort-ignore="true">Description</th>
                    <th>Efficiency</th>
                    <th>Price</th>
                    <th>Stock</th>
                </tr>
            </thead>
            <tbody>
            	<?php
            		for ($i = 0; $i < sizeof($modifierList); $i++)
            		{
                        $id = $modifierList[$i]->getItemID();

            			echo "<tr id='$id' data-toggle='modal' data-target='#update_capital' style='cursor: pointer'>";
                        echo "<td>" . $modifierList[$i]->getItemName() . "</td>";
            			echo "<td>" . $modifierList[$i]->getItemType() . "</td>";
                        echo "<td>" . $modifierList[$i]->getItemDescription() . "</td>";
                        echo "<td>" . $modifierList[$i]->getEfficiency() . "</td>";
                        echo "<td data-type='numeric' data-value='" . $modifierList[$i]->getPrice() . "'><span class='text-success'>" . Economy::MONETARY_SYMBOL . $modifierList[$i]->getPrice() . "</span></td>";
                        echo "<td data-type='numeric'>" . $modifierList[$i]->getItemStock() . "</td>";
            			echo "</tr>";
            		}
            		if (sizeof($modifierList) == 0)
            		{
            			echo "<tr><td><em>No capital found.</em></td></tr>";
            		}
            	?>
            </tbody>
        </table>
  	</div>
</div>

<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/site/capital.js"></script>
