
<!-- Rules Panel -->
<div id="rules">
    <form action="/index.php/accessor/update-economy-rules" method="post">
        <div class="row form-group">
            <div class="col-xs-6 col-md-4">
                <div class="input-group">
                    <span class="input-group-addon">Search</span>
                    <input class="form-control input-sm" id="filter" role="search" type="text">
                </div>
            </div>
            <div class="col-xs-6 col-md-4 pull-right">
                <div class="btn-group btn-group-justified">
                    <div class="btn-group">
                        <button class="btn btn-sm btn-default btn-cancel" type="button">Cancel</button>
                    </div>
                    <div class="btn-group">
                        <button class="btn btn-sm btn-primary" type="submit">Save</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="scroller-large">
            <table class="table-hover fix-header" data-filter="#filter" data-filter-text-only="true" data-sort="false">
                <thead>
                    <tr>
                        <th>Rule Name</th>
                        <th>Value</th>
                        <th>State</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        for ($i = 0; $i < sizeof($ruleList); $i++)
                        {
                            $id = $ruleList[$i]->getRuleID();
                            $ruleType = $ruleList[$i]->getRuleType();

                            echo "<tr>";
                            echo "<td>" . $ruleList[$i]->getRuleName() . "</td>";
                            echo "<td>";

                            if (RuleType::hasValue($ruleType))
                            {
                                $value = $ruleList[$i]->getRuleValue();
                                echo "<input class='form-control-small' type='text' name='Rule[list][$id][value]' id='Rule_list_$id' value='" . $value . "' required>";
                            }
                            echo "</td><td>";

                            if (RuleType::hasState($ruleType))
                            {
                                echo "<input type='hidden' name='Rule[list][$id][activated]' value='0'>";
                                echo "<input class='switch' name='Rule[list][$id][activated]' id='Rule_list_$id' value='1' type='checkbox' ";
                                
                                if ($ruleList[$i]->isRuleActivated())
                                {
                                    echo "checked" . " ";
                                }
                                echo ">";
                            }
                            echo "</td>";
                            echo "</tr>";
                        }
                    ?>
                </tbody>
            </table>
        </div>
    </form>
</div>
