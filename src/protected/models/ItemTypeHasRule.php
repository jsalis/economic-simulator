<?php

Yii::import('application.models._base.BaseItemTypeHasRule');

/**
 * The ItemTypeHasRule class is there to represent every bit of detail when it comes to 
 * the relation of the Entities and Rules. This relation only goes as deep as both the
 * primary keys of the ItemType and Rule. So the only methods for this class are report
 * methods that get either all the Rule ids for the item type or vice versa.
 * 
 * @author   <cmicklis@stetson.edu>, <jsalis@stetson.edu>
 * @since 	 v0.0.0
 * 
 */

class ItemTypeHasRule extends BaseItemTypeHasRule
{
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * 	The createItemTypeHasRule method adds a rule associated with the item type to the
	 * 	ItemTypeHasRule table. This allows the Entity Type to have a relation to an rule, or
	 * 	multiple rules.
	 *
	 *  @param int      $economyID       Represents the economy ID.
	 *  @param int      $entityTypeID    Represents the item type ID.
	 *  @param int      $ruleID          Represents the rule ID.
	 */
	public static function createItemTypeHasRule($economyID, $itemTypeID, $ruleID)
	{
		$itemTypeHasRule = new ItemTypeHasRule;
		$itemTypeHasRule->economy_id = $economyID;
		$itemTypeHasRule->item_type_id = $itemTypeID;
		$itemTypeHasRule->rule_id = $ruleID;
		$itemTypeHasRule->save();

		return $itemTypeHasRule;
	}
}
