<?php

Yii::import('application.models._base.BaseEconomyRelation');

/**
 * The EconomyRelation is there to represent the relation between
 * multiple economies, stating what economies are tradable and which
 * are not. This means that if two economies are both relatable on the Economy
 * Relation table then one economy may export goods to the market of another
 * economy.
 * 
 * @author   <cmicklis@stetson.edu>, <jsalis@stetson.edu>
 * @since 	 v0.0.0
 */

class EconomyRelation extends BaseEconomyRelation
{
	private $_economyRelationRuleList;

	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * 	The createEconomyRelation method adds a economyRelation using all information
	 * 	given into the EconomyRelation table.
	 * 	
	 *  @param int      $economyFromID    Represents the economy ID where goods are traded from.
	 *  @param int      $economyToID      Represents the economy ID where goods are traded to.
	 */
	public static function createEconomyRelation($economyFromID, $economyToID)
	{
		$economyRelation = new EconomyRelation;
		$economyRelation->economy_from_id = $economyFromID;
		$economyRelation->economy_to_id = $economyToID;
		$economyRelation->save();

		EconomyRelation::model()->copyEconomyRelationRules($economyRelation->economy_relation_id);

		return $economyRelation->economy_relation_id;
	}

	/**
	 *	Finds the rules for the default economy relation and copies those rules 
	 *	to the specified economy relation.
	 * 
	 * 	@param  int 	$economyRelationID 	The ID of the economy relation to receive rules.
	 */
	public static function copyEconomyRelationRules($economyRelationID)
	{
		$defaultRelationID = EconomyRelation::model()->findRelationID(Economy::ECONOMY_DEFAULT_ID, Economy::ECONOMY_DEFAULT_ID);
		$ruleList = Rule::model()->getEconomyRelationRules($defaultRelationID, Economy::ECONOMY_DEFAULT_ID);

		for ($i = 0; $i < sizeof($ruleList); $i++)
		{
			$newRule = Rule::model()->copyRule($ruleList[$i]);
			EconomyRelationHasRule::model()->createEconomyRelationHasRule($economyRelationID, $newRule->getRuleID());
		}
	}

	/**
	 * 	The removeEconomyRelation method removes a economy relation using all information
	 * 	given into the EconomyRelation table for the specific row defined by the primary key.
	 * 	
	 *  @param int      $economyFromID    Represents the economy ID where goods are traded from.
	 *  @param int      $economyToID      Represents the economy ID where goods are traded to.
	 */
	public static function removeEconomyRelation($economyFromID, $economyToID)
	{
		$record = EconomyRelation::model()->find(array(
			'select' => 'economy_relation_id',
			'condition' => 'economy_from_id=:economyFromID AND economy_to_id=:economyToID',
			'params' => array(
				':economyFromID' => $economyFromID,
				':economyToID' => $economyToID,
			),
		));

		// TO DO : This function should also delete the rules for the economy relation.

		EconomyRelation::model()->deleteByPK($record->economy_relation_id);
	}

	/**
		 _______  _______  _______  _        _______  _______           _______  _______  _        _______ __________________ _______  _          _______  _______  _______  _______  _______ _________ _______ 
		(  ____ \(  ____ \(  ___  )( (    /|(  ___  )(       )|\     /|(  ____ )(  ____ \( \      (  ___  )\__   __/\__   __/(  ___  )( (    /|  (  ____ )(  ____ \(  ____ )(  ___  )(  ____ )\__   __/(  ____ \
		| (    \/| (    \/| (   ) ||  \  ( || (   ) || () () |( \   / )| (    )|| (    \/| (      | (   ) |   ) (      ) (   | (   ) ||  \  ( |  | (    )|| (    \/| (    )|| (   ) || (    )|   ) (   | (    \/
		| (__    | |      | |   | ||   \ | || |   | || || || | \ (_) / | (____)|| (__    | |      | (___) |   | |      | |   | |   | ||   \ | |  | (____)|| (__    | (____)|| |   | || (____)|   | |   | (_____ 
		|  __)   | |      | |   | || (\ \) || |   | || |(_)| |  \   /  |     __)|  __)   | |      |  ___  |   | |      | |   | |   | || (\ \) |  |     __)|  __)   |  _____)| |   | ||     __)   | |   (_____  )
		| (      | |      | |   | || | \   || |   | || |   | |   ) (   | (\ (   | (      | |      | (   ) |   | |      | |   | |   | || | \   |  | (\ (   | (      | (      | |   | || (\ (      | |         ) |
		| (____/\| (____/\| (___) || )  \  || (___) || )   ( |   | |   | ) \ \__| (____/\| (____/\| )   ( |   | |   ___) (___| (___) || )  \  |  | ) \ \__| (____/\| )      | (___) || ) \ \__   | |   /\____) |
		(_______/(_______/(_______)|/    )_)(_______)|/     \|   \_/   |/   \__/(_______/(_______/|/     \|   )_(   \_______/(_______)|/    )_)  |/   \__/(_______/|/       (_______)|/   \__/   )_(   \_______)
	                                                                                                                                                                                                        
	 */
	
	/**
	 * Checks if there is a relation from one economy to another economy.
	 * 
	 * @param  String  $economyFromID   Represents the 'from' economy ID.
	 * @param  String  $economyToID     Represents the 'to' economy ID.
	 * @return boolean                	Whether the first economy has a relation to the second economy.
	 */
	public static function hasRelation($economyFromID, $economyToID)
	{
		return EconomyRelation::model()->exists(array(
			'condition' => 'economy_from_id=:economyFromID AND economy_to_id=:economyToID',
			'params' => array(
				':economyFromID' => $economyFromID,
				':economyToID' => $economyToID,
			),
		));
	}

	/**
	 * 	Finds a specific Economy Relation ID that defines the relation from one economy
	 * 	to another economy.
	 * 	
	 *  @param  int    $economyFromID 		The ID of the economy that defines the sender.
	 *  @param  int    $economyToID 		The ID of the economy that defines the receiver.
	 *  @return int 						The ID of the economy relation.
	 */
	public static function findRelationID($economyFromID, $economyToID)
	{
		$result = EconomyRelation::model()->find(array(
			'select' => 'economy_relation_id',
			'condition' => 'economy_from_id=:economyFromID && economy_to_id=:economyToID',
			'params' => array(
				':economyFromID' => $economyFromID,
				':economyToID' => $economyToID,
			),
		));
		return $result->getEconomyRelationID();
	}

	/**
	 * 	Finds all EconomyRelation objects where the specified economy ID is 
	 * 	the 'from' part of the economy relationship.
	 * 	
	 *  @param  int     $economyID        	Represents the economy ID.
	 *  @return array<EconomyRelation>      An array of economy relation objects.
	 */
	public static function findByEconomyFromID($economyID)
	{
		return EconomyRelation::model()->findAll(array(
			'condition' => 'economy_from_id=:economyID',
			'params' => array(':economyID' => $economyID),
		));
	}

	/**
	 * 	Gets the government tariff for the specified economy relation.
	 * 	
	 *  @param  int    $economyRelationID	The ID of the economy relation.
	 *  @return int 						The economy relation tariff.
	 */
	public static function getEconomyRelationTariff($economyRelationID)
	{
		$rule = Rule::findEconomyRelationRule($economyRelationID, RuleType::TAXATION_FROM_PURCHASER, Economy::TAX_ATTRIBUTE_TARIFF);
		return (is_null($rule)) ? null : $rule->getRuleValue();
	}

	/**
		 _______  _______ _________          _______  ______   _______ 
		(       )(  ____ \\__   __/|\     /|(  ___  )(  __  \ (  ____ \
		| () () || (    \/   ) (   | )   ( || (   ) || (  \  )| (    \/
		| || || || (__       | |   | (___) || |   | || |   ) || (_____ 
		| |(_)| ||  __)      | |   |  ___  || |   | || |   | |(_____  )
		| |   | || (         | |   | (   ) || |   | || |   ) |      ) |
		| )   ( || (____/\   | |   | )   ( || (___) || (__/  )/\____) |
		|/     \|(_______/   )_(   |/     \|(_______)(______/ \_______)
		                                                                                                                                
	 */

	public function getEconomyRelationID()
	{
		return $this->economy_relation_id;
	}

	public function getEconomyFrom()
	{
		return $this->economyFrom;
	}

	public function getEconomyTo()
	{
		return $this->economyTo;
	}

	public function getRuleList()
	{
		if (is_null($this->_economyRelationRuleList))
		{
			$this->_economyRelationRuleList = Rule::getEconomyRelationRules($this->getEconomyRelationID);
		}
		return $this->_economyRelationRuleList;
	}
}
