<?php

Yii::import('application.models._base.BaseUser');

/**
 * The User class is there to represent every bit of detail that a User may have
 * from the entities that a user has created or been given to, to the users type. The
 * Class is setup so that the Doorkeeper may also use it to identify logins and check 
 * their validity. The class may also be used to get specific reports about users, So
 * that the Admin or Teacher may be able to follow specific users and track their 
 * performance.
 *
 * @author   <cmicklis@stetson.edu>, <jsalis@stetson.edu>
 * @since 	 v0.0.0
 * 
 */

class User extends BaseUser
{
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * 	The createUser method adds a user using all information given into both the User 
	 * 	table and then also the Login table with the newly created user as the id for
	 * 	the Login table.
	 * 	
	 *  @param String   $emailAddress Represents the users inputted email address.
	 *  @param String   $firstName    Represents the users inputted first name.
	 *  @param String   $lastName     Represents the users inputted last name.
	 *  @param String   $cellPhone    Represents the users inputted cell phone.
	 *  @param String   $password     Represents the users inputted password.
	 *  @param String   $userType     Represents the users inputted user type.
	 *  @return int                   The user ID.
	 */
	public static function createUser($emailAddress, $firstName, $lastName, $cellPhone, $password, $userType)
	{
		$user = new User;
		$user->email_address = $emailAddress;
		$user->first_name = $firstName;
		$user->last_name = $lastName;
		$user->cell_phone = $cellPhone;
		$user->user_type_id = UserType::getUserTypeIDByName($userType);
		$user->save();

		Login::createLogin($user->user_id, $password);

		return $user->user_id;
	}

	/**
	 *  The verifyUserLogin method checks to see if the User has sent in the proper
	 *  credentials. The user name needs to be in the User table and the password 
	 *  needs to been in the Login table.
	 * 
	 * 	@param  String   $emailAddress Represents the user login email address.
	 *  @param  String   $password     Represents the users login password.
	 *  @return boolean                States whether the user information is correct
	 *                                 or not.
	 */
	public static function verifyUserLogin($emailAddress, $password)
	{
		$userID = User::getUserIDByEmailAddress($emailAddress);
		if (is_null($userID))
		{
			return false;
		}
		else 
		{
			return Login::authenticate($userID, $password);
		}
	}

	/**
	 * 	The verifyEmailAddress method checks to see if an email address has not yet been 
	 * 	created yet on the User table.
	 * 	
	 *  @param  String   $emailAddress  A user's email address.
	 *  @return boolean                 Whether or not the email address is 
	 *                                  in the User table.
	 */
	public static function verifyEmailAddress($emailAddress)
	{
		$exists = User::model()->exists(array(
			'condition' => 'email_address=:emailAddress',
			'params' => array(':emailAddress' => $emailAddress)
		));
		return !$exists;
	}

	/**
	 * 	The updateUser method updates a user using all information given into both the User 
	 * 	table and then also the Login table with the newly created user as the id for
	 * 	the Login table, this is all done to effect the row with the specific userID.
	 * 	
	 *  @param String   $userID 	Represents the ID of the user.
	 *  @param String   $firstName 	Represents the first name of the user.
	 *  @param String   $lastName  	Represents the last name of the user.
	 *  @param String   $cellPhone 	Represents the cell phone of the user.
	 */
	public static function updateUser($userID, $firstName, $lastName, $cellPhone)
	{
		User::model()->updateByPk($userID, array(
			'first_name' => $firstName,
			'last_name' => $lastName,
			'cell_phone' => $cellPhone,
		));
	}

	/**
	 * 	The updateUserLogin method updates a user's login password.
	 * 	
	 *  @param  String   $emailAddress Represents the users inputted email address.
	 *  @param  String   $password     Represents the users inputted password.
	 */
	public static function updateUserLogin($emailAddress, $password)
	{
		$userID = User::getUserIDByEmailAddress($emailAddress);
		Login::updateLogin($userID, $password);
	}

	/**
	 * 	The createUserHasEntity method adds a entity associated with the user to the
	 * 	UserHasEntity table. This allows the User to have a relation to an entity, or
	 * 	multiple entities.
	 * 	
	 *  @param int      $userID       Represents the users id.
	 *  @param int      $entityID     Represents the entity id.
	 */
	public static function createUserHasEntity($userID, $entityID)
	{
		$userHasEntity = new UserHasEntity;
		$userHasEntity->user_id = $userID;
		$userHasEntity->entity_id = $entityID;
		$userHasEntity->save();
	}

	/**
	              _______  _______  _______    _______  _______  _______  _______  _______ _________ _______ 
		|\     /|(  ____ \(  ____ \(  ____ )  (  ____ )(  ____ \(  ____ )(  ___  )(  ____ )\__   __/(  ____ \
		| )   ( || (    \/| (    \/| (    )|  | (    )|| (    \/| (    )|| (   ) || (    )|   ) (   | (    \/
		| |   | || (_____ | (__    | (____)|  | (____)|| (__    | (____)|| |   | || (____)|   | |   | (_____ 
		| |   | |(_____  )|  __)   |     __)  |     __)|  __)   |  _____)| |   | ||     __)   | |   (_____  )
		| |   | |      ) || (      | (\ (     | (\ (   | (      | (      | |   | || (\ (      | |         ) |
		| (___) |/\____) || (____/\| ) \ \__  | ) \ \__| (____/\| )      | (___) || ) \ \__   | |   /\____) |
		(_______)\_______)(_______/|/   \__/  |/   \__/(_______/|/       (_______)|/   \__/   )_(   \_______)
                                                                                                     
	 */
	
	public function entity($entityID)
	{
	    $this->getDbCriteria()->mergeWith(array(
        	'with' => array('userHasEntities' => array('select' => false)),
			'condition' => 'userHasEntities.entity_id=:entityID',
			'params' => array(':entityID' => $entityID),
        ));
	    return $this;
	}

	public function economy($economyID)
	{
	    $this->getDbCriteria()->mergeWith(array(
        	'with' => array('userHasEntities.entity.economyHasEntities' => array('select' => false)),
			'condition' => 'economyHasEntities.economy_id=:economyID',
			'params' => array(':economyID' => $economyID),
        ));
	    return $this;
	}
	
	/**
	 * 	Gets the ID of the user who has the provided email address.
	 * 	
	 *  @param  String     $emailAddress    The email address of the user.
	 *  @return int                         The ID of the user. Null if no user was found
	 *                                      with the email address.
	 */
	public static function getUserIDByEmailAddress($emailAddress)
	{
		$result = User::model()->find(array(
			'select' => 'user_id',
			'condition' => 'email_address=:emailAddress',
			'params' => array(':emailAddress' => $emailAddress)
		));
		return (is_null($result)) ? null : $result->getUserID();
	}

	/**
	 * 	Gets the email address of the user with the provided ID.
	 * 	
	 *  @param  int     $userID    The ID of the user.
	 *  @return int                The email address of the user.
	 */
	public static function getUserEmailAddressByID($userID)
	{
		$result = User::model()->find(array(
			'select' => 'email_address',
			'condition' => 'user_id=:userID',
			'params' => array(':userID' => $userID)
		));
		return $result->getUserEmailAddress();
	}

	/**
	              _______  _______  _______    _______  _______ _________          _______  ______   _______ 
	 	|\     /|(  ____ \(  ____ \(  ____ )  (       )(  ____ \\__   __/|\     /|(  ___  )(  __  \ (  ____ \
		| )   ( || (    \/| (    \/| (    )|  | () () || (    \/   ) (   | )   ( || (   ) || (  \  )| (    \/
		| |   | || (_____ | (__    | (____)|  | || || || (__       | |   | (___) || |   | || |   ) || (_____ 
		| |   | |(_____  )|  __)   |     __)  | |(_)| ||  __)      | |   |  ___  || |   | || |   | |(_____  )
		| |   | |      ) || (      | (\ (     | |   | || (         | |   | (   ) || |   | || |   ) |      ) |
		| (___) |/\____) || (____/\| ) \ \__  | )   ( || (____/\   | |   | )   ( || (___) || (__/  )/\____) |
		(_______)\_______)(_______/|/   \__/  |/     \|(_______/   )_(   |/     \|(_______)(______/ \_______)
                                                                                  
	 */
	
	/**
	 *  Updates the login password for the user.
	 * 
	 *  @param  String 	$currentPassword 	The user's current password.
	 *  @param  String 	$newPassword     	The new password to be set.
	 *  @param  String 	$repeatPassword  	The new password to be set.
	 *  @return boolean 					Whether the password change was successful.
	 */
	public function updateUserPassword($currentPassword, $newPassword, $repeatPassword)
	{
		if (!Login::authenticate($this->getUserID(), $currentPassword))
		{
			$this->addError('password', 'Incorrect password.');
			return false;
		}
		else if ($newPassword != $repeatPassword)
		{
			$this->addError('password', 'Passwords do not match.');
			return false;
		}
		else
		{
			User::updateUserLogin($this->getUserEmailAddress(), $newPassword);
			return true;
		}
	}

	/**
	 *  Gets all of the user's entities in the specified economy.
	 * 
	 *  @param  String  $economyID   Represents the economy ID.
	 *  @return array<Entity>
	 */
	public function getUserEntitiesInEconomy($economyID)
	{
		return Entity::model()->user($this->getUserID())->economy($economyID)->findAll();
	}

	/**
	 *  Sets the current economy. The user must have an entity in an
	 *  economy for it to be selected.
	 * 
	 *  @param  int     $economyID   The economy ID to select.
	 *  @return boolean              Whether the economy was selected.
	 */
	public function selectEconomy($economyID)
	{
		if (Entity::model()->user($this->getUserID())->economy($economyID)->count() > 0)
		{
			Yii::app()->user->economyID = $economyID;
			return true;
		}
		else
		{
			return false;
		}
	}

	/**
	 *  Sets the user's current role in the economy as the entity
	 *  represented by a given entity ID or a given entity type. Selecting
	 *  by entity type searches for the first available entity of that type.
	 * 
	 *  @param  int  $mixed    The entity ID or the entity type to select as a role.
	 *  @return boolean        Whether the desired role was selected.
	 */
	public function selectRole($mixed)
	{
		if (Yii::app()->user->economyID == UserIdentity::NULL_STATE_VALUE)
		{
			return false;
		}

		$entityList;
		$userType = $this->getUserType();

		if ($userType == UserType::USER_TYPE_SUPER_NAME || $userType == UserType::USER_TYPE_ADMIN_NAME)
		{
			$entityList = Entity::model()->economy(Yii::app()->user->economyID)->findAll();
		}
		else
		{
			$entityList = $this->getUserEntitiesInEconomy(Yii::app()->user->economyID);
		}

		// Search for a matching entity type or a matching user entity ID.
		for ($i = 0; $i < sizeof($entityList); $i++)
		{
			if ($mixed === $entityList[$i]->getEntityType() || 
				$mixed === $entityList[$i]->getEntityID())
			{
				$entityList[$i]->updateSimulation();
				Yii::app()->user->roleID = $entityList[$i]->getEntityID();
				Yii::app()->user->roleType = $entityList[$i]->getEntityType();
				return true;
			}
		}

		// Initialize user role ID and role type to the first available entity.
		$entityList[0]->updateSimulation();
		Yii::app()->user->roleID = $entityList[0]->getEntityID();
		Yii::app()->user->roleType = $entityList[0]->getEntityType();

		return false;
	}

	/**
	 *  Gets the list of entities that are associated with the user.
	 * 
	 *  @return array<Entity>
	 */
	public function getEntityList()
	{
		return Entity::getEntitiesByUserID($this->getUserID());
	}

	/**
	 *	Gets the list of economies that are associated with the user.
	 *
	 *  @return array<Economy>
	 */
	public function getEconomyList()
	{
		return Economy::model()->user($this->getUserID())->findAll();
	}

	public function getUserID()
	{
		return $this->user_id;
	}

	public function getUserEmailAddress()
	{
		return $this->email_address;
	}

	public function getUserFirstName()
	{
		return $this->first_name;
	}

	public function getUserLastName()
	{
		return $this->last_name;
	}

	public function getUserCellPhone()
	{
		return $this->cell_phone;
	}

	public function getUserType()
	{
		return $this->userType->user_type;
	}
}
