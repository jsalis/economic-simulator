<?php

Yii::import('application.models._base.BaseEntityHasRule');

/**
 * The EntityHasRule class is there to represent every bit of detail when it comes to 
 * the relation of the Entities and Rules. This relation only goes as deep as both the
 * primary keys of the Entity and Rule. So the only methods for this class are report
 * methods that get either all the Rule ids for the entity or vice versa.
 * 
 * @author   <cmicklis@stetson.edu>, <jsalis@stetson.edu>
 * @since 	 v0.0.0
 * 
 */

class EntityHasRule extends BaseEntityHasRule
{
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * 	Creates a new rule associated with the entity to the EntityHasRule table. 
	 * 	This allows the Entity to have a relation to one or more rules.
	 * 	
	 *  @param int      $entityID 	Represents the entity ID.
	 *  @param int      $ruleID 	Represents the rule ID.
	 *  @return EntityHasRule 		The new EntityHasRule object.
	 */
	public static function createEntityHasRule($entityID, $ruleID)
	{
		$entityHasRule = new EntityHasRule;
		$entityHasRule->entity_id = $entityID;
		$entityHasRule->rule_id = $ruleID;
		$entityHasRule->save();

		return $entityHasRule;
	}
}
