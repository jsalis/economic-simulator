<?php

Yii::import('application.models._base.BaseMarketItem');

/**
 * The MarketItem class is there to represent all of the items that have
 * been created by a worker that are either for sale or have already been
 * purchased. This shows the amount of work that has been produced overtime.
 * Items in this Market are then also purchasable from other economies.
 * 
 * @author   <cmicklis@stetson.edu>, <jsalis@stetson.edu>
 * @since 	 v0.0.0
 */

class MarketItem extends BaseMarketItem
{
	const MARKET_ITEM_PURCHASED = 1;
	const MARKET_ITEM_NOT_PURCHASED = 0;

	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * 	Creates a new market item.
	 *
	 * 	@param  int   $economyID       The ID of the economy that the item is located in.
	 * 	@param  int   $itemID          The ID of the item that the market item is to represent.
	 * 	@param  int   $entityMakerID   The ID of the entity that was commisioned to create the item.
	 *  @return int 		           Returns the ID of the new market item.
	 */
	public static function createMarketItem($economyID, $itemID, $entityMakerID)
	{
		$marketItem = new MarketItem;
		$marketItem->economy_id = $economyID;
		$marketItem->item_id = $itemID;
		$marketItem->entity_maker_id = $entityMakerID;
		$marketItem->creation_date = DateUtility::getCurrentDate(Yii::app()->user->economyID);
		$marketItem->purchased = MarketItem::MARKET_ITEM_NOT_PURCHASED;
		$marketItem->save();

		return $marketItem->market_item_id;
	}

	/**
	 * Sets a specified market item as purchased.
	 * 
	 * @param int 	$id  	The ID of the market item.
	 */
	public static function purchaseMarketItem($id)
	{
		MarketItem::model()->updateByPK($id, array(
			'purchased' => MarketItem::MARKET_ITEM_PURCHASED
		));
	}

	/**
		 _______  _______  _______  _        _______ _________  __________________ _______  _______    _______  _______  _______  _______  _______ _________ _______ 
		(       )(  ___  )(  ____ )| \    /\(  ____ \\__   __/  \__   __/\__   __/(  ____ \(       )  (  ____ )(  ____ \(  ____ )(  ___  )(  ____ )\__   __/(  ____ \
		| () () || (   ) || (    )||  \  / /| (    \/   ) (        ) (      ) (   | (    \/| () () |  | (    )|| (    \/| (    )|| (   ) || (    )|   ) (   | (    \/
		| || || || (___) || (____)||  (_/ / | (__       | |        | |      | |   | (__    | || || |  | (____)|| (__    | (____)|| |   | || (____)|   | |   | (_____ 
		| |(_)| ||  ___  ||     __)|   _ (  |  __)      | |        | |      | |   |  __)   | |(_)| |  |     __)|  __)   |  _____)| |   | ||     __)   | |   (_____  )
		| |   | || (   ) || (\ (   |  ( \ \ | (         | |        | |      | |   | (      | |   | |  | (\ (   | (      | (      | |   | || (\ (      | |         ) |
		| )   ( || )   ( || ) \ \__|  /  \ \| (____/\   | |     ___) (___   | |   | (____/\| )   ( |  | ) \ \__| (____/\| )      | (___) || ) \ \__   | |   /\____) |
		|/     \||/     \||/   \__/|_/    \/(_______/   )_(     \_______/   )_(   (_______/|/     \|  |/   \__/(_______/|/       (_______)|/   \__/   )_(   \_______)
		                                                                                                                                                             
	 */
	
	public function scopes()
    {
    	$yesterdayDate = DateUtility::getResetYesterday(Yii::app()->user->economyID);
        return array(
        	'beforeToday' => array(
                'condition' => 't.creation_date < :yesterdayDate',
                'params' => array(':yesterdayDate' => $yesterdayDate),
            ),
        	'today' => array(
                'condition' => 't.creation_date >= :yesterdayDate',
                'params' => array(':yesterdayDate' => $yesterdayDate),
            ),
            'purchased' => array(
                'condition' => 'purchased = 1',
            ),
            'available' => array(
                'condition' => 'purchased = 0',
            ),
        );
    }

    public function item($itemID)
	{
	    $this->getDbCriteria()->mergeWith(array(
	        'condition' => 'item_id = :itemID',
            'params' => array(':itemID' => $itemID),
	    ));
	    return $this;
	}

	public function itemType($type)
	{
	    $this->getDbCriteria()->mergeWith(array(
	    	'with' => array('item.itemType' => array('select' => false)),
	        'condition' => 'itemType.item_type = :type',
            'params' => array(':type' => $type),
	    ));
	    return $this;
	}

	public function upperDate($upperDate)
	{
	    $this->getDbCriteria()->mergeWith(array(
        	'condition' => 't.creation_date < :upperDate',
        	'params' => array(':upperDate' => $upperDate),
        ));
	    return $this;
	}

	public function lowerDate($lowerDate)
	{
	    $this->getDbCriteria()->mergeWith(array(
        	'condition' => 't.creation_date >= :lowerDate',
        	'params' => array(':lowerDate' => $lowerDate),
        ));
	    return $this;
	}

	public function economy($economyID)
	{
	    $this->getDbCriteria()->mergeWith(array(
	        'condition' => 't.economy_id = :economyID',
            'params' => array(':economyID' => $economyID),
	    ));
	    return $this;
	}

	/**
		 _______  _______  _______  _        _______ _________  __________________ _______  _______    _______  _______ _________          _______  ______   _______ 
		(       )(  ___  )(  ____ )| \    /\(  ____ \\__   __/  \__   __/\__   __/(  ____ \(       )  (       )(  ____ \\__   __/|\     /|(  ___  )(  __  \ (  ____ \
		| () () || (   ) || (    )||  \  / /| (    \/   ) (        ) (      ) (   | (    \/| () () |  | () () || (    \/   ) (   | )   ( || (   ) || (  \  )| (    \/
		| || || || (___) || (____)||  (_/ / | (__       | |        | |      | |   | (__    | || || |  | || || || (__       | |   | (___) || |   | || |   ) || (_____ 
		| |(_)| ||  ___  ||     __)|   _ (  |  __)      | |        | |      | |   |  __)   | |(_)| |  | |(_)| ||  __)      | |   |  ___  || |   | || |   | |(_____  )
		| |   | || (   ) || (\ (   |  ( \ \ | (         | |        | |      | |   | (      | |   | |  | |   | || (         | |   | (   ) || |   | || |   ) |      ) |
		| )   ( || )   ( || ) \ \__|  /  \ \| (____/\   | |     ___) (___   | |   | (____/\| )   ( |  | )   ( || (____/\   | |   | )   ( || (___) || (__/  )/\____) |
		|/     \||/     \||/   \__/|_/    \/(_______/   )_(     \_______/   )_(   (_______/|/     \|  |/     \|(_______/   )_(   |/     \|(_______)(______/ \_______)
		                                                                                                                                                             
	 */
	
	public function getMarketItemID()
	{
		return $this->market_item_id;
	}

	public function getEconomyID()
	{
		return $this->economy_id;
	}

	public function getItemID()
	{
		return $this->item_id;
	}

	public function getEntityMakerID()
	{
		return $this->entity_maker_id;
	}

	public function getCreationDate()
	{
		return $this->creation_date;
	}

	public function getPurchased()
	{
		return $this->purchased;
	}
}
