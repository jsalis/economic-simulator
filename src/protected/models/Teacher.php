<?php

/**
 * The Teacher class is there to define the funcitonality of the
 * teacher user, and also then defines the entity that the Teacher
 * has (Accessor). The Teacher user has access to everything that
 * has to do with her class (Economy), so that she can add students
 * to her class, check trends as to how the entire class is doing, and 
 * anything else that includes controlling her students in her own class,
 * as well as what classes are related to her own class.
 *
 * @author   <cmicklis@stetson.edu>, <jsalis@stetson.edu>
 * @since 	 v0.0.0
 */
class Teacher extends User
{
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	public function defaultScope()
	{
		// only read records that are of user type Teacher
		return array(
			'with' => array('userType' => array('select' => false)),
			'condition' => 'user_type=:type',
			'params' => array(':type' => UserType::USER_TYPE_TEACHER_NAME),
		);
    }

	/**
	 * 	Gets all students associated with the specified economy.
	 * 
	 *  @return array<Student> 		An array of student users.
	 */
	public function getStudentsInEconomy($economyID)
	{
		return Student::model()->economy($economyID)->findAll();
	}
	
	/**
	 * Adds a student user to an economy by creating a new worker and adding that
	 * worker to the economy.
	 * 
	 * @param int       $emailAddress  The student email address.
	 * @param int       $economyID     The ID of the economy to add the user to.
	 * @return boolean                 Whether the user was added successfully.
	 */
	public function addStudentToEconomy($emailAddress, $economyID)
	{
		$userID = User::getUserIDByEmailAddress($emailAddress);

		if (is_null($userID))
		{
			$this->addError('emailAddress', 'No students found with this email address.');
			return false;
		}

		$user = Student::model()->findByPk($userID);
		$entityCount = Entity::model()->user($userID)->economy($economyID)->count();

		if (is_null($user) || $entityCount != 0)
		{
			$this->addError('emailAddress', 'No students found with this email address.');
			return false;
		}

		// Create a worker for the student.
		$entityName = $user->getUserFirstName() . ' ' . $user->getUserLastName();
		$entityID = Entity::createEntity($entityName, EntityType::ENTITY_TYPE_WORKER_NAME, $economyID);

		User::createUserHasEntity($userID, $entityID);
		Economy::createEconomyHasEntity($economyID, $entityID);
		return true;
	}

	/**
	 * 	Removes a student user from the economy by removing all entities associated 
	 * 	with that user.
	 * 	
	 *  @param int      $userID       Represents the entity ID that is her student.
	 *  @param int      $economyID    Represents the economy ID.
	 */
	public function removeStudentFromEconomy($entityID, $economyID)
	{

	}

	/**
	 * 	Creates an economy relation for students to purchase good from
	 * 	one economy and also set specific rules associated with them
	 * 	for taxation and other options.
	 * 
	 *  @param array    $relationList   An array where the key represents an economy ID
	 *                                  and the value is a boolean representing the 
	 *                                  the existence of the relation.
	 *  @param int      $economyID      Represents the economy ID.
	 */
	public function updateEconomyRelations($relationList, $economyID)
	{
		foreach ($relationList as $id => $hasRelation)
		{
			$relationExists = EconomyRelation::hasRelation($economyID, $id);
			if ($hasRelation && !$relationExists)
			{
				EconomyRelation::createEconomyRelation($economyID, $id);
			}
			else if (!$hasRelation && $relationExists)
			{
				EconomyRelation::removeEconomyRelation($economyID, $id);
			}
		}
	}

	/**
	 *	The method creates an economy with the parameters given. A teacher should have
	 *	an accessor entity in order for an economy to be created.
	 *	
	 *  @param String   $economyName         Represents the economy name.
	 *  @param String   $economyDescription  Represents the economy description.
	 *  @return boolean                      Whether economy creation was successful.
	 */
	public function createEconomy($economyName, $economyDescription)
	{
		$accessorID = Accessor::model()->user($this->getUserID())->find()->getEntityID();
		$economyID = Economy::createEconomy($economyName, $economyDescription);
		Economy::createEconomyHasEntity($economyID, $accessorID);
	}

	/**
	 * 	Updates the name and description of an existing economy.
	 *
	 *  @param int      $economyID           Represents the economy ID.
	 *  @param String   $economyName         Represents the economy's inputted name.
	 *  @param String   $economyDescription  Represents the economy's inputted description.
	 */
	public function updateEconomy($economyID, $economyName, $economyDescription)
	{
		Economy::updateEconomy($economyID, $economyName, $economyDescription);
	}

	/**
	
		Data Reports

	 */

	/**
	 * 	The getEnteredEconomyList method goes and gets all of the requested information
	 * 	for the users who have entered the economy, usually this information is the time
	 * 	that the user entered the economy and the users name.
	 * 	
	 *  @return array<array>    			Represents all the information of the users and when
	 *                                      they logged into the economy.
	 */
	public function getEnteredEconomyList()
	{
		$messageList = EntityHasMessage::model()
			->messageType( MessageType::MESSAGE_TYPE_ENTERED_ECONOMY )
			->receiver( Economy::getPublicMessengerID(Yii::app()->user->economyID) )
			->findAll();

		for ($i = 0; $i < sizeof($messageList); $i++)
		{
			$name = $messageList[$i]->entitySender->getEntityName();
			$date = $messageList[$i]->getDateReceived()->format('Y-m-d H:i:s');

			$messageList[$i] = array(
				'user_name' => $name,
				'date_entered' => $date,
			);
		}
		return $messageList;
	}

	/**
	 * 	The getEnteredLaborMarketList method goes and gets all of the requested information
	 * 	for the users whom have entered the labor market, usually this information is the time
	 * 	that the user entered the economy and the users name.
	 * 	
	 *  @return array<array>    			Represents all the information of the users and when
	 *                                      they logged into the labor market.
	 */
	public function getEnteredLaborMarketList()
	{
		$messageList = EntityHasMessage::model()
			->messageType( MessageType::MESSAGE_TYPE_ENTERED_LABOR_MARKET )
			->receiver( Economy::getPublicMessengerID(Yii::app()->user->economyID) )
			->findAll();

		for ($i = 0; $i < sizeof($messageList); $i++)
		{
			$name = $messageList[$i]->entitySender->getEntityName();
			$date = $messageList[$i]->getDateReceived()->format('Y-m-d H:i:s');

			$messageList[$i] = array(
				'user_name' => $name,
				'date_entered' => $date,
			);
		}
		return $messageList;
	}

	/**
	 * 	The getEntitiesWithJobs method goes and gets all of the entities with jobs in the
	 * 	economy. To get this information it goes and gets all of the mesages with the type 
	 * 	MESSAGE_TYPE_ACCEPT_JOB_OFFER_NAME that are sent to the economy entity. Each of these
	 * 	messages contains all of the necessary information of entities having jobs including
	 * 	the workers name, the price that they were given, the units that they produced, and the
	 * 	date in which the worker did their work. The worker depends on the entity type, if the 
	 * 	sender is the worker then he is the one whom produced the items, else its the receiver
	 * 	whom is the worker that produced the items. The entity is then associated by the economy
	 * 	by getting all of the workers that are in the economy and their messages.
	 * 	
	 * 	The list is then gone through to get all of messages of this type and run through the 
	 * 	array to convert each object in the array into another array that holds all of the 
	 * 	necessary information to then be returned.
	 *
	 *  @return array<array>    			Represents all the information of the users and when
	 *                                      they worked and how much they did for that job.
	 */
	public function getWorkersWithJobs()
	{
		$workerList = Worker::model()->economy(Yii::app()->user->economyID)->findAll();
		$jobMessageList = array();

		for ($i = 0; $i < sizeof($workerList); $i++) // create new scope ??
		{
			$workerID = $workerList[$i]->getEntityID();

			$tempMessageList = EntityHasMessage::model()
				->messageType( MessageType::MESSAGE_TYPE_ACCEPT_JOB_OFFER_NAME )
				->senderOrReceiver( $workerID, $workerID )
				->findAll();

			for ($j = 0; $j < sizeof($tempMessageList); $j++)
			{
				$jobMessageList[] = $tempMessageList[$j];
			}
		}

		for ($i = 0; $i < sizeof($jobMessageList); $i++)
		{
			$entityName = '';
			$sender = $jobMessageList[$i]->entitySender;
			$receiver = $jobMessageList[$i]->entityReceiver;

			if ($sender->getEntityType() == EntityType::ENTITY_TYPE_WORKER_NAME)
			{
				$entityName = $sender->getEntityName();
			}
			else
			{
				$entityName = $receiver->getEntityName();
			}

			$message = $jobMessageList[$i]->getMessage();
			$payment = $message->findByKey(Message::MESSAGE_KEY_MONEY);
			$unitsProduced = $message->findByKey(Message::MESSAGE_KEY_UNITS);
			$dateEntered = $jobMessageList[$i]->getDateReceived()->format('Y-m-d H:i:s');

			$jobMessageList[$i] = array(
				'worker_name' => $entityName,
				'payment' => $payment,
				'units_produced' => $unitsProduced,
				'date_entered' => $dateEntered,
			);
		}
		return $jobMessageList;
	}

	/**
	 *	The getUnfilledJobOffers method goes and gets all of the job offers that have not been
	 *	accepted. These messages are sent to the economy under the type MESSAGE_TYPE_SEND_JOB_OFFER_NAME
	 *	and when a job offer has been accepted then the message will be replied and so not be availble
	 *	for anyone else to respond to. The list will contain all important information pertaining to the
	 *	job offer including the company that created that job offer, the price that was offered, the name
	 *	of the item that the company chose to produce, and the date when it was entered.
	 * 
	 *  @return array<array>    			Represents all the information of the job offers and when
	 *                                      they were sent out to the economy.
	 */
	public function getUnfilledJobOffers()
	{
		$jobMessageList = EntityHasMessage::model()
			->messageType( MessageType::MESSAGE_TYPE_SEND_JOB_OFFER_NAME )
			->receiver( Economy::getPublicMessengerID(Yii::app()->user->economyID) )
			->replied( false )
			->findAll();

		for ($i = 0; $i < sizeof($jobMessageList); $i++)
		{
			$message = $jobMessageList[$i]->getMessage();
			$price = $message->findByKey(Message::MESSAGE_KEY_MONEY);
			$itemID = $message->findByKey(Message::MESSAGE_KEY_ITEM_ID);
			$dateEntered = $jobMessageList[$i]->getDateReceived()->format('Y-m-d H:i:s');
			$senderName = $jobMessageList[$i]->entitySender->getEntityName();

			$jobMessageList[$i] = array(
				'job_offer' => $senderName,
				'price' => $price,
				'item_name' => Item::getItemNameByID($itemID),
				'date_entered' => $dateEntered,
			);
		}
		return $jobMessageList;
	}

	/**
	 * 	The getWillingnessToPay method goes and gets all of the MESSAGE_TYPE_PRODUCT_DEMAND_SURVEY
	 * 	messages that were sent to the economy and then returns them with all of the information that
	 * 	the message contains. Each of these messages are then converted to an array containing the 
	 * 	name of who responded to the survey, the price that they stated the would pay, the item type,
	 * 	the item count that they would pay for, and the date that the survey was responded to.
	 * 	
	 *  @return array<array>    			Represents all the information of the surveys that were
	 *                                      taken in the economy product market.
	 */
	public function getWillingnessToPay()
	{
		$surveyMessageList = EntityHasMessage::model()
			->messageType( MessageType::MESSAGE_TYPE_PRODUCT_DEMAND_SURVEY )
			->receiver( Economy::getPublicMessengerID(Yii::app()->user->economyID) )
			->findAll();

		for ($i = 0; $i < sizeof($surveyMessageList); $i++)
		{
			$message = $surveyMessageList[$i]->getMessage();
			$price = $message->findByKey(Message::MESSAGE_KEY_MONEY);
			$itemType = $message->findByKey(Message::MESSAGE_KEY_ITEM_TYPE);
			$order = $message->findByKey(Message::MESSAGE_KEY_UNITS);
			$dateEntered = $surveyMessageList[$i]->getDateReceived()->format('Y-m-d H:i:s');
			$senderID = $surveyMessageList[$i]->getEntitySenderID();

			$surveyMessageList[$i] = array(
				'entity_name' => Entity::getEntityNameByID($senderID),
				'price' => $price,
				'item_type' => $itemType,
				'order' => $order,
				'date_entered' => $dateEntered,
			);
		}
		return $surveyMessageList;
	}

	/**
	 * 	The getItemAndProduction method goes and gets all of the Items in the economy and gets
	 * 	the name, type, price, stock, and stock in the economy. All this information is stored
	 * 	in an array and then returned to the user.
	 * 	
	 *  @return array<array>    			Represents all the information of the items in relation,
	 *                                      to the baic information and the value that they hold in 
	 *                                      the economy. 
	 */
	public function getItemAndProduction()
	{
		$itemList = Economy::getItemsInEconomy(Yii::app()->user->economyID);

		for ($i = 0; $i < sizeof($itemList); $i++)
		{
			$replace = array(
				'item_name' => $itemList[$i]->getItemName(),
				'item_type' => $itemList[$i]->getItemType(),
				'price' => $itemList[$i]->getPrice(),
				'stock' => $itemList[$i]->getItemStock(),
				'stock_in_economy' => $itemList[$i]->getItemStockInEconomy(Yii::app()->user->economyID)
			);
			$itemList[$i] = $replace;
		}
		return $itemList;
	}

	public function getEntitiesInEconomy()
	{
		$entityList = Economy::getEntitiesInEconomy(Yii::app()->user->economyID);

		for ($i = 0; $i < sizeof($entityList); $i++)
		{
			$name = $entityList[$i]->getEntityName();
			$type = $entityList[$i]->getEntityType();
			$balance = $entityList[$i]->getEntityBalance();

			$entityList[$i] = array(
				'entity_name' => $name,
				'entity_type' => $type,
				'balance' => $balance,
			);
		}
		return $entityList;
	}

	public function getPurchasedMarketItems()
	{
		$entityList = Entity::model()->economy(Yii::app()->user->economyID)->findAll();
		$itemData = array();

		foreach ($entityList as $entity)
		{
			foreach ($entity->getEntityHasItems() as $hasItem)
			{
				$datePurchased = $hasItem->getDatePurchased();

				$itemData[] = array(
					'item_name' => $hasItem->getItem()->getItemName(),
					'item_type' => $hasItem->getItem()->getItemType(),
					'date_purchased' => $datePurchased->format('Y-m-d H:i:s'),
					'price_paid' => $hasItem->getPricePaid(),
					'owner' => $entity->getEntityName(),
				);
			}
		}
		return $itemData;
	}

	public function getAvailableMarketItems()
	{
		$itemList = MarketItem::model()->economy(Yii::app()->user->economyID)->available()->findAll();
		$itemData = array();

		for ($i = 0; $i < sizeof($itemList); $i++)
		{
			$itemData[] = array(
				'item_name' => $itemList[$i]->item->getItemName(),
				'item_type' => $itemList[$i]->item->getItemType(),
				'date_produced' => $itemList[$i]->getCreationDate(),
			);
		}
		return $itemData;
	}

	public function getItemsInEconomy()
	{
		$itemList = Economy::getItemsInEconomy(Yii::app()->user->economyID);

		for ($i = 0; $i < sizeof($itemList); $i++)
		{
			$name = $itemList[$i]->getItemName();
			$type = $itemList[$i]->getItemType();

			$itemList[$i] = array(
				'item_name' => $name,
				'item_type' => $type,
			);
		}
		return $itemList;
	}

	public function getDailyData()
	{
		$data = array();

		$creationDate = Economy::model()->findByPk(Yii::app()->user->economyID)->creation_date;
		$lowerDate = DateTime::createFromFormat('Y-m-d H:i:s', $creationDate);
		$resetHour = Economy::getEconomyReset(Yii::app()->user->economyID);
		$lowerDate->setTime($resetHour, 0, 0);

		$today = new DateTime('now');

		$jobList = EntityHasMessage::model()
			->messageType(MessageType::MESSAGE_TYPE_ACCEPT_JOB_OFFER_NAME)
			->economy(Yii::app()->user->economyID)
			->findAll();

		while ($lowerDate < $today)
		{
			$upperDate = clone $lowerDate;
			$upperDate->modify('+1 day');

			$lowerDateStr = $lowerDate->format('Y-m-d H:i:s');
			$upperDateStr = $upperDate->format('Y-m-d H:i:s');

			$totalWages = 0;
			$jobCount = 0;

			foreach ($jobList as $job)
			{
				if ($job->getDateReceived() > $lowerDate && $job->getDateReceived() < $upperDate)
				{
					$jobCount ++;
					$totalWages += $job->getMessage()->findByKey(Message::MESSAGE_KEY_MONEY);
				}
			}

			$enteredLaborMarketCount = EntityHasMessage::model()
				->messageType(MessageType::MESSAGE_TYPE_ENTERED_LABOR_MARKET)
				->economy(Yii::app()->user->economyID)
				->lowerDate($lowerDateStr)
				->upperDate($upperDateStr)
				->count();

			// Calculate food revenue
			$hasFoodItems = EntityHasItem::model()
				->economy(Yii::app()->user->economyID)
				->itemType(ItemType::ITEM_TYPE_FOOD_NAME)
				->lowerDate($lowerDateStr)
				->upperDate($upperDateStr)
				->findAll(array('select' => 'price_paid'));

			$foodRevenue = 0;

			foreach ($hasFoodItems as $hasItem)
			{
				$foodRevenue += $hasItem->price_paid;
			}

			// Calculate non-food revenue
			$hasNonFoodItems = EntityHasItem::model()
				->economy(Yii::app()->user->economyID)
				->itemType(ItemType::ITEM_TYPE_NONFOOD_NAME)
				->lowerDate($lowerDateStr)
				->upperDate($upperDateStr)
				->findAll(array('select' => 'price_paid'));

			$nonFoodRevenue = 0;

			foreach ($hasNonFoodItems as $hasItem)
			{
				$nonFoodRevenue += $hasItem->price_paid;
			}

			$foodProduced = MarketItem::model()
				->economy(Yii::app()->user->economyID)
				->itemType(ItemType::ITEM_TYPE_FOOD_NAME)
				->lowerDate($lowerDateStr)
				->upperDate($upperDateStr)
				->count();

			$foodSold = EntityHasItem::model()
				->economy(Yii::app()->user->economyID)
				->itemType(ItemType::ITEM_TYPE_FOOD_NAME)
				->lowerDate($lowerDateStr)
				->upperDate($upperDateStr)
				->count();

			$nonFoodProduced = MarketItem::model()
				->economy(Yii::app()->user->economyID)
				->itemType(ItemType::ITEM_TYPE_NONFOOD_NAME)
				->lowerDate($lowerDateStr)
				->upperDate($upperDateStr)
				->count();

			$nonFoodSold = EntityHasItem::model()
				->economy(Yii::app()->user->economyID)
				->itemType(ItemType::ITEM_TYPE_NONFOOD_NAME)
				->lowerDate($lowerDateStr)
				->upperDate($upperDateStr)
				->count();

			$data[] = array(
				'Date' => $lowerDate->format('Y-m-d'),
				'Workers Entered Labor Market' => $enteredLaborMarketCount,
				'Workers Hired' => $jobCount,
				'Total Wages' => $totalWages,
				'Food Produced' => $foodProduced,
				'Food Sold' => $foodSold,
				'Food Revenue' => $foodRevenue / Economy::CENTS_PER_DOLLAR,
				'Non-Food Produced' => $nonFoodProduced,
				'Non-Food Sold' => $nonFoodSold,
				'Non-Food Revenue' => $nonFoodRevenue / Economy::CENTS_PER_DOLLAR,
			);

			$lowerDate = clone $upperDate;
		}

		return $data;
	}
}
