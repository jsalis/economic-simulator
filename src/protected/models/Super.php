<?php

/**
 * The Super class is there to represent the functionality that the super user
 * user should have.
 *
 * @author   <jsalis@stetson.edu>
 * @since 	 v0.0.0
 */
class Super extends Admin
{
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	public function defaultScope()
	{
		// only read records that are of user type Super
		return array(
			'with' => array('userType' => array('select' => false)),
			'condition' => 'user_type=:type',
			'params' => array(':type' => UserType::USER_TYPE_SUPER_NAME),
		);
    }
}
