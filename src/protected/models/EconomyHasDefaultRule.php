<?php

Yii::import('application.models._base.BaseEconomyHasDefaultRule');

/**
 * The EconomyHasDefaultRule class is there to represent every bit of detail when it comes to 
 * the relation of the Economies and Rules. This relation only goes as deep as both the
 * primary keys of the Economy and Rule. So the only methods for this class are report
 * methods that get either all the Rule ids for the economy or vice versa.
 * 
 * @author   <cmicklis@stetson.edu>, <jsalis@stetson.edu>
 * @since 	 v0.0.0
 * 
 */

class EconomyHasDefaultRule extends BaseEconomyHasDefaultRule
{
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}
}
