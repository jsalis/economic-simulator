<?php

Yii::import('application.models._base.BaseRule');

/**
 * The Rule class is there to represent everybit of detail that a Rule may have
 * from the name, description, and if that rule is activated. The Class is setup so that the
 * Worker, Accessor, Company, Goverment, or Bank can easily access all of the information
 * needed. The RuleForm also exists to work as a ReportForm for all things an modifier will have.
 *
 * @author   <cmicklis@stetson.edu>, <jsalis@stetson.edu>
 * @since 	 v0.0.0
 */

class Rule extends BaseRule
{
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * 	Creates a new rule using all information given into the rule table.
	 * 	
	 *  @param  String   $ruleName 			Represents the rule name.
	 *  @param  String   $ruleValue  		Represents the rule value.
	 *  @param  String   $ruleAttribute 	Represents the rule attribute.
	 *  @param  String   $ruleType  		Represents the type of rule.
	 *  @param  boolean  $ruleActivated 	Represents if the rule is active or not.
	 *  @return Rule 						Represents the new rule.                             
	 */
	public static function createRule($ruleName, $ruleValue, $ruleAttribute, $ruleType, $ruleActivated)
	{
		$rule = new Rule;
		$rule->rule_type_id = RuleType::getRuleTypeIDByRuleType($ruleType);
		$rule->rule_name = $ruleName;
		$rule->rule_value = $ruleValue;
		$rule->rule_attribute = $ruleAttribute;
		$rule->rule_activated = $ruleActivated;
		$rule->save();

		return $rule;
	}

	/**
	 * Creates a new rule by copying the fields of an existing rule.
	 * 
	 * @param  Rule 	$rule 			The rule to copy fields from.
	 * @return Rule 					The new rule.  
	 */
	public static function copyRule($rule)
	{
		$ruleCopy = new Rule;
		$ruleCopy->rule_type_id = $rule->ruleType->rule_type_id;
		$ruleCopy->rule_name = $rule->getRuleName();
		$ruleCopy->rule_value = $rule->getRuleValue();
		$ruleCopy->rule_attribute = $rule->getRuleAttribute();
		$ruleCopy->rule_activated = $rule->isRuleActivated();
		$ruleCopy->save();

		return $ruleCopy;
	}

	/**
	 * Updates the value associated with a specific rule.
	 * 
	 * @param  int      $ruleID    		The ID of the rule.
	 * @param  String   $ruleValue 		The new value for the rule.
	 */
	public static function updateRuleValue($ruleID, $ruleValue)
	{
		Rule::model()->updateByPk($ruleID, array(
			'rule_value' => $ruleValue,
		));
	}

	/**
	 * Updates the attribute associated with a specific rule.
	 * 
	 * @param  int      $ruleID    		The ID of the rule.
	 * @param  String   $ruleAttribute 	The new attribute for the rule.
	 */
	public static function updateRuleAttribute($ruleID, $ruleAttribute)
	{
		Rule::model()->updateByPk($ruleID, array(
			'rule_attribute' => $ruleAttribute,
		));
	}

	/**
		 _______           _        _______    _______  _______  _______  _______  _______ _________ _______ 
		(  ____ )|\     /|( \      (  ____ \  (  ____ )(  ____ \(  ____ )(  ___  )(  ____ )\__   __/(  ____ \
		| (    )|| )   ( || (      | (    \/  | (    )|| (    \/| (    )|| (   ) || (    )|   ) (   | (    \/
		| (____)|| |   | || |      | (__      | (____)|| (__    | (____)|| |   | || (____)|   | |   | (_____ 
		|     __)| |   | || |      |  __)     |     __)|  __)   |  _____)| |   | ||     __)   | |   (_____  )
		| (\ (   | |   | || |      | (        | (\ (   | (      | (      | |   | || (\ (      | |         ) |
		| ) \ \__| (___) || (____/\| (____/\  | ) \ \__| (____/\| )      | (___) || ) \ \__   | |   /\____) |
		|/   \__/(_______)(_______/(_______/  |/   \__/(_______/|/       (_______)|/   \__/   )_(   \_______)
		                                                                                                     
	 */
	
	/**
	 * [getEconomyRules description]
	 * 
	 * @param  [type] $economyID [description]
	 * @return [type]            [description]
	 */
	public static function getEconomyRules($economyID)
	{
		$economyTable = ($economyID == Economy::ECONOMY_DEFAULT_ID) ? 'economyHasDefaultRules' : 'economyHasRules';

		return Rule::model()->findAll(array(
			'with' => array(
				$economyTable => array('select' => false),
			),
			'condition' => 'economy_id=:economyID',
			'params' => array(':economyID' => $economyID),
		));
	}

	/**
	 * [findEconomyRule description]
	 * 
	 * @param  [type] $economyID     [description]
	 * @param  [type] $ruleType      [description]
	 * @param  [type] $ruleAttribute [description]
	 * @return [type]                [description]
	 */
	public static function findEconomyRule($economyID, $ruleType, $ruleAttribute = null)
	{
		$table = ($economyID == Economy::ECONOMY_DEFAULT_ID) ? 'economyHasDefaultRules' : 'economyHasRules';
		$condition = 'economy_id=:economyID';
		$params = array(':economyID' => $economyID);

		return Rule::findRule($table, $condition, $params, $ruleType, $ruleAttribute);
	}
	
	/**
	 * [getEntityTypeRules description]
	 * 
	 * @param  [type] $economyID  [description]
	 * @param  [type] $entityType [description]
	 * @return [type]             [description]
	 */
	public static function getEntityTypeRules($economyID, $entityType = null)
	{
		$table = ($economyID == Economy::ECONOMY_DEFAULT_ID) ? 'entityTypeHasDefaultRules' : 'entityTypeHasRules';
		$condition = 'economy_id=:economyID';
		$params = array(':economyID' => $economyID);

		if (!is_null($entityType))
		{
			$condition .= '&& entity_type=:entityType';
			$params[':entityType'] = $entityType;
		}

		return Rule::model()->findAll(array(
			'with' => array(
				$table => array('with' => 'entityType'),
			),
			'condition' => $condition,
			'params' => $params,
		));
	}

	/**
	 * [getEntityRules description]
	 * 
	 * @param  [type] $entityID [description]
	 * @return [type]           [description]
	 */
	public static function getEntityRules($entityID)
	{
		return Rule::model()->findAll(array(
			'with' => array(
				'entityHasRules' => array('select' => false),
			),
			'condition' => 'entity_id=:entityID',
			'params' => array(':entityID' => $entityID),
		));
	}

	/**
	 * [findEntityRule description]
	 * 
	 * @param  [type] $entityID      [description]
	 * @param  [type] $ruleType      [description]
	 * @param  [type] $ruleAttribute [description]
	 * @return [type]                [description]
	 */
	public static function findEntityRule($entityID, $ruleType, $ruleAttribute = null)
	{
		$table = 'entityHasRules';
		$condition = 'entity_id=:entityID';
		$params = array(':entityID' => $entityID);

		return Rule::findRule($table, $condition, $params, $ruleType, $ruleAttribute);
	}

	/**
	 * [getItemTypeRules description]
	 * 
	 * @param  [type] $economyID [description]
	 * @param  [type] $itemType  [description]
	 * @return [type]            [description]
	 */
	public static function getItemTypeRules($economyID, $itemType = null)
	{
		$table = ($economyID == Economy::ECONOMY_DEFAULT_ID) ? 'itemTypeHasDefaultRules' : 'itemTypeHasRules';
		$condition = 'economy_id=:economyID';
		$params = array(':economyID' => $economyID);

		if (!is_null($itemType))
		{
			$condition .= '&& item_type=:itemType';
			$params[':itemType'] = $itemType;
		}

		return Rule::model()->findAll(array(
			'with' => array(
				$table => array('with' => 'itemType'),
			),
			'condition' => $condition,
			'params' => $params,
		));
	}

	/**
	 * [getItemRules description]
	 * 
	 * @param  [type] $itemID [description]
	 * @return [type]         [description]
	 */
	public static function getItemRules($itemID)
	{
		return Rule::model()->findAll(array(
			'with' => array(
				'itemHasRules' => array('select' => false),
			),
			'condition' => 'item_id=:itemID',
			'params' => array(':itemID' => $itemID),
		));
	}

	/**
	 * [findItemRule description]
	 * 
	 * @param  [type] $itemID        [description]
	 * @param  [type] $ruleType      [description]
	 * @param  [type] $ruleAttribute [description]
	 * @return [type]                [description]
	 */
	public static function findItemRule($itemID, $ruleType, $ruleAttribute = null)
	{
		$table = 'itemHasRules';
		$condition = 'item_id=:itemID';
		$params = array(':itemID' => $itemID);

		return Rule::findRule($table, $condition, $params, $ruleType, $ruleAttribute);
	}

	/**
	 * [getEconomyRelationRules description]
	 * 
	 * @param  [type] $economyRelationID [description]
	 * @param  [type] $economyID         [description]
	 * @return [type]                    [description]
	 */
	public static function getEconomyRelationRules($economyRelationID, $economyID = null)
	{
		if (!is_null($economyID) && $economyID == Economy::ECONOMY_DEFAULT_ID)
		{
			$table = 'economyRelationHasDefaultRules';
		}
		else
		{
			$table = 'economyRelationHasRules';
		}

		return Rule::model()->findAll(array(
			'with' => array(
				$table => array('select' => false),
			),
			'condition' => 'economy_relation_id=:economyRelationID',
			'params' => array(':economyRelationID' => $economyRelationID),
		));
	}

	/**
	 * [findEconomyRelationRule description]
	 * 
	 * @param  [type] $economyRelationID [description]
	 * @param  [type] $ruleType          [description]
	 * @param  [type] $ruleAttribute     [description]
	 * @return [type]                    [description]
	 */
	public static function findEconomyRelationRule($economyRelationID, $ruleType, $ruleAttribute = null)
	{
		$table = 'economyRelationHasRules';
		$condition = 'economy_relation_id=:economyRelationID';
		$params = array(':economyRelationID' => $economyRelationID);

		return Rule::findRule($table, $condition, $params, $ruleType, $ruleAttribute);
	}

	/**
	 * [findRule description]
	 * 
	 * @param  [type] $table         [description]
	 * @param  [type] $condition     [description]
	 * @param  [type] $params        [description]
	 * @param  [type] $ruleType      [description]
	 * @param  [type] $ruleAttribute [description]
	 * @return [type]                [description]
	 */
	public static function findRule($table, $condition, $params, $ruleType, $ruleAttribute = null)
	{
		$condition .= '&& rule_type=:ruleType';
		$params[':ruleType'] = $ruleType;

		if (!is_null($ruleAttribute))
		{
			$condition .= '&& rule_attribute=:ruleAttribute';
			$params[':ruleAttribute'] = $ruleAttribute;
		}

		return Rule::model()->find(array(
			'with' => array(
				'ruleType' => array('select' => false),
				$table => array('select' => false),
			),
			'condition' => $condition,
			'params' => $params,
		));
	}

	/**
		 _______           _        _______    _______  _______ _________          _______  ______   _______ 
		(  ____ )|\     /|( \      (  ____ \  (       )(  ____ \\__   __/|\     /|(  ___  )(  __  \ (  ____ \
		| (    )|| )   ( || (      | (    \/  | () () || (    \/   ) (   | )   ( || (   ) || (  \  )| (    \/
		| (____)|| |   | || |      | (__      | || || || (__       | |   | (___) || |   | || |   ) || (_____ 
		|     __)| |   | || |      |  __)     | |(_)| ||  __)      | |   |  ___  || |   | || |   | |(_____  )
		| (\ (   | |   | || |      | (        | |   | || (         | |   | (   ) || |   | || |   ) |      ) |
		| ) \ \__| (___) || (____/\| (____/\  | )   ( || (____/\   | |   | )   ( || (___) || (__/  )/\____) |
		|/   \__/(_______)(_______/(_______/  |/     \|(_______/   )_(   |/     \|(_______)(______/ \_______)
		                                                                                                     
	 */

	public function getRuleID()
	{
		return $this->rule_id;
	}

	public function getRuleName()
	{
		return $this->rule_name;
	}

	public function getRuleType()
	{
		return $this->ruleType->rule_type;
	}

	public function getRuleValue()
	{
		return $this->rule_value;
	}

	public function getRuleAttribute()
	{
		return $this->rule_attribute;
	}

	public function isRuleActivated()
	{
		return $this->rule_activated;
	}

	public function getEntityTypeID()
	{
		if (sizeof($this->entityTypeHasDefaultRules) > 0)
		{
			return $this->entityTypeHasDefaultRules[0]->entity_type_id;
		}
		else if (sizeof($this->entityTypeHasRules) > 0)
		{
			return $this->entityTypeHasRules[0]->entity_type_id;
		}
		else return null;
	}

	public function getItemTypeID()
	{
		if (sizeof($this->itemTypeHasDefaultRules) > 0)
		{
			return $this->itemTypeHasDefaultRules[0]->item_type_id;
		}
		else if (sizeof($this->itemTypeHasRules) > 0)
		{
			return $this->itemTypeHasRules[0]->item_type_id;
		}
		else return null;
	}
}
