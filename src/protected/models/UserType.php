<?php

Yii::import('application.models._base.BaseUserType');

/**
 * The UserType class is there to represent all of the necessary interction that is
 * needed by the User class to get information about the UserType. It gives methods
 * in which one can access anything from the UserType table using only the primary
 * key of the table and the name of the row (since each row is baisically unique).
 *
 * @author   <cmicklis@stetson.edu>
 * @since 	 v0.0.0
 */

class UserType extends BaseUserType
{
	const USER_TYPE_STUDENT_NAME = 'Student';
	const USER_TYPE_TEACHER_NAME = 'Teacher';
	const USER_TYPE_ADMIN_NAME = 'Admin';
	const USER_TYPE_SUPER_NAME = 'Super';

	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * 	The getUserTypeNameByID method searches the primary key of the usertype
	 * 	on the UserType table and then gets the name of that User type.
	 * 	
	 * 	@param  int    $userTypeID 	Represents the primary key of the User type.
	 *  @return String        		Represents the name on the UserType table.
	 */
	public static function getUserTypeNameByID($userTypeID)
	{
		$result = UserType::model()->find(array(
			'select' => 'user_type',
			'condition' => 'user_type_id=:userTypeID',
			'params' => array(':userTypeID' => $userTypeID)
		));
		return $result->user_type;
	}

	/**
	 * 	The getUserTypeIDByName method searches the name of the usertype
	 * 	on the UserType table and then gets the primary key of that User type.
	 * 	
	 * 	@param  String $userType 	Represents the name of the User type.
	 *  @return int           		Represents the primary key on the
	 *                              UserType table.
	 */
	public static function getUserTypeIDByName($userType)
	{
		$result = UserType::model()->find(array(
			'select' => 'user_type_id',
			'condition' => 'user_type=:userType',
			'params' => array(':userType' => $userType)
		));
		return $result->user_type_id;
	}
}
