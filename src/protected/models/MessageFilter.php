<?php

/**
 *  The MessageFilter class is used to perform error checking and other logic 
 *  before and after a message is sent.
 * 
 *  @author   <cmicklis@stetson.edu>, <jsalis@stetson.edu>
 *  @since 	  v0.0.0
 */
class MessageFilter
{
    const ERROR_KEY = 'message';
    const ERROR_MAX_WORKLOAD = 'Sorry, you have reached your work limit for today.';
    const ERROR_NOT_ENOUGH_HEALTH = 'Sorry, you are sick and can not work today.';
    const ERROR_NOT_ENOUGH_MONEY = 'Sorry, you do not have enough money for this transaction.';
    const ERROR_ITEM_OUT_OF_STOCK = 'Sorry, the requested item is out of stock.';
    const ERROR_ITEM_RENT_IN_PROGRESS = 'Sorry, this item is currently being rented.';
    const ERROR_PREVIOUSLY_ACCEPTED_OFFER = 'Sorry, this offer has already been accepted.';

    private $_model;

    function __construct($model)
    {
        $this->_model = $model;
    }

    /**
    
        Before Message Action

     */

    /**
     *  Checks for errors based on the type of message to be sent. Returns false if there
     *  are errors, true otherwise.
     * 
     *  @param  int         $senderID       The ID of the entity to send the message.
     *  @param  int         $receiverID     The ID of the entity to receive the message.
     *  @param  int         $messageType    The type of message to be passed.
     *  @param  array       $append         An array of key values to be encoded into the message.
     *  @return boolean                     Whether the message should be sent.
     */
    public function beforeMessageAction($senderID, $receiverID, $messageType, $append)
    {
        switch ($messageType)
        {
            case MessageType::MESSAGE_TYPE_REVISE_JOB_OFFER_NAME:
            case MessageType::MESSAGE_TYPE_ACCEPT_JOB_OFFER_NAME:
                $this->beforeJobOffer();
                $this->isPublicMessageAvailable($append);
                break;

            case MessageType::MESSAGE_TYPE_REVISE_RENT_OFFER:
            case MessageType::MESSAGE_TYPE_ACCEPT_RENT_OFFER:
                $this->beforeRentOffer($append);
                $this->isPublicMessageAvailable($append);
                break;
        }
        return !$this->_model->hasErrors();
    }

    /**
     *  Checks if the worker is currently able to work.
     */
    private function beforeJobOffer()
    {
        if ($this->_model instanceof Worker)
        {
            if ($this->_model->hasWorkedToday())
            {
                $this->_model->addError(MessageFilter::ERROR_KEY, MessageFilter::ERROR_MAX_WORKLOAD);
            }
            if (!$this->_model->hasEnoughHealth())
            {
                $this->_model->addError(MessageFilter::ERROR_KEY, MessageFilter::ERROR_NOT_ENOUGH_HEALTH);
            }
        }
    }

    /**
     *  Checks if the entity item is available for rent.
     * 
     *  @param  array       $append         An array of key values to be encoded into the message.
     */
    private function beforeRentOffer($append)
    {
        $previousMessageID = $append[Message::MESSAGE_KEY_PREVIOUS_MESSAGE];
        $previousMessage = Message::model()->findByPk($previousMessageID);
        $rootMessage = $previousMessage->getRootMessage();
        $entityHasItemID = $rootMessage->findByKey(Message::MESSAGE_KEY_ITEM_ID);

        if (EntityRentEntityHasItem::isRentInProgress($entityHasItemID))
        {
            $this->_model->addError(MessageFilter::ERROR_KEY, MessageFilter::ERROR_ITEM_RENT_IN_PROGRESS);
        }
    }

    /**
     *  Checks if the message to be sent is a reply to a public message that has not 
     *  been replied to. If the test fails then the previous message is set to replied
     *  so it is hidden from the user and no further actions can be taken.
     * 
     *  @param  array       $append         An array of key values to be encoded into the message.
     */
    private function isPublicMessageAvailable($append)
    {
        $previousMessageID = $append[Message::MESSAGE_KEY_PREVIOUS_MESSAGE];
        $previousMessage = Message::model()->findByPk($previousMessageID);
        $rootMessage = $previousMessage->getRootMessage();
        $entityHasMessageRoot = EntityHasMessage::model()->message($rootMessage->getMessageID())->find();
        $entityType = $entityHasMessageRoot->entityReceiver->getEntityType();

        if ($entityType == EntityType::ENTITY_TYPE_PUBLIC_MESSENGER_NAME && $entityHasMessageRoot->getMessageReplied())
        {
            $id = EntityHasMessage::model()->message($previousMessageID)->find()->getEntityMessageID();
            EntityHasMessage::replyMessage($id);

            $this->_model->addError(MessageFilter::ERROR_KEY, MessageFilter::ERROR_PREVIOUSLY_ACCEPTED_OFFER);
        }
    }

    /**
    
        After Message Action

     */

    /**
     *  Reacts to the sending of a message by calling an appropriate action based on 
     *  the message type.
     * 
     *  @param EntityHasMessage  $entityHasMessage  An object representing all information 
     *                                              of the sent message.     
     */
    public function afterMessageAction($entityHasMessage)
    {
        $message = $entityHasMessage->getMessage();
        $messageType = $message->getMessageType();

        switch ($messageType)
        {
            case MessageType::MESSAGE_TYPE_CHAT_NAME:
                $this->afterChatMessage($message);
                break;

            case MessageType::MESSAGE_TYPE_REJECT_JOB_OFFER_NAME:
            case MessageType::MESSAGE_TYPE_REJECT_LOAN_REQUEST_NAME:
            case MessageType::MESSAGE_TYPE_REJECT_RENT_OFFER:
                $this->afterRejectMessage($message);
                break;

            case MessageType::MESSAGE_TYPE_REVISE_JOB_OFFER_NAME:
            case MessageType::MESSAGE_TYPE_REVISE_LOAN_REQUEST_NAME:
            case MessageType::MESSAGE_TYPE_REVISE_RENT_OFFER:
                $this->afterReviseMessage($message);
                break;

            case MessageType::MESSAGE_TYPE_ACCEPT_JOB_OFFER_NAME:
                $this->afterAcceptMessage($message);
                $this->afterAcceptJobOffer($entityHasMessage);
                break;

            case MessageType::MESSAGE_TYPE_ACCEPT_RENT_OFFER:
                $this->afterAcceptMessage($message);
                $this->afterAcceptRentOffer($entityHasMessage);
                break;

            case MessageType::MESSAGE_TYPE_ACCEPT_LOAN_REQUEST_NAME:
                $this->afterAcceptMessage($message);
                break;
        }
    }

    /**
     *  Sets the previous message of the previous message as replied.
     *
     *  @param  Message  $message    The message that was sent.
     */
    private function afterChatMessage($message)
    {
        if (!is_null($previousMessage = $message->getPreviousMessage()))
        {
            $messageResponseID = $previousMessage->findByKey(Message::MESSAGE_KEY_PREVIOUS_MESSAGE);

            if (sizeof($messageResponseID) > 0)
            {
                $id = EntityHasMessage::model()->message($messageResponseID)->find()->getEntityMessageID();
                EntityHasMessage::replyMessage($id);
            }
        }
    }

    /**
     *  Sets the previous message as replied.
     *  
     *  @param  Message  $message    The message that was sent.
     */
    private function afterRejectMessage($message)
    {
        $previousMessageID = $message->findByKey(Message::MESSAGE_KEY_PREVIOUS_MESSAGE);

        if (sizeof($previousMessageID) > 0)
        {
            $id = EntityHasMessage::model()->message($previousMessageID)->find()->getEntityMessageID();
            EntityHasMessage::replyMessage($id);
        }
    }

    /**
     *  Sets the previous message as replied if it is not a public message.
     *  
     *  @param  Message  $message    The message that was sent.
     */
    private function afterReviseMessage($message)
    {
        $previousMessageID = $message->findByKey(Message::MESSAGE_KEY_PREVIOUS_MESSAGE);

        if (sizeof($previousMessageID) > 0)
        {
            $entityHasMessage = EntityHasMessage::model()->message($previousMessageID)->find();
            $entityType = $entityHasMessage->entityReceiver->getEntityType();

            if ($entityType != EntityType::ENTITY_TYPE_PUBLIC_MESSENGER_NAME)
            {
                EntityHasMessage::replyMessage($entityHasMessage->getEntityMessageID());
            }
        }
    }

    /**
     *  Sets the previous message and root message as replied.
     * 
     *  @param  Message  $message    The message that was sent.    
     */
    private function afterAcceptMessage($message)
    {
        $previousMessageID = $message->findByKey(Message::MESSAGE_KEY_PREVIOUS_MESSAGE);

        if (sizeof($previousMessageID) > 0)
        {
            $id = EntityHasMessage::model()->message($previousMessageID)->find()->getEntityMessageID();
            EntityHasMessage::replyMessage($id);

            $rootMessage = $message->getRootMessage();
            $id = EntityHasMessage::model()->message($rootMessage->getMessageID())->find()->getEntityMessageID();
            EntityHasMessage::replyMessage($id);
        }
    }

    /**
     *  [afterAcceptJobOffer description]
     * 
     *  @param EntityHasMessage  $entityHasMessage  An object representing all information 
     *                                              of the sent message. 
     */
    private function afterAcceptJobOffer($entityHasMessage)
    {
        $message = $entityHasMessage->getMessage();
        $jobOfferMessage = $message->getRootMessage();
        $itemID = $jobOfferMessage->findByKey(Message::MESSAGE_KEY_ITEM_ID);
        $payment = $message->findByKey(Message::MESSAGE_KEY_MONEY);
        $unitsProduced = 0;

        if ($this->_model instanceof Worker)
        {
            $companyID = $entityHasMessage->getEntityReceiverID();
            $unitsProduced = $this->_model->workForCompany($itemID, $payment, $companyID);
        }
        else if ($this->_model instanceof Company)
        {
            $companyID = $entityHasMessage->getEntitySenderID();
            $worker = Worker::model()->findByPk($entityHasMessage->getEntityReceiverID());
            $unitsProduced = $worker->workForCompany($itemID, $payment, $companyID);
        }

        Message::model()->appendToMessage($message->getMessageID(), array(
            Message::MESSAGE_KEY_UNITS => $unitsProduced,
        ));

        if (Item::model()->getItemTypeByID($itemID) == ItemType::ITEM_TYPE_PUBLIC_GOOD_NAME)
        {
            BenefitFunction::calculateBenefits($itemID, $unitsProduced, Yii::app()->user->economyID);
        }
    }

    /**
     *  After a rent offer has been accepted then a row is created in the Entity Rent Entity
     *  Has Item table defining length of the rent period.
     * 
     *  @param EntityHasMessage  $entityHasMessage  An object representing all information 
     *                                              of the sent message. 
     */
    private function afterAcceptRentOffer($entityHasMessage)
    {
        $message = $entityHasMessage->getMessage();
        $jobOfferMessage = $message->getRootMessage();
        $offerSenderID = $jobOfferMessage->entityHasMessages[0]->getEntitySenderID();
        
        $offerSender = Entity::model()->findByPk($offerSenderID);
        $offerReceiver;

        if ($entityHasMessage->getEntitySenderID() == $offerSenderID)
        {
            $offerReceiver = Entity::model()->findByPk($entityHasMessage->getEntityReceiverID());
        }
        else
        {
            $offerReceiver = Entity::model()->findByPk($entityHasMessage->getEntitySenderID());
        }

        $entityHasItemID = $jobOfferMessage->findByKey(Message::MESSAGE_KEY_ITEM_ID);
        $payment = $message->findByKey(Message::MESSAGE_KEY_MONEY);

        EntityRentEntityHasItem::model()->createEntityRentEntityHasItem($offerReceiver->getEntityID(), $entityHasItemID, $payment, 1);

        $offerSender->updateSimulation();
        $offerReceiver->updateSimulation();
    }
}
