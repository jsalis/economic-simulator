<?php

/**
 * The Worker class represents all of the actions of a worker.
 * The Worker can create items for a company, purchase items, or
 * transfer any sort of information or objects (money, items, modifiers).
 *
 * @author   <cmicklis@stetson.edu>, <jsalis@stetson.edu>
 * @since 	 v0.0.0
 */
class Worker extends Entity
{
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

    public function defaultScope()
	{
		// only read records that are of entity type Worker
		return array(
			'with' => array('entityType' => array('select' => false)),
			'condition' => 'entity_type=:type',
			'params' => array(':type' => EntityType::ENTITY_TYPE_WORKER_NAME),
		);
    }

	public function updateSimulation()
	{
		parent::updateSimulation();
		$this->payWealthTax();
	}

	/**
	 * 	The workForCompany method makes the items of the company that
	 * 	the worker was hired for and then it pays the worker for his services.
	 * 	
	 * 	The amount of items created are dependent on the Production function of
	 * 	the Economy, the production function is controlled by the Teacher. The 
	 * 	production function then returns the amount of the items that are to be 
	 * 	made and does so, adding all those items to the market.
	 *
	 * 	@param  int     $itemID		The ID of the item that the worker is to create.
	 * 	@param  int     $payment    The amount of money that the worker will recieve 
	 * 	                            from the company after the item is created.
	 * 	@param  int  	$companyID  The ID of the Company.
	 * 	@return int 				The number of item units produced.
	 */
	public function workForCompany($itemID, $payment, $companyID)
	{
		$amount = ProductionFunction::amountOfItemsToProduce(Yii::app()->user->economyID, $companyID, $this->getEntityID());
		$amount = floor($amount);
		$item = Item::model()->findByPk($itemID);
		$economyID = $item->getExportEconomy();
		
		for ($i = 0; $i < $amount; $i++)
		{
			MarketItem::createMarketItem($economyID, $itemID, $this->getEntityID());
		}

		// pay tariff
		if ($economyID != Yii::app()->user->economyID)
		{
			$governmentID = Economy::getGovernmentID($economyID);
			$economyRelationID = EconomyRelation::findRelationID($economyID, $economyID);
			$tariff = EconomyRelation::getEconomyRelationTariff($economyRelationID);
			$totalTariff = $amount * $tariff;
			Entity::transferMonetary($item->entity_creator_id, $governmentID, $totalTariff, 'tariff');
		}

		$governmentID = Economy::getGovernmentID(Yii::app()->user->economyID);
		$this->transferMonetaryAndTax($companyID, $this->getEntityID(), $payment, $governmentID);

		return $amount;
	}

	/**
	 * 	Checks if the worker has already accepted a job offer for the current day.
	 *
	 *  @return boolean 			Whether the worker has worked today.
	 */
	public function hasWorkedToday()
	{
		$jobCount = EntityHasMessage::model()
			->messageType( MessageType::MESSAGE_TYPE_ACCEPT_JOB_OFFER_NAME )
			->senderOrReceiver( $this->getEntityID(), $this->getEntityID() )
			->lowerDate( DateUtility::getResetYesterday(Yii::app()->user->economyID) )
			->count();

		return $jobCount > 0;
	}

	/**
	 * 	The hasVoted function states if the worker has already voted while an election
	 *	is in progress. It does this by comparing the most recent vote message sent by 
	 *	the worker to the most recent election end date. If the vote date is greater than 
	 *	the end election date then the worker has voted.
	 *
	 *  @return boolean 			Whether the worker has voted already.
	 */
	public function hasVoted()
	{
		$publicMessengerID = Economy::getPublicMessengerID(Yii::app()->user->economyID);
		$recentEndElectionDate = EntityHasMessage::getMostRecentMessageDate(MessageType::MESSAGE_TYPE_END_ELECTION, $publicMessengerID);
		$recentVoteDate = EntityHasMessage::getMostRecentMessageDate(MessageType::MESSAGE_TYPE_VOTE_FOR_CANDIDATE, $this->getEntityID());

		return ($recentVoteDate > $recentEndElectionDate);
	}

	/**
	 * 	Checks if the worker received the required amount of health yesterday.
	 * 	If so, then the worker is considered healthy.
	 * 	
	 *  @return boolean 			Whether the worker is currently healthy.
	 */
	public function hasEnoughHealth()
	{
		// Check if its the worker's first day in the economy.
		$creation = new DateTime($this->creation_date);
		$yesterday = new DateTime(DateUtility::getResetYesterday(Yii::app()->user->economyID));

		if ($creation > $yesterday)
		{
			return true;
		}

		// Find the total health that the worker received yesterday.
		$upperDate = DateUtility::getResetYesterday(Yii::app()->user->economyID);
		$lowerDate = DateUtility::getResetDateByBeginningAndDayOffset($upperDate, -2, Yii::app()->user->economyID);

		$messageList = EntityHasMessage::model()
			->messageType( MessageType::MESSAGE_TYPE_HEALTH_TRANSFER_NAME )
			->receiver( $this->getEntityID() )
			->upperDate( $upperDate )
			->lowerDate( $lowerDate )
			->findAll();

		$sum = 0;
		for ($i = 0; $i < sizeof($messageList); $i++)
		{
			$sum += $messageList[$i]->getMessage()->findByKey(Message::MESSAGE_KEY_HEALTH);
		}

		return ($sum >= $this->getEntityHealthNecessity());
	}

	/**
	 * Checks if the worker has sent any product demand surveys during the current day.
	 * A worker must submit a survey in order to purchase products for the day. 
	 * 
	 * @return  boolean     Whether the worker has sent one or more demand survey messages.
	 */
	public function hasSentDemandSurvey()
	{
		$surveyCount = EntityHasMessage::model()
			->messageType( MessageType::MESSAGE_TYPE_PRODUCT_DEMAND_SURVEY )
			->sender( $this->getEntityID() )
			->lowerDate( DateUtility::getResetYesterday(Yii::app()->user->economyID) )
			->count();

		return $surveyCount > 0;
	}

	/**
	 * Submits a product demand survey as a message to the public messenger.
	 * 
	 * @param  int     $price     The price that the worker is willing to pay
	 * @param  String  $itemType  The product type.
	 * @param  int     $quantity  The quantity of product.
	 */
	public function sendDemandSurvey($price, $itemType, $quantity)
	{
		$receiverID = Economy::getPublicMessengerID(Yii::app()->user->economyID);

		$this->sendMessage($receiverID, MessageType::MESSAGE_TYPE_PRODUCT_DEMAND_SURVEY, array(
			Message::MESSAGE_KEY_MONEY => $price,
			Message::MESSAGE_KEY_ITEM_TYPE => $itemType,
			Message::MESSAGE_KEY_UNITS => $quantity,
		));
	}

	/**
	 * Creates a new rent offer using a given entityHasItem ID and daily payment,
	 * which is sent as a message to the public entity.
	 * 
	 * @param  int 	$itemID  	The ID of the entityHasItem to offer for rent.
	 * @param  int 	$payment 	The expected daily payment in dollars.
	 */
	public function createRentOffer($itemID, $payment)
	{
		$receiverID = Economy::getPublicMessengerID(Yii::app()->user->economyID);

		$this->sendMessage($receiverID, MessageType::MESSAGE_TYPE_SEND_RENT_OFFER, array(
			Message::MESSAGE_KEY_CONTENT => $this->getEntityName() . ' has offered an item for rent.',
			Message::MESSAGE_KEY_ITEM_ID => $itemID,
			Message::MESSAGE_KEY_MONEY => $payment,
		));
	}

	/**
	 * Sends a message to the public messenger of the economy stating that the user has entered the 
	 * labor market. Only one message can be sent per user each day.
	 */
	public function enteredLaborMarket()
	{
		$exists = EntityHasMessage::model()
			->messageType( MessageType::MESSAGE_TYPE_ENTERED_LABOR_MARKET )
			->sender( $this->getEntityID() )
			->lowerDate( DateUtility::getResetYesterday(Yii::app()->user->economyID) )
			->exists();

		if (!$exists)
		{
			$receiverID = Economy::getPublicMessengerID(Yii::app()->user->economyID);

			$this->sendMessage($receiverID, MessageType::MESSAGE_TYPE_ENTERED_LABOR_MARKET, array(
				Message::MESSAGE_KEY_CONTENT => $this->getEntityName() . ' has entered the labor market.',
			));
		}
	}
	
	/**
	 * 	The purchaseItem method puts the item in the EntityHasItem table representing 
	 * 	that the worker owns that item. It then sets the the market item as purchased. 
	 * 	The sales tax for the economy is added to the item price so that it can
	 *  be checked with the current savings of the worker.
	 *
	 *  If the item is of type food then the worker must have his health increased.
	 * 
	 *  @param  int     $itemID       		Represents the item ID.
	 *  @param  int     $economyID 			Represents the ID of the economy to purchase the item from.
	 */
	public function purchaseItem($itemID, $economyID)
	{
		$item = Item::model()->findByPk($itemID);
		$marketItem = $item->findAvailableMarketItem($economyID);

		if (is_null($marketItem))
		{
			$this->addError(MessageFilter::ERROR_KEY, MessageFilter::ERROR_ITEM_OUT_OF_STOCK);
			return;
		}

		$salesTax = Economy::getEconomySalesTax($economyID);

		$totalPrice = $item->getPrice() + $salesTax;

		if ($this->getEntityBalance() - $totalPrice >= 0)
		{
			MarketItem::purchaseMarketItem($marketItem->getMarketItemID());
			EntityHasItem::createEntityHasItem($this->getEntityID(), $itemID, $item->getPrice());

			$entityMakerID = $item->getItemEntityID();
			$governmentID = Economy::getGovernmentID(Yii::app()->user->economyID);

			$this->transferMonetaryAndTax($this->getEntityID(), $entityMakerID, $item->getPrice(), $governmentID);
			$this->transferMonetaryAndTax($this->getEntityID(), $governmentID, $salesTax, $governmentID, 'sales tax');

			if ($item->getItemType() == ItemType::ITEM_TYPE_FOOD_NAME)
			{
				Entity::transferHealth($entityMakerID, $this->getEntityID(), $item->getHealth());
			}
			else
			{
				Entity::transferItem($entityMakerID, $this->getEntityID(), $item->getItemID());
			}
		}
		else
		{
			$this->addError(MessageFilter::ERROR_KEY, MessageFilter::ERROR_NOT_ENOUGH_MONEY);
		}
	}

	/**
	 *	Transfers money from the sender to the receiver and checks to see if the transfered money 
	 *	needs to be taxed.
	 *	 
	 *  @param  int     $senderID  			Represents the entity sender ID.
	 *  @param  int     $receiverID			Represents the entity receiver ID.
	 *  @param  int     $amount    			Represents the amount of money.
	 *  @param  int     $governmentID		Represents the ID of the government.
	 *  @return boolean                		Whether the transaction was successful.
	 */
	public function transferMonetaryAndTax($senderID, $receiverID, $amount, $governmentID, $description = '')
	{
		if (Entity::transferMonetary($senderID, $receiverID, $amount, $description) == false)
		{
			return false;
		}
		else
		{
			if ($amount <= 0) return true;

			$receiverType = Entity::getEntityTypeByEntityID($receiverID);

			if ($receiverType == EntityType::ENTITY_TYPE_COMPANY_NAME)
			{
				$company = Company::model()->findByPk($receiverID);
				$companyType = $company->getCompanyType();
				$tax = Economy::getEconomySalesTaxForCompany(Yii::app()->user->economyID, $companyType);
				Entity::transferMonetary($receiverID, $governmentID, $tax, 'sales tax');
			}
			else if ($receiverType == EntityType::ENTITY_TYPE_WORKER_NAME)
			{
				$tax = Economy::getEconomyIncomeTaxForWorker(Yii::app()->user->economyID);
				Entity::transferMonetary($receiverID, $governmentID, $tax, 'income tax');
			}
			return true;
		}
	}
}
