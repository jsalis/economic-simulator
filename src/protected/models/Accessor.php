<?php

/**
 * The Accessor class is there to represent an Entity class with all
 * the functionality including that of creating, updating, and removing,
 * items, entities, modifiers, rules, and messages. The Accessor class
 * has all rights to displaying the objects as well. In fact the whole
 * purpose of the accessor class is to be able to create, update, read,
 * and delete anything that an entity would be able to.
 *
 * @author   <cmicklis@stetson.edu>
 * @since 	 v0.0.0
 */
class Accessor extends Entity
{
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

    public function defaultScope()
	{
		// only read records that are of entity type Accessor
		return array(
			'with' => array('entityType' => array('select' => false)),
			'condition' => 'entity_type=:type',
			'params' => array(':type' => EntityType::ENTITY_TYPE_ACCESSOR_NAME),
		);
    }

	/**
	 * 	The startNomination method sends a message to the public messenger
	 * 	of the economy stating that the season for workers being elected has 
	 * 	started. If an nomination or election is currently in progress then the 
	 * 	nomination may not start again.
	 */
	public function startNomination()
	{
		$economyID = Yii::app()->user->economyID;
		if (!Economy::isNominationInProgress($economyID) && !Economy::isElectionInProgress($economyID))
		{
			$receiverID = Economy::getPublicMessengerID($economyID);
			$this->sendMessage($receiverID, MessageType::MESSAGE_TYPE_START_NOMINATION, array(
				Message::MESSAGE_KEY_CONTENT => 'Nomination started.',
			));
		}
	}

	/**
	 * 	The startElection method sends a message to the public messenger
	 * 	of the economy stating that the season for workers to start voting for 
	 * 	the candidates has started. If an nomination or election is currently in 
	 * 	progress then the election may not start again.
	 */
	public function startElection()
	{
		$economyID = Yii::app()->user->economyID;
		if (Economy::isNominationInProgress($economyID) && !Economy::isElectionInProgress($economyID) && !Economy::isElectionEnded($economyID))
		{
			$receiverID = Economy::getPublicMessengerID($economyID);
			$this->sendMessage($receiverID, MessageType::MESSAGE_TYPE_START_ELECTION, array(
				Message::MESSAGE_KEY_CONTENT => 'Election started.',
			));
		}
	}

	/**
	 * 	The endElection method sends a message to the publicMessenger
	 * 	of the economy stating that the season for workers to vote for 
	 * 	candidates has ended. If an nomination is currently in progress or 
	 * 	the election has already ended then the election may not end again.
	 */
	public function endElection()
	{
		$economyID = Yii::app()->user->economyID;
		if (!Economy::isNominationInProgress($economyID) && Economy::isElectionInProgress($economyID) && !Economy::isElectionEnded($economyID))
		{
			$receiverID = Economy::getPublicMessengerID(Yii::app()->user->economyID);
			$this->sendMessage($receiverID, MessageType::MESSAGE_TYPE_END_ELECTION, array(
				Message::MESSAGE_KEY_CONTENT => 'Election Ended.',
			));

			$candidateList = Economy::getCandidateList(Yii::app()->user->economyID);
			$this->removeCongress($economyID);
			$this->createCongress($economyID, $candidateList);
		}
	}

	/**
	 * 	The createCongress method creates all of the candidates that have
	 *	been voted in. The candidates are the users that have the most votes, ranked by the
	 *	highest vote count.
	 *
	 *  @param  int 				$economyID    		The ID of the economy to create congress for.
	 *  @param  array<Candidates>	$candidateList    	From the getCandidateList method in the
	 *													Economy class. Contains all of the information
	 *													of the candidate that was nominated.
	 */
	public function createCongress($economyID, $candidateList)
	{
		$governmentID = Economy::model()->getGovernmentID($economyID);
		$seatsInCongress = Economy::model()->getEconomySeatsInCongress($economyID);
		
		for ($i = 0; $i < $seatsInCongress; $i++)
		{
			if (count($candidateList) == 0)
				break;

			$max = 0;
			for ($j = 0; $j < count($candidateList); $j++)
			{
				if ($candidateList[$j]['voteCount'] > $candidateList[$max]['voteCount'])
				{
					$max = $j;
				}
			}

			$userID = $candidateList[$max]['userID'];
			User::model()->createUserHasEntity($userID, $governmentID);

			array_splice($candidateList, $max, 1);
		}
	}

	/**
	 * 	The removeCongress method deletes all user relations to the government entity.
	 *  
	 *  @param  int 	$economyID    	The ID of the economy to remove congress from.
	 */
	public function removeCongress($economyID)
	{
		$governmentID = Economy::model()->getGovernmentID($economyID);
		UserHasEntity::model()->deleteRelationsToEntity($governmentID);
	}
	
	/**
		 _______  _       ___________________________         
		(  ____ \( (    /|\__   __/\__   __/\__   __/|\     /|
		| (    \/|  \  ( |   ) (      ) (      ) (   ( \   / )
		| (__    |   \ | |   | |      | |      | |    \ (_) / 
		|  __)   | (\ \) |   | |      | |      | |     \   /  
		| (      | | \   |   | |      | |      | |      ) (   
		| (____/\| )  \  |   | |   ___) (___   | |      | |   
		(_______/|/    )_)   )_(   \_______/   )_(      \_/   
		                                                      
	 */
	
	/**
	 * Gets a list of all entities in the specified economy. They are constructed
	 * as the proper Entity subclass defined by the entity type.
	 * 
	 * @return array<Entity>
	 */
	public function getEntitiesInEconomy()
	{
		$governmentList = Government::model()->economy(Yii::app()->user->economyID)->with('userHasEntities')->findAll();
		$workerList = Worker::model()->economy(Yii::app()->user->economyID)->with('userHasEntities')->findAll();
		$companyList = Company::model()->economy(Yii::app()->user->economyID)->with('userHasEntities')->findAll();
		$bankList = Bank::model()->economy(Yii::app()->user->economyID)->with('userHasEntities')->findAll();

		return array_merge($governmentList, $workerList, $companyList, $bankList);
	}

	/**
	 * Creates a new Entity, a new UserHasEntity, and a new EconomyHasEntity.
	 *
	 * @return $entityID 	The ID of the new entity.
	 */
	public function createEntityForStudent($emailAddress, $entityType, $economyID)
	{
		$entityName = Entity::getDefaultEntityName($entityType);
		$entityID = Entity::createEntity($entityName, $entityType, $economyID);
		$userID = User::getUserIDByEmailAddress($emailAddress);
		User::createUserHasEntity($userID, $entityID);
		Economy::createEconomyHasEntity($economyID, $entityID);
		return $entityID;
	}

	/**
	 * Creates a company for a student and sets the company type.
	 * 
	 * @param  String 	$emailAddress The email address of the user who will own the company.
	 * @param  String 	$companyType  The type of company.
	 * @param  int 		$economyID    The ID of the economy to create the company in.
	 */
	public function giveCompany($emailAddress, $companyType, $economyID)
	{
		$entityID = $this->createEntityForStudent($emailAddress, EntityType::ENTITY_TYPE_COMPANY_NAME, $economyID);
		Company::model()->findByPk($entityID)->updateCompanyType($companyType);
	}

	/**
	 * Gives money to all entities of a specified type.
	 *
	 *	@param  int     $entityType    The type of entity to receive money.
	 *	@param  int     $amount        The amount of money to give.
	 */
	public function giveMoney($entityType, $amount)
	{
		$entityList = Entity::model()->economy(Yii::app()->user->economyID)->type($entityType)->findAll();

		for ($i = 0; $i < sizeof($entityList); $i++)
		{
			$id = $entityList[$i]->getEntityID();
			Entity::addMonetary($id, $amount);
		}
	}

	/**
	 *	Gives an amount of health to all workers in the current economy, or a single
	 *	worker if specified.
	 * 
	 *	@param  int     $health         The amount of health to give.
	 *	@param  int     $workerID       An optional workerID to send health to.
	 *	@return boolean					Whether the transfer was successful.
	 */
	public function giveHealth($health, $workerID = null)
	{
		if (!is_null($workerID))
		{
			if (Entity::getEntityTypeByEntityID($workerID) == EntityType::ENTITY_TYPE_WORKER_NAME)
			{
				Entity::transferHealth($this->getEntityID(), $workerID, $health);
				return true;
			}
			else return false;
		}
		else
		{
			$workerIDs = array();
			$workerList = Worker::model()->economy(Yii::app()->user->economyID)->findAll();
			if (sizeof($workerList) == 0) return false;

			for ($i = 0; $i < sizeof($workerList); $i++)
			{
				$workerIDs[] = $workerList[$i]->getEntityID();
			}
			Entity::transferHealth($this->getEntityID(), $workerIDs, $health);
			return true;
		}
	}

	/**
		__________________ _______  _______ 
		\__   __/\__   __/(  ____ \(       )
		   ) (      ) (   | (    \/| () () |
		   | |      | |   | (__    | || || |
		   | |      | |   |  __)   | |(_)| |
		   | |      | |   | (      | |   | |
		___) (___   | |   | (____/\| )   ( |
		\_______/   )_(   (_______/|/     \|
		                                    
	 */
	
	/**
	 * Adds items to the market of the economy.
	 * 
	 * @param int  $itemID   	The ID of the item.
	 * @param int  $quantity 	The quantity of items to add to the market.
	 */
	public function addItemToMarket($itemID, $quantity)
	{
		for ($i = 0; $i < $quantity; $i++)
		{
			MarketItem::createMarketItem(Yii::app()->user->economyID, $itemID, $this->getEntityID());
		}
	}

	/**
	 * Gives the entity a specified item.
	 * 
	 * @param int  $itemID   	The ID of the item.
	 * @param int  $entityID 	The ID of the entity.
	 */
	public function giveItemToEntity($itemID, $entityID)
	{
		EntityHasItem::createEntityHasItem($entityID, $itemID, 0);
		Entity::transferItem($this->getEntityID(), $entityID, $itemID);
	}
	
	/**
	 * Creates a new Item given the criteria passed through the Accessor
	 * Controller.
	 */
	public function createItem()
	{

	}
	
	/**
	 * Updates a specific Item given the criteria passed through by the 
	 * Accessor controller, this includes the specific Item that is
	 * defined by the primary key of the Item table
	 */
	public function updateItem()
	{

	}
	
	/**
		 _______  _______  ______  _________ _______  _______  _______ 
		(       )(  ___  )(  __  \ \__   __/(  ____ \(  ____ \(  ____ )
		| () () || (   ) || (  \  )   ) (   | (    \/| (    \/| (    )|
		| || || || |   | || |   ) |   | |   | (__    | (__    | (____)|
		| |(_)| || |   | || |   | |   | |   |  __)   |  __)   |     __)
		| |   | || |   | || |   ) |   | |   | (      | (      | (\ (   
		| )   ( || (___) || (__/  )___) (___| )      | (____/\| ) \ \__
		|/     \|(_______)(______/ \_______/|/       (_______/|/   \__/
		                                                               
	 */

	/**
	 * 	Creates a new Item, in the sub criteria of a modifier given the criteria
	 * 	passed through the Accessor Controller.
	 *
	 * @param  String  $itemName   		    Represents item name.
	 * @param  String  $itemType   		    Represents type of item.
	 * @param  String  $itemDescription     Represents item description.
	 * @param  String  $efficiency          Represents item efficiency.
	 * @param  int     $price               Represents price of the item.
	 */
	public function createModifier($itemName, $itemType, $itemDescription, $efficiency, $price)
	{
		$governmentID = Economy::getGovernmentID(Yii::app()->user->economyID);
		
		$itemID = Item::createItem($itemName, $itemType, $governmentID, $itemDescription, Yii::app()->user->economyID);
		$item = Item::model()->findByPk($itemID);
		$item->updatePrice($price);
		$item->updateEfficiency($efficiency);
	}
	
	/**
	 * 	Updates a new Item, in the sub criteria of a modifier given the criteria
	 * 	passed through the Accessor Controller.
	 *
	 * @param  String  $itemName   		    Represents item name.
	 * @param  String  $itemType   		    Represents type of item.
	 * @param  String  $itemDescription     Represents item description.
	 * @param  String  $efficiency          Represents item efficiency.
	 * @param  int     $price               Represents price of the item.
	 */
	public function updateModifier($itemID, $itemName, $itemType, $itemDescription, $efficiency, $price)
	{
		Item::model()->updateByPk($itemID, array(
			'item' => $itemName,
			'item_type_id' => ItemType::getItemTypeIDByItemType($itemType),
			'item_description' => $itemDescription,
		));	
		$item = Item::model()->findByPk($itemID);
		$item->updatePrice($price);
		$item->updateEfficiency($efficiency);
	}
	
	/**
	 * Gets all of the modifiers created by the accessor.
	 * 
	 * @return array<Item> 		Represents an array of modifiers created by the accessor.
	 */
	public function getModifierList()
	{
		$governmentID = Economy::getGovernmentID(Yii::app()->user->economyID);

		$degreeList = Item::getItemsByCreator($governmentID, ItemType::ITEM_TYPE_DEGREE_NAME);
		$machineList = Item::getItemsByCreator($governmentID, ItemType::ITEM_TYPE_MACHINE_NAME);
		$landList = Item::getItemsByCreator($governmentID, ItemType::ITEM_TYPE_LAND_NAME);

		return array_merge($degreeList, $machineList, $landList);
	}

	/**
		 _______           ______   _       _________ _______    _______  _______  _______  ______  
		(  ____ )|\     /|(  ___ \ ( \      \__   __/(  ____ \  (  ____ \(  ___  )(  ___  )(  __  \ 
		| (    )|| )   ( || (   ) )| (         ) (   | (    \/  | (    \/| (   ) || (   ) || (  \  )
		| (____)|| |   | || (__/ / | |         | |   | |        | |      | |   | || |   | || |   ) |
		|  _____)| |   | ||  __ (  | |         | |   | |        | | ____ | |   | || |   | || |   | |
		| (      | |   | || (  \ \ | |         | |   | |        | | \_  )| |   | || |   | || |   ) |
		| )      | (___) || )___) )| (____/\___) (___| (____/\  | (___) || (___) || (___) || (__/  )
		|/       (_______)|/ \___/ (_______/\_______/(_______/  (_______)(_______)(_______)(______/ 
		                                                                                            
	 */
	
	/**
	 * 	Creates a new Item, in the sub criteria of a public good given the criteria
	 * 	passed through the Accessor Controller.
	 *
	 * @param  String  $itemName   		    	Represents item name.
	 * @param  String  $itemType   		   		Represents type of item.
	 * @param  String  $itemDescription     	Represents item description.
	 * @param  double  $monetaryStart      		Represents where the benefit for monetary will
	 *                                       	start.
	 * @param  double  $monetaryDecreaseRate	Represents how quickly the benefit for the 
	 *                                       	monetary will decrease.
	 * @param  double  $efficiencyStart    		Represents where the benefit for efficiency will
	 *                                       	start.
	 * @param  double  $efficiencyDecreaseRate	Represents how quickly the benefit for the 
	 *                                       	efficiency will decrease.
	 * @param  double  $healthStart	    		Represents where the benefit for health will
	 *                                       	start.
	 * @param  double  $healthDecreaseRate		Represents how quickly the benefit for the 
	 *                                       	health will decrease.
	 * @param  String  $entityType 		    	Represents the entity type that will receive
	 *                                    		the benefit.
	 */
	public function createPublicGoood($itemName, $itemType, $itemDescription, $monetaryStart, 
		$monetaryDecreaseRate, $efficiencyStart, $efficiencyDecreaseRate, $healthStart, 
		$healthDecreaseRate, $entityType)
	{
		$governmentID = Economy::getGovernmentID(Yii::app()->user->economyID);

		$itemID = Item::createItem($itemName, $itemType, $governmentID, $itemDescription, Yii::app()->user->economyID);
		$item = Item::model()->findByPk($itemID);
		$item->updateBenefitStart($monetaryStart, Entity::ENTITY_ATTRIBUTE_MONETARY);
		$item->updateBenefitDecreaseRate($monetaryDecreaseRate, Entity::ENTITY_ATTRIBUTE_MONETARY);
		$item->updateBenefitStart($efficiencyStart, Entity::ENTITY_ATTRIBUTE_EFFICIENCY);
		$item->updateBenefitDecreaseRate($efficiencyDecreaseRate, Entity::ENTITY_ATTRIBUTE_EFFICIENCY);
		$item->updateBenefitStart($healthStart, Entity::ENTITY_ATTRIBUTE_HEALTH);
		$item->updateBenefitDecreaseRate($healthDecreaseRate, Entity::ENTITY_ATTRIBUTE_HEALTH);
		$item->updateBenefitReceiver($entityType);
	}

	/**
	 * 	Updates a new Public Good, in the sub criteria of a modifier given the criteria
	 * 	passed through the Accessor Controller.
	 *
	 * @param  String  $itemID 		  		    Represents item primary key.
	 * @param  String  $itemName   		    	Represents item name.
	 * @param  String  $itemType   		   		Represents type of item.
	 * @param  String  $itemDescription     	Represents item description.
	 * @param  double  $monetaryStart      		Represents where the benefit for monetary will
	 *                                       	start.
	 * @param  double  $monetaryDecreaseRate	Represents how quickly the benefit for the 
	 *                                       	monetary will decrease.
	 * @param  double  $efficiencyStart    		Represents where the benefit for efficiency will
	 *                                       	start.
	 * @param  double  $efficiencyDecreaseRate	Represents how quickly the benefit for the 
	 *                                       	efficiency will decrease.
	 * @param  double  $healthStart	    		Represents where the benefit for health will
	 *                                       	start.
	 * @param  double  $healthDecreaseRate		Represents how quickly the benefit for the 
	 *                                       	health will decrease.
	 * @param  String  $entityType 		    	Represents the entity type that will receive
	 *                                    		the benefit.
	 */
	public function updatePublicGood($itemID, $itemName, $itemType, $itemDescription, $monetaryStart, 
		$monetaryDecreaseRate, $efficiencyStart, $efficiencyDecreaseRate, $healthStart, 
		$healthDecreaseRate, $entityType)
	{
		Item::model()->updateByPk($itemID, array(
			'item' => $itemName,
			'item_type_id' => ItemType::getItemTypeIDByItemType($itemType),
			'item_description' => $itemDescription,
		));	
		
		$item = Item::model()->findByPk($itemID);
		$item->updateBenefitStart($monetaryStart, Entity::ENTITY_ATTRIBUTE_MONETARY);
		$item->updateBenefitDecreaseRate($monetaryDecreaseRate, Entity::ENTITY_ATTRIBUTE_MONETARY);
		$item->updateBenefitStart($efficiencyStart, Entity::ENTITY_ATTRIBUTE_EFFICIENCY);
		$item->updateBenefitDecreaseRate($efficiencyDecreaseRate, Entity::ENTITY_ATTRIBUTE_EFFICIENCY);
		$item->updateBenefitStart($healthStart, Entity::ENTITY_ATTRIBUTE_HEALTH);
		$item->updateBenefitDecreaseRate($healthDecreaseRate, Entity::ENTITY_ATTRIBUTE_HEALTH);
		$item->updateBenefitReceiver($entityType);
	}
	

	public function getPublicGoodList()
	{
		$governmentID = Economy::getGovernmentID(Yii::app()->user->economyID);
		
		return Item::getItemsByCreator($governmentID, ItemType::ITEM_TYPE_PUBLIC_GOOD_NAME);
	}

	/**
		 _______           _        _______ 
		(  ____ )|\     /|( \      (  ____ \
		| (    )|| )   ( || (      | (    \/
		| (____)|| |   | || |      | (__    
		|     __)| |   | || |      |  __)   
		| (\ (   | |   | || |      | (      
		| ) \ \__| (___) || (____/\| (____/\
		|/   \__/(_______)(_______/(_______/
		                                    
	 */
	
	/**
	 * Updates a specific Rule given the criteria passed through by the 
	 * Accessor controller, this includes the specific Rule that is
	 * defined by the primary key of the Rule table
	 * 
	 * @param  array 	$ruleList 	[description]
	 */
	public function updateEconomyRules($ruleList)
	{
		foreach ($ruleList as $id => $rule)
		{
			if (isset($rule['value']))
			{
				Rule::updateRuleValue($id, $rule['value']);
			}
			if (isset($rule['activated']))
			{
				Rule::model()->updateByPk($id, array(
					'rule_activated' => $rule['activated'],
				));
			}
		}
	}
}
