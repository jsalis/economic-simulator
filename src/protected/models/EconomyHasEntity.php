<?php

Yii::import('application.models._base.BaseEconomyHasEntity');

/**
 * The EconomyHasEntity class is there to represent every bit of detail when it comes to 
 * the relation of the Economies and Entities. This relation only goes as deep as both the
 * primary keys of the Economy and Entity. So the only methods for this class are report
 * methods that get either all the Entity ids for the economy or vice versa.
 * 
 * @author   <cmicklis@stetson.edu>, <jsalis@stetson.edu>
 * @since 	 v0.0.0
 * 
 */

class EconomyHasEntity extends BaseEconomyHasEntity
{
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}


	/**
	 * 	The getEntityIDsByEconomyID method searches the primary key of the economy
	 * 	on the EconomyHasEntity table and then gets the primary keys of the Entities
	 * 	associated with that Economy.
	 * 	
	 * 	@param  int    $economyID 	Represents the primary key of the Economy.
	 *  @return array<int>  	    Represents the primary keys of the Entities associated
	 *                              with the specific economy primary key.
	 */
	public static function getEntityIDsByEconomyID($economyID)
	{
		$entityIDs = self::model()->findAll(array(
			'select' => 'entity_id',
			'condition' => 'economy_id=:economyID',
			'params' => array(':economyID' => $economyID)
		));

		return $entityIDs;
	}

	/**
	 * 	The getEntityIDsByEconomyID method searches the primary key of the economy
	 * 	on the EconomyHasEntity table and then gets the primary keys of the Entities
	 * 	associated with that Economy. It then runs through all of the entities to find
	 * 	the ones that have a similar entity type associated with their id, and adds
	 * 	that entity id on to the array. It then returns that array.
	 * 	
	 * 	@param  int    $economyID 	Represents the primary key of the Economy.
	 * 	@param  String $entityType  Represnts the type of the entity.
	 *  @return array<int>  	    Represents the Entities id associated with the 
	 *                              specific economy primary key, and entity type.
	 */
	public static function getEntityIDsByEconomyIDAndEntityType($economyID, $entityType)
	{
		$entityIDs = self::model()->findAll(array(
			'select' => 'entity_id',
			'condition' => 'economy_id=:economyID',
			'params' => array(':economyID' => $economyID)
		));

		$entityIDList = array();
		for ($i = 0; $i < sizeof($entityIDs); $i++)
		{
			if (Entity::getEntityTypeByEntityID($entityIDs[$i]->entity_id) == $entityType)
			{
				$entityIDList[] = $entityIDs[$i]->entity_id;
			}
		}
		return $entityIDList;
	}

 	/**
 	 * 	The getEconomyIDsByEntityID method searches the primary key of the entity
	 * 	on the EconomyHasEntity table and then gets the primary keys of the Economies
	 * 	associated with that Entity.
	 * 	
	 * 	@param  int    $entityID   	Represents the primary key of the Entity.
	 *  @return array<int>  	    Represents the primary keys of the Economy
	 *                              Relation associated with the specific user primary key.
	 */
	public static function getEconomyIDsByEntityID($entityID)
	{
		$economyIDs = self::model()->findAll(array(
			'select' => 'economy_id',
			'condition' => 'entity_id=:entityID',
			'params' => array('entityID' => $entityID)
		));

		return $economyIDs;
	}

	/**
	 * Checks if there is a relation between a specific economy and entity.
	 * 
	 * @param  String  $economyID     Represents the economy ID.
	 * @param  String  $entityID      Represents the entity ID.
	 * @return boolean                Whether the economy has the entity.
	 */
	public static function hasRelation($economyID, $entityID)
	{
		return EconomyHasEntity::model()->exists(array(
			'condition' => 'economy_id=:economyID AND entity_id=:entityID',
			'params' => array(
				':economyID' => $economyID,
				':entityID' => $entityID,
			),
		));
	}
}
