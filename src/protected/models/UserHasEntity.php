<?php

Yii::import('application.models._base.BaseUserHasEntity');

/**
 * The UserHasEntity class is there to represent every bit of detail when it comes to 
 * the relation of the Entities and Entitys. This relation only goes as deep as both the
 * primary keys of the User and Entity. So the only methods for this class are report
 * methods that get either all the Entity ids for the User type or vice versa.
 * 
 * @author   <cmicklis@stetson.edu>, <jsalis@stetson.edu>
 * @since 	 v0.0.0
 * 
 */

class UserHasEntity extends BaseUserHasEntity
{
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * 	The getEntityIDsByUserID method searches the primary key of the enity
	 * 	on the UserHasEntity table and then gets the primary keys of the Entitys
	 * 	associated with that User.
	 * 	
	 * 	@param  int    $userID 		Represents the primary key of the User.
	 *  @return array<int>  	    Represents the primary keys of the Entitys associated
	 *                              with the specific User type primary key.
	 */
	public static function getEntityIDsByUserID($userID)
	{
		$entityIDs = UserHasEntity::model()->findAll(array(
			'select' => 'entity_id',
			'condition' => 'user_id=:userID',
			'params' => array('userID' => $userID)
		));
		
		$entityIDList = array();
		for ($i = 0; $i < sizeof($entityIDs); $i++)
		{
			$entityIDList[] = $entityIDs[$i]->entity_id;
		}
		return $entityIDList;
	}

 	/**
 	 * 	The getUserIDsByEntityID method searches the primary key of the Entity
	 * 	on the UserHasEntity table and then gets the primary keys of the Entities
	 * 	associated with that Entity.
	 * 	
	 * 	@param  int    $entityID   	Represents the primary key of the Entity.
	 *  @return array<int>  	    Represents the primary keys of the User associated
	 *                              with the specific entity primary key.
	 */
	public static function getUserIDsByEntityID($entityID)
	{
		$userIDs = UserHasEntity::model()->findAll(array(
			'select' => 'user_id',
			'condition' => 'entity_id=:entityID',
			'params' => array('entityID' => $entityID)
		));

		$userIDList = array();
		for ($i = 0; $i < sizeof($userIDs); $i++)
		{
			$userIDList[] = $userIDs[$i]->user_id;
		}
		return $userIDList;
	}

	/**
	 * Checks if there is a relation between a specific user and entity.
	 * 
	 * @param  String  $userID     Represents the user ID.
	 * @param  String  $entityID   Represents the entity ID.
	 * @return boolean             Whether the user has the entity.
	 */
	public static function hasRelation($userID, $entityID)
	{
		return UserHasEntity::model()->exists(array(
			'condition' => 'user_id=:userID AND entity_id=:entityID',
			'params' => array(
				':userID' => $userID,
				':entityID' => $entityID,
			),
		));
	}

	/**
	 * Deletes a relation between a specific user and entity.
	 * 
	 * @param  String  $userID     Represents the user ID.
	 * @param  String  $entityID   Represents the entity ID.
	 */
	public static function deleteRelation($userID, $entityID)
	{
		UserHasEntity::model()->deleteAll(array(
			'condition' => 'user_id=:userID AND entity_id=:entityID',
			'params' => array(
				':userID' => $userID,
				':entityID' => $entityID,
			),
		));
	}

	/**
	 * Deletes all relations to the specified entity.
	 * 
	 * @param  String  $entityID   Represents the entity ID.
	 */
	public static function deleteRelationsToEntity($entityID)
	{
		UserHasEntity::model()->deleteAll(array(
			'condition' => 'entity_id=:entityID',
			'params' => array(':entityID' => $entityID),
		));
	}
}
