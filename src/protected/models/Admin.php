<?php

/**
 * The Admin class is there to represent the functionality that the admin
 * user should have. That is the admin should have total control over all
 * possibly existing economies. The Admin has total CRUD (create read update
 * remove) to everything, and therefore defines the entity of the user (and
 * therefore the only entity) as Accessor, which has rights to every table.
 *
 * @author   <cmicklis@stetson.edu>, <jsalis@stetson.edu>
 * @since 	 v0.0.0
 */
class Admin extends Teacher
{
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	public function defaultScope()
	{
		// only read records that are of user type Admin
		return array(
			'with' => array('userType' => array('select' => false)),
			'condition' => 'user_type=:type',
			'params' => array(':type' => UserType::USER_TYPE_ADMIN_NAME),
		);
    }

    /**
	 *  Sets the current economy. This overrides the function in the User class
	 *  so any economy can be selected.
	 * 
	 *  @param  int     $economyID   The economy ID to select.
	 *  @return boolean              Whether the economy was selected.
	 */
	public function selectEconomy($economyID)
	{
		Yii::app()->user->economyID = $economyID;
		return true;
	}

	/**
	 *  Gets a list of all economies.
	 *  
	 *  @return array<Economy>
	 */
	public function getAllEconomies()
	{
		return Economy::model()->findAll();
	}

	/**
	 * 	The removeUser method is there to remove a row in the User table defined 
	 *  by the id of the user which is the primary key of the User table.
	 * 
	 *  @param  int     $userID 	  Represents the user ID.
	 */
	public function removeUser($userID)
	{

	}

	/**
	 * 	The removeEconomy method is there to remove a row in the Economy table defined 
	 *  by the id of the user which is the primary key of the Economy table.
	 * 
	 *  @param  int     $economyID 	  Represents the economy ID.
	 */
	public function removeEconomy($economyID)
	{

	}
}
