<?php

Yii::import('application.models._base.BaseItemType');

/**
 * The ItemType class is there to represent all of the necessary interction that is
 * needed by the Item class to get information about the ItemType. It gives methods
 * in which one can access anything from the ItemType table using only the primary
 * key of the table and the name of the row (since each row is baisically unique).
 *
 * @author   <cmicklis@stetson.edu>
 * @since 	 v0.0.0
 */

class ItemType extends BaseItemType
{
	const ITEM_TYPE_FOOD_NAME = 'Food';
	const ITEM_TYPE_NONFOOD_NAME = 'Non-Food';
	const ITEM_TYPE_LOAN_NAME = 'Loan';
	const ITEM_TYPE_DEGREE_NAME = 'Degree';
	const ITEM_TYPE_MACHINE_NAME = 'Machine';
	const ITEM_TYPE_LAND_NAME = 'Land';
	const ITEM_TYPE_PUBLIC_GOOD_NAME = 'Public Good';

	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * 	The getItemTypeByItemTypeID method searches the primary key of the itemtype
	 * 	on the ItemType table and then gets the name of that Item type.
	 * 	
	 * 	@param  int    $itemTypeID 	Represents the primary key of the Item type.
	 *  @return String        		Represents the name on the ItemType table.
	 */
	public static function getItemTypeByItemTypeID($itemTypeID)
	{
		$result = ItemType::model()->find(array(
			'select' => 'item_type',
			'condition' => 'item_type_id=:itemTypeID',
			'params' => array(':itemTypeID' => $itemTypeID)
		));
		return $result->item_type;
	}

	/**
	 * 	The getItemTypeIDByItemType method searches the name of the Itemtype
	 * 	on the ItemType table and then gets the primary key of that Item type.
	 * 	
	 * 	@param  String $itemType 	Represents the name of the Item type.
	 *  @return int           			Represents the primary key on the
	 *                                  ItemType table.
	 */
	public static function getItemTypeIDByItemType($itemType)
	{
		$result = ItemType::model()->find(array(
			'select' => 'item_type_id',
			'condition' => 'item_type=:itemType',
			'params' => array(':itemType' => $itemType)
		));
		return $result->item_type_id;
	}
}
