<?php

Yii::import('application.models._base.BaseItemHasRule');

/**
 * The ItemHasRule class is there to represent every bit of detail when it comes to 
 * the relation of the Entities and Rules. This relation only goes as deep as both the
 * primary keys of the Item and Rule. So the only methods for this class are report
 * methods that get either all the Rule ids for the item type or vice versa.
 * 
 * @author   <cmicklis@stetson.edu>, <jsalis@stetson.edu>
 * @since 	 v0.0.0
 * 
 */

class ItemHasRule extends BaseItemHasRule
{
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * 	Creates a new rule associated with the item to the ItemHasRule table. 
	 * 	This allows the Item to have a relation to one or more rules.
	 * 	
	 *  @param int      $itemID 	Represents the item ID.
	 *  @param int      $ruleID 	Represents the rule ID.
	 *  @return ItemHasRule 		The new ItemHasRule object.
	 */
	public static function createItemHasRule($itemID, $ruleID)
	{
		$itemHasRule = new ItemHasRule;
		$itemHasRule->item_id = $itemID;
		$itemHasRule->rule_id = $ruleID;
		$itemHasRule->save();

		return $itemHasRule;
	}
}
