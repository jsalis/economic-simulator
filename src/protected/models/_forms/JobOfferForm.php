<?php

/**
 * The job offer form handles validation of job offer creation and revision.
 * 
 * @author   John Salis <jsalis@stetson.edu>
 */
class JobOfferForm extends CFormModel
{
	public $content = '';

	public $previous_message_id;
	public $message_type;
	public $receiver_id;
	public $item_id;
	public $payment;

	/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
		return array(
			array('item_id', 'required', 'on' => 'create'),
			array('previous_message_id, message_type', 'required', 'on' => 'reply'),
			array('payment, receiver_id', 'required'),
			array('payment', 'inRange'),
			array('payment', 'canAfford'),
			array('content', 'safe'),
		);
	}

	/**
	 * This validator verifies that the payment is in a valid range.
	 */
	public function inRange()
	{
		$max = Rule::findEconomyRule(Yii::app()->user->economyID, RuleType::MAX_VALUE, 'Wage')->getRuleValue();
		$min = Rule::findEconomyRule(Yii::app()->user->economyID, RuleType::MIN_VALUE, 'Wage')->getRuleValue();

		if ($this->payment > $max)
		{
			$this->addError('payment', "Payment cannot be greater than {$max}.");
		}

		if ($this->payment < $min)
		{
			$this->addError('payment', "Payment cannot be lower than {$min}.");
		}
	}

	/**
	 * This validator verifies that the company can afford to pay the worker.
	 */
	public function canAfford()
	{
		if ($this->message_type == MessageType::MESSAGE_TYPE_ACCEPT_JOB_OFFER_NAME)
		{
			$receiverType = Entity::model()->findByPk($this->receiver_id)->getEntityType();
			$rootMessage = Message::model()->findByPk($this->previous_message_id)->getRootMessage();
			$balance = $rootMessage->entityHasMessages[0]->entitySender->getEntityBalance();

			if ($this->payment > $balance)
			{
				if ($receiverType == EntityType::ENTITY_TYPE_WORKER_NAME)
				{
					$this->addError('payment', 'Not enough money to pay the worker.');
				}
				else
				{
					$this->addError('payment', 'Company is unable to pay you this amount.');
				}
			}
		}
	}

	/**
	 * Declares the attribute labels.
	 */
	public function attributeLabels()
	{
		return array(
			'receiver_id' => 'Receiver',
			'item_id' => 'Product',
			'payment' => 'Payment',
		);
	}

	/**
	 * Gets an array of the attributes that can be appended to a message.
	 */
	public function getAppend()
	{
		$append = array(
			Message::MESSAGE_KEY_PREVIOUS_MESSAGE => $this->previous_message_id,
			Message::MESSAGE_KEY_ITEM_ID => $this->item_id,
			Message::MESSAGE_KEY_MONEY => $this->payment,
			Message::MESSAGE_KEY_CONTENT => $this->content,
		);
		
		foreach ($append as $key => $val)
		{
			if (empty($val))
			{
				unset($append[$key]);
			}
		}
		return $append;
	}
}
