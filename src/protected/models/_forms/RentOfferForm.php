<?php

/**
 * The rent offer form handles validation of rent offer creation and revision.
 * 
 * @author   John Salis <jsalis@stetson.edu>
 */
class RentOfferForm extends CFormModel
{
	public $content = '';

	public $previous_message_id;
	public $message_type;
	public $receiver_id;
	public $item_id;
	public $price;

	/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
		return array(
			array('item_id', 'required', 'on' => 'create'),
			array('previous_message_id, message_type, receiver_id', 'required', 'on' => 'reply'),
			array('price', 'required'),
			array('price', 'inRange'),
			array('content', 'safe'),
		);
	}

	/**
	 * This validator verifies that the price is in a valid range.
	 */
	public function inRange()
	{
		$max = Rule::findEconomyRule(Yii::app()->user->economyID, RuleType::MAX_VALUE, 'Rent')->getRuleValue();
		$min = Rule::findEconomyRule(Yii::app()->user->economyID, RuleType::MIN_VALUE, 'Rent')->getRuleValue();

		if ($this->price > $max)
		{
			$this->addError('price', "Price cannot be greater than {$max}.");
		}

		if ($this->price < $min)
		{
			$this->addError('price', "Price cannot be lower than {$min}.");
		}
	}

	/**
	 * Declares the attribute labels.
	 */
	public function attributeLabels()
	{
		return array(
			'receiver_id' => 'Receiver',
			'item_id' => 'Item',
			'price' => 'Price',
		);
	}

	/**
	 * Gets an array of the attributes that can be appended to a message.
	 */
	public function getAppend()
	{
		$append = array(
			Message::MESSAGE_KEY_PREVIOUS_MESSAGE => $this->previous_message_id,
			Message::MESSAGE_KEY_ITEM_ID => $this->item_id,
			Message::MESSAGE_KEY_MONEY => $this->price,
			Message::MESSAGE_KEY_CONTENT => $this->content,
		);
		
		foreach ($append as $key => $val)
		{
			if (empty($val))
			{
				unset($append[$key]);
			}
		}
		return $append;
	}
}
