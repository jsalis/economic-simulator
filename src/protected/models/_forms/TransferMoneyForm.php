<?php

/**
 * The transfer money form handles validation of money transfers between the entities 
 * of a single student.
 * 
 * @author   John Salis <jsalis@stetson.edu>
 */
class TransferMoneyForm extends CFormModel
{
	public $sender_id;
	public $receiver_id;
	public $amount;

	/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
		return array(
			array('sender_id, receiver_id, amount', 'required'),
			array('amount', 'inRange'),
		);
	}

	/**
	 * This validator verifies that the amount is in a valid range.
	 */
	public function inRange()
	{
		$maxAmount = Entity::model()->findByPk($this->sender_id)->getEntityBalance();

		if ($this->amount > $maxAmount)
		{
			$this->addError('amount', "Amount cannot be greater than {$maxAmount}.");
		}

		if ($this->amount <= 0)
		{
			$this->addError('amount', "Amount must be greater than zero.");
		}
	}

	/**
	 * Declares the attribute labels.
	 */
	public function attributeLabels()
	{
		return array(
			'sender_id' => 'Sender',
			'receiver_id' => 'Receiver',
			'amount' => 'Amount',
		);
	}
}
