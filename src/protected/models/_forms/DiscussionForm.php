<?php

/**
 * The discussion form handles validation of discussion posts and replies.
 * 
 * @author   John Salis <jsalis@stetson.edu>
 */
class DiscussionForm extends CFormModel
{
	public $receiver_id;
	public $subject;
	public $content;
	public $previous_message_id;

	/**
	 * Initializes form attributes.
	 */
	public function init()
	{
		$this->receiver_id = Economy::model()->getPublicMessengerID(Yii::app()->user->economyID);
	}

	/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
		return array(
			
			array('receiver_id, content', 'required'),
			array('subject', 'required', 'on' => 'create'),
			array('previous_message_id', 'required', 'on' => 'reply'),
			array('subject', 'length', 'max' => 40),
			array('content', 'length', 'max' => 200),
			array('receiver_id', 'unsafe'),
		);
	}

	/**
	 * Declares the attribute labels.
	 */
	public function attributeLabels()
	{
		return array(
			'subject' => 'Subject',
			'content' => 'Message',
		);
	}

	/**
	 * Gets an array of the attributes that can be appended to a message.
	 */
	public function getAppend()
	{
		$append = array(
			Message::MESSAGE_KEY_SUBJECT => $this->subject,
			Message::MESSAGE_KEY_CONTENT => $this->content,
			Message::MESSAGE_KEY_PREVIOUS_MESSAGE => $this->previous_message_id,
		);
		
		foreach ($append as $key => $val)
		{
			if (empty($val))
			{
				unset($append[$key]);
			}
		}
		return $append;
	}
}
