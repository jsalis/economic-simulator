<?php

/**
 * The product form handles validation of creating and updating of company products.
 * 
 * @author   John Salis <jsalis@stetson.edu>
 */
class ProductForm extends CFormModel
{
	public $item_id;
	public $name;
	public $type;
	public $description;
	public $price;
	public $economy_id;

	/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
		return array(
			array('item_id', 'required', 'on' => 'update'),
			array('name, type, description, price, economy_id', 'required'),
			array('name', 'length', 'max' => 45),
			array('description', 'length', 'max' => 255),
			array('price', 'inRange'),
		);
	}

	/**
	 * This validator verifies that the price is in a valid range.
	 */
	public function inRange()
	{
		$attribute = $this->type . ' Price';

		$max = Rule::findEconomyRule($this->economy_id, RuleType::MAX_VALUE, $attribute)->getRuleValue();
		$min = Rule::findEconomyRule($this->economy_id, RuleType::MIN_VALUE, $attribute)->getRuleValue();

		if ($this->price > $max)
		{
			$this->addError('price', "Price cannot be greater than {$max}.");
		}

		if ($this->price < $min)
		{
			$this->addError('price', "Price cannot be lower than {$min}.");
		}
	}

	/**
	 * Declares the attribute labels.
	 */
	public function attributeLabels()
	{
		return array(
			'name' => 'Product Name',
			'description' => 'Description',
			'price' => 'Price',
			'economy_id' => 'Export Economy',
		);
	}
}
