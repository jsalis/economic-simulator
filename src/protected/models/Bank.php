<?php

/**
 * The Bank class's purpose is to represent the actions of a bank
 * in the form of an entity. So that the bank has all of the access
 * to all of the loans that its created, and also access to all of 
 * the rules associated with the created loans.
 *
 * @author   <cmicklis@stetson.edu>, <jsalis@stetson.edu>
 * @since 	 v0.0.0
 */
class Bank extends Entity
{
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

    public function defaultScope()
	{
		// only read records that are of entity type Bank
		return array(
			'with' => array('entityType' => array('select' => false)),
			'condition' => 'entity_type=:type',
			'params' => array(':type' => EntityType::ENTITY_TYPE_BANK_NAME),
		);
    }
}
