<?php

Yii::import('application.models._base.BaseEntityTypeHasRule');

/**
 * The EntityTypeHasRule class is there to represent every bit of detail when it comes to 
 * the relation of the Entities and Rules. This relation only goes as deep as both the
 * primary keys of the EntityType and Rule. So the only methods for this class are report
 * methods that get either all the Rule ids for the entityType or vice versa.
 * 
 * @author   <cmicklis@stetson.edu>, <jsalis@stetson.edu>
 * @since 	 v0.0.0
 * 
 */

class EntityTypeHasRule extends BaseEntityTypeHasRule
{
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * 	The createEntityTypeHasRule method adds a rule associated with the entity type to the
	 * 	EntityTypeHasRule table. This allows the Entity Type to have a relation to an rule, or
	 * 	multiple rules.
	 *
	 *  @param int      $economyID       Represents the economy ID.
	 *  @param int      $entityTypeID    Represents the entity type ID.
	 *  @param int      $ruleID          Represents the rule ID.
	 */
	public static function createEntityTypeHasRule($economyID, $entityTypeID, $ruleID)
	{
		$entityTypeHasRule = new EntityTypeHasRule;
		$entityTypeHasRule->economy_id = $economyID;
		$entityTypeHasRule->entity_type_id = $entityTypeID;
		$entityTypeHasRule->rule_id = $ruleID;
		$entityTypeHasRule->save();

		return $entityTypeHasRule;
	}
}
