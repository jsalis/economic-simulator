<?php

/**
 * The Student class is there to define all of the functionality
 * that a student user may have. These rights are specific to their
 * entities and the economies that their entities are in. This means
 * that the Student class only gives students the rights to Worker,
 * Company, Goverment, and Bank controllers which give them access
 * to whatever those controllers have access to.
 * 
 * @author   <cmicklis@stetson.edu>, <jsalis@stetson.edu>
 * @since 	 v0.0.0
 */
class Student extends User
{
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	public function defaultScope()
	{
		// only read records that are of user type Student
		return array(
			'with' => array('userType' => array('select' => false)),
			'condition' => 'user_type=:type',
			'params' => array(':type' => UserType::USER_TYPE_STUDENT_NAME),
		);
    }

	/**
	 * 	Sends a message to the public messenger of the economy that the user is 
	 * 	logging into, stating that the user has entered that respective economy.
	 */
	public function enteredEconomy()
	{
		$entity = Entity::model()->findByPk(Yii::app()->user->roleID);

		$exists = EntityHasMessage::model()
			->messageType( MessageType::MESSAGE_TYPE_ENTERED_ECONOMY )
			->sender( $entity->getEntityID() )
			->lowerDate( DateUtility::getResetYesterday(Yii::app()->user->economyID) )
			->exists();

		if (!$exists)
		{
			$publicMessengerID = Economy::model()->getPublicMessengerID(Yii::app()->user->economyID);
			
			$entity->sendMessage($publicMessengerID, MessageType::MESSAGE_TYPE_ENTERED_ECONOMY, array(
				Message::MESSAGE_KEY_CONTENT => $entity->getEntityName() . ' has entered the economy.',
			));
		}
	}	
}
