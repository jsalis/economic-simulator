<?php

Yii::import('application.models._base.BaseEntityHasMessage');

/**
 * The EntityHasMessage class is there to represent every bit of detail when it comes to 
 * the relation of the Entities and Messages. This relation goes as deep to go and get all
 * of the messages by not only the receiver and the sender but also vice versa.
 * 
 * @author   <cmicklis@stetson.edu>, <jsalis@stetson.edu>
 * @since 	 v0.0.0
 * 
 */

class EntityHasMessage extends BaseEntityHasMessage
{
	const MESSAGE_CHECKED = 1;
	const MESSAGE_REPLIED = 1;
	const MESSAGE_NOT_CHECKED = 0;
	const MESSAGE_NOT_REPLIED = 0;
	
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * 	The createEntityHasMessage method adds a EntityHasMessage using all information given 
	 * 	into the EntityHasMessage table.
	 * 	
	 *  @param int      $messageID         Represents the message id.
	 *  @param int      $entityReceiverID  Represents the entity of the receiver.
	 *  @param int      $entitySenderID    Represents the entity of the sender.
	 *  @return int 				   	   Represents the EntityHasMessage primary key.
	 */
	public static function createEntityHasMessage($messageID, $entityReceiverID, $entitySenderID)
	{
		$entityHasMessage = new EntityHasMessage;
		$entityHasMessage->entity_sender_id = $entitySenderID;
		$entityHasMessage->entity_receiver_id = $entityReceiverID;
		$entityHasMessage->message_id = $messageID;
		$entityHasMessage->message_checked = EntityHasMessage::MESSAGE_NOT_CHECKED;
		$entityHasMessage->message_replied = EntityHasMessage::MESSAGE_NOT_REPLIED;
		$entityHasMessage->date_received = DateUtility::getCurrentDate(Yii::app()->user->economyID);
		$entityHasMessage->save();

		return $entityHasMessage->entity_has_message_id;
	}

	public static function checkMessage($id)
	{
		EntityHasMessage::model()->updateByPK($id, array(
			'message_checked' => EntityHasMessage::MESSAGE_CHECKED,
		));
	}

	public static function replyMessage($id)
	{
		EntityHasMessage::model()->updateByPK($id, array(
			'message_replied' => EntityHasMessage::MESSAGE_REPLIED,
		));
	}

	/**
		 _______  _______  _______  _______  _______ _________ _______ 
		(  ____ )(  ____ \(  ____ )(  ___  )(  ____ )\__   __/(  ____ \
		| (    )|| (    \/| (    )|| (   ) || (    )|   ) (   | (    \/
		| (____)|| (__    | (____)|| |   | || (____)|   | |   | (_____ 
		|     __)|  __)   |  _____)| |   | ||     __)   | |   (_____  )
		| (\ (   | (      | (      | |   | || (\ (      | |         ) |
		| ) \ \__| (____/\| )      | (___) || ) \ \__   | |   /\____) |
		|/   \__/(_______/|/       (_______)|/   \__/   )_(   \_______)
		                                                               
	 */
	
	public function scopes()
    {
        return array(
            'chat' => array(
            	'with' => array(
            		'message' => array('select' => '*'),
            		'message.messageType' => array('select' => false),
            	),
                'condition' => 'message_type = :chat',
                'params' => array(':chat' => MessageType::MESSAGE_TYPE_CHAT_NAME),
            ),
            'notChat' => array(
            	'with' => array(
            		'message' => array('select' => '*'),
            		'message.messageType' => array('select' => false),
            	),
                'condition' => 'message_type != :chat',
                'params' => array(':chat' => MessageType::MESSAGE_TYPE_CHAT_NAME),
            ),
        );
    }

    public function message($messageID)
	{
	    $this->getDbCriteria()->mergeWith(array(
        	'condition' => 'message_id = :messageID',
        	'params' => array(':messageID' => $messageID),
        ));
	    return $this;
	}

	public function previousMessage($messageID)
	{
		$match = Message::MESSAGE_KEY_PREVIOUS_MESSAGE . $messageID;
		$this->getDbCriteria()->mergeWith(array(
			'with' => array('message'),
			'condition' => 'message.message LIKE :match',
			'params' => array(':match' => "%$match%")
		));
		return $this;
	}

    public function messageType($messageType)
	{
	    $this->getDbCriteria()->mergeWith(array(
	    	'with' => array(
        		'message' => array('select' => '*'),
        		'message.messageType' => array('select' => false),
        	),
	        'condition' => 'message_type = :messageType',
	        'params' => array(':messageType' => $messageType),
	    ));
	    return $this;
	}

	public function checked($isChecked)
	{
	    $this->getDbCriteria()->mergeWith(array(
        	'condition' => 'message_checked = :isChecked',
        	'params' => array(':isChecked' => $isChecked),
        ));
	    return $this;
	}

	public function replied($isReplied)
	{
	    $this->getDbCriteria()->mergeWith(array(
        	'condition' => 'message_replied = :isReplied',
        	'params' => array(':isReplied' => $isReplied),
        ));
	    return $this;
	}

	public function upperDate($upperDate)
	{
	    $this->getDbCriteria()->mergeWith(array(
        	'condition' => 'date_received < :upperDate',
        	'params' => array(':upperDate' => $upperDate),
        ));
	    return $this;
	}

	public function lowerDate($lowerDate)
	{
	    $this->getDbCriteria()->mergeWith(array(
        	'condition' => 'date_received >= :lowerDate',
        	'params' => array(':lowerDate' => $lowerDate),
        ));
	    return $this;
	}

	public function sender($senderID)
	{
	    $this->getDbCriteria()->mergeWith(array(
        	'condition' => 'entity_sender_id=:senderID',
        	'params' => array(':senderID' => $senderID),
        ));
	    return $this;
	}

	public function receiver($receiverID)
	{
	    $this->getDbCriteria()->mergeWith(array(
        	'condition' => 'entity_receiver_id=:receiverID',
        	'params' => array(':receiverID' => $receiverID),
        ));
	    return $this;
	}

	public function senderOrReceiver($senderID, $receiverID) // TO DO : Change to one parameter.
	{
	    $this->getDbCriteria()->mergeWith(array(
        	'condition' => '(entity_sender_id=:senderID || entity_receiver_id=:receiverID)',
        	'params' => array(':senderID' => $senderID, ':receiverID' => $receiverID),
        ));
	    return $this;
	}

	public function economy($economyID)
	{
	    $this->getDbCriteria()->mergeWith(array(
	    	'with' => array(
	    		'entitySender.economyHasEntities' => array('alias' => 'r1'),
	    		'entityReceiver.economyHasEntities' => array('alias' => 'r2'),
	    	),
        	'condition' => '(r1.economy_id = :economyID || r2.economy_id = :economyID)',
        	'params' => array(':economyID' => $economyID),
        ));
	    return $this;
	}

	/**
	 *  Gets the date of the most recent occurrence of a message type sent to
	 *  or received by a specific entity. If no messages are found, then FIRST_DATE 
	 *  is returned as a DateTime object.
	 * 
	 *  @param   String    $messageType  The name of the message type.
	 *  @param   int       $entityID     The ID of the sender or receiver entity.
	 *  @return  DateTime                An object representing the date of the most recent message.
	 */
	public static function getMostRecentMessageDate($messageType, $entityID)
	{
		$mostRecentDate = DateTime::createFromFormat('Y-m-d', Economy::FIRST_DATE);
		$messageList = EntityHasMessage::model()
			->messageType( $messageType )
			->senderOrReceiver( $entityID, $entityID )
			->findAll();
		
		for ($i = 0; $i < sizeof($messageList); $i++)
		{
			$tempDate = $messageList[$i]->getDateReceived();
			if ($tempDate > $mostRecentDate)
			{
				$mostRecentDate = $tempDate;
			}
		}
		return $mostRecentDate;
	}

	/**
		 _______  _______ _________          _______  ______   _______ 
		(       )(  ____ \\__   __/|\     /|(  ___  )(  __  \ (  ____ \
		| () () || (    \/   ) (   | )   ( || (   ) || (  \  )| (    \/
		| || || || (__       | |   | (___) || |   | || |   ) || (_____ 
		| |(_)| ||  __)      | |   |  ___  || |   | || |   | |(_____  )
		| |   | || (         | |   | (   ) || |   | || |   ) |      ) |
		| )   ( || (____/\   | |   | )   ( || (___) || (__/  )/\____) |
		|/     \|(_______/   )_(   |/     \|(_______)(______/ \_______)
	                                                                                                     
	 */
	
	public function getEntityMessageID()
	{
		return $this->entity_has_message_id;
	}

	public function getEntitySenderID()
	{
		return $this->entity_sender_id;
	}

	public function getEntityReceiverID()
	{
		return $this->entity_receiver_id;
	}

	public function getMessage()
	{
		return $this->message;
	}

	public function getMessageChecked()
	{
		return $this->message_checked == EntityHasMessage::MESSAGE_CHECKED;
	}

	public function getMessageReplied()
	{
		return $this->message_replied == EntityHasMessage::MESSAGE_REPLIED;
	}

	public function getDateReceived()
	{
		return DateTime::createFromFormat('Y-m-d H:i:s', $this->date_received);
	}
}
