<?php

Yii::import('application.models._base.BaseEntityRentEntityHasItem');

/**
 * The EconomyHasEntity class is there to represent every bit of detail when it comes to 
 * the relation of the Enties renting items that already have ownership. This includes the
 * time period of the renting status, if that renting period is over, the entity_has_item
 * ID which states the owner of the item and the item itself, the entity borrowing that item,
 * and the daily price the entity will be renting that item for.
 * 
 * @author   <cmicklis@stetson.edu>, <jsalis@stetson.edu>
 * @since 	 v0.0.0
 * 
 */

class EntityRentEntityHasItem extends BaseEntityRentEntityHasItem
{
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * 	Creates a new EntityRentEntityHasItem using all the information given.
	 * 	
	 *  @param  int      $entityID          Represents the entity ID.
	 *  @param  int      $entityItemID      Represents the ID of the entityHasItem.
	 *  @param  int      $pricePerDay       Represents the price per day in dollars.
	 *  @param  int      $amountOfDays      Represents the renting period.
	 *  @return EntityRentEntityHasItem 	Represents the new EntityRentEntityHasItem object.
	 */
	public static function createEntityRentEntityHasItem($entityID, $entityHasItemID, $pricePerDay, $amountOfDays)
	{
		$entityRentEntityHasItem = new EntityRentEntityHasItem;
		$entityRentEntityHasItem->entity_id = $entityID;
		$entityRentEntityHasItem->entity_has_item_id = $entityHasItemID;
		$entityRentEntityHasItem->price_per_day = floor($pricePerDay * Economy::CENTS_PER_DOLLAR);
		
		$currentDate = DateUtility::getCurrentDate(Yii::app()->user->economyID);
		$endDate = DateUtility::getResetDateByBeginningAndDayOffset($currentDate, $amountOfDays, Yii::app()->user->economyID);

		$entityRentEntityHasItem->beginning_date = $currentDate;
		$entityRentEntityHasItem->end_date = $endDate;
		$entityRentEntityHasItem->save();

		return $entityRentEntityHasItem;
	}

	/**
		 _______  _______  _       __________________ _        _______    _______  _______  _______  _______  _______ _________ _______ 
		(  ____ )(  ____ \( (    /|\__   __/\__   __/( (    /|(  ____ \  (  ____ )(  ____ \(  ____ )(  ___  )(  ____ )\__   __/(  ____ \
		| (    )|| (    \/|  \  ( |   ) (      ) (   |  \  ( || (    \/  | (    )|| (    \/| (    )|| (   ) || (    )|   ) (   | (    \/
		| (____)|| (__    |   \ | |   | |      | |   |   \ | || |        | (____)|| (__    | (____)|| |   | || (____)|   | |   | (_____ 
		|     __)|  __)   | (\ \) |   | |      | |   | (\ \) || | ____   |     __)|  __)   |  _____)| |   | ||     __)   | |   (_____  )
		| (\ (   | (      | | \   |   | |      | |   | | \   || | \_  )  | (\ (   | (      | (      | |   | || (\ (      | |         ) |
		| ) \ \__| (____/\| )  \  |   | |   ___) (___| )  \  || (___) |  | ) \ \__| (____/\| )      | (___) || ) \ \__   | |   /\____) |
		|/   \__/(_______/|/    )_)   )_(   \_______/|/    )_)(_______)  |/   \__/(_______/|/       (_______)|/   \__/   )_(   \_______)
		                                                                                                                                
	 */
	
	/**
	 * 	Finds all EntityRentEntityHasItem objects associated with the specified 
	 * 	renter entity that are within the rent period.
	 * 	
	 * 	@param  int    $entityID 					The ID of the entity renter.
	 *  @return array<EntityRentEntityHasItem> 		An array of EntityRentEntityHasItem objects.
	 */
	public static function findByEntityID($entityID)
	{
		$currentDate = DateUtility::getCurrentDate(Yii::app()->user->economyID);

		return EntityRentEntityHasItem::model()->findAll(array(
			'with' => 'entityHasItem',
			'condition' => 't.entity_id=:entityID && beginning_date<=:currentDate && end_date>=:currentDate',
			'params' => array(
				':entityID' => $entityID,
				':currentDate' => $currentDate,
			),
		));
	}

	/**
	 * 	Checks if the specified entityHasItem is currently being rented.
	 * 	item relation.
	 * 	
	 * 	@param  int    $entityHasItemID 	The ID of the entityHasItem relation.
	 *  @return boolean 					Whether the item is currently being rented.
	 */
	public static function isRentInProgress($entityHasItemID)
	{
		$currentDate = DateUtility::getCurrentDate(Yii::app()->user->economyID);

		return EntityRentEntityHasItem::model()->exists(array(
			'condition' => 'entity_has_item_id=:entityHasItemID && beginning_date<=:currentDate && end_date>=:currentDate',
			'params' => array(
				':entityHasItemID' => $entityHasItemID,
				':currentDate' => $currentDate,
			),
		));
	}
	
	/**
		 _______  _______  _       __________________ _        _______    _______  _______ _________          _______  ______   _______ 
		(  ____ )(  ____ \( (    /|\__   __/\__   __/( (    /|(  ____ \  (       )(  ____ \\__   __/|\     /|(  ___  )(  __  \ (  ____ \
		| (    )|| (    \/|  \  ( |   ) (      ) (   |  \  ( || (    \/  | () () || (    \/   ) (   | )   ( || (   ) || (  \  )| (    \/
		| (____)|| (__    |   \ | |   | |      | |   |   \ | || |        | || || || (__       | |   | (___) || |   | || |   ) || (_____ 
		|     __)|  __)   | (\ \) |   | |      | |   | (\ \) || | ____   | |(_)| ||  __)      | |   |  ___  || |   | || |   | |(_____  )
		| (\ (   | (      | | \   |   | |      | |   | | \   || | \_  )  | |   | || (         | |   | (   ) || |   | || |   ) |      ) |
		| ) \ \__| (____/\| )  \  |   | |   ___) (___| )  \  || (___) |  | )   ( || (____/\   | |   | )   ( || (___) || (__/  )/\____) |
		|/   \__/(_______/|/    )_)   )_(   \_______/|/    )_)(_______)  |/     \|(_______/   )_(   |/     \|(_______)(______/ \_______)
		                                                                                                                                
	 */
	
	public function getEntityRentEntityHasItemID()
	{
		return $this->entity_rent_entity_has_item_id;
	}

	public function getEntityID()
	{
		return $this->entity_id;
	}

	public function getEntityHasItemID()
	{
		return $this->entity_has_item_id;
	}

	public function getItem()
	{
		return $this->entityHasItem->item;
	}

	public function getOwner()
	{
		return $this->entityHasItem->entity;
	}

	public function getPricePerDay()
	{
		return $this->price_per_day / Economy::CENTS_PER_DOLLAR;
	}

	public function getBeginningDate()
	{
		// return DateTime::createFromFormat('Y-m-d H:i:s', $this->beginning_date);
		return $this->beginning_date;
	}

	public function getEndDate()
	{
		// return DateTime::createFromFormat('Y-m-d H:i:s', $this->end_date);
		return $this->end_date;
	}
}
