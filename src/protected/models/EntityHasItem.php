<?php

Yii::import('application.models._base.BaseEntityHasItem');

/**
 * The EntityHasItem class is there to represent every bit of detail when it comes to 
 * the relation of the Entities and Items. This relation only goes as deep as both the
 * primary keys of the Entity and Item. So the only methods for this class are report
 * methods that get either all the Item ids for the entity or vice versa.
 * 
 * @author   <cmicklis@stetson.edu>, <jsalis@stetson.edu>
 * @since 	 v0.0.0
 * 
 */

class EntityHasItem extends BaseEntityHasItem
{
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	public static function createEntityHasItem($entityID, $itemID, $pricePaid)
	{
		$entityHasItem = new EntityHasItem;
		$entityHasItem->entity_id = $entityID;
		$entityHasItem->item_id = $itemID;
		$entityHasItem->price_paid = floor($pricePaid * Economy::CENTS_PER_DOLLAR);
		$entityHasItem->date_purchased = DateUtility::getCurrentDate(Yii::app()->user->economyID);
		$entityHasItem->save();

		return $entityHasItem->getID();
	}

	/**

		Reports

	 */
	
	public function economy($economyID)
	{
	    $this->getDbCriteria()->mergeWith(array(
	    	'with' => array('entity.economyHasEntities' => array('select' => false)),
	        'condition' => 'economyHasEntities.economy_id = :economyID',
            'params' => array(':economyID' => $economyID),
	    ));
	    return $this;
	}

	public function upperDate($upperDate)
	{
	    $this->getDbCriteria()->mergeWith(array(
        	'condition' => 't.date_purchased < :upperDate',
        	'params' => array(':upperDate' => $upperDate),
        ));
	    return $this;
	}

	public function lowerDate($lowerDate)
	{
	    $this->getDbCriteria()->mergeWith(array(
        	'condition' => 't.date_purchased >= :lowerDate',
        	'params' => array(':lowerDate' => $lowerDate),
        ));
	    return $this;
	}

	public function itemType($type)
	{
	    $this->getDbCriteria()->mergeWith(array(
	    	'with' => array('item.itemType' => array('select' => false)),
	        'condition' => 'itemType.item_type = :type',
            'params' => array(':type' => $type),
	    ));
	    return $this;
	}

	/**
	 * 	Gets a list of entityHasItem objects related to the specified entity.
	 * 	
	 * 	@param  int    $entityID 		The ID of the entity.
	 * 	@param  int    $itemType 		The type of item.
	 *  @return array<EntityHasItem>  	A list of entityHasItem objects.
	 */
	public static function findByEntityID($entityID, $itemType = null)
	{
		$condition = 'entity_id=:entityID';
		$params = array(':entityID' => $entityID);

		if (!is_null($itemType))
		{
			$condition .= '&& item_type=:itemType';
			$params[':itemType'] = $itemType;
		}

		return EntityHasItem::model()->with('item.itemType')->findAll($condition, $params);
	}

	/**
	 *  Gets a list of entityHasItem objects related to the specified item.
	 * 
	 *  @param  int    $itemID 			The ID of the item.
	 *  @return array<EntityHasItem>  	A list of entityHasItem objects.
	 */
	public static function findByItemID($itemID)
	{
		return EntityHasItem::model()->count(array(
			'condition' => 'item_id=:itemID',
			'params' => array(':itemID' => $itemID),
		));
	}

	/**

		Methods

	 */

	public function getID()
	{
		return $this->entity_has_item_id;
	}

	public function getEntity()
	{
		return $this->entity;
	}

	public function getItem()
	{
		return $this->item;
	}

	public function getPricePaid()
	{
		return $this->price_paid / Economy::CENTS_PER_DOLLAR;
	}

	public function getDatePurchased()
	{
		return DateTime::createFromFormat('Y-m-d H:i:s', $this->date_purchased);
	}
}
