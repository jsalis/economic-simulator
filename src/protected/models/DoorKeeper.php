<?php

/**
 * The DoorKeeper class handles validation of login and signup.
 *
 * @author   <jsalis@stetson.edu>
 * @since 	 v0.0.0
 * 
 */
class DoorKeeper extends CFormModel
{
	public $emailAddress;
	public $password;
	public $rememberMe = false;

	public $userType;
	public $firstName;
	public $lastName;
	public $cellPhone;
	public $passwordRepeat;

	private $_identity;

	/**
	 * Declares the validation rules.
	 * The rules state that emailAddress and password are required, and 
	 * password needs to be authenticated on login.
	 */
	public function rules()
	{
		return array(
			array('emailAddress, password', 'required', 'on' => 'login, signup'),
			array('password', 'authenticate', 'on' => 'login'),
			array('userType, firstName, lastName, cellPhone, passwordRepeat', 'required', 'on' => 'signup'),
			array('password', 'compare', 'compareAttribute' => 'passwordRepeat', 'on' => 'signup'),
			array('emailAddress', 'verifyEmailAddress', 'on' => 'signup'),
			array('rememberMe', 'boolean'),
		);
	}

	/**
	 * Authenticates the password.
	 * This is the 'authenticate' validator as declared in rules().
	 */
	public function authenticate()
	{
		$this->_identity = new UserIdentity($this->emailAddress, $this->password);
		if (!$this->_identity->authenticate())
		{
            $this->addError('password', 'Incorrect email or password.');
        }
	}

	/**
	 * Verifies that the provided email address is unique.
	 * This is the 'verifyEmailAddress' validator as declared in rules().
	 */
	public function verifyEmailAddress()
	{
		if (!User::verifyEmailAddress($this->emailAddress))
		{
			$this->addError('emailAddress', 'An account already exists with this email address.');
		}
	}

	/**
	 * If the form passes validation, the user is logged in using the given email 
	 * and password.
	 * 
	 * @return boolean 	Whether login was successful.
	 */
	public function login()
	{
		if ($this->validate())
		{
			$maxDuration = 3600 * 24 * 30; // 30 days

			$this->_identity->createSession();
			$duration = ($this->rememberMe) ? $maxDuration : 0;
			Yii::app()->user->login($this->_identity, $duration);
			return true;
		}
		else
		{
			return false;
		}
	}

	/**
	 * If the form passes validation, a new user account is created with the given 
	 * information. A unique email address must be provided. Once the User has been
	 * created with the unique email. 
	 *
	 * If the new user is a teacher, then an accessor entity is created and associated
	 * with that user.
	 * 
	 * @return boolean 	Whether user creation was successful.
	 */
	public function signup()
	{
		if ($this->validate())
		{
			$userID = User::createUser($this->emailAddress, $this->firstName, $this->lastName, $this->cellPhone, $this->password, $this->userType);
			if ($this->userType == UserType::USER_TYPE_TEACHER_NAME)
			{
				// Create an accessor for the teacher.
				$entityName = $this->firstName . ' ' . $this->lastName;
				$entityID = Entity::createEntity($entityName, EntityType::ENTITY_TYPE_ACCESSOR_NAME);
				User::createUserHasEntity($userID, $entityID);
			}
			return true;
		}
		else
		{
			return false;
		}
	}
}
