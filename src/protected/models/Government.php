<?php

/**
 * The Goverment class's purpose is to represent the Goverment object
 * in the fashion of an entity so that it has all the rights to creating
 * the proper messages, and receiving them as well. The Goverment also 
 * has rights to all of the rules and as such can create or update any
 * rules that seem fitting.
 *
 * @author   <cmicklis@stetson.edu>, <jsalis@stetson.edu>
 * @since 	 v0.0.0
 */
class Government extends Entity
{
	const VOTE_YES = 'yes';
	const VOTE_NO = 'no';

	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

    public function defaultScope()
	{
		// only read records that are of entity type Government
		return array(
			'with' => array('entityType' => array('select' => false)),
			'condition' => 'entity_type=:type',
			'params' => array(':type' => EntityType::ENTITY_TYPE_GOVERNMENT_NAME),
		);
    }

	/**
	 * [createRulePetition description]
	 * 
	 * @param  int 		$ruleID     	The ID of the rule to change.
	 * @param  string 	$ruleValue  	The new value of the rule.
	 */
	public function createRulePetition($ruleID, $ruleValue)
	{
		$userID = Yii::app()->user->getId();
		$economyID = Yii::app()->user->economyID;
		$worker = Worker::model()->user($userID)->economy($economyID)->find();
		
		$rule = Rule::model()->findByPk($ruleID);
		$receiverID = Economy::model()->getPublicMessengerID(Yii::app()->user->economyID);
		$content = $worker->getEntityName() . ' would like to change ' . $rule->getRuleName() . ' to ' . $ruleValue;

		$worker->sendMessage($receiverID, MessageType::MESSAGE_TYPE_PETITION_FOR_RULE_NAME, array(
			Message::MESSAGE_KEY_CONTENT => $content,
			Message::MESSAGE_KEY_RULE_ID => $ruleID,
			Message::MESSAGE_KEY_RULE_VALUE => $ruleValue,
		));
	}

	/**
	 * 	The createPublicGoodPetition method goes and creates a petition of public good type.
	 *  This means that the petition states that the owner of the type entity is to receive
	 *  a pulblic good company that will allow him to create public goods in the economy that
	 *  the specified entity is in.
	 * 
	 * 	@param  int     $entityID 	 	The ID of the worker to receive a company.
	 */
	public function createPublicGoodPetition($entityID)
	{
		$userID = Yii::app()->user->getId();
		$economyID = Yii::app()->user->economyID;
		$worker = Worker::model()->user($userID)->economy($economyID)->find();

		$receiverID = Economy::model()->getPublicMessengerID(Yii::app()->user->economyID);
		$content = $worker->getEntityName() . ' would like to give ' . Entity::getEntityNameByID($entityID) . ' a public good company.';

		$worker->sendMessage($receiverID, MessageType::MESSAGE_TYPE_PETITION_FOR_PUBLIC_GOOD_NAME, array(
			Message::MESSAGE_KEY_CONTENT => $content,
			Message::MESSAGE_KEY_ENTITY_ID => $entityID,
		));
	}

	/**
	 * 	The createMoneyTransferPetition method goes and creates a petition for an entity, meaning
	 *  that the petition is a proposal to send money from the current goverment over to the
	 *  entity that is described in the petition.
	 * 
	 * 	@param  int     $entityID 	 	The ID of the entity to receive money.
	 * 	@param  float   $money 		 	The amount of money that the entity is to receive.
	 */
	public function createMoneyTransferPetition($entityID, $money)
	{
		$userID = Yii::app()->user->getId();
		$economyID = Yii::app()->user->economyID;
		$worker = Worker::model()->user($userID)->economy($economyID)->find();

		$receiverID = Economy::model()->getPublicMessengerID(Yii::app()->user->economyID);
		$content = $worker->getEntityName() . ' would like to give $' . $money . ' to ' . Entity::getEntityNameByID($entityID);

		$worker->sendMessage($receiverID, MessageType::MESSAGE_TYPE_PETITION_FOR_ENTITY_NAME, array(
			Message::MESSAGE_KEY_CONTENT => $content,
			Message::MESSAGE_KEY_ENTITY_ID => $entityID,
			Message::MESSAGE_KEY_MONEY => $money,
		));
	}

	/**
	 *	The votePetition method votes on the specified petition. If everyone has voted then a 
	 *	decision is made depending on the percentage of agreement votes.
	 *	
	 * 	@param  int     $creatorID  	The ID of the entity that created the petition.
	 * 	@param  int     $petitionID 	The ID of the petition message.
	 * 	@param  String  $vote 		 	The vote choice (agree or disagree).	 
	 */
	public function votePetition($creatorID, $petitionID, $vote)
	{
		$entityPetition = EntityHasMessage::model()->message($petitionID)->find();

		// Check if petition is closed
		if ($entityPetition->getMessageReplied())
		{
			return;
		}

		$userID = Yii::app()->user->getId();
		$economyID = Yii::app()->user->economyID;
		$worker = Worker::model()->user($userID)->economy($economyID)->find();

		$worker->sendMessage($creatorID, MessageType::MESSAGE_TYPE_VOTE_FOR_PETITION_NAME, array(
			Message::MESSAGE_KEY_CONTENT => 'Voted for petition.',
			Message::MESSAGE_KEY_VOTE_RESPONSE => $vote,
			Message::MESSAGE_KEY_PREVIOUS_MESSAGE => $petitionID,
		));

		// Calculate the current win percentage
		$congressCount = User::model()->entity($this->getEntityID())->count();
		$voteData = $this->getVoteCountForPetition($creatorID, $petitionID);
		$currentWinRatio = $voteData['voteAgreeCount'] / $congressCount;

		// Get the required win percentage
		$winRatio = Economy::getEconomyPetitionPassPercentage(Yii::app()->user->economyID);

		if ($currentWinRatio >= $winRatio)
		{
			$this->processPetition($petitionID);
			EntityHasMessage::replyMessage($entityPetition->getEntityMessageID());
			Message::appendToMessage($petitionID, array(
				Message::MESSAGE_KEY_VOTE_RESPONSE => Government::VOTE_YES,
			));
		}
		else if ($voteData['voteCount'] >= $congressCount)
		{
			EntityHasMessage::replyMessage($entityPetition->getEntityMessageID());
			Message::appendToMessage($petitionID, array(
				Message::MESSAGE_KEY_VOTE_RESPONSE => Government::VOTE_NO,
			));
		}
	}

	/**
	 * 	Processes a petition by taking the appropriate action based on the petition type.
	 * 	
	 * 	@param  int     $petitionID 		The ID of the petition message.
	 */
	public function processPetition($petitionID)
	{
		$petition = Message::model()->findByPk($petitionID);

		if ($petition->getMessageType() == MessageType::MESSAGE_TYPE_PETITION_FOR_RULE_NAME)
		{
			$ruleID = $petition->findByKey(Message::MESSAGE_KEY_RULE_ID);
			$ruleValue = $petition->findByKey(Message::MESSAGE_KEY_RULE_VALUE);
			Rule::updateRuleValue($ruleID, $ruleValue);
		}
		else if ($petition->getMessageType() == MessageType::MESSAGE_TYPE_PETITION_FOR_PUBLIC_GOOD_NAME)
		{
			$userID = UserHasEntity::getUserIDsByEntityID($petition->findByKey(Message::MESSAGE_KEY_ENTITY_ID))[0]; // fix

			$entityName = Entity::getDefaultEntityName(EntityType::ENTITY_TYPE_COMPANY_NAME);
			$entityID = Entity::createEntity($entityName, EntityType::ENTITY_TYPE_COMPANY_NAME, Yii::app()->user->economyID);
			Entity::updateEntityBalance($entityID, 0);

			Company::model()->findByPk($entityID)->updateCompanyType(Company::COMPANY_TYPE_PUBLIC_GOOD);
			User::createUserHasEntity($userID, $entityID);
			Economy::createEconomyHasEntity(Yii::app()->user->economyID, $entityID);
		}
		else if ($petition->getMessageType() == MessageType::MESSAGE_TYPE_PETITION_FOR_ENTITY_NAME)
		{
			$receiver = $petition->findByKey(Message::MESSAGE_KEY_ENTITY_ID);
			$payment = $petition->findByKey(Message::MESSAGE_KEY_MONEY);
			Entity::transferMonetary($this->getEntityID(), $receiver, $payment);
		}
	}

	/**
	 *	Gets all of the petitions that are currently open.
	 *
	 * 	@return array<EntityHasMessage> 	An array of the petitions that are currently open.
	 */
	public function getPetitions()
	{
		$publicMessengerID = Economy::getPublicMessengerID( Yii::app()->user->economyID );

		$petitionRuleList = EntityHasMessage::model()
			->messageType( MessageType::MESSAGE_TYPE_PETITION_FOR_RULE_NAME )
			->receiver( $publicMessengerID )
			->findAll();

		$petitionPublicGoodList = EntityHasMessage::model()
			->messageType( MessageType::MESSAGE_TYPE_PETITION_FOR_PUBLIC_GOOD_NAME )
			->receiver( $publicMessengerID )
			->findAll();

		$petitionEntityList = EntityHasMessage::model()
			->messageType( MessageType::MESSAGE_TYPE_PETITION_FOR_ENTITY_NAME )
			->receiver( $publicMessengerID )
			->findAll();

		return array_merge($petitionRuleList, $petitionPublicGoodList, $petitionEntityList);
	}

	/**
	 *	The getVoteCountForPetition method gets vote data for a specified petition.
	 * 
	 * 	@param  int     $creatorID  	The ID of the entity that created the petition.
	 * 	@param  int     $petitionID 	The ID of the petition message.
	 * 	@return array<int> 		 		An array of the total votes, and the number of agreed votes.
	 */
	public function getVoteCountForPetition($creatorID, $petitionID)
	{
		$voteList = EntityHasMessage::model()
			->messageType( MessageType::MESSAGE_TYPE_VOTE_FOR_PETITION_NAME )
			->receiver( $creatorID )
			->replied( false )
			->findAll();

		$voteCount = 0;
		$voteAgreeCount = 0;

		for ($i = 0; $i < sizeof($voteList); $i++)
		{
			$petitionMessageID = $voteList[$i]->getMessage()->findByKey(Message::MESSAGE_KEY_PREVIOUS_MESSAGE);
			$voteResponse = $voteList[$i]->getMessage()->findByKey(Message::MESSAGE_KEY_VOTE_RESPONSE);

			if ($petitionMessageID == $petitionID)
			{
				if ($voteResponse == Government::VOTE_YES)
				{
					$voteAgreeCount ++;
				}
				$voteCount ++;
			}
		}

		return array(
			'voteCount' => $voteCount,
			'voteAgreeCount' => $voteAgreeCount,
		);
	}

	/**
	 * Checks if a given worker has voted for a given petition.
	 * 
	 * @param  int  	$creatorID  	The ID of the entity that created the petition.
	 * @param  int  	$petitionID 	The ID of the petition message.
	 * @param  int  	$workerID 		The ID of the worker to check.
	 * @return boolean            		Whether the worker has voted.
	 */
	public function hasVotedForPetition($creatorID, $petitionID, $workerID)
	{
		$voteList = EntityHasMessage::model()
			->messageType( MessageType::MESSAGE_TYPE_VOTE_FOR_PETITION_NAME )
			->sender( $workerID )
			->receiver( $creatorID )
			->replied( false )
			->findAll();

		for ($i = 0; $i < sizeof($voteList); $i++)
		{
			if ($voteList[$i]->getMessage()->findByKey(Message::MESSAGE_KEY_PREVIOUS_MESSAGE) == $petitionID)
			{
				return true;
			}
		}
		return false;
	}
}
