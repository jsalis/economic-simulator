<?php

Yii::import('application.models._base.BaseEntityType');

/**
 * The EntityType class is there to represent all of the necessary interction that is
 * needed by the Entity class to get information about the EntityType. It gives methods
 * in which one can access anything from the EntityType table using only the primary
 * key of the table and the name of the row (since each row is baisically unique).
 *
 * @author   <cmicklis@stetson.edu>
 * @since 	 v0.0.0
 */

class EntityType extends BaseEntityType
{	
	const ENTITY_TYPE_WORKER_NAME = 'Worker';
	const ENTITY_TYPE_COMPANY_NAME = 'Company';
	const ENTITY_TYPE_GOVERNMENT_NAME = 'Government';
	const ENTITY_TYPE_BANK_NAME = 'Bank';
	const ENTITY_TYPE_ACCESSOR_NAME = 'Accessor';
	const ENTITY_TYPE_PUBLIC_MESSENGER_NAME = "Public Messenger";

	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * 	The getEntityTypeNameByID method searches the primary key of the entity type
	 * 	on the EntityType table and then gets the name of that Entity type.
	 * 	
	 * 	@param  int    $entityTypeID 	Represents the primary key of the Entity type.
	 *  @return String        		Represents the name on the EntityType table.
	 */
	public static function getEntityTypeNameByID($entityTypeID)
	{
		$result = EntityType::model()->find(array(
			'select' => 'entity_type',
			'condition' => 'entity_type_id=:entityTypeID',
			'params' => array(':entityTypeID' => $entityTypeID)
		));
		return $result->entity_type;
	}

	/**
	 * 	The getEntityTypeIDByName method searches the name of the Entity type
	 * 	on the EntityType table and then gets the primary key of that Entity type.
	 * 	
	 * 	@param  String $entityType 	Represents the name of the entity type.
	 *  @return int           		Represents the primary key on the
	 *                              EntityType table.
	 */
	public static function getEntityTypeIDByName($entityType)
	{
		$result = EntityType::model()->find(array(
			'select' => 'entity_type_id',
			'condition' => 'entity_type=:entityType',
			'params' => array(':entityType' => $entityType)
		));
		return $result->entity_type_id;
	}
}
