<?php

/**
 * The Company class is there to represent a relation to that of the entity type
 * Company and all of its functionality, from having not only all the methods and 
 * variables an entity has but also the right to create and update items, the rights
 * to create rules specifically for that item, and to hire and fire workers as
 * seen fit by the company.
 * 
 * @author   <cmicklis@stetson.edu>, <jsalis@stetson.edu>
 * @since 	 v0.0.0
 */
class Company extends Entity
{
	const COMPANY_TYPE_FOOD = ItemType::ITEM_TYPE_FOOD_NAME;
	const COMPANY_TYPE_NON_FOOD = ItemType::ITEM_TYPE_NONFOOD_NAME;
	const COMPANY_TYPE_PUBLIC_GOOD = ItemType::ITEM_TYPE_PUBLIC_GOOD_NAME;

	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

    public function defaultScope()
	{
		// only read records that are of entity type Company
		return array(
			'with' => array('entityType' => array('select' => false)),
			'condition' => 'entity_type=:type',
			'params' => array(':type' => EntityType::ENTITY_TYPE_COMPANY_NAME),
		);
    }

    public function updateSimulation()
    {
    	parent::updateSimulation();
    	$this->payWealthTax();
    }
	
	/**
	 * 	Creates a new Item given the criteria passed through the Company
	 *  Controller. The item created should be of type Food or Non-Food.
	 *
	 * @param  String  $itemName   		    Represents item name.
	 * @param  String  $itemType   		    Represents type of item.
	 * @param  String  $itemDescription     Represents item description.
	 * @param  int     $price               Represents price of the item.
	 * @param  int     $economyID           Represents the ID of the economy to export the item.
	 */
	public function createProduct($itemName, $itemType, $itemDescription, $price, $economyID)
	{
		$itemID = Item::createItem($itemName, $itemType, $this->getEntityID(), $itemDescription, Yii::app()->user->economyID);
		$item = Item::model()->findByPk($itemID);
		$item->updateExportEconomy($economyID);
		$item->updatePrice($price);
	}
	
	/**
	 * Updates a specific Item given the criteria passed through by the 
	 * Company controller, this includes the specific Item that is
	 * defined by the primary key of the Item table. This Item will only
	 * be related to the items in association with foods and non-foods.
	 *
	 * @param  int     $itemID   		    Represents the item ID.
	 * @param  String  $itemName   		    Represents item name.
	 * @param  String  $itemDescription     Represents item description.
	 * @param  int     $price               Represents price of the item.
	 * @param  int     $economyID           Represents the ID of the economy to export the item.
	 */
	public function updateProduct($itemID, $itemName, $itemDescription, $price, $economyID)
	{
		Item::model()->updateByPk($itemID, array(
			'item' => $itemName,
			'item_description' => $itemDescription,
		));

		$item = Item::model()->findByPk($itemID);
		$item->updateExportEconomy($economyID);
		$item->updatePrice($price);
	}

	/**
	 * 	The purchaseItem method puts the item in the EntityHasItem table representing 
	 * 	that the company owns that item. It then sets the the market item as purchased. 
	 * 	The sales tax for the economy is added to the item price so that it can
	 *  be checked with the current savings of the company.
	 * 
	 *  @param  int     $itemID       		Represents the item ID.
	 *  @param  int     $economyID 			Represents the ID of the economy to purchase the item from.
	 */
	public function purchaseItem($itemID, $economyID)
	{
		$item = Item::model()->findByPk($itemID);
		$marketItem = $item->findAvailableMarketItem($economyID);
		
		if (is_null($marketItem))
		{
			$this->addError(MessageFilter::ERROR_KEY, MessageFilter::ERROR_ITEM_OUT_OF_STOCK);
			return;
		}

		$salesTax = Economy::getEconomySalesTax($economyID);

		$totalPrice = $item->getPrice() + $salesTax;

		if ($this->getEntityBalance() - $totalPrice >= 0)
		{
			MarketItem::purchaseMarketItem($marketItem->getMarketItemID());
			EntityHasItem::createEntityHasItem($this->getEntityID(), $itemID, $item->getPrice());

			$entityMakerID = $item->getItemEntityID();
			$governmentID = Economy::getGovernmentID(Yii::app()->user->economyID);

			$this->transferMonetaryAndTax($this->getEntityID(), $entityMakerID, $item->getPrice());
			$this->transferMonetaryAndTax($this->getEntityID(), $governmentID, $salesTax);

			Entity::transferItem($entityMakerID, $this->getEntityID(), $item->getItemID());
		}
		else
		{
			$this->addError(MessageFilter::ERROR_KEY, MessageFilter::ERROR_NOT_ENOUGH_MONEY);
		}
	}

	/**
	 *	Transfers money from the sender to the receiver and checks to see if the transfered money 
	 *	needs to be taxed.
	 *	 
	 *  @param  int     $senderID  			Represents the entity sender ID.
	 *  @param  int     $receiverID			Represents the entity receiver ID.
	 *  @param  int     $amount    			Represents the dollar amount of money.
	 *  @return boolean                		Whether the transaction was successful.
	 */
	public function transferMonetaryAndTax($senderID, $receiverID, $amount)
	{
		if (Entity::transferMonetary($senderID, $receiverID, $amount) == false)
		{
			return false;
		}
		else
		{
			// $receiverType = Entity::getEntityTypeByEntityID($receiverID);
			// TO DO : no taxes yet
			return true;
		}
	}

	/**
	 * Gets all of the items associated with the company.
	 * 
	 * @return array<Item> 				An array of items created by the company
	 */
	public function getCompanyItemList()
	{
		return Item::getItemsByCreator($this->getEntityID());
	}

	/**
	 * 	Gets the total factor productivity of the company (the technology of the company).
	 * 	The total factor productivity is stated as a rule assocaited with the company entity.
	 * 	Currently this function is only used by the Cobb-Douglas production function.
	 *
	 *  @param  int    $economyID		The ID of the economy.
	 *  @return int               		The total factor productivity (technology) of the company.
	 */
	public function getTotalFactorProductivity($economyID)
	{
		return Economy::getTotalFactorProductivity($economyID, $this->getCompanyType());
	}

	/**
	 * 	Gets the output elasticity (alpha) of the company. The output elasticity is defined as
	 * 	an economy rule for a specific company type. Currently this function is only used 
	 * 	by the Cobb-Douglas production function.
	 *
	 * 	The company type is defined as a company entity rule. 
	 * 	The output elasticity is defined as an economy rule.
	 * 	
	 *  @param  int    $economyID		The ID of the economy.
	 *  @return int               		The output elasticity (alpha) of the company.
	 */
	public function getOutputElasticity($economyID)
	{
		return Economy::getCompanyTypeOutputElasticity($economyID, $this->getCompanyType());
	}

	/**
	 * Updates the value of the rule that defines the company type.
	 * 
	 * @param String 	$companyType 	Represents the company type.
	 */
	public function updateCompanyType($companyType)
	{
		$rule = Rule::findEntityRule($this->getEntityID(), RuleType::COMPANY_TYPE);
		Rule::updateRuleValue($rule->getRuleID(), $companyType);
	}

	/**
	 * 	Gets the type of company.
	 * 	
	 *  @return String 		Represents the company type.
	 */
	public function getCompanyType()
	{
		$rule = Rule::findEntityRule($this->getEntityID(), RuleType::COMPANY_TYPE);
		return (is_null($rule)) ? null : $rule->getRuleValue();
	}

	/**
	 * 	The getCompanyCapital function returns the current entities efficiency.
	 * 	Currently this function is only used by the Cobb-Douglas production function.
	 * 	
	 *  @return int 		The capital of the company.
	 */
	public function getCompanyCapital()
	{
		if ($this->getEntityEfficiency() == 0)
		{
			return 1;
		}
		else
		{
			return $this->getEntityEfficiency();
		}
	}

	/**
	 * Creates a new rent offer using a given entityHasItem ID and daily price,
	 * which is sent as a message to the public entity.
	 * 
	 * @param  int 	$itemID  	The ID of the entityHasItem to offer for rent.
	 * @param  int 	$price 		The expected daily price in dollars.
	 */
	public function createRentOffer($itemID, $price)
	{
		$receiverID = Economy::getPublicMessengerID(Yii::app()->user->economyID);

		$this->sendMessage($receiverID, MessageType::MESSAGE_TYPE_SEND_RENT_OFFER, array(
			Message::MESSAGE_KEY_CONTENT => $this->getEntityName() . ' has offered an item for rent.',
			Message::MESSAGE_KEY_ITEM_ID => $itemID,
			Message::MESSAGE_KEY_MONEY => $price,
		));
	}

	/**
	 * Creates a new job offer.
	 *
	 * @param  int 	$receiverID The ID of the receiver.
	 * @param  int 	$itemID  	The ID of the item to produce.
	 * @param  int 	$payment 	The payment in dollars.
	 */
	public function createJobOffer($receiverID, $itemID, $payment)
	{
		$this->sendMessage($receiverID, MessageType::MESSAGE_TYPE_SEND_JOB_OFFER_NAME, array(
			Message::MESSAGE_KEY_CONTENT => $this->getEntityName() . ' has offered you a job.',
			Message::MESSAGE_KEY_ITEM_ID => $itemID,
			Message::MESSAGE_KEY_MONEY => $payment,
		));
	}

	/**
	 * 	Gets all worker entities that have accepted a job offer from the company
	 * 	during the current day.
	 * 	
	 *  @return array<Entity> 	An array that represents all of the employees of the company.
	 */
	public function getCurrentEmployees()
	{
		$lowerDate = DateUtility::getResetYesterday( Yii::app()->user->economyID );

		$messageList = EntityHasMessage::model()
			->messageType( MessageType::MESSAGE_TYPE_ACCEPT_JOB_OFFER_NAME )
			->senderOrReceiver( $this->getEntityID(), $this->getEntityID() )
			->lowerDate( $lowerDate )
			->findAll();

		$employees = array();

		for ($i = 0; $i < sizeof($messageList); $i++)
		{
			if ($messageList[$i]->getEntityReceiverID() != $this->getEntityID())
			{
				$employees[] = $messageList[$i]->entityReceiver;
			}
			else
			{
				$employees[] = $messageList[$i]->entitySender;
			}
		}
		return $employees;
	}

	/**
	 * Gets the efficiency of the company.
	 * 
	 * @return double 	The comapny efficiency.
	 */
	public function getCompanyEfficiency()
	{
		$efficiency = $this->getEntityEfficiency();
		$rentItemList = $this->getRentItemList();

		for ($i = 0; $i < sizeof($rentItemList); $i++)
		{
			$efficiency += $rentItemList[$i]->getItem()->getEfficiency();
		}
		return $efficiency;
	}
}
