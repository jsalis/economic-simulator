<?php

Yii::import('application.models._base.BaseLogin');

/**
 * The Login class provides static functions for creating and
 * updating user login information which is stored as pairs
 * of user IDs and hashed passwords. The password hashing and
 * verification is handled internally.
 *
 * @author   <jsalis@stetson.edu>
 * @since 	 v0.0.0
 * 
 */

class Login extends BaseLogin
{
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	/**
	 *  Validates a given password against the hashed password found
	 *  in the Login table for a specific user ID.
	 * 
	 * 	@param  String   $userID    The ID of the user to validate for.
	 *  @param  String   $password  The users atempted login password.
	 *  @return boolean             Whether the password is a match.
	 */
	public static function authenticate($userID, $password)
	{
        $login = Login::model()->find(array(
			"select" => "password", 
			"condition" => "user_id=:id", 
			"params" => array(":id" => $userID)
		));
		return Login::validatePassword($password, $login->password);
	}

	/**
	 * Creates a new login for a user.
	 * 
	 * @param  String   $userID     The ID of the user.
	 * @param  String   $password   The password of the user.
	 */
	public static function createLogin($userID, $password)
	{
		$login = new Login;
		$login->user_id = $userID;
		$login->password = Login::createHash($password);
		$login->save();
	}

	/**
	 * Updates the login for a user.
	 * 
	 * @param  String   $userID     The ID of the user.
	 * @param  String   $password   The password of the user.
	 */
	public static function updateLogin($userID, $password)
	{
		Login::model()->updateByPk($userID, array(
			'password' => Login::createHash($password)
		));
	}

	/**
	 * Creates a hash for a given password.
	 * 
	 * @param  String   $password   The password to be hashed.
	 * @return String               The hashed password.
	 */
	public static function createHash($password)
	{
		return Yii::app()->hash->create_hash($password);
	}

	/**
	 * Compares a given password to a hashed login password.
	 * 
	 * @param  String $password     The given password.
	 * @param  String $hashPassword The hashed login password.
	 * @return boolean              Whether the password is a match.
	 */
	public static function validatePassword($password, $hashPassword)
    {
        return Yii::app()->hash->validate_password($password, $hashPassword);
    }
}
