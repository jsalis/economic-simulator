<?php

Yii::import('application.models._base.BaseEconomy');

/**
 * The Economy class is there to represent the Economy,
 * the economy has rules, and economy has entity tables, and 
 * how they are structered. This class also acts like a basic Report
 * so that anyone (Students, Teachers, Admins) can view basic economy
 * information. 
 * 
 * @author   <cmicklis@stetson.edu>, <jsalis@stetson.edu>
 * @since 	 v0.0.0
 */

class Economy extends BaseEconomy
{
	/* The ID of the economy used to copy rules for new economies. */
	const ECONOMY_DEFAULT_ID = 1;

	const CENTS_PER_DOLLAR = 100;
	const MONETARY_SYMBOL = '$';
	const FIRST_DATE = '1998-01-01';

	const TAX_ATTRIBUTE_INCOME = 'Income';
	const TAX_ATTRIBUTE_SALES = 'Sales';
	const TAX_ATTRIBUTE_PROPERTY = 'Property';
	const TAX_ATTRIBUTE_TARIFF = 'Tariff';
	const TAX_ATTRIBUTE_WEALTH = 'Wealth';
	const TAX_ATTRIBUTE_WEALTH_THRESHOLD = 'Threshold';

	private $_ruleList;
	private $_entityList;
	private $_economyRelationList;

	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * 	The createEconomy method adds a economy using all information given into the 
	 * 	Economy	table. It also adds the economy Relation as well as copies all
	 * 	of the default rules over to the new Economy allowing the Accessor to 
	 * 	define it to their needs.
	 *
	 * 	The method also creates a public messenger entity and a government entity.
	 * 	
	 *  @param String   $economyName         Represents the economy inputted name.
	 *  @param String   $economyDescription  Represents the economy inputted description.
	 */
	public static function createEconomy($economyName, $economyDescription)
	{
		$economy = new Economy;
		$economy->economy = $economyName;
		$economy->economy_description = $economyDescription;
		$economy->save();

		$id = $economy->getEconomyID();

		// Copy rules
		Economy::model()->copyEconomyRules($id, Economy::ECONOMY_DEFAULT_ID);
		Entity::model()->copyEntityTypeRules($id, Economy::ECONOMY_DEFAULT_ID);
		Item::model()->copyItemTypeRules($id, Economy::ECONOMY_DEFAULT_ID);

		// Create economy self relation
		$economyRelationID = EconomyRelation::model()->createEconomyRelation($id, $id);

		// Create public messenger
		$entityID = Entity::model()->createEntity($economyName, EntityType::ENTITY_TYPE_PUBLIC_MESSENGER_NAME, $id);
		Economy::model()->createEconomyHasEntity($id, $entityID);

		// Create government
		$entityID = Entity::model()->createEntity(EntityType::ENTITY_TYPE_GOVERNMENT_NAME, EntityType::ENTITY_TYPE_GOVERNMENT_NAME, $id);
		Economy::model()->createEconomyHasEntity($id, $entityID);

		// Set economy creation date
		Economy::model()->updateByPk($id, array(
			'creation_date' => DateUtility::getCurrentDate($id),
		));

		return $id;
	}

	/**
	 * 	Updates the name and description of an existing economy.
	 * 	
	 *  @param int      $economyID           Represents the economy primary key.
	 *  @param String   $economyName         Represents the economy inputted name.
	 *  @param String   $economyDescription  Represents the economy inputted description.
	 */
	public static function updateEconomy($economyID, $economyName, $economyDescription)
	{
		Economy::model()->updateByPk($economyID, array(
			'economy' => $economyName,
			'economy_description' => $economyDescription,
		));
	}

	/**
	 *	Creates all of the rules that the Economy needs, adds the relation
	 *	to all of the object types and relation types, and then adds the
	 *	rule relations to the economy has table.
	 * 
	 * 	@param  int   $newEconomyID 		The ID of the economy to receive rules.
	 * 	@param  int   $originalEconomyID 	The ID of the economy to copy rules from.
	 */
	public static function copyEconomyRules($newEconomyID, $originalEconomyID)
	{
		$ruleList = Rule::model()->getEconomyRules($originalEconomyID);

		for ($i = 0; $i < sizeof($ruleList); $i++)
		{
			$newRule = Rule::model()->copyRule($ruleList[$i]);
			Economy::model()->createEconomyHasRule($newEconomyID, $newRule->getRuleID());
		}
	}

	/**
	 * 	The createEconomyHasEntity method adds a Entity associated with the economy to the
	 * 	EconomyHasEntity table. This allows the Economy to have a relation to an Entity, or
	 * 	multiple Entities.
	 * 	
	 *  @param int      $economyID       Represents the economy ID.
	 *  @param int      $entityID        Represents the entity ID.
	 */
	public static function createEconomyHasEntity($economyID, $entityID)
	{
		$economyHasEntity = new EconomyHasEntity;
		$economyHasEntity->economy_id = $economyID;
		$economyHasEntity->entity_id = $entityID;
		$economyHasEntity->save();
	}

	/**
	 * 	The createEconomyHasRule method adds a rule associated with the economy to the
	 * 	EconomyHasRule table. This allows the Economy to have a relation to an rule, or
	 * 	multiple rules.
	 * 	
	 *  @param int      $economyID       Represents the economy ID.
	 *  @param int      $ruleID          Represents the rule ID.
	 */
	public static function createEconomyHasRule($economyID, $ruleID)
	{
		$economyHasRule = new EconomyHasRule;
		$economyHasRule->economy_id = $economyID;
		$economyHasRule->rule_id = $ruleID;
		$economyHasRule->save();
	}

	/**
	 * 	Adds a specified user to the list of candidates, allowing other users to 
	 * 	vote for that candidate during the election. A nomination must be in
	 * 	progress for candidates to be added.
	 * 	
	 *	@param  int     $userID         The ID of the User.
	 *	@param  int     $economyID      The ID of the Economy.
	 *	@return boolean                 Whether the user was added as a candidate.
	 */
	public static function addCandidate($userID, $economyID)
	{
		$worker = Worker::model()->user($userID)->economy($economyID)->find();

		if (is_null($worker))
		{
			return false;
		}
		
		if (Economy::isNominationInProgress($economyID))
		{
			if (!Economy::alreadyNominated($userID, $economyID))
			{
				$receiverID = Economy::getPublicMessengerID(Yii::app()->user->economyID);
				$publicMessenger = Entity::model()->findByPk($receiverID);

				$publicMessenger->sendMessage($receiverID, MessageType::MESSAGE_TYPE_CANDIDATE_BALLOT, array(
					Message::MESSAGE_KEY_ENTITY_ID => $worker->getEntityID(),
				));
				return true;
			}
		}
		return false;
	}

	/**
	 * 	The getCandidateList returns all of the candidates that are in the current election.
	 *
	 *	@param  int     $economyID      The ID of the economy.
	 *  @return array   				A list of all the current candidates for the election
	 *                        			including the name and email address of the candidate
	 *                        			as well as the EntityHasMessage object.
	 */
	public static function getCandidateList($economyID)
	{
	    $isNomination = Economy::isNominationInProgress($economyID);
	    $isElection = Economy::isElectionInProgress($economyID);
	    $isElectionEnded = Economy::isElectionEnded($economyID);
		$candidateList = array();

	    if ($isElection || $isNomination || $isElectionEnded)
	    {
			$receiverID = Economy::getPublicMessengerID(Yii::app()->user->economyID);
			$recentElectionDate = EntityHasMessage::getMostRecentMessageDate(MessageType::MESSAGE_TYPE_START_NOMINATION, $receiverID);
			$recentElectionDate = $recentElectionDate->format('Y-m-d H:i:s');

			$ballotList = EntityHasMessage::model()
				->messageType( MessageType::MESSAGE_TYPE_CANDIDATE_BALLOT )
				->receiver( $receiverID )
				->lowerDate( $recentElectionDate )
				->findAll();

			for ($i = 0; $i < sizeof($ballotList); $i++)
			{
				$entityID = $ballotList[$i]->getMessage()->findByKey(Message::MESSAGE_KEY_ENTITY_ID);
				$user = User::model()->entity($entityID)->find();

				$candidateList[] = array(
					'userID' => $user->getUserID(),
					'name' => Entity::getEntityNameByID($entityID),
					'emailAddress' => $user->getUserEmailAddress(),
					'entityHasMessage' => $ballotList[$i],
					'voteCount' => Economy::getVoteCount($entityID, $recentElectionDate, $receiverID),
				);
			}
	    }

		return $candidateList;
	}

	/**
	 * 	The getVoteCount method counts all of the messages that are of message type
	 * 	MESSAGE_TYPE_VOTE_FOR_CANDIDATE after a specified period that have the entity id specified
	 * 	in the parameters in the content of the message. It then counts up all of these messages
	 * 	that fit that criteria and returns that count.
	 * 	
	 *  @param  int 	$entityID       	The primary key of the entity that is to be voted for.
	 *  @param  Date    $recentElectionDate The date of the most recent start of the elections, used
	 *                                      to get messages of a specific type after this date.
	 *  @param  int 	$receiverID         The receiver of the messages.
	 *  @return int 	                    The amount of votes for the candidate.
	 */
	public static function getVoteCount($entityID, $recentElectionDate, $receiverID)
	{
		$voteList = EntityHasMessage::model()
			->messageType( MessageType::MESSAGE_TYPE_VOTE_FOR_CANDIDATE )
			->receiver( $receiverID )
			->lowerDate( $recentElectionDate )
			->findAll();

		$voteCount = 0;

		for ($i = 0; $i < sizeof($voteList); $i++)
		{
			$entityNomineeID = $voteList[$i]->getMessage()->findByKey(Message::MESSAGE_KEY_ENTITY_ID);

			if ($entityID == $entityNomineeID)
			{
				$voteCount ++;
			}
		}
		return $voteCount;
	}

	/**
	 *	Checks if the user has already been nominated for the current election.
	 * 	
	 *	@param  String  $userID      	The email address of the user.
	 *	@param  int     $economyID      The ID of the Economy.
	 *  @return boolean      			Whether the user has already been nominated.
	 */
	public static function alreadyNominated($userID, $economyID)
	{
		$candidateList = Economy::getCandidateList($economyID);

		for ($i = 0; $i < sizeof($candidateList); $i++)
		{
			if ($userID == $candidateList[$i]['userID'])
			{
				return true;
			}
		}
		return false;
	}

	/**
		 _______  _______  _______  _        _______  _______             _______  _______  _______  _______  _______ _________ _______ 
		(  ____ \(  ____ \(  ___  )( (    /|(  ___  )(       )|\     /|  (  ____ )(  ____ \(  ____ )(  ___  )(  ____ )\__   __/(  ____ \
		| (    \/| (    \/| (   ) ||  \  ( || (   ) || () () |( \   / )  | (    )|| (    \/| (    )|| (   ) || (    )|   ) (   | (    \/
		| (__    | |      | |   | ||   \ | || |   | || || || | \ (_) /   | (____)|| (__    | (____)|| |   | || (____)|   | |   | (_____ 
		|  __)   | |      | |   | || (\ \) || |   | || |(_)| |  \   /    |     __)|  __)   |  _____)| |   | ||     __)   | |   (_____  )
		| (      | |      | |   | || | \   || |   | || |   | |   ) (     | (\ (   | (      | (      | |   | || (\ (      | |         ) |
		| (____/\| (____/\| (___) || )  \  || (___) || )   ( |   | |     | ) \ \__| (____/\| )      | (___) || ) \ \__   | |   /\____) |
		(_______/(_______/(_______)|/    )_)(_______)|/     \|   \_/     |/   \__/(_______/|/       (_______)|/   \__/   )_(   \_______)
		                                                                                                                                
	 */
	
	public function user($userID)
	{
	    $this->getDbCriteria()->mergeWith(array(
        	'with' => array('economyHasEntities.entity.userHasEntities' => array('select' => false)),
			'condition' => 'userHasEntities.user_id=:userID',
			'params' => array(':userID' => $userID),
        ));
	    return $this;
	}

	/**
	 * 	Gets the name of the economy with the specified ID.
	 * 	
	 *  @param  int     $economyID    Represents the economy ID.
	 *  @return String	       	      The name of the economy.
	 */
	public static function getEconomyNameByID($economyID)
	{
		$economy = Economy::model()->find(array(
			'select' => 'economy',
			'condition' => 'economy_id=:economyID',
			'params' => array(':economyID' => $economyID)
		));
		return $economy->getEconomyName();
	}

	/**
	 * 	The getEntitiesInEconomy method searches for all entities in a specified economy
	 * 	and with a specified enitity type.
	 * 	
	 *  @param  int      $economyID    Represents an economy ID.
	 *  @param  String   $entityType   Represents an entity type.
	 *  @return array<Entity>          An array of all entities that match the criteria.
	 */
	public static function getEntitiesInEconomy($economyID, $entityType = null)
	{
		$condition = 'economy_id=:economyID';
		$params = array(':economyID' => $economyID);

		if (!is_null($entityType))
		{
			$condition .= '&& entity_type=:entityType';
			$params[':entityType'] = $entityType;
		}

		return Entity::model()->findAll(array(
			'with' => array(
				'economyHasEntities' => array('select' => false),
				'entityType' => array('select' => false),
			),
			'condition' => $condition,
			'params' => $params,
		));
	}

	/**
	 * Gets the ID of the accessor for the specified economy.
	 * 
	 * @param  int 		$economyID  	The ID of the economy.
	 * @return int            			The ID of the accessor for the economy.
	 */
	public static function getAccessorID($economyID)
	{
		return Accessor::model()->economy($economyID)->find()->getEntityID();
	}

	/**
	 * Gets the ID of the public messenger for the specified economy.
	 * 
	 * @param  int 		$economyID  	The ID of the economy.
	 * @return int            			The ID of the public messenger for the economy.
	 */
	public static function getPublicMessengerID($economyID)
	{
		$publicMessenger = Economy::getEntitiesInEconomy($economyID, EntityType::ENTITY_TYPE_PUBLIC_MESSENGER_NAME);
		return $publicMessenger[0]->getEntityID();
	}

	/**
	 * Gets the ID of the government for the specified economy.
	 * 
	 * @param  int 		$economyID  	The ID of the economy.
	 * @return int            			The ID of the government for the economy.
	 */
	public static function getGovernmentID($economyID)
	{
		return Government::model()->economy($economyID)->find()->getEntityID();
	}

	/**
	 * 	The getItemsInEconomy method searches for all items in a specified economy
	 * 	and with a specified enitity type.
	 * 	
	 *  @param  int      $economyID    	Represents an economy ID.
	 *  @param  String   $itemType     	Represents an entity type.
	 *  @return array<Item>          	An array of all items that match the criteria.
	 */
	public static function getItemsInEconomy($economyID, $itemType = null)
	{
		$itemTypeID = (is_null($itemType)) ? null : ItemType::getItemTypeIDByItemType($itemType);


		$companyList = Economy::getEntitiesInEconomy(Yii::app()->user->economyID, EntityType::ENTITY_TYPE_COMPANY_NAME);
		$accessorList = Economy::getEntitiesInEconomy(Yii::app()->user->economyID, EntityType::ENTITY_TYPE_ACCESSOR_NAME);

		$itemList = array();
		for ($i = 0; $i < sizeof($companyList); $i++)
		{
			$tempList = Item::getItemsByCreator($companyList[$i]->getEntityID());
			for ($j = 0; $j < sizeof($tempList); $j++)
			{
				$itemList[] = $tempList[$j];
			}
		}
		for ($i = 0; $i < sizeof($accessorList); $i++)
		{
			$tempList = Item::getItemsByCreator($accessorList[$i]->getEntityID());
			for ($j = 0; $j < sizeof($tempList); $j++)
			{
				$itemList[] = $tempList[$j];
			}
		}

		return $itemList;
	}

	/**
	 * Checks if a nomination is in progress for a specific economy.
	 * 
	 * @param   int      $economyID  		The ID of the economy.
	 * @return  boolean              		Whether a nomination is in progress.
	 */
	public static function isNominationInProgress($economyID)
	{
		$receiverID = Economy::getPublicMessengerID(Yii::app()->user->economyID);

		$recentNominationDate = EntityHasMessage::getMostRecentMessageDate(MessageType::MESSAGE_TYPE_START_NOMINATION, $receiverID);
		$recentStartElectionDate = EntityHasMessage::getMostRecentMessageDate(MessageType::MESSAGE_TYPE_START_ELECTION, $receiverID);
		$recentEndElectionDate = EntityHasMessage::getMostRecentMessageDate(MessageType::MESSAGE_TYPE_END_ELECTION, $receiverID);

		return ($recentNominationDate > $recentStartElectionDate && $recentNominationDate > $recentEndElectionDate);
	}

	/**
	 * Checks if an election is in progress for a specific economy.
	 *
	 * @param   int      $economyID  		The ID of the economy.
	 * @return  boolean              		Whether an election is in progress.
	 */
	public static function isElectionInProgress($economyID)
	{
		$receiverID = Economy::getPublicMessengerID(Yii::app()->user->economyID);
		
		$recentNominationDate = EntityHasMessage::getMostRecentMessageDate(MessageType::MESSAGE_TYPE_START_NOMINATION, $receiverID);
		$recentStartElectionDate = EntityHasMessage::getMostRecentMessageDate(MessageType::MESSAGE_TYPE_START_ELECTION, $receiverID);
		$recentEndElectionDate = EntityHasMessage::getMostRecentMessageDate(MessageType::MESSAGE_TYPE_END_ELECTION, $receiverID);

		return ($recentStartElectionDate > $recentEndElectionDate && $recentStartElectionDate > $recentNominationDate);
	}

	/**
	 * Checks if an election has ended for a specific economy.
	 *
	 * @param   int      $economyID  		The ID of the economy.
	 * @return  boolean              		Whether an election has ended.
	 */
	public static function isElectionEnded($economyID)
	{
		$receiverID = Economy::getPublicMessengerID(Yii::app()->user->economyID);
		
		$recentNominationDate = EntityHasMessage::getMostRecentMessageDate(MessageType::MESSAGE_TYPE_START_NOMINATION, $receiverID);
		$recentStartElectionDate = EntityHasMessage::getMostRecentMessageDate(MessageType::MESSAGE_TYPE_START_ELECTION, $receiverID);
		$recentEndElectionDate = EntityHasMessage::getMostRecentMessageDate(MessageType::MESSAGE_TYPE_END_ELECTION, $receiverID);
		
		return ($recentEndElectionDate > $recentStartElectionDate && $recentEndElectionDate > $recentNominationDate);
	}

	/**
	 * 	Checks if the specified action can be performed in the given economy.
	 * 	
	 *  @param  int    $economyID 			The ID of the economy.
	 *  @param  String $action  			The controller action.
	 *  @return boolean 					Whether the action can be performed.
	 */
	public static function canPerformAction($economyID, $action)
	{
		$rule = Rule::findEconomyRule($economyID, RuleType::ENTITY_ACTION, $action);
		return (is_null($rule)) ? true : $rule->isRuleActivated();
	}

	/**
	 * 	Gets the name of the production function for the specified economy.
	 * 	
	 *  @param  int    $economyID 			The ID of the economy.
	 *  @return String 						The economy production function name.
	 */
	public static function getEconomyProductionFunction($economyID)
	{
		$rule = Rule::findEconomyRule($economyID, RuleType::ECONOMY_PRODUCTION_FUNCTION);
		return (is_null($rule)) ? null : $rule->getRuleValue();
	}

	/**
	 * 	Gets the name of the benefit function for the specified economy.
	 * 	
	 *  @param  int    $economyID 			The ID of the economy.
	 *  @return String 						The economy benefit function name.
	 */
	public static function getEconomyBenefitFunction($economyID)
	{
		$rule = Rule::findEconomyRule($economyID, RuleType::ECONOMY_BENEFIT_FUNCTION);
		return (is_null($rule)) ? null : $rule->getRuleValue();
	}

	/**
	 * 	Gets the required percentage of votes that allow a petition to pass.
	 * 	
	 *  @param  int    $economyID 			The ID of the economy.
	 *  @return String 						The percentage of people that need to agree.
	 */
	public static function getEconomyPetitionPassPercentage($economyID)
	{
		$rule = Rule::findEconomyRule($economyID, RuleType::PETITION_PASS_PERCENTAGE);
		return (is_null($rule)) ? null : $rule->getRuleValue();
	}

	/**
	 * 	Gets the hourly time difference between the UTC.
	 * 	
	 *  @param  int    $economyID 			The ID of the economy.
	 *  @return int 						The economy time difference from UTC.
	 */
	public static function getEconomyTimeDifference($economyID)
	{
		$rule = Rule::findEconomyRule($economyID, RuleType::ECONOMY_TIME_DIFFERENCE_FROM_UTC);
		return (is_null($rule)) ? null : $rule->getRuleValue();
	}

	/**
	 * 	Gets the hourly time difference between midnight and the daily economy reset time.
	 * 	
	 *  @param  int    $economyID 			The ID of the economy.
	 *  @return int 						The economy time difference from UTC.
	 */
	public static function getEconomyReset($economyID)
	{
		$rule = Rule::findEconomyRule($economyID, RuleType::ECONOMY_RESET_AFTER_MIDNIGHT);
		return (is_null($rule)) ? null : $rule->getRuleValue();
	}

	/**
	 * 	Gets the balance that the entity should start with for a given economy.
	 * 	
	 *  @param  int    $economyID 			The ID of the economy.
	 *  @param  String $entityType  		The entity type.
	 *  @return int 						The initial entity balance for the economy.
	 */
	public static function getInitialEntityBalance($economyID, $entityType)
	{
		$rule = Rule::findEconomyRule($economyID, RuleType::INITIAL_ENTITY_BALANCE, $entityType);
		return (is_null($rule)) ? null : $rule->getRuleValue();
	}

	/**
	 * 	Gets the output elasticity (alpha) of the company type.
	 * 	
	 *  @param  int    $economyID 			The ID of the economy.
	 *  @param  String $companyType  		The company type.
	 *  @return int 						The output elasticity (alpha) for the company type.
	 */
	public static function getCompanyTypeOutputElasticity($economyID, $companyType)
	{
		$rule = Rule::findEconomyRule($economyID, RuleType::OUTPUT_ELASTICITY_FOR_COMPANY_TYPE, $companyType);
		return (is_null($rule)) ? null : $rule->getRuleValue();
	}

	/**
	 * 	Gets the total factor productivity (technology) of the company type.
	 * 	
	 *  @param  int    $economyID 			The ID of the economy.
	 *  @param  String $companyType  		The company type.
	 *  @return int 						The total factor productivity (technology) for the company type.
	 */
	public static function getTotalFactorProductivity($economyID, $companyType)
	{
		$rule = Rule::findEconomyRule($economyID, RuleType::COMPANY_TOTAL_FACTOR_PRODUCTIVITY, $companyType);
		return (is_null($rule)) ? null : $rule->getRuleValue();
	}

	/**
	 * 	Gets the number of people that can be contained in one govermental body.
	 * 	
	 *  @param  int    $economyID 			The ID of the economy.
	 *  @return int 						The economies seats in congress.
	 */
	public static function getEconomySeatsInCongress($economyID)
	{
		$rule = Rule::findEconomyRule($economyID, RuleType::SEATS_IN_CONGRESS);
		return (is_null($rule)) ? null : $rule->getRuleValue();
	}

	/**
	 * 	Gets the property tax defined by its rule within a specified economy.
	 * 	
	 *  @param  int    $economyID 			The ID of the economy.
	 *  @return int 						The economy property tax.
	 */
	public static function getEconomyPropertyTax($economyID)
	{
		$rule = Rule::findEconomyRule($economyID, RuleType::TAXATION_FROM_PURCHASER, Economy::TAX_ATTRIBUTE_PROPERTY);
		return (is_null($rule)) ? null : $rule->getRuleValue();
	}

	/**
	 * 	Gets the sales tax defined by its rule within a specified economy.
	 * 	
	 *  @param  int    $economyID 			The ID of the economy.
	 *  @return int 						The economy sales tax.
	 */
	public static function getEconomySalesTax($economyID)
	{
		$rule = Rule::findEconomyRule($economyID, RuleType::TAXATION_FROM_PURCHASER, Economy::TAX_ATTRIBUTE_SALES);
		return (is_null($rule)) ? null : $rule->getRuleValue();
	}

	/**
	 * 	Gets the wealth tax defined by its rule within a specified economy.
	 * 	
	 *  @param  int    $economyID 			The ID of the economy.
	 *  @return Rule 						The economy wealth tax.
	 */
	public static function getWealthTax($economyID)
	{
		$rule = Rule::findEconomyRule($economyID, RuleType::TAXATION_OF_ENTITY_WEALTH, Economy::TAX_ATTRIBUTE_WEALTH);
		return (is_null($rule)) ? null : $rule->getRuleValue();
	}

	/**
	 * 	Gets the wealth tax threshold defined by its rule within a specified economy.
	 * 	
	 *  @param  int    $economyID 			The ID of the economy.
	 *  @return Rule 						The economy wealth tax threshold.
	 */
	public static function getWealthTaxThreshold($economyID)
	{
		$rule = Rule::findEconomyRule($economyID, RuleType::TAXATION_OF_ENTITY_WEALTH, Economy::TAX_ATTRIBUTE_WEALTH_THRESHOLD);
		return (is_null($rule)) ? null : $rule->getRuleValue();
	}

	/**
	 * 	Gets the worker income tax defined by its rule within a specified economy.
	 * 	
	 *  @param  int    $economyID 			The ID of the economy.
	 *  @return int 						The economy income tax for worker.
	 */
	public static function getEconomyIncomeTaxForWorker($economyID)
	{
		$rule = Rule::findEconomyRule($economyID, RuleType::TAXATION_FROM_PURCHASER, Economy::TAX_ATTRIBUTE_INCOME);
		return (is_null($rule)) ? null : $rule->getRuleValue();
	}

	/**
	 * 	Gets the revenue tax for a company type defined by its rule within a specified economy.
	 * 	
	 *  @param  int    $economyID 			The ID of the economy.
	 *  @param  String $companyType 		The type of company.
	 *  @return int 						The revenue tax for the company type.
	 */
	public static function getEconomySalesTaxForCompany($economyID, $companyType)
	{
		$rule = Rule::findEconomyRule($economyID, RuleType::TAXATION_FROM_PURCHASER_GROUP, $companyType);
		return (is_null($rule)) ? null : $rule->getRuleValue();
	}

	/**
	 * 	The getEconomyGDP method calculates out the gross domestic product and then
	 * 	returns the results as an double. It calculates out this by getting first the
	 * 	nominal GDP and then divides that by the Real GDP. The result of that division 
	 * 	is then multiplied by cents per dollar and returned.
	 *
	 * 	First it gets all of the entities in the economy that are companies, then
	 * 	the method gets all of the items that the company has created. The array of
	 * 	items is then sent to both the Real and Nominal GDP methods. It then gets the
	 * 	results of these methods divides the Nominal by the Real and then multiplies
	 * 	it by cents per dollar.
	 * 	
	 *  @param  int      $economyID 	The ID of the economy.
	 *  @return double                 	The GDP of that economy.
	 */
	public static function getEconomyGDP($economyID)
	{
		$companyIDList = EconomyHasEntity::getEntityIDsByEconomyIDAndEntityType($economyID, EntityType::ENTITY_TYPE_COMPANY_NAME);

		$itemList = array();
		for ($i = 0; $i < sizeof($companyIDList); $i++)
		{
			$company = Company::model()->findByPk($companyIDList[$i]);
			$companyItemList = $company->getCompanyItemList();
			for ($j = 0; $j < sizeof($companyItemList); $j++)
			{
				$itemList[] = $companyItemList[$j];
			}
		}

		$nominalGDP = Economy::calculateNominalGDP($itemList);
		$realGDP = Economy::calculateRealGDP($itemList);
		$GDP = ($nominalGDP / $realGDP) * Economy::CENTS_PER_DOLLAR;

		return $GDP;
	}

	/**
	 * 	The calculateNominalGDP method goes and calculates the Nomimal GDP using the
	 * 	itemList and then returns the results from the itemList.
	 *
	 * 	It does this by defining a total variable to hold the summation of the quantity
	 * 	of the item multiplied by its price. Once the method has gone through all of the 
	 * 	items in the list it then returns that sum.
	 * 	
	 *  @param  array<Item> $itemList 	A list of items in the economy.
	 *  @return double                 	The Nominal GDP of that economy.
	 */
	public static function calculateNominalGDP($itemList)
	{
		$sum = 0;
		for ($i = 0; $i < sizeof($itemList); $i++)
		{
			$sum += $itemList[$i]->getItemStock() * $itemList[$i]->getPrice();
		}
		return $sum;
	}

	/**
	 * 	The calculateRealGDP method calculates the real GDP using a list of items
	 * 	and returns the result.
	 * 	
	 *  @param  array<Item> $itemList 	A list of items in the economy.
	 *  @return double                 	The real GDP of that economy.
	 */
	public static function calculateRealGDP($itemList)
	{
		$itemCount = sizeof($itemList);
		$totalFoodPrice = 0;
		$totalNonFoodPrice = 0;
		$foodQuantity = 0;
		$nonFoodQuantity = 0;

		// Count the number of food and non-food items
		for ($i = 0; $i < $itemCount; $i++)
		{
			if ($itemList[$i]->getItemType() == ItemType::ITEM_TYPE_FOOD_NAME)
			{
				$foodQuantity += $itemList[$i]->getItemStock();
				$totalFoodPrice += $itemList[$i]->getPrice();
			}
			else if ($itemList[$i]->getItemType() == ItemType::ITEM_TYPE_NONFOOD_NAME)
			{
				$nonFoodQuantity += $itemList[$i]->getItemStock();
				$totalNonFoodPrice += $itemList[$i]->getPrice();
			}
		}

		// Calculate the average price for food and non-food items
		$averageFoodPrice = $totalFoodPrice / $itemCount;
		$averageNonFoodPrice = $totalNonFoodPrice / $itemCount;

		// Calculate and return the real GDP
		return ($averageFoodPrice * $foodQuantity) + ($averageNonFoodPrice * $nonFoodQuantity);
	}

	/**
	 * [getEconomyCPI description]
	 * 
	 * @param  [type] $economyID [description]
	 * @return [type]            [description]
	 */
	public static function getEconomyCPI($economyID)
	{

	}

	/**
	 * 	The getAverageWages method gets all of the wages of the worker,
	 * 	averages them up and then returns that average. This method may also be
	 * 	date restricted to get daily values.
	 *
	 * 	It does this by going through each individual workers messages of
	 * 	a specific type, this message may be of a type MESSAGE_TYPE_ACCEPT_JOB_OFFER_NAME.
	 * 	It uses this message to get the previous message that the individual (either
	 * 	the Company or the Worker) accepted. Then it gets the monetary amount of the
	 * 	message using the specified key. Once the method has the monetary amount it then 
	 * 	adds that to the sum of the wages. The average Wage is then calcualted by the 
	 * 	total monetary amount divided by the total amount of accepted job offers.
	 *
	 * 	The wages can be date specified by only grabbing the specified message between
	 * 	a range of two dates. That range is defined in the start and end date.
	 * 
	 *  @param  int      $economyID 		The primary key of the economy.
	 *  @param  date  	 $startDate 		Represents the starting date.
	 *  @param  date  	 $endDate 			Represents the ending date.
	 *  @return double 						Represents the Average Wage.
	 */
	public static function getAverageWages($economyID)
	{

	}

	/**
	 * Calculates the total money supply in the given economy.
	 * 
	 * @param  int      $economyID 		The ID of the economy.
	 * @return double            		The money supply in the given economy.
	 */
	public static function getEconomyMoneySupply($economyID)
	{
		$entityIDs = EconomyHasEntity::getEntityIDsByEconomyID($economyID);
		$sum = 0;
		for ($i = 0; $i < sizeof($entityIDs); $i++)
		{
			if (Entity::getEntityTypeByEntityID($entityIDs[$i]->entity_id) != EntityType::ENTITY_TYPE_ACCESSOR_NAME && 
				Entity::getEntityTypeByEntityID($entityIDs[$i]->entity_id) != EntityType::ENTITY_TYPE_PUBLIC_MESSENGER_NAME)
			{
				$sum += Entity::getEntityBalanceByID($entityIDs[$i]->entity_id);
			}
		}
		return $sum;
	}

	/**
		 _______  _______  _______  _        _______  _______             _______  _______ _________          _______  ______   _______ 
		(  ____ \(  ____ \(  ___  )( (    /|(  ___  )(       )|\     /|  (       )(  ____ \\__   __/|\     /|(  ___  )(  __  \ (  ____ \
		| (    \/| (    \/| (   ) ||  \  ( || (   ) || () () |( \   / )  | () () || (    \/   ) (   | )   ( || (   ) || (  \  )| (    \/
		| (__    | |      | |   | ||   \ | || |   | || || || | \ (_) /   | || || || (__       | |   | (___) || |   | || |   ) || (_____ 
		|  __)   | |      | |   | || (\ \) || |   | || |(_)| |  \   /    | |(_)| ||  __)      | |   |  ___  || |   | || |   | |(_____  )
		| (      | |      | |   | || | \   || |   | || |   | |   ) (     | |   | || (         | |   | (   ) || |   | || |   ) |      ) |
		| (____/\| (____/\| (___) || )  \  || (___) || )   ( |   | |     | )   ( || (____/\   | |   | )   ( || (___) || (__/  )/\____) |
		(_______/(_______/(_______)|/    )_)(_______)|/     \|   \_/     |/     \|(_______/   )_(   |/     \|(_______)(______/ \_______)
		                                                                                                                                
	 */

	public function isClosed()
	{
		return false;
	}

	public function getEconomyID()
	{
		return $this->economy_id;
	}

	public function getEconomyName()
	{
		return $this->economy;
	}

	public function getEconomyDescription()
	{
		return $this->economy_description;
	}

	public function getEconomyCreationDate()
	{
		return $this->creation_date;
	}

	public function getEconomyEndDate()
	{
		return $this->end_date;
	}

	public function getRuleList()
	{
		if (is_null($this->_ruleList))
		{
			$relationID = EconomyRelation::findRelationID($this->getEconomyID(), $this->getEconomyID());
			$relationRuleList = Rule::getEconomyRelationRules($relationID);
			$economyRuleList = Rule::getEconomyRules($this->getEconomyID());
			$workerRuleList = Rule::getEntityTypeRules($this->getEconomyID(), EntityType::ENTITY_TYPE_WORKER_NAME);
			$this->_ruleList = array_merge($economyRuleList, $relationRuleList, $workerRuleList);
		}
		return $this->_ruleList;
	}

	public function getEntityList()
	{
		if (is_null($this->_entityList))
		{
			$this->_entityList = Economy::getEntitiesInEconomy($this->getEconomyID());
		}
		return $this->_entityList;
	}

	public function getEconomyRelationList()
	{
		if (is_null($this->_economyRelationList))
		{
			$this->_economyRelationList = EconomyRelation::findByEconomyFromID($this->getEconomyID());
		}
		return $this->_economyRelationList;
	}
}
