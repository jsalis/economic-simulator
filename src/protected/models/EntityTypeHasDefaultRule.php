<?php

Yii::import('application.models._base.BaseEntityTypeHasDefaultRule');

/**
 * The EntityTypeHasDefaultRule class is there to represent every bit of detail when it comes to 
 * the relation of the Entities and Rules. This relation only goes as deep as both the
 * primary keys of the EntityType and Rule. So the only methods for this class are report
 * methods that get either all the Rule ids for the entityType or vice versa.
 * 
 * @author   <cmicklis@stetson.edu>, <jsalis@stetson.edu>
 * @since 	 v0.0.0
 * 
 */

class EntityTypeHasDefaultRule extends BaseEntityTypeHasDefaultRule
{
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}
}
