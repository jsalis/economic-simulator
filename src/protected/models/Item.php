<?php

Yii::import('application.models._base.BaseItem');

/**
 * The Item class is there to represent every bit of detail that a Item may have
 * from the name and description to all the rules associated with that item.
 * The Class is setup so that the Worker, Accessor, Company, Goverment, or Bank can easily
 * access all of the information needed. The Item also exists to work as a ReportForm
 * for all things an item will have.
 *
 * @author   <cmicklis@stetson.edu>, <jsalis@stetson.edu>
 * @since 	 v0.0.0
 */

class Item extends BaseItem
{
	private $_itemRuleList;

	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * 	Creates a new item using all information given into the Item table. 
	 * 	
	 *  @param String   $itemName         Represents the item name.
	 *  @param String   $itemType         Represents the item name.
	 *  @param String   $creatorID        Represents the entity ID of the creator.
	 *  @param String   $itemDescription  Represents the description of the item.
	 *  @param String   $economyID     	  Represents the economy that the item will exist in.
	 *  @return  int 					  Represents the new item ID.                                 
	 */
	public static function createItem($itemName, $itemType, $creatorID, $itemDescription, $economyID)
	{		
		$item = new Item;
		$item->item = $itemName;
		$item->item_type_id = ItemType::getItemTypeIDByItemType($itemType);
		$item->entity_creator_id = $creatorID;
		$item->item_description = $itemDescription;
		$item->creation_date = DateUtility::getCurrentDate(Yii::app()->user->economyID);
		$item->save();

		Item::model()->copyItemRules($item->item_id, $economyID, $itemType);

		return $item->item_id;
	}

	/**
	 *	Finds the rules for a specific item type within an economy, copies those
	 *	rules, and relates them to the specified item.
	 * 
	 * 	@param  int   $itemID 		The ID of the item to receive rules.
	 * 	@param  int   $economyID 	The ID of the economy that has the rules for the item.
	 * 	@param  int   $itemType 	Represents the item type.
	 */
	public static function copyItemRules($itemID, $economyID, $itemType)
	{
		$ruleList = Rule::model()->getItemTypeRules($economyID, $itemType);

		for ($i = 0; $i < sizeof($ruleList); $i++)
		{
			$newRule = Rule::model()->copyRule($ruleList[$i]);
			ItemHasRule::model()->createItemHasRule($itemID, $newRule->getRuleID());
		}
	}

	/**
	 *	Finds the rules for all item types within an economy, copies those
	 *	rules, and relates them to the specified economy.
	 * 
	 * 	@param  int   $newEconomyID 		The ID of the economy to receive rules.
	 * 	@param  int   $originalEconomyID 	The ID of the economy to copy rules from.
	 */
	public static function copyItemTypeRules($newEconomyID, $originalEconomyID)
	{
		$ruleList = Rule::model()->getItemTypeRules($originalEconomyID);

		for ($i = 0; $i < sizeof($ruleList); $i++)
		{
			$itemTypeID = $ruleList[$i]->getItemTypeID();
			$newRule = Rule::model()->copyRule($ruleList[$i]);
			ItemTypeHasRule::model()->createItemTypeHasRule($newEconomyID, $itemTypeID, $newRule->getRuleID());
		}
	}
	
	/**
		__________________ _______  _______    _______  _______  _______  _______  _______ _________ _______ 
		\__   __/\__   __/(  ____ \(       )  (  ____ )(  ____ \(  ____ )(  ___  )(  ____ )\__   __/(  ____ \
		   ) (      ) (   | (    \/| () () |  | (    )|| (    \/| (    )|| (   ) || (    )|   ) (   | (    \/
		   | |      | |   | (__    | || || |  | (____)|| (__    | (____)|| |   | || (____)|   | |   | (_____ 
		   | |      | |   |  __)   | |(_)| |  |     __)|  __)   |  _____)| |   | ||     __)   | |   (_____  )
		   | |      | |   | (      | |   | |  | (\ (   | (      | (      | |   | || (\ (      | |         ) |
		___) (___   | |   | (____/\| )   ( |  | ) \ \__| (____/\| )      | (___) || ) \ \__   | |   /\____) |
		\_______/   )_(   (_______/|/     \|  |/   \__/(_______/|/       (_______)|/   \__/   )_(   \_______)
		                                                                                                     
	 */
	
	public function scopes()
    {
    	$yesterdayDate = DateUtility::getResetYesterday(Yii::app()->user->economyID);
        return array(
        	'today' => array(
        		'with' => array('marketItems' => array('select' => false)),
                'condition' => 'marketItems.creation_date>=:yesterdayDate',
                'params' => array(':yesterdayDate' => $yesterdayDate),
            ),
            'purchased' => array(
            	'with' => array('marketItems' => array('select' => false)),
                'condition' => 'purchased=1',
            ),
            'today' => array(
            	'with' => array('marketItems' => array('select' => false)),
                'condition' => 'marketItems.creation_date>=:yesterdayDate',
                'params' => array(':yesterdayDate' => $yesterdayDate),
            ),
            'available' => array(
            	'with' => array('marketItems' => array('select' => false)),
                'condition' => 'purchased=0',
            ),
        );
    }

	public function type($itemType)
	{
	    $this->getDbCriteria()->mergeWith(array(
	    	'with' => 'itemType',
	        'condition' => 'item_type=:itemType',
	        'params' => array(':itemType' => $itemType),
	    ));
	    return $this;
	}
	
	/**
	 * 	Gets the name of the item with the specified ID.
	 * 	
	 *  @param  int     $itemID     Represents the item ID.
	 *  @return String	       	    The name of the item.
	 */
	public static function getItemNameByID($itemID)
	{
		$result = Item::model()->find(array(
			'select' => 'item',
			'condition' => 'item_id=:itemID',
			'params' => array(':itemID' => $itemID)
		));
		return $result->getItemName();
	}

	/**
	 * 	Gets the type of the item with the specified ID.
	 * 	
	 *  @param  int     $itemID     Represents the item ID.
	 *  @return String	       	    The type of the item.
	 */
	public static function getItemTypeByID($itemID)
	{
		$result = Item::model()->find(array(
			'select' => 'item_type_id',
			'with' => 'itemType',
			'condition' => 'item_id=:itemID',
			'params' => array(':itemID' => $itemID)
		));
		return $result->itemType->item_type;
	}

	/**
	 * 	Gets the list of items owned by the specified entity
	 * 	
	 * 	@param  int    $entityID 	Represents the entity owner ID.
	 *  @return array<int>  	    An array of all items owned by the entity.
	 */
	public static function getItemsByOwner($entityID)
	{
		return Item::model()->findAll(array(
			'with' => array('entityHasItems' => array('select' => false)),
			'condition' => 'entityHasItems.entity_id=:entityID',
			'params' => array(':entityID' => $entityID),
		));
	}
	
	/**
	 * 	Gets the list of items created by the specified entity and of an optional item type.
	 * 	
	 *  @param  int     $entityID 		Represents the entity creator ID.
	 *  @param  int     $itemType 		Represents an item type.
	 *  @return array<Item> 			An array of all items created by the entity.
	 */
	public static function getItemsByCreator($entityID, $itemType = null)
	{
		$condition = 'entity_creator_id=:entityID';
		$params = array(':entityID' => $entityID);

		if (!is_null($itemType))
		{
			$condition .= '&& item_type=:itemType';
			$params[':itemType'] = $itemType;
		}

		return Item::model()->findAll(array(
			'with' => 'itemType',
			'condition' => $condition,
			'params' => $params,
		));
	}

	/**
	 *  Finds all items that have a market item produced in the
	 *  specified economy. Use scopes to filter the search.
	 * 
	 *  @param  int 	$economyID 		Represents the economy ID.
	 *  @return array<Item> 			An array of items.
	 */
	public static function findByEconomyMarketItem($economyID)
	{
		return Item::model()->findAll(array(
			'with' => array(
				'entityCreator' => array('select' => '*'),
				'marketItems' => array('select' => false),
			),
			'condition' => 'economy_id=:economyID',
			'params' => array(':economyID' => $economyID),
		));
	}
	
	/**
		__________________ _______  _______    _______  _______ _________          _______  ______   _______ 
		\__   __/\__   __/(  ____ \(       )  (       )(  ____ \\__   __/|\     /|(  ___  )(  __  \ (  ____ \
		   ) (      ) (   | (    \/| () () |  | () () || (    \/   ) (   | )   ( || (   ) || (  \  )| (    \/
		   | |      | |   | (__    | || || |  | || || || (__       | |   | (___) || |   | || |   ) || (_____ 
		   | |      | |   |  __)   | |(_)| |  | |(_)| ||  __)      | |   |  ___  || |   | || |   | |(_____  )
		   | |      | |   | (      | |   | |  | |   | || (         | |   | (   ) || |   | || |   ) |      ) |
		___) (___   | |   | (____/\| )   ( |  | )   ( || (____/\   | |   | )   ( || (___) || (__/  )/\____) |
		\_______/   )_(   (_______/|/     \|  |/     \|(_______/   )_(   |/     \|(_______)(______/ \_______)
	                                                                                                     
	 */
	
	/**
	 * 	Updates the production economy for the item.
	 *
	 *  @return int 	The ID of the economy that the item will be produced in.
	 */
	public function updateExportEconomy($economyID)
	{
		$rule = Rule::findItemRule($this->getItemID(), RuleType::ITEM_EXPORT_ECONOMY);
		Rule::updateRuleValue($rule->getRuleID(), $economyID);
	}

	/**
	 * 	Gets the ID of the economy that the item is produced in.
	 * 	
	 *  @return int 	The economy that the item is produced in.
	 */
	public function getExportEconomy()
	{
		$rule = Rule::findItemRule($this->getItemID(), RuleType::ITEM_EXPORT_ECONOMY);
		return (is_null($rule)) ? null : $rule->getRuleValue();
	}

	/**
	 * 	Updates the value of the rule that defines the price of the item.
	 */
	public function updatePrice($price)
	{
		$rule = Rule::findItemRule($this->getItemID(), RuleType::ATTRIBUTE_EFFECT_ON_OWNER, Entity::ENTITY_ATTRIBUTE_MONETARY);
		Rule::updateRuleValue($rule->getRuleID(), $price * Economy::CENTS_PER_DOLLAR);
	}

	/**
	 * 	Gets the price of the item.
	 * 	
	 *  @return int 	The price of the item.
	 */
	public function getPrice()
	{
		$rule = Rule::findItemRule($this->getItemID(), RuleType::ATTRIBUTE_EFFECT_ON_OWNER, Entity::ENTITY_ATTRIBUTE_MONETARY);
		return (is_null($rule)) ? null : $rule->getRuleValue() / Economy::CENTS_PER_DOLLAR;
	}

	/**
	 * 	Updates the value of the rule that defines the efficiency of the item.
	 */
	public function updateEfficiency($efficency)
	{
		$rule = Rule::findItemRule($this->getItemID(), RuleType::ATTRIBUTE_EFFECT_ON_OWNER, Entity::ENTITY_ATTRIBUTE_EFFICIENCY);
		Rule::updateRuleValue($rule->getRuleID(), $efficency);
	}

	/**
	 * 	Gets the efficiency of the item.
	 * 	
	 *  @return int 	The efficiency of the item.
	 */
	public function getEfficiency()
	{
		$rule = Rule::findItemRule($this->getItemID(), RuleType::ATTRIBUTE_EFFECT_ON_OWNER, Entity::ENTITY_ATTRIBUTE_EFFICIENCY);
		return (is_null($rule)) ? null : $rule->getRuleValue();
	}

	/**
	 * 	Updates the value of the rule that defines the health of the item.
	 */
	public function updateHealth($health)
	{
		$rule = Rule::findItemRule($this->getItemID(), RuleType::ATTRIBUTE_EFFECT_ON_OWNER, Entity::ENTITY_ATTRIBUTE_HEALTH);
		Rule::updateRuleValue($rule->getRuleID(), $health);
	}

	/**
	 * 	Gets the health value of the item.
	 * 	
	 *  @return int 	The health value of the item.
	 */
	public function getHealth()
	{
		$rule = Rule::findItemRule($this->getItemID(), RuleType::ATTRIBUTE_EFFECT_ON_OWNER, Entity::ENTITY_ATTRIBUTE_HEALTH);
		return (is_null($rule)) ? null : $rule->getRuleValue();
	}

	public function getBenefitStart($entityAttibute)
	{
		$rule = Rule::findItemRule($this->getItemID(), RuleType::ITEM_BENEFIT_START, $entityAttibute);
		return (is_null($rule)) ? null : $rule->getRuleValue();
	}

	public function getBenefitDecreaseRate($entityAttibute)
	{
		$rule = Rule::findItemRule($this->getItemID(), RuleType::ITEM_BENEFIT_DECREASE_RATE, $entityAttibute);
		return (is_null($rule)) ? null : $rule->getRuleValue();
	}

	public function getBenefitReceiver()
	{
		$rule = Rule::findItemRule($this->getItemID(), RuleType::ITEM_BENEFIT_RECEIVER);
		return (is_null($rule)) ? null : $rule->getRuleValue();
	}

	public function updateBenefitStart($start, $entityAttibute)
	{
		$rule = Rule::findItemRule($this->getItemID(), RuleType::ITEM_BENEFIT_START, $entityAttibute);
		Rule::updateRuleValue($rule->getRuleID(), $start);
	}

	public function updateBenefitDecreaseRate($decreaseRate, $entityAttibute)
	{
		$rule = Rule::findItemRule($this->getItemID(), RuleType::ITEM_BENEFIT_DECREASE_RATE, $entityAttibute);
		Rule::updateRuleValue($rule->getRuleID(), $decreaseRate);
	}

	public function updateBenefitReceiver($entityType)
	{
		$rule = Rule::findItemRule($this->getItemID(), RuleType::ITEM_BENEFIT_RECEIVER);
		Rule::updateRuleValue($rule->getRuleID(), $entityType);
	}

	public function getItemStock()
	{
		if ($this->getItemType() == ItemType::ITEM_TYPE_FOOD_NAME)
		{
			return MarketItem::model()->item($this->getItemID())->available()->today()->count();
		}
		else
		{
			return MarketItem::model()->item($this->getItemID())->available()->count();
		}
	}

	public function getItemStockInEconomy($economyID)
	{
		if ($this->getItemType() == ItemType::ITEM_TYPE_FOOD_NAME)
		{
			return MarketItem::model()->economy($economyID)->item($this->getItemID())->available()->today()->count();
		}
		else
		{
			return MarketItem::model()->economy($economyID)->item($this->getItemID())->available()->count();
		}
	}

	public function findAvailableMarketItem($economyID)
	{
		if ($this->getItemType() == ItemType::ITEM_TYPE_FOOD_NAME)
		{
			return MarketItem::model()->economy($economyID)->item($this->getItemID())->available()->today()->find();
		}
		else
		{
			return MarketItem::model()->economy($economyID)->item($this->getItemID())->available()->find();
		}
	}

	public function getUnitsSold()
	{
		return MarketItem::model()->item($this->getItemID())->purchased()->count();
	}

	public function getUnitsPerished()
	{
		if ($this->getItemType() == ItemType::ITEM_TYPE_FOOD_NAME)
		{
			return MarketItem::model()->item($this->getItemID())->available()->beforeToday()->count();
		}
		else
		{
			return 0;
		}
	}
	
	public function getItemID()
	{
		return $this->item_id;
	}

	public function getItemType()
	{
		return $this->itemType->item_type;
	}
	
	public function getItemEntityID()
	{
		return $this->entity_creator_id;
	}
	
	public function getItemName()
	{
		return $this->item;
	}
	
	public function getItemDescription()
	{
		return $this->item_description;
	}
	
	public function getItemCreationDate()
	{
		return $this->creation_date;
	}
	
	public function getItemRuleList()
	{
		if (is_null($this->_itemRuleList))
		{
			$this->_itemRuleList = Rule::getItemRules($this->getItemID());
		}
		return $this->_itemRuleList;
	}
}
