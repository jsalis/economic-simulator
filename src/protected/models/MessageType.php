<?php

Yii::import('application.models._base.BaseMessageType');

/**
 * The MessageType class is there to represent all of the necessary interction that is
 * needed by the Message class to get information about the MessageType. It gives methods
 * in which one can access anything from the MessageType table using only the primary
 * key of the table and the name of the row (since each row is baisically unique).
 *
 * @author   <cmicklis@stetson.edu>
 * @since 	 v0.0.0
 */
class MessageType extends BaseMessageType
{
	/* Chat */
	const MESSAGE_TYPE_CHAT_NAME = 'Chat';

	/* Transfer */
	const MESSAGE_TYPE_ITEM_TRANSFER_NAME = 'Item Transfer';
	const MESSAGE_TYPE_MONETARY_TRANSFER_NAME = 'Monetary Transfer';
	const MESSAGE_TYPE_HEALTH_TRANSFER_NAME = 'Health Transfer';
	const MESSAGE_TYPE_EFFICIENCY_TRANSFER_NAME = 'Efficiency Transfer';

	/* Job Offer */
	const MESSAGE_TYPE_SEND_JOB_OFFER_NAME = 'Job Offer';
	const MESSAGE_TYPE_ACCEPT_JOB_OFFER_NAME = 'Accept Job Offer';
	const MESSAGE_TYPE_REJECT_JOB_OFFER_NAME = 'Reject Job Offer';
	const MESSAGE_TYPE_REVISE_JOB_OFFER_NAME = 'Revise Job Offer';

	/* Rent Offer */
	const MESSAGE_TYPE_SEND_RENT_OFFER = 'Rent Offer';
	const MESSAGE_TYPE_ACCEPT_RENT_OFFER = 'Accept Rent Offer';
	const MESSAGE_TYPE_REVISE_RENT_OFFER = 'Revise Rent Offer';
	const MESSAGE_TYPE_REJECT_RENT_OFFER = 'Reject Rent Offer';
	const MESSAGE_TYPE_PAID_RENT = 'Paid Rent';

	/* Loan Request */
	const MESSAGE_TYPE_SEND_LOAN_REQUEST_NAME = 'Loan Request';
	const MESSAGE_TYPE_ACCEPT_LOAN_REQUEST_NAME = 'Accept Loan Request';
	const MESSAGE_TYPE_REJECT_LOAN_REQUEST_NAME = 'Reject Loan Request';
	const MESSAGE_TYPE_REVISE_LOAN_REQUEST_NAME = 'Revise Loan Request';

	/* Government Election */
	const MESSAGE_TYPE_START_NOMINATION = 'Start Nomination';
	const MESSAGE_TYPE_START_ELECTION = 'Start Election';
	const MESSAGE_TYPE_END_ELECTION = 'End Election';
	const MESSAGE_TYPE_CANDIDATE_BALLOT = 'Candidate Ballot';
	const MESSAGE_TYPE_VOTE_FOR_CANDIDATE = 'Vote For Candidate';

	/* Petition */
	const MESSAGE_TYPE_PETITION_FOR_ENTITY_NAME = 'Petition For Entity';
	const MESSAGE_TYPE_PETITION_FOR_RULE_NAME = 'Petition For Rule';
	const MESSAGE_TYPE_PETITION_FOR_PUBLIC_GOOD_NAME = 'Petition For Public Good';
	const MESSAGE_TYPE_VOTE_FOR_PETITION_NAME = 'Vote For Petition';

	/* Tax */
	const MESSAGE_TYPE_PAID_PROPERTY_TAX = 'Paid Property Tax';
	const MESSAGE_TYPE_PAID_WEALTH_TAX = 'Paid Wealth Tax';

	/* Survey */
	const MESSAGE_TYPE_PRODUCT_DEMAND_SURVEY = 'Product Demand Survey';

	/* Student Actions */
	const MESSAGE_TYPE_ENTERED_ECONOMY = 'Entered Economy';
	const MESSAGE_TYPE_ENTERED_LABOR_MARKET = 'Entered Labor Market';

	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * 	The getMessageTypeNameByID method searches the primary key of the Messagetype
	 * 	on the MessageType table and then gets the name of that Message type.
	 * 	
	 * 	@param  int    $messageTypeID 	Represents the primary key of the Message type.
	 *  @return String        			Represents the name on the MessageType table.
	 */
	public static function getMessageTypeNameByID($messageTypeID)
	{
		$messageInfo = MessageType::model()->find(array(
			'select' => 'message_type',
			'condition' => 'message_type_id=:messageTypeID',
			'params' => array(':messageTypeID' => $messageTypeID)
		));
		return $messageInfo->message_type;
	}

	/**
	 * 	The getMessageTypeIDByName method searches the name of the Messagetype
	 * 	on the MessageType table and then gets the primary key of that Message type.
	 * 	
	 * 	@param  String $messageType 	Represents the name of the Message type.
	 *  @return int           			Represents the primary key on the
	 *                                  MessageType table.
	 */
	public static function getMessageTypeIDByName($messageType)
	{
		$messageInfo = MessageType::model()->find(array(
			'select' => 'message_type_id',
			'condition' => 'message_type=:messageType',
			'params' => array(':messageType' => $messageType)
		));
		return $messageInfo->message_type_id;
	}
}
