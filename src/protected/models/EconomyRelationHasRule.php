<?php

Yii::import('application.models._base.BaseEconomyRelationHasRule');

/**
 * The EconomyRelationHasRule class is there to represent every bit of detail when it comes to 
 * the relation of the EconomyRelations and Rules. This relation only goes as deep as both the
 * primary keys of the EconomyRelation and Rule. So the only methods for this class are report
 * methods that get either all the Rule ids for the EconomyRelation or vice versa.
 * 
 * @author   <cmicklis@stetson.edu>, <jsalis@stetson.edu>
 * @since 	 v0.0.0
 * 
 */

class EconomyRelationHasRule extends BaseEconomyRelationHasRule
{
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * 	Creates a new rule associated with the economy relation to the EconomyRelationHasRule table.
	 * 	This allows the economy relation to have a relation to one or more rules.
	 * 	
	 *  @param int      $economyRelationID    	  Represents the EconomyRelation ID.
	 *  @param int      $ruleID                   Represents the rule ID.
	 */
	public static function createEconomyRelationHasRule($economyRelationID, $ruleID)
	{
		$economyRelationHasRule = new EconomyRelationHasRule;
		$economyRelationHasRule->economy_relation_id = $economyRelationID;
		$economyRelationHasRule->rule_id = $ruleID;
		$economyRelationHasRule->save();

		return $economyRelationHasRule;
	}
}
