<?php

/**
 * This is the model base class for the table "{{user_has_entity}}".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "UserHasEntity".
 *
 * Columns in table "{{user_has_entity}}" available as properties of the model,
 * followed by relations of table "{{user_has_entity}}" available as properties of the model.
 *
 * @property integer $user_has_entity_id
 * @property integer $user_id
 * @property integer $entity_id
 *
 * @property Entity $entity
 * @property User $user
 */
abstract class BaseUserHasEntity extends GxActiveRecord {

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function tableName() {
		return '{{user_has_entity}}';
	}

	public static function label($n = 1) {
		return Yii::t('app', 'UserHasEntity|UserHasEntities', $n);
	}

	public static function representingColumn() {
		return 'user_has_entity_id';
	}

	public function rules() {
		return array(
			array('user_id, entity_id', 'required'),
			array('user_id, entity_id', 'numerical', 'integerOnly'=>true),
			array('user_has_entity_id, user_id, entity_id', 'safe', 'on'=>'search'),
		);
	}

	public function relations() {
		return array(
			'entity' => array(self::BELONGS_TO, 'Entity', 'entity_id'),
			'user' => array(self::BELONGS_TO, 'User', 'user_id'),
		);
	}

	public function pivotModels() {
		return array(
		);
	}

	public function attributeLabels() {
		return array(
			'user_has_entity_id' => Yii::t('app', 'User Has Entity'),
			'user_id' => null,
			'entity_id' => null,
			'entity' => null,
			'user' => null,
		);
	}

	public function search() {
		$criteria = new CDbCriteria;

		$criteria->compare('user_has_entity_id', $this->user_has_entity_id);
		$criteria->compare('user_id', $this->user_id);
		$criteria->compare('entity_id', $this->entity_id);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}
}