<?php

Yii::import('application.models._base.BaseRuleType');

/**
 * The RuleType class is there to represent all of the necessary interction that is
 * needed by the Rule class to get information about the RuleType. It gives methods
 * in which one can access anything from the RuleType table using only the primary
 * key of the table and the name of the row (since each row is baisically unique).
 *
 * @author   <cmicklis@stetson.edu>, <jsalis@stetson.edu>
 * @since 	 v0.0.0
 */
class RuleType extends BaseRuleType
{
	const ENTITY_ACTION = 'Entity Action';
	const INITIAL_ENTITY_BALANCE = 'Initial Entity Balance';
	const ENTITY_ATTRIBUTE_NECESSITY = 'Entity Attribute Necessity';

	const ATTRIBUTE_EFFECT_ON_OWNER = 'Attribute Effect On Owner';
	const ATTRIBUTE_EFFECT_ON_OWNER_LIST = 'Attribute Effect On Owner List';
	const ATTRIBUTE_EFFECT_ON_OWNER_ECONOMY = 'Attribute Effect On Owner Economy';

	const TAXATION_FROM_PURCHASER = 'Taxation From Purchaser';
	const TAXATION_FROM_PURCHASER_GROUP = 'Taxation From Purchaser Group';
	const TAXATION_FROM_PURCHASER_ECONOMY = 'Taxation From Purchaser Economy';
	const TAXATION_OF_ENTITY_WEALTH = 'Taxation of Entity Wealth';

	const COMPANY_TYPE = 'Company Type';
	const COMPANY_TOTAL_FACTOR_PRODUCTIVITY = 'Company Total Factor Productivity';
	const OUTPUT_ELASTICITY_FOR_COMPANY_TYPE = 'Output Elasticity For Company Type';
	
	const ECONOMY_TIME_DIFFERENCE_FROM_UTC = 'Economy Time Difference From UTC';
	const ECONOMY_RESET_AFTER_MIDNIGHT = 'Economy Reset After Midnight';
	const ECONOMY_PRODUCTION_FUNCTION = 'Economy Production Function';
	const ECONOMY_BENEFIT_FUNCTION = 'Economy Benefit Function';

	const SEATS_IN_CONGRESS = 'Seats In Congress';
	const PETITION_PASS_PERCENTAGE = 'Petition Pass Percentage';

	const ITEM_EXPORT_ECONOMY = 'Item Export Economy';
	const ITEM_BENEFIT_START = 'Item Benefit Start';
	const ITEM_BENEFIT_DECREASE_RATE = 'Item Benefit Decrease Rate';
	const ITEM_BENEFIT_RECEIVER = 'Item Benefit Receiver';
	
	const MIN_VALUE = 'Minimum Value';
	const MAX_VALUE = 'Maximum Value';

	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * Checks if a rule type is able to change state.
	 * 
	 * @param  String  $ruleType   The rule type to check.
	 * @return boolean             Whether the rule type is able to change state.
	 */
	public static function hasState($ruleType)
	{
		return $ruleType == RuleType::ENTITY_ACTION ||
			   $ruleType == RuleType::ATTRIBUTE_EFFECT_ON_OWNER ||
			   $ruleType == RuleType::ATTRIBUTE_EFFECT_ON_OWNER_LIST ||
			   $ruleType == RuleType::ATTRIBUTE_EFFECT_ON_OWNER_ECONOMY;
	}

	/**
	 * Checks if a rule type has a value associated with it.
	 * 
	 * @param  String  $ruleType   The rule type to check.
	 * @return boolean             Whether the rule type has a value.
	 */
	public static function hasValue($ruleType)
	{
		return $ruleType != RuleType::ENTITY_ACTION;
	}

	/**
	 * 	The getRuleTypeByRuleTypeID method searches the primary key of the Ruletype
	 * 	on the RuleType table and then gets the name of that Rule type.
	 * 	
	 * 	@param  int    $ruleTypeID 	    Represents the primary key of the Rule type.
	 *  @return String        			Represents the name on the RuleType table.
	 */
	public static function getRuleTypeByRuleTypeID($ruleTypeID)
	{
		$ruleInfo = RuleType::model()->find(array(
			'select' => 'rule_type',
			'condition' => 'rule_type_id=:ruleTypeID',
			'params' => array(':ruleTypeID' => $ruleTypeID)
		));
		return $ruleInfo->rule_type;
	}

	/**
	 * 	The getRuleTypeIDByRuleType method searches the name of the Ruletype
	 * 	on the RuleType table and then gets the primary key of that Rule type.
	 * 	
	 * 	@param  String $ruleType 	Represents the name of the Rule type.
	 *  @return int           			Represents the primary key on the
	 *                                  RuleType table.
	 */
	public static function getRuleTypeIDByRuleType($ruleType)
	{
		$ruleInfo = RuleType::model()->find(array(
			'select' => 'rule_type_id',
			'condition' => 'rule_type=:ruleType',
			'params' => array(':ruleType' => $ruleType)
		));
		return $ruleInfo->rule_type_id;
	}
}
