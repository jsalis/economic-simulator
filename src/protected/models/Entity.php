<?php

Yii::import('application.models._base.BaseEntity');

/**
 * The EntityForm class is there to represent every bit of detail that a Entity may have
 * from the total balance of the entity to the items and rules associated with that entity.
 * The Class is setup so that the Worker, Accessor, Company, Goverment, or Bank can easily
 * access all of the information needed. The Entity also exists to work as a ReportForm
 * for all things an entity will have.
 *
 * @author   <cmicklis@stetson.edu>, <jsalis@stetson.edu>
 * @since 	 v0.0.0
 */

class Entity extends BaseEntity
{
	const ENTITY_INITIAL_BALANCE = 1000;
	const ENTITY_BASE_EFFICIENCY = 0;

	const ENTITY_ATTRIBUTE_MONETARY = 'Monetary';
	const ENTITY_ATTRIBUTE_HEALTH = 'Health';
	const ENTITY_ATTRIBUTE_EFFICIENCY = 'Efficiency';

	private $_health;
	private $_efficiency;
	private $_entityRuleList;
	private $_rentedItemList;

	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * 	Creates a new entity using all information given into the Entity table.
	 * 	
	 *  @param  String   $name         Represents the entity name.
	 *  @param  String   $entityType   Represents the entity type.
	 * 	@param  int      $economyID    The ID of the economy that has the rules for the entity.
	 *  @return int 				   Represents the entity ID.
	 */
	public static function createEntity($name, $entityType, $economyID = Economy::ECONOMY_DEFAULT_ID)
	{
		$entity = new Entity;
		$entity->entity_type_id = EntityType::getEntityTypeIDByName($entityType);
		$entity->entity = $name;

		$balance = Economy::model()->getInitialEntityBalance($economyID, $entityType);
		$entity->balance = (is_null($balance)) ? 0 : floor($balance * Economy::CENTS_PER_DOLLAR);

		$entity->creation_date = DateUtility::getCurrentDate($economyID);

		$entity->save();

		Entity::model()->copyEntityRules($entity->entity_id, $economyID, $entityType);

		return $entity->entity_id;
	}

	/**
	 *	Finds the rules for a specific entity type within an economy, copies those
	 *	rules, and relates them to the specified entity.
	 * 
	 * 	@param  int   $entityID 	The ID of the entity to receive rules.
	 * 	@param  int   $economyID 	The ID of the economy that has the rules for the entity.
	 * 	@param  int   $entityType 	Represents the entity type.
	 */
	public static function copyEntityRules($entityID, $economyID, $entityType)
	{
		$ruleList = Rule::model()->getEntityTypeRules($economyID, $entityType);

		for ($i = 0; $i < sizeof($ruleList); $i++)
		{
			$newRule = Rule::model()->copyRule($ruleList[$i]);
			EntityHasRule::model()->createEntityHasRule($entityID, $newRule->getRuleID());
		}
	}

	/**
	 *	Finds the rules for all entity types within an economy, copies those
	 *	rules, and relates them to the specified economy.
	 * 
	 * 	@param  int   $newEconomyID 		The ID of the economy to receive rules.
	 * 	@param  int   $originalEconomyID 	The ID of the economy to copy rules from.
	 */
	public static function copyEntityTypeRules($newEconomyID, $originalEconomyID)
	{
		$ruleList = Rule::model()->getEntityTypeRules($originalEconomyID);

		for ($i = 0; $i < sizeof($ruleList); $i++)
		{
			$entityTypeID = $ruleList[$i]->getEntityTypeID();
			$newRule = Rule::model()->copyRule($ruleList[$i]);
			EntityTypeHasRule::model()->createEntityTypeHasRule($newEconomyID, $entityTypeID, $newRule->getRuleID());
		}
	}

	/**
	 *  Adds money to the balance of an entity.
	 * 
	 *  @param  int    $receiverID     Represents the ID of the entity receiver.
	 *  @param  int    $amount         The dollar amount to send.
	 */
	public static function addMonetary($receiverID, $amount)
	{
		if ($amount == 0) return;
		$amountCents = floor($amount * Economy::CENTS_PER_DOLLAR);
		$currentBalance = Entity::getEntityBalanceByID($receiverID);
		$newBalance = $currentBalance + $amountCents;

		if ($newBalance < 0)
		{
			$newBalance = 0;
		}

		$messageContent = ($amountCents < 0) ? 'Money has been removed from your account.' : 'Money has been added to your account.';

		Entity::updateEntityBalance($receiverID, $newBalance);
		Entity::createEntityHasMessage($receiverID, $receiverID, MessageType::MESSAGE_TYPE_MONETARY_TRANSFER_NAME, array(
			Message::MESSAGE_KEY_CONTENT => $messageContent,
			Message::MESSAGE_KEY_MONEY => ($newBalance - $currentBalance) / Economy::CENTS_PER_DOLLAR,
		));
	}

	/**
	 *  Transfers monetary from one entity to another.
	 * 
	 *  @param  int    $senderID       Represents the ID of the entity sender.
	 *  @param  int    $receiverID     Represents the ID of the entity receiver.
	 *  @param  int    $payment        The dollar amount to send.
	 *  @param  int    $description    An optional payment description.
	 *  @return boolean                Whether the transaction was successful.
	 */
	public static function transferMonetary($senderID, $receiverID, $payment, $description = '')
	{
		if ($payment == 0) return true;
		$paymentCents = floor($payment * Economy::CENTS_PER_DOLLAR);
		$senderNewBalance = Entity::getEntityBalanceByID($senderID) - $paymentCents;
		$receiverNewBalance = Entity::getEntityBalanceByID($receiverID) + $paymentCents;

		if ($description != '')
		{
			$description .= ' ';
		}

		if ($senderNewBalance >= 0)
		{
			Entity::updateEntityBalance($senderID, $senderNewBalance);
			Entity::updateEntityBalance($receiverID, $receiverNewBalance);

			Entity::createEntityHasMessage($senderID, $receiverID, MessageType::MESSAGE_TYPE_MONETARY_TRANSFER_NAME, array(
				Message::MESSAGE_KEY_CONTENT => Entity::getEntityNameByID($senderID) . ' has sent you a ' . $description . 'payment.',
				Message::MESSAGE_KEY_MONEY => $paymentCents / Economy::CENTS_PER_DOLLAR,
			));

			Entity::createEntityHasMessage($receiverID, $senderID, MessageType::MESSAGE_TYPE_MONETARY_TRANSFER_NAME, array(
				Message::MESSAGE_KEY_CONTENT => 'A ' . $description . 'payment has been sent to ' . Entity::getEntityNameByID($receiverID) . '.',
				Message::MESSAGE_KEY_MONEY => $paymentCents / Economy::CENTS_PER_DOLLAR * -1,
			));

			return true;
		}
		else
		{
			return false;
		}
	}

	/**
	 *  Transfers health to an entity or a list of entities.
	 * 
	 *  @param int    $senderID       Represents the ID of the entity sender.
	 *  @param array  $receiverID     Represents the ID, or a list of IDs, of the entity receiver.
	 *  @param int    $health         The amount of health to send.
	 */
	public static function transferHealth($senderID, $receiverID, $health)
	{
		if ($health == 0) return;
		$messageContent = ($health < 0) ? 'Your health has decreased.' : 'Your health has increased.';

		Entity::createEntityHasMessage($senderID, $receiverID, MessageType::MESSAGE_TYPE_HEALTH_TRANSFER_NAME, array(
			Message::MESSAGE_KEY_CONTENT => $messageContent,
			Message::MESSAGE_KEY_HEALTH => $health,
		));
	}

	/**
	 *  Transfers item from one entity to another.
	 * 
	 *  @param int    $senderID       Represents the ID of the entity sender.
	 *  @param int    $receiverID     Represents the ID of the entity receiver.
	 *  @param int    $itemID         The ID of the item.
	 */
	public static function transferItem($senderID, $receiverID, $itemID)
	{
		Entity::createEntityHasMessage($senderID, $receiverID, MessageType::MESSAGE_TYPE_ITEM_TRANSFER_NAME, array(
			Message::MESSAGE_KEY_CONTENT => Entity::getEntityNameByID($senderID) . ' has sent you an item.',
			Message::MESSAGE_KEY_ITEM_ID => $itemID,
		));

		Entity::createEntityHasMessage($receiverID, $senderID, MessageType::MESSAGE_TYPE_ITEM_TRANSFER_NAME, array(
			Message::MESSAGE_KEY_CONTENT => 'An item has been sent to ' . Entity::getEntityNameByID($receiverID) . '.',
			Message::MESSAGE_KEY_ITEM_ID => $itemID,
		));
	}

	/**
	 *  Creates a new message and a new entityHasMessage which associates the message
	 *  between a sender entity and a receiver entity. An array of receiver IDs can also be passed.
	 * 
	 *  @param int      $senderID       Represents the ID of the entity sender.
	 *  @param array    $receiverID     Represents the ID, or a list of IDs, of the entity receiver.
	 * 	@param int    	$messageType	The type of message to be created.
	 * 	@param array	$append 		An array of key values to be encoded into 
	 * 	                       			the message. Can be retieved later using the keys.
	 *  @return int 				   	Represents the EntityHasMessage ID.
	 */
	public static function createEntityHasMessage($senderID, $receiverID, $messageType, $append)
	{
		$messageID = Message::createMessage($messageType, $append);

		if (is_numeric($receiverID))
		{
			return EntityHasMessage::createEntityHasMessage($messageID, $receiverID, $senderID);
		}
		else
		{
			$entityHasMessageIDs = array();
			for ($i = 0; $i < sizeof($receiverID); $i++)
			{
				$entityHasMessageIDs[] = EntityHasMessage::createEntityHasMessage($messageID, $receiverID[$i], $senderID);
			}
			return $entityHasMessageIDs;
		}
	}

	/**
	 *  The updateEntityBalance method updates the balance of the entity using
	 *  the balance and the entities ID given in the parameters.
	 * 
	 *  @param  int        $id      		The ID of the entity.
	 *  @param  int        $balance 		The new balance of the entity.
	 */
	public static function updateEntityBalance($id, $balance)
	{
		Entity::model()->updateByPk($id, array(
			'balance' => $balance
		));
	}

	/**
		 _______  _       ___________________________            _______  _______  _______  _______  _______ _________ _______ 
		(  ____ \( (    /|\__   __/\__   __/\__   __/|\     /|  (  ____ )(  ____ \(  ____ )(  ___  )(  ____ )\__   __/(  ____ \
		| (    \/|  \  ( |   ) (      ) (      ) (   ( \   / )  | (    )|| (    \/| (    )|| (   ) || (    )|   ) (   | (    \/
		| (__    |   \ | |   | |      | |      | |    \ (_) /   | (____)|| (__    | (____)|| |   | || (____)|   | |   | (_____ 
		|  __)   | (\ \) |   | |      | |      | |     \   /    |     __)|  __)   |  _____)| |   | ||     __)   | |   (_____  )
		| (      | | \   |   | |      | |      | |      ) (     | (\ (   | (      | (      | |   | || (\ (      | |         ) |
		| (____/\| )  \  |   | |   ___) (___   | |      | |     | ) \ \__| (____/\| )      | (___) || ) \ \__   | |   /\____) |
		(_______/|/    )_)   )_(   \_______/   )_(      \_/     |/   \__/(_______/|/       (_______)|/   \__/   )_(   \_______)
                                                                                                                       
	 */
	
	public function type($entityType)
	{
	    $this->getDbCriteria()->mergeWith(array(
	    	'with' => 'entityType',
	        'condition' => 'entity_type=:entityType',
	        'params' => array(':entityType' => $entityType),
	    ));
	    return $this;
	}

    public function user($userID)
	{
	    $this->getDbCriteria()->mergeWith(array(
        	'with' => array('userHasEntities' => array('select' => false)),
			'condition' => 'userHasEntities.user_id=:userID',
			'params' => array(':userID' => $userID),
        ));
	    return $this;
	}

	public function economy($economyID)
	{
	    $this->getDbCriteria()->mergeWith(array(
        	'with' => array('economyHasEntities' => array('select' => false)),
			'condition' => 'economyHasEntities.economy_id=:economyID',
			'params' => array(':economyID' => $economyID),
        ));
	    return $this;
	}
	
	/**
	 * 	The getDefaultEntityName method returns a string that contains the 
	 * 	default name for an entity type. When a new entity is created without 
	 * 	a name it will be set to the default name.
	 *
	 * 	@param  String  $entityType  The type of Entity.
	 *  @return String 			     The default name of the Entity.
	 */
	public static function getDefaultEntityName($entityType)
	{
		return $entityType . ' Name';
	}
	
	/**
	 * 	The getEntityNameByID method gets the name of the entity with
	 * 	the specified ID.
	 * 	
	 *  @param  int     $entityID     Represents the entity ID.
	 *  @return String	       	      The name of the entity.
	 */
	public static function getEntityNameByID($entityID)
	{
		$result = Entity::model()->find(array(
			'select' => 'entity',
			'condition' => 'entity_id=:entityID',
			'params' => array(':entityID' => $entityID)
		));
		return $result->entity;
	}

	/**
	 * 	The getEntityBalanceByID method gets the balance of the specified entity in cents.
	 * 	
	 *  @param  int     $entityID     Represents the entity ID.
	 *  @return int  	       	      The balance of the entity in cents.
	 */
	public static function getEntityBalanceByID($entityID)
	{
		$result = Entity::model()->find(array(
			'select' => 'balance',
			'condition' => 'entity_id=:entityID',
			'params' => array(':entityID' => $entityID)
		));
		return $result->balance;
	}

	/**
	 * 	The getEntityTypeByEntityID method gets the type of the entity with
	 * 	the specified ID.
	 * 	
	 *  @param  int     $entityID     Represents the entity ID.
	 *  @return String	       	      The entity type.
	 */
	public static function getEntityTypeByEntityID($entityID)
	{
		$result = Entity::model()->find(array(
			'select' => 'entity_type_id',
			'condition' => 'entity_id=:entityID',
			'params' => array(':entityID' => $entityID)
		));
		return EntityType::getEntityTypeNameByID($result->entity_type_id);
	}

	/**
	 * [getEntityByID description]
	 * 
	 * @param  int  	$entityID      	[description]
	 * @return Entity                 	[description]
	 */
	public static function getEntityByID($entityID)
	{
		$entity = null;
		switch (Entity::getEntityTypeByEntityID($entityID))
		{
			case EntityType::ENTITY_TYPE_WORKER_NAME:
				$entity = Worker::model()->findByPk($entityID);
				break;

			case EntityType::ENTITY_TYPE_COMPANY_NAME:
				$entity = Company::model()->findByPk($entityID);
				break;

			case EntityType::ENTITY_TYPE_GOVERNMENT_NAME:
				$entity = Government::model()->findByPk($entityID);
				break;

			case EntityType::ENTITY_TYPE_BANK_NAME:
				$entity = Bank::model()->findByPk($entityID);
				break;

			case EntityType::ENTITY_TYPE_ACCESSOR_NAME:
				$entity = Accessor::model()->findByPk($entityID);
				break;
		}
		return $entity;
	}
	
	/**
	 * 	Gets a list of entities that the specified user has a relation with.
	 * 	Entity are constructed as the class defined by their type.
	 * 	
	 *  @param  int     	$userID 	  	Tthe user ID.
	 *  @return array<Entity> 	      		An array of entities.
	 */
	public static function getEntitiesByUserID($userID)
	{
		$entityIDList = UserHasEntity::getEntityIDsByUserID($userID);
		$entityList = array();

		for ($i = 0; $i < sizeof($entityIDList); $i++)
		{
			$id = $entityIDList[$i];
			$entityList[] = Entity::model()->getEntityByID($id);
		}
		return $entityList;
	}

	/**
	 * 	The getEntityHealthByID method returns the current health that an entity has
	 * 	for that day.
	 *
	 * 	It does this by getting all messages that are of type MESSAGE_TYPE_HEALTH_TRANSFER_NAME
	 * 	since the last economy reset date, and adds their health values.
	 * 	
	 *  @param  int      $entityID 			The ID of the Entity.
	 *  @return int           	           	The amount of health that entity has.
	 */
	public static function getEntityHealthByID($entityID)
	{
		$messageList = EntityHasMessage::model()
			->messageType( MessageType::MESSAGE_TYPE_HEALTH_TRANSFER_NAME )
			->receiver( $entityID )
			->lowerDate( DateUtility::getResetYesterday(Yii::app()->user->economyID) )
			->findAll();

		$sum = 0;
		for ($i = 0; $i < sizeof($messageList); $i++)
		{
			$sum += $messageList[$i]->getMessage()->findByKey(Message::MESSAGE_KEY_HEALTH);
		}
		return $sum;
	}

	/**
	 * 	The getEntityEfficiencyByID method finds all of the items that the entity has
	 * 	and sums the value of their efficiency.
	 *  
	 *  @param  int      $entityID 			The ID of the Entity.
	 *  @return int                         Represents the efficiency of the entity.
	 */
	public static function getEntityEfficiencyByID($entityID)
	{		
		$sum = Entity::ENTITY_BASE_EFFICIENCY;
		$entityHasItems = EntityHasItem::model()->findByEntityID($entityID);

		for ($i = 0; $i < sizeof($entityHasItems); $i++)
		{
			$id = $entityHasItems[$i]->getID();
			if (!EntityRentEntityHasItem::model()->isRentInProgress($id))
			{
				$efficiency = $entityHasItems[$i]->getItem()->getEfficiency();

				if (!is_null($efficiency))
				{
					$sum += $efficiency;
				}
			}
		}
		
		if ($sum == Entity::ENTITY_BASE_EFFICIENCY && 
			Entity::getEntityTypeByEntityID($entityID) == EntityType::ENTITY_TYPE_WORKER_NAME)
		{
			$sum ++;
		}

		return $sum;
	}

	/**
	 *	The getEntityTotalEarnings method gets all of the received messages that
	 *	are of transfer monetary type and then checks to see if the amount stored
	 *	in the message is positive, if it is then it is an earning for the entity.
	 *	It then sums up all these messages and returns the summed up value.
	 *	
	 *  @param  int         $entityID 		The ID of the entity
	 *  @return int           				The total earnings of the entity.
	 */
	public static function getEntityTotalEarnings($entityID)
	{
		$messageList = EntityHasMessage::model()
			->messageType( MessageType::MESSAGE_TYPE_MONETARY_TRANSFER_NAME )
			->receiver( $entityID )
			->findAll();

		$sum = 0;
		for ($i = 0; $i < sizeof($messageList); $i++)
		{
			$amount = $messageList[$i]->getMessage()->findByKey(Message::MESSAGE_KEY_MONEY);

			if ($amount > 0)
			{
				$sum += $amount;
			}
		}
		return $sum;
	}

	/**	
	 *  [description]
	 *	
	 *  @param  int         $entityID 		The ID of the entity
	 *  @return int           				The total earnings of the entity.
	 */
	public static function getEntityTotalSpendings($entityID)
	{
		$messageList = EntityHasMessage::model()
			->messageType( MessageType::MESSAGE_TYPE_MONETARY_TRANSFER_NAME )
			->sender( $entityID )
			->findAll();

		$sum = 0;
		for ($i = 0; $i < sizeof($messageList); $i++)
		{
			$amount = $messageList[$i]->getMessage()->findByKey(Message::MESSAGE_KEY_MONEY);

			if ($amount > 0)
			{
				$sum += $amount;
			}
		}
		return $sum;
	}

	/**
		 _______  _       ___________________________            _______  _______ _________          _______  ______   _______ 
		(  ____ \( (    /|\__   __/\__   __/\__   __/|\     /|  (       )(  ____ \\__   __/|\     /|(  ___  )(  __  \ (  ____ \
		| (    \/|  \  ( |   ) (      ) (      ) (   ( \   / )  | () () || (    \/   ) (   | )   ( || (   ) || (  \  )| (    \/
		| (__    |   \ | |   | |      | |      | |    \ (_) /   | || || || (__       | |   | (___) || |   | || |   ) || (_____ 
		|  __)   | (\ \) |   | |      | |      | |     \   /    | |(_)| ||  __)      | |   |  ___  || |   | || |   | |(_____  )
		| (      | | \   |   | |      | |      | |      ) (     | |   | || (         | |   | (   ) || |   | || |   ) |      ) |
		| (____/\| )  \  |   | |   ___) (___   | |      | |     | )   ( || (____/\   | |   | )   ( || (___) || (__/  )/\____) |
		(_______/|/    )_)   )_(   \_______/   )_(      \_/     |/     \|(_______/   )_(   |/     \|(_______)(______/ \_______)
                                                                                                                       
	 */
	
	public function updateSimulation()
    {
    	// Pay item rent
    	$rentItemList = $this->getRentItemList();
		for ($i = 0; $i < sizeof($rentItemList); $i++)
		{
			$this->payItemRent($rentItemList[$i]);
		}

		// Pay property tax on land items
		$tax = Economy::getEconomyPropertyTax(Yii::app()->user->economyID);
		$entityHasItems = $this->getEntityHasItems(ItemType::ITEM_TYPE_LAND_NAME);
		for ($i = 0; $i < sizeof($entityHasItems); $i++)
		{
			$this->payPropertyTax($entityHasItems[$i]->getItem(), $tax);
		}
    }

    /**
	 * 	Checks if the daily rent has been paid for the specified item. If not then 
	 * 	money is transfered to the owner entity. If successful, a message is sent to 
	 * 	the public messenger stating that the entity has paid rent.
	 * 	
	 *  @param  EntityRentEntityHasItem    	$rentItem 	Represents the item that the 
	 *                                                 	company is currently renting.
	 */
	public function payItemRent($rentItem)
	{
		$paidRentMessageList = EntityHasMessage::model()
			->messageType( MessageType::MESSAGE_TYPE_PAID_RENT )
			->sender( $this->getEntityID() )
			->lowerDate( DateUtility::getResetYesterday(Yii::app()->user->economyID) )
			->findAll();

		$entityHasItemID = $rentItem->getEntityHasItemID();
		$isPaid = false;

		for ($i = 0; $i < sizeof($paidRentMessageList); $i++)
		{
			if ($entityHasItemID == $paidRentMessageList[$i]->getMessage()->findByKey(Message::MESSAGE_KEY_ITEM_ID))
			{
				$isPaid = true;
				break;
			}
		}

		if (!$isPaid)
		{
			$owner = $rentItem->getOwner();
			$price = $rentItem->getPricePerDay();
			
			if (Entity::transferMonetary($this->getEntityID(), $owner->getEntityID(), $price, 'rent'))
			{
				$publicMessengerID = Economy::getPublicMessengerID(Yii::app()->user->economyID);

				$this->sendMessage($publicMessengerID, MessageType::MESSAGE_TYPE_PAID_RENT, array(
					Message::MESSAGE_KEY_CONTENT => $this->getEntityName() . ' has paid rent.',
					Message::MESSAGE_KEY_ITEM_ID => $entityHasItemID,
				));
			}
		}
	}

	/**
	 * 	Checks if property tax has been paid for an item during the current day. If not, 
	 * 	then the property tax is transfered to the government and the economy is notified 
	 * 	that the entity has paid the tax.
	 * 	
	 *  @param  Item 	$item    	Represents an item that the entity owns.
	 *  @param  int 	$tax    	Represents the propert tax.
	 */
	public function payPropertyTax($item, $tax)
	{
		if ($tax == 0) return;

		$propertyTaxList = EntityHasMessage::model()
			->messageType( MessageType::MESSAGE_TYPE_PAID_PROPERTY_TAX )
			->sender( $this->getEntityID() )
			->lowerDate( DateUtility::getResetYesterday(Yii::app()->user->economyID) )
			->findAll();

		$isPaid = false;

		for ($i = 0; $i < sizeof($propertyTaxList); $i++)
		{
			if ($item->getItemID() == $propertyTaxList[$i]->getMessage()->findByKey(Message::MESSAGE_KEY_ITEM_ID))
			{
				$isPaid = true;
				break;
			}
		}

		if (!$isPaid)
		{
			$governmentID = Economy::getGovernmentID(Yii::app()->user->economyID);

			if (Entity::transferMonetary($this->getEntityID(), $governmentID, $tax, 'property tax'))
			{
				$publicMessengerID = Economy::getPublicMessengerID(Yii::app()->user->economyID);
				$this->sendMessage($publicMessengerID, MessageType::MESSAGE_TYPE_PAID_PROPERTY_TAX, array(
					Message::MESSAGE_KEY_CONTENT => $this->getEntityName() . ' has paid the property tax.',
					Message::MESSAGE_KEY_ITEM_ID => $item->getItemID(),
				));
			}
		}
	}

	/**
	 * 	Checks to see if the balance of the entity is above the wealth tax threshold. 
	 * 	If it is then it checks to see if the entity has paid its daily wealth tax. 
	 * 	If the user has then he must not pay it again until tomorrow. Otherwise the wealth
	 * 	tax is transfered to the government and the economy is notified that the entity has 
	 * 	paid the tax. (Rich Bastard).
	 */
	public function payWealthTax()
	{
		$wealthThreshold = Economy::getWealthTaxThreshold(Yii::app()->user->economyID);
		$tax = Economy::getWealthTax(Yii::app()->user->economyID);

		if ($tax == 0 || $this->getEntityBalance() < $wealthThreshold)
		{
			return;
		}

		$taxMessageCount = EntityHasMessage::model()
			->messageType( MessageType::MESSAGE_TYPE_PAID_WEALTH_TAX )
			->sender( $this->getEntityID() )
			->lowerDate( DateUtility::getResetYesterday(Yii::app()->user->economyID) )
			->count();

		if ($taxMessageCount == 0)
		{
			$governmentID = Economy::getGovernmentID(Yii::app()->user->economyID);

			if (Entity::transferMonetary($this->getEntityID(), $governmentID, $tax, 'wealth tax'))
			{
				$publicMessengerID = Economy::getPublicMessengerID(Yii::app()->user->economyID);
				$this->sendMessage($publicMessengerID, MessageType::MESSAGE_TYPE_PAID_WEALTH_TAX, array(
					Message::MESSAGE_KEY_CONTENT => $this->getEntityName() . ' has paid the wealth tax.',
				));
			}
		}
	}

	/**
	 * 	Updates the name of an existing entity.
	 *
	 *  @param int      $entityID           Represents the entity ID.
	 *  @param String   $entityName         Represents the entity name.
	 */
	public function updateEntityName($entityID, $entityName)
	{
		if (empty($entityName))
		{
			return;
		}
		Entity::model()->updateByPk($entityID, array(
			'entity' => $entityName,
		));	
	}

	/**
	 * 	Gets the entity health necessity.
	 * 	
	 *  @return int     Represents the entity health necessity.
	 */
	public function getEntityHealthNecessity()
	{
		$rule = Rule::findEntityRule($this->getEntityID(), RuleType::ENTITY_ATTRIBUTE_NECESSITY, Entity::ENTITY_ATTRIBUTE_HEALTH);
		return (is_null($rule)) ? null : $rule->getRuleValue();
	}

	/**
	 * Creates and sends a message from this entity to another entity.
	 * 
	 * 	@param int 	  	$receiverID 	The ID of the entity to receive the message.
	 * 	@param int    	$messageType	The type of message to be passed.
	 * 	@param array	$append 		An array of key values to be encoded into 
	 * 	                       			the message. Can be retieved later using the keys.
	 * 	@return boolean                 Whether the message was sent.
	 */
	public function sendMessage($receiverID, $messageType, $append)
	{
		$messageFilter = new MessageFilter($this);
		$canSendMessage = $messageFilter->beforeMessageAction($this->getEntityID(), $receiverID, $messageType, $append);

		if ($canSendMessage)
		{
			$id = Entity::createEntityHasMessage($this->getEntityID(), $receiverID, $messageType, $append);	
			$entityHasMessage = EntityHasMessage::model()->findByPk($id);
			$messageFilter->afterMessageAction($entityHasMessage);
		}

		return $canSendMessage;
	}

	/**
	 * 	Gets the number of messages that the user has not yet checked.
	 * 	
	 *  @return 	int 	The number of messages the user has not checked.
	 */
	public function getUnreadMessageCount()
	{
		$lowerDate = DateUtility::getResetYesterday( Yii::app()->user->economyID );
		$upperDate = DateUtility::getResetToday( Yii::app()->user->economyID );

		return EntityHasMessage::model()
			->receiver( $this->getEntityID() )
			->checked( false )
			->lowerDate( $lowerDate )
			->upperDate( $upperDate )
			->count();
	}

	/**
	 * 	Gets the list of messages that were received during the current day.
	 * 	Excludes chat messages.
	 * 	
	 *  @return 	array<EntityHasMessage> 	The list of recent messages.
	 */
	public function getRecentMessageList()
	{
		$lowerDate = DateUtility::getResetYesterday( Yii::app()->user->economyID );
		$upperDate = DateUtility::getResetToday( Yii::app()->user->economyID );

		return EntityHasMessage::model()
			->receiver( $this->getEntityID() )
			->replied( false )
			->lowerDate( $lowerDate )
			->upperDate( $upperDate )
			->notChat()
			->findAll();
	}

	/**
	 *  Gets the list of all unreplied chat messages received by the entity.
	 *  
	 *  @return 	array<EntityHasMessage> 	The list of chat messages.
	 */
	public function getChatMessages()
	{
		return EntityHasMessage::model()
			->receiver( $this->getEntityID() )
			->replied( false )
			->chat()
			->findAll();
	}

	public function getEntityHasMessage($id)
	{
		$entityHasMessage = EntityHasMessage::model()->findByPk($id);

		if ($this->getEntityID() == $entityHasMessage->getEntityReceiverID())
		{
			EntityHasMessage::checkMessage($id);
			return $entityHasMessage;
		}
		else return null;
	}
	
	public function getEntityID()
	{
		return $this->entity_id;
	}

	public function getEntityType()
	{
		return $this->entityType->entity_type;
	}

	public function getEntityName()
	{
		return $this->entity;
	}

	public function getEntityBalance()
	{
		return $this->balance / Economy::CENTS_PER_DOLLAR;
	}

	public function getEntityHasItems($itemType = null)
	{
		if (is_null($itemType))
		{
			return EntityHasItem::model()->findByEntityID($this->getEntityID());
		}
		else
		{
			return EntityHasItem::model()->findByEntityID($this->getEntityID(), $itemType);
		}
	}

	public function getRentItemList()
	{
		if (is_null($this->_rentedItemList))
		{
			$this->_rentedItemList = EntityRentEntityHasItem::findByEntityID($this->getEntityID());
		}
		return $this->_rentedItemList;
	}

	public function getEntityHealth()
	{
		if (is_null($this->_health))
		{
			$this->_health = Entity::getEntityHealthByID($this->getEntityID());
		}
		return $this->_health;
	}

	public function getEntityEfficiency()
	{
		if (is_null($this->_efficiency))
		{
			$this->_efficiency = Entity::getEntityEfficiencyByID($this->getEntityID());
		}
		return $this->_efficiency;
	}

	public function getEntityRuleList()
	{
		if (is_null($this->_entityRuleList))
		{
			$this->_entityRuleList = Rule::getEntityRules($this->getEntityID());
		}
		return $this->_entityRuleList;
	}
}
