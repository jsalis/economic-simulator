<?php

Yii::import('application.models._base.BaseMessage');

/**
 * The Message class is there to represent every bit of detail that a Message may have
 * from the name and description to all the rules associated with that entity.
 * The Class is setup so that the Worker, Accessor, Company, Goverment, or Bank can easily
 * access all of the information needed. The Message also exists to work as a ReportForm
 * for all things an modifier will have.
 *
 * @author   <cmicklis@stetson.edu>, <jsalis@stetson.edu>
 * @since 	 v0.0.0
 */

class Message extends BaseMessage
{
	const MESSAGE_DELIMITER = ':';
	const MESSAGE_DELIMITER_REPLACEMENT = '&#58;';
	const MESSAGE_KEY_LENGTH = 1;
	const MESSAGE_KEY_CONTENT = 'c';
	const MESSAGE_KEY_PREVIOUS_MESSAGE = 'p';
	const MESSAGE_KEY_ITEM_TYPE = 't';
	const MESSAGE_KEY_ITEM_ID = 'i';
	const MESSAGE_KEY_ENTITY_ID = 'e';
	const MESSAGE_KEY_MONEY = 'm';
	const MESSAGE_KEY_UNITS = 'u';
	const MESSAGE_KEY_HEALTH = 'h';
	const MESSAGE_KEY_RULE_ID = 'r';
	const MESSAGE_KEY_RULE_VALUE = 'v';
	const MESSAGE_KEY_VOTE_RESPONSE = 'o';
	const MESSAGE_KEY_SUBJECT = 's';

	private $_messageParts;

	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	public function afterFind()
	{
		parent::afterFind();
		$this->_messageParts = explode(Message::MESSAGE_DELIMITER, $this->message);
	}

	/**
	 * 	The createMessage method adds a message using all information given into the Message table.
	 *
	 * 	@param String   $messageType	The type of message to be passed.
	 * 	@param array	$append 		An array of key values to be encoded into 
	 * 	                       			the message. Can be retieved later using the keys.
	 *  @return int 					Represents the ID of the new message.
	 */
	public static function createMessage($messageType, $append)
	{
		if (isset($append[Message::MESSAGE_KEY_CONTENT]))
		{
			$append[Message::MESSAGE_KEY_CONTENT] = 
				str_replace(Message::MESSAGE_DELIMITER, Message::MESSAGE_DELIMITER_REPLACEMENT, $append[Message::MESSAGE_KEY_CONTENT]);
		}

		$content = '';
		$firstPass = true;
		foreach ($append as $key => $value)
    	{
    		if ($firstPass)
    		{
    			$content .= $key . $value;
    			$firstPass = false;
    		}
    		else
    		{
    			$content .= Message::MESSAGE_DELIMITER . $key . $value;
    		}
		}

		$message = new Message;
		$message->message = $content;
		$message->message_type_id = MessageType::getMessageTypeIDByName($messageType);
		$message->save();

		return $message->message_id;
	}

	/**
	 * Adds an array of key value pairs to an existing message.
	 * 
	 * @param  int     $messageID  The ID of the message to update.
	 * @param  array   $append     An array of key values to be encoded into the 
	 *                             content. Can be retieved later using the keys.
	 */
	public static function appendToMessage($messageID, $append)
	{
		$message = Message::model()->findByPk($messageID);
		$content = $message->getMessage();

		foreach ($append as $key => $value)
    	{
			$content .= Message::MESSAGE_DELIMITER . $key . $value;
		}

		Message::model()->updateByPk($messageID, array(
			'message' => $content,
		));
	}

	/**
	 	 _______  _______  _______  _______  _______  _______  _______    _______  _______  _______  _______  _______ _________ _______ 
		(       )(  ____ \(  ____ \(  ____ \(  ___  )(  ____ \(  ____ \  (  ____ )(  ____ \(  ____ )(  ___  )(  ____ )\__   __/(  ____ \
		| () () || (    \/| (    \/| (    \/| (   ) || (    \/| (    \/  | (    )|| (    \/| (    )|| (   ) || (    )|   ) (   | (    \/
		| || || || (__    | (_____ | (_____ | (___) || |      | (__      | (____)|| (__    | (____)|| |   | || (____)|   | |   | (_____ 
		| |(_)| ||  __)   (_____  )(_____  )|  ___  || | ____ |  __)     |     __)|  __)   |  _____)| |   | ||     __)   | |   (_____  )
		| |   | || (            ) |      ) || (   ) || | \_  )| (        | (\ (   | (      | (      | |   | || (\ (      | |         ) |
		| )   ( || (____/\/\____) |/\____) || )   ( || (___) || (____/\  | ) \ \__| (____/\| )      | (___) || ) \ \__   | |   /\____) |
		|/     \|(_______/\_______)\_______)|/     \|(_______)(_______/  |/   \__/(_______/|/       (_______)|/   \__/   )_(   \_______)
		                                                                                                                                
	 */

	/**
	 * 	The getMessagesByMessageType method searches the table Message for a specific primary
	 * 	key, that defines the Message, by the message type given by the user.
	 * 	
	 *  @param  String $messageType   Represents the messages type.
	 *  @return array<Message>        An array of message objects.
	 */
	public static function getMessagesByMessageType($messageType)
	{
		return Message::model()->find(array(
			'select' => 'message_id',
			'with' => 'messageType',
			'condition' => 'message_type=:type',
			'params' => array(':type' => $messageType)
		));
	}

	/**
	 * 	Gets the message type of the message with the specified ID.
	 * 	
	 *  @param  int        $id 			The ID of the message.
	 *  @return String 					The type of the message.
	 */
	public static function getMessageTypeByMessageID($id)
	{
		$result = Message::model()->find(array(
			'select' => 'message_type_id',
			'with' => 'messageType',
			'condition' => 'message_id=:id',
			'params' => array(':id' => $id)
		));
		return $result->getMessageType();
	}
	
	/**
		 _______  _______  _______  _______  _______  _______  _______    _______  _______ _________          _______  ______   _______ 
		(       )(  ____ \(  ____ \(  ____ \(  ___  )(  ____ \(  ____ \  (       )(  ____ \\__   __/|\     /|(  ___  )(  __  \ (  ____ \
		| () () || (    \/| (    \/| (    \/| (   ) || (    \/| (    \/  | () () || (    \/   ) (   | )   ( || (   ) || (  \  )| (    \/
		| || || || (__    | (_____ | (_____ | (___) || |      | (__      | || || || (__       | |   | (___) || |   | || |   ) || (_____ 
		| |(_)| ||  __)   (_____  )(_____  )|  ___  || | ____ |  __)     | |(_)| ||  __)      | |   |  ___  || |   | || |   | |(_____  )
		| |   | || (            ) |      ) || (   ) || | \_  )| (        | |   | || (         | |   | (   ) || |   | || |   ) |      ) |
		| )   ( || (____/\/\____) |/\____) || )   ( || (___) || (____/\  | )   ( || (____/\   | |   | )   ( || (___) || (__/  )/\____) |
		|/     \|(_______/\_______)\_______)|/     \|(_______)(_______/  |/     \|(_______/   )_(   |/     \|(_______)(______/ \_______)
		                                                                                                                                
	 */
	
	/**
	 * Finds a value that was appended to the message using a specific key.
	 * If the key is shared by more than one value, then all values are returned
	 * as an array.
	 * 
	 * @param  String 		$searchKey 	The key that identifies the value.
	 * @return array<String>            An array of values with a matching key.
	 */
	public function findByKey($searchKey)
	{
		$valueList = array();
		for ($i = 0; $i < sizeof($this->_messageParts); $i++)
		{
			$key = substr($this->_messageParts[$i], 0, Message::MESSAGE_KEY_LENGTH);
			if ($key == $searchKey)
			{
				$valueList[] = substr($this->_messageParts[$i], Message::MESSAGE_KEY_LENGTH);
			}
		}
		return (sizeof($valueList) == 1) ? $valueList[0] : $valueList;
	}
	
	/**
	 * 	The getPreviousMessage method searches for the message represented by the previous 
	 * 	message ID stored in the content of this message. The return value is null when
	 * 	no previous message is found.
	 *
	 *  @return Message 				Represents the previous message. Null if not found.
	 */
	public function getPreviousMessage()
	{

		$messageID = $this->findByKey(Message::MESSAGE_KEY_PREVIOUS_MESSAGE);
		return (sizeof($messageID) == 0) ? null : Message::model()->findByPk($messageID);
	}

	/**
	 * 	The getMessageHistory method gets all previous messages and returns them as an 
	 * 	array, with the last message in the array being the root message.
	 * 	
	 *  @return array<Message> 			An array of all the messages that are linked together.
	 */
	public function getMessageHistory()
	{
		$messageHistory = array();
    	$messageTemp = $this;

    	while ($messageTemp != null)
    	{
    		$messageHistory[] = $messageTemp;
    		$messageTemp = $messageTemp->getPreviousMessage();
    	}
    	return $messageHistory;
	}

	public function getRootMessage()
	{
		$messageHistory = $this->getMessageHistory();
		return $messageHistory[sizeof($messageHistory) - 1];
	}

	public function getMessageID()
	{
		return $this->message_id;
	}

	public function getMessageType()
	{
		return $this->messageType->message_type;
	}

	public function getMessage()
	{
		return $this->message;
	}
}
