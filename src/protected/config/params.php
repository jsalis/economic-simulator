<?php

// this contains the application parameters that can be maintained via GUI

return array(
	// this is displayed in the header section
	'title' => 'Economic Simulator',

	// this is used in error pages
	'adminEmail' => 'webmaster@example.com',

	// the copyright information displayed in the footer section
	'copyrightInfo' => 'Copyright &copy; 2014.',

	// The date format used by the database
	'dbDateFormat' => 'Y-m-d H:i:s',
);
