<?php

/**
 * The CompanyController class is there to communicate the Yii framework, and
 * Restful API to the Company class, this includes creating actions to all of the 
 * functions that an Company may have.
 *
 * @author   <cmicklis@stetson.edu>, <jsalis@stetson.edu>
 * @since 	 v0.0.0
 */
class CompanyController extends EntityController
{
	public $layout = StudentController::DEFAULT_LAYOUT;

	/**
	 * 	Sets up the model before an action is called.
	 * 	
	 * 	@param  Action    $action 	Represents the action to be called.
	 *  @return boolean           	Whether the action should be called.
	 */
	protected function beforeAction($action)
	{
		if (!parent::beforeAction($action))
		{
			return false;
		}

		if (Yii::app()->user->roleType != EntityType::ENTITY_TYPE_COMPANY_NAME)
		{
			$this->redirect(array(Yii::app()->user->type . '/'));
			return false;
		}
		else
		{
			$this->_entity = Company::model()->findByPk(Yii::app()->user->roleID);
			return true;
		}
	}

	public function actionUpdateName()
	{
		if (isset($_POST['Entity']))
		{
			$entityID = $_POST['Entity']['id'];
			$name = $_POST['Entity']['name'];

			$this->_entity->updateEntityName($entityID, $name);
		}
		$this->redirect(array(strtolower(Yii::app()->user->roleType) . '/'));
	}

	public function actionCreateProduct()
	{
		if (isset($_POST['ProductForm']))
		{
			$form = new ProductForm('create');
			$form->attributes = $_POST['ProductForm'];
			$this->performAjaxValidation($form);

			if ($form->validate())
			{
				$this->_entity->createProduct($form->name, $form->type, $form->description, $form->price, $form->economy_id);
			}
		}
		$this->redirect(array(strtolower(Yii::app()->user->roleType) . '/#products'));
	}

	public function actionUpdateProduct()
	{
		if (isset($_POST['ProductForm']))
		{
			$form = new ProductForm('update');
			$form->attributes = $_POST['ProductForm'];
			$this->performAjaxValidation($form);

			if ($form->validate())
			{
				$this->_entity->updateProduct($form->item_id, $form->name, $form->description, $form->price, $form->economy_id);
			}
		}
		$this->redirect(array(strtolower(Yii::app()->user->roleType) . '/#products'));
	}

	public function actionPurchaseItem()
	{
		if (isset($_POST['Item']))
		{
			$itemID = $_POST['Item']['id'];
			$economyID = $_POST['Item']['economyID'];

			$this->_entity->purchaseItem($itemID, $economyID);
		}
		$this->redirect(array('capital-market'));
	}

	public function actionCreateRentOffer()
	{
		if (isset($_POST['RentOfferForm']))
		{
			$form = new RentOfferForm('create');
			$form->attributes = $_POST['RentOfferForm'];
			$this->performAjaxValidation($form);

			if ($form->validate())
			{
				$this->_entity->createRentOffer($form->item_id, $form->price);
			}
		}
		$this->redirect(array('rent-market'));
	}

	public function actionReplyRentOffer()
	{
		if (isset($_POST['RentOfferForm']))
		{
			$form = new RentOfferForm('reply');
			$form->attributes = $_POST['RentOfferForm'];
			$this->performAjaxValidation($form);

			if ($form->validate())
			{
				$this->_entity->sendMessage($form->receiver_id, $form->message_type, $form->append);
			}
		}
		$this->render('/market/rent-market', array(
			'model' => $this->_entity,
		));
	}

	public function actionCreateJobOffer()
	{
		if (isset($_POST['JobOfferForm']))
		{
			$form = new JobOfferForm('create');
			$form->attributes = $_POST['JobOfferForm'];
			$this->performAjaxValidation($form);

			if ($form->validate())
			{
				$this->_entity->createJobOffer($form->receiver_id, $form->item_id, $form->payment);
			}
		}
		$this->render('/market/labor-market', array(
			'model' => $this->_entity,
		));
	}

	public function actionReplyJobOffer()
	{
		if (isset($_POST['JobOfferForm']))
		{
			$form = new JobOfferForm('reply');
			$form->attributes = $_POST['JobOfferForm'];
			$this->performAjaxValidation($form);

			if ($form->validate())
			{
				$this->_entity->sendMessage($form->receiver_id, $form->message_type, $form->append);
			}
		}
		$this->render('/market/labor-market', array(
			'model' => $this->_entity,
		));
	}

	/**
	 * The actionCloseJobOffer method goes and closes the current job offer. The method goes and checks if
	 * the job offer is associated with the company, if it isn't then the action will not go through.
	 */
	public function actionCloseJobOffer()
	{
		if (isset($_POST['JobOfferForm']))
		{
			$entityHasMessage = EntityHasMessage::model()->findByPk($_POST['JobOfferForm']['entity_has_message_id']);

			if ($entityHasMessage->entitySender->getEntityID() == Yii::app()->user->roleID)
			{
            	EntityHasMessage::replyMessage($_POST['JobOfferForm']['message_id']);
			}
		}
		$this->render('/market/labor-market', array(
			'model' => $this->_entity,
		));
	}
}
