<?php

Yii::import('application.models._forms.*');

/**
 * The EntityController class represents actions that all subclasses of
 * entity can perform. Before each action is called, the economy rules 
 * are checked to see if the requested action has been disabled. If so
 * then the user is redirected to the index with an error message.
 *
 * @author   <jsalis@stetson.edu>
 * @since 	 v0.0.0
 */
class EntityController extends Controller
{
	const ERROR_KEY = 'action';
	const ERROR_DISABLED_ACTION = 'Sorry, the requested action has been disabled by the teacher.';

	protected $_entity;

	/**
	 * 	Sets up the model before an action is called. 
	 * 	
	 * 	@param  Action    $action 	Represents the action to be called.
	 *  @return boolean           	Whether the action should be called.
	 */
	protected function beforeAction($action)
	{
		if (!parent::beforeAction($action))
		{
			return false;
		}
		else
		{
			$this->_user = User::model()->findByPk(Yii::app()->user->getId());
			$action = $this->uniqueId . '/' . $this->action->Id;

			if (Economy::canPerformAction(Yii::app()->user->economyID, $action))
			{
				return true;
			}
			else
			{
				$this->_entity = Entity::model()->getEntityByID(Yii::app()->user->roleID);
				$this->_user->addError(EntityController::ERROR_KEY, EntityController::ERROR_DISABLED_ACTION);
				$this->actionIndex();
				return false;
			}
		}
	}

	public function actionIndex()
	{
		$this->setPageTitle('Profile - ' . $this->_entity->getEntityName());
		$this->render('/profile/profile', array(
			'model' => $this->_user,
			'entity' => $this->_entity,
		));
	}

	public function actionProductMarket()
	{
		$this->setPageTitle(Yii::app()->name . ' - Product Market');
		$this->render('/market/product-market', array(
			'model' => $this->_entity,
		));
	}

	public function actionLaborMarket()
	{
		$this->setPageTitle(Yii::app()->name . ' - Labor Market');
		$this->render('/market/labor-market', array(
			'model' => $this->_entity,
		));
	}

	public function actionCapitalMarket()
	{
		$this->setPageTitle(Yii::app()->name . ' - Capital Market');
		$this->render('/market/capital-market', array(
			'model' => $this->_entity,
		));
	}
	
	public function actionRentMarket()
	{
		$this->setPageTitle(Yii::app()->name . ' - Rent Market');
		$this->render('/market/rent-market', array(
			'model' => $this->_entity,
		));
	}

	public function actionForum()
	{
		$this->setPageTitle(Yii::app()->name . ' - Forum');
		$this->render('/forum/forum', array(
			'model' => $this->_entity,
		));
	}

	/**
	 * This action transfers money from the current entity to another entity owned by the student.
	 */
	public function actionTransferMoney()
	{
		if (isset($_POST['TransferMoneyForm']))
		{
			$form = new TransferMoneyForm;
			$form->attributes = $_POST['TransferMoneyForm'];
			$form->sender_id = Yii::app()->user->roleID;
			$this->performAjaxValidation($form);

			if ($form->validate())
			{
				Entity::transferMonetary($form->sender_id, $form->receiver_id, $form->amount);
			}
		}
		$this->redirect(array('index'));
	}

	/**
	 * This action creates a new discussion post. It is essentially a chat message to the public messenger entity.
	 */
	public function actionCreateDiscussion()
	{
		if (isset($_POST['DiscussionForm']))
		{
			$form = new DiscussionForm('create');
			$form->attributes = $_POST['DiscussionForm'];
			$this->performAjaxValidation($form);

			if ($form->validate())
			{
				$this->_entity->sendMessage($form->receiver_id, MessageType::MESSAGE_TYPE_CHAT_NAME, $form->append);
			}
		}
		$this->redirect(array('forum#discussion'));
	}

	/**
	 * This action replies to a discussion post. It is essentially a chat message to the public messenger entity.
	 */
	public function actionReplyDiscussion()
	{
		if (isset($_POST['DiscussionForm']))
		{
			$form = new DiscussionForm('reply');
			$form->attributes = $_POST['DiscussionForm'];
			$this->performAjaxValidation($form);

			if ($form->validate())
			{
				$this->_entity->sendMessage($form->receiver_id, MessageType::MESSAGE_TYPE_CHAT_NAME, $form->append);
			}
		}
		$this->redirect(array('forum#discussion'));
	}

	/**
	 * [actionViewDiscussion description]
	 */
	public function actionViewDiscussion($id = null)
	{
		if (isset($id) && Yii::app()->request->isAjaxRequest)
		{
			$entityHasMessage = EntityHasMessage::model()->message($id)->find();

			$this->renderPartial('/forum/_view-discussion', array(
				'entityHasMessage' => $entityHasMessage,
			));

			Yii::app()->end();
		}
		else
	    {
	    	throw new CHttpException('400', 'Invalid request.');
	    }
	}

	/**
	 * This action displays a message to the user.
	 * 	
	 * @param  int   $id 	The ID of the message to be displayed.
	 */
	public function actionDisplayMessage()
	{
		$this->setPageTitle(Yii::app()->name . ' - Messages');

		if (isset($_POST['EntityHasMessage']))
		{
			$id = $_POST['EntityHasMessage']['id'];
			$entityHasMessage = $this->_entity->getEntityHasMessage($id);

			$this->render('/messaging/message-list', array(
				'model' => $this->_entity,
				'entityHasMessage' => $entityHasMessage,
			));
		}
		else
		{
			$this->render('/messaging/message-list', array(
				'model' => $this->_entity,
			));
		}
	}

	/**
	 * Sends a message from the entity. A message needs a type, then it needs the parameters of 
	 * that type, the message usually has some content and then some paramters that can be added on 
	 * at the end. The extra parameters (append) are defined in the view.
	 */
	public function actionSendMessage()
	{
		if (isset($_POST['Message']))
		{
			$entityID = $_POST['Message']['entityID'];
			$messageType = $_POST['Message']['messageType'];
			$append = $_POST['Message']['append'];

			$this->_entity->sendMessage($entityID, $messageType, $append);

			$view = (isset($_POST['View'])) ? $_POST['View'] : '/messaging/message-list';
			$this->render($view, array(
				'model' => $this->_entity,
			));
		}
		else
		{
			$this->actionDisplayMessage();
		}
	}
}
