<?php

/**
 * The TeacherController class is there to communicate the Yii framework, and
 * Restful API to the Teacher class, this includes creating actions to all of the 
 * functions that an Teacher may have.
 *
 * @author   <cmicklis@stetson.edu>, <jsalis@stetson.edu>
 * @since 	 v0.0.0
 */
class TeacherController extends Controller
{
	const DEFAULT_LAYOUT = 'teacher-layout';

	public $layout = TeacherController::DEFAULT_LAYOUT;

	/**
	 * 	Sets up the model before an action is called. 
	 * 	Also checks if an economy has been selected.
	 * 	
	 * 	@param  Action    $action 	Represents the action to be called.
	 *  @return boolean           	Whether the action should be called.
	 */
	protected function beforeAction($action)
	{
		if (!parent::beforeAction($action))
		{
			return false;
		}
		if (Yii::app()->user->type != 'teacher')
		{
			$this->redirect(array(Yii::app()->user->type . '/'));
			return false;
		}
		else if (Yii::app()->user->economyID == UserIdentity::NULL_STATE_VALUE 
			     && $action->id != 'index' && $action->id != 'selectEconomy'
			     && $action->id != 'createEconomy')
		{
			$this->redirect(array('teacher/'));
			return false;
		}
		else
		{
			$this->_user = Teacher::model()->findByPk(Yii::app()->user->getId());
			return true;
		}
	}

	public function actionIndex()
	{
		$this->layout = DoorKeeperController::DEFAULT_LAYOUT;
		$this->render('/doorkeeper/economy-list', array(
			'form' => 'select_economy',
			'economyList' => $this->_user->getEconomyList(),
		));
	}

	public function actionSelectEconomy($id)
	{
		$this->_user->selectEconomy($id);
		$this->_user->selectRole(EntityType::ENTITY_TYPE_ACCESSOR_NAME);
		if ($id == Economy::ECONOMY_DEFAULT_ID)
		{
			$this->redirect(array(Yii::app()->user->type . '/rule-settings'));
		}
		else
		{
			$this->redirect(array(Yii::app()->user->type . '/student-settings'));
		}
	}

	public function actionSelectRole($id)
	{
		$this->_user->selectRole($id);
		$this->redirect(array(Yii::app()->user->type . '/student-settings'));
	}

	public function actionCreateEconomy()
	{
		if (isset($_POST['Economy']))
		{
			$this->_user->createEconomy($_POST['Economy']['name'], $_POST['Economy']['description']);
			$this->redirect(array(Yii::app()->user->type . '/'));
		}
		else
		{
			$this->layout = DoorKeeperController::DEFAULT_LAYOUT;
			$this->render('/economy-list', array(
				'form' => 'create_economy',
				'model' => $this->_user,
			));
		}
	}

	public function actionUpdateEconomy()
	{
		if (isset($_POST['Economy']))
		{
			$this->_user->updateEconomy(Yii::app()->user->economyID, $_POST['Economy']['name'], $_POST['Economy']['description']);
		}
		$this->redirect(array(Yii::app()->user->type . '/student-settings'));
	}

	public function actionStudentSettings()
	{
		$this->setPageTitle(Yii::app()->name . ' - Students');
		$this->render('/teacher/economy-settings', array(
			'model' => $this->_user,
			'accessor' => Accessor::model()->findByPk(Yii::app()->user->roleID, false),
			'tab' => 'students',
		));
	}

	public function actionElectionSettings()
	{
		$this->setPageTitle(Yii::app()->name . ' - Election');
		$this->render('/teacher/economy-settings', array(
			'model' => $this->_user,
			'accessor' => Accessor::model()->findByPk(Yii::app()->user->roleID, false),
			'tab' => 'election',
		));
	}

	public function actionCapitalSettings()
	{
		$this->setPageTitle(Yii::app()->name . ' - Capital');
		$this->render('/teacher/economy-settings', array(
			'model' => $this->_user,
			'accessor' => Accessor::model()->findByPk(Yii::app()->user->roleID, false),
			'tab' => 'capital',
		));
	}

	public function actionPublicGoodSettings()
	{
		$this->setPageTitle(Yii::app()->name . ' - Public Goods');
		$this->render('/teacher/economy-settings', array(
			'model' => $this->_user,
			'accessor' => Accessor::model()->findByPk(Yii::app()->user->roleID, false),
			'tab' => 'public-goods',
		));
	}

	public function actionRuleSettings()
	{
		$this->setPageTitle(Yii::app()->name . ' - Economy Rules');
		$this->render('/teacher/economy-settings', array(
			'model' => $this->_user,
			'accessor' => Accessor::model()->findByPk(Yii::app()->user->roleID, false),
			'tab' => 'rules',
		));
	}

	public function actionTradeSettings()
	{
		$this->setPageTitle(Yii::app()->name . ' - Economic Trade');
		$this->render('/teacher/economy-settings', array(
			'model' => $this->_user,
			'accessor' => Accessor::model()->findByPk(Yii::app()->user->roleID, false),
			'tab' => 'trade',
		));
	}

	public function actionEconomyStatistics()
	{
		$this->setPageTitle(Yii::app()->name . ' - Economy Statistics');
		$this->render('/teacher/economy-settings', array(
			'model' => $this->_user,
			'accessor' => Accessor::model()->findByPk(Yii::app()->user->roleID, false),
			'tab' => 'statistics',
		));
	}
	
	public function actionAddStudentToEconomy()
	{
		if (isset($_POST['Student']))
		{
			$this->_user->addStudentToEconomy($_POST['Student']['emailAddress'], Yii::app()->user->economyID);
		}
		$this->redirect(array(Yii::app()->user->type . '/student-settings'));
	}
	
	public function actionRemoveStudentFromEconomy()
	{
		if (isset($_POST['Student']))
		{
			// remove student
		}
		$this->redirect(array(Yii::app()->user->type . '/student-settings'));
	}
	
	public function actionUpdateEconomyRelations()
	{
		if (isset($_POST['Economy']))
		{
			$this->_user->updateEconomyRelations($_POST['Economy']['relation'], Yii::app()->user->economyID);
		}
		$this->redirect(array(Yii::app()->user->type . '/trade-settings'));
	}

	public function actionAccountSettings()
	{
		$this->setPageTitle(Yii::app()->name . ' - Account Settings');
		$this->render('/doorkeeper/account-settings', array(
			'model' => $this->_user,
			'tab' => 'account',
		));
	}

	public function actionUpdateUser()
	{
		if (isset($_POST['User']))
		{
			$firstName = $_POST['User']['firstName'];
			$lastName = $_POST['User']['lastName'];
			$cellPhone = $_POST['User']['cellPhone'];

			User::updateUser($this->_user->getUserID(), $firstName, $lastName, $cellPhone);
		}
		$this->redirect(array(Yii::app()->user->type . '/account-settings'));
	}

	public function actionUpdatePassword()
	{
		if (isset($_POST['User']))
		{
			$currentPassword = $_POST['User']['currentPassword'];
			$newPassword = $_POST['User']['newPassword'];
			$repeatPassword = $_POST['User']['repeatPassword'];

			$this->_user->updateUserPassword($currentPassword, $newPassword, $repeatPassword);
		}
		$this->setPageTitle(Yii::app()->name . ' - Account Settings');
		$this->render('/doorkeeper/account-settings', array(
			'model' => $this->_user,
			'tab' => 'password',
		));
	}

	/**
	
		CSV Creation

	 */

	public function actionEnteredEconomy()
	{
		$info = $this->_user->getEnteredEconomyList();
		$this->arrayToCSV($info, 'EnteredEconomy.csv');
	}

	public function actionEnteredLaborMarket()
	{
		$info = $this->_user->getEnteredLaborMarketList();
		$this->arrayToCSV($info, 'EnteredLaborMarket.csv');
	}

	public function actionWorkersWithJobs()
	{
		$info = $this->_user->getWorkersWithJobs();
		$this->arrayToCSV($info, 'WorkersWithJobs.csv');
	}

	public function actionEntitiesInEconomy()
	{
		$info = $this->_user->getEntitiesInEconomy();
		$this->arrayToCSV($info, 'EntitiesInEconomy.csv');
	}

	public function actionItemsInEconomy()
	{
		$info = $this->_user->getItemsInEconomy();
		$this->arrayToCSV($info, 'ItemsInEconomy.csv');
	}

	public function actionUnfilledJobOffers()
	{
		$info = $this->_user->getUnfilledJobOffers();
		$this->arrayToCSV($info, 'UnfilledJobOffers.csv');
	}	

	public function actionWillingnessToPay()
	{
		$info = $this->_user->getWillingnessToPay();
		$this->arrayToCSV($info, 'WillingnessToPay.csv');
	}

	public function actionItemAndProduction()
	{
		$info = $this->_user->getItemAndProduction();
		$this->arrayToCSV($info, 'ItemAndProduction.csv');
	}

	public function actionPurchasedMarketItems()
	{
		$info = $this->_user->getPurchasedMarketItems();
		$this->arrayToCSV($info, 'PurchasedMarketItems.csv');
	}

	public function actionAvailableMarketItems()
	{
		$info = $this->_user->getAvailableMarketItems();
		$this->arrayToCSV($info, 'AvailableMarketItems.csv');
	}

	public function actionDailyData()
	{
		$info = $this->_user->getDailyData();
		$this->arrayToCSV($info, 'DailyData.csv');
	}

	public function arrayToCSV($array, $filename = 'export.csv', $delimeter = ',')
	{
		if (sizeof($array) != 0)
		{
			$f = fopen('php://memory', 'w');
			fputcsv($f, array_keys($array[0]), $delimeter);
			foreach ($array as $line)
			{
				fputcsv($f, $line, $delimeter);
			}

			fseek($f, 0);
			header('Content-Type: application/csv');
			header('Content-Disposition: attachment; filename="' . $filename . '"');
			fpassthru($f);
		}
	}
}
