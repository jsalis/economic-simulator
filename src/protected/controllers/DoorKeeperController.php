<?php

/**
 * The DoorKeeperController class 
 *
 * @author   <jsalis@stetson.edu>
 * @since 	 v0.0.0
 * 
 */
class DoorKeeperController extends Controller
{
	const DEFAULT_LAYOUT = 'doorkeeper-layout';

	public $layout = DoorKeeperController::DEFAULT_LAYOUT;

	private $_doorKeeper;

	/**
	 * 	In the DoorKeeperController every action must first check if the
	 *  user has already logged in. If the user is logged in then it redirects 
	 *  to the appropriate controller (e.g. if the user is an Admin then it 
	 *  redirects to the actionIndex of the AdminController, and vice versa for 
	 *  the Teacher and Student). If the user has not yet logged in then it 
	 *  sets the layout and continues with the requested action.
	 * 
	 * 	@param  Action    $action Represents the action to be called.
	 *  @return boolean           Whether the action should be called.
	 */
	protected function beforeAction($action)
	{
		if (!Yii::app()->user->isGuest && $action->id != 'logout' && $action->id != 'error')
		{
			$this->redirect(array(Yii::app()->user->type . '/'));
			return false;
		}
		else
		{
			return true;
		}
	}

	/**
	 * 	The default action.
	 */
	public function actionIndex()
	{
		$this->render('index', array(
			'form' => 'login',
		));
	}

	/**
	 * 	The login action attempts to log in the user, and renders the
	 * 	login view when unsuccessful.
	 */
	public function actionLogin()
	{
		$this->_doorKeeper = new DoorKeeper('login');

		if (isset($_POST['DoorKeeper']))
		{
			$this->_doorKeeper->attributes = $_POST['DoorKeeper'];
			if ($this->_doorKeeper->login())
			{
				$this->redirect(array(Yii::app()->user->type . '/'));
				return;
			}
		}
		$this->render('index', array(
			'form' => 'login',
		    'model' => $this->_doorKeeper,
		));
	}

	/**
	 * Logs out the user and redirects to the doorkeeper index.
	 */
	public function actionLogout()
	{
		Yii::app()->user->logout();
		$this->redirect(array('doorkeeper/'));
	}

	/**
	 * 	The signup action attempts to create a new user, and renders the
	 * 	signup view when unsuccessful.
	 */
	public function actionSignup()
	{
		$this->_doorKeeper = new DoorKeeper('signup');

		if (isset($_POST['DoorKeeper']))
		{
			$this->_doorKeeper->attributes = $_POST['DoorKeeper'];
			if ($this->_doorKeeper->signup())
			{
				$this->actionLogin();
				return;
			}
		}
		$this->render('index', array(
			'form' => 'signup',
			'model' => $this->_doorKeeper,
		));
	}

	public function actionError()
	{
	    if ($error = Yii::app()->errorHandler->error)
	    {
	        $this->render('error', $error);
	    }
	}

	private function checkAjaxValidationRequest()
	{
		if (isset($_POST['ajax']) && $_POST['ajax'] === 'doorkeeper')
		{
			echo CActiveForm::validate($this->_doorKeeper);
			Yii::app()->end();
		}
	}
}
