<?php

/**
 * The StudentController class is there to communicate the Yii framework, and
 * Restful API to the Student class, this includes creating actions to all of the 
 * functions that an Student may have.
 *
 * @author   <cmicklis@stetson.edu>, <jsalis@stetson.edu>
 * @since 	 v0.0.0
 */
class StudentController extends Controller
{
	const DEFAULT_LAYOUT = 'student-layout';

	public $layout = StudentController::DEFAULT_LAYOUT;

	/**
	 * 	Sets up the model before an action is called. 
	 * 	Also checks if an economy has been selected.
	 * 	
	 * 	@param  Action    $action 	Represents the action to be called.
	 *  @return boolean           	Whether the action should be called.
	 */
	protected function beforeAction($action)
	{
		if (!parent::beforeAction($action))
		{
			return false;
		}
		if (Yii::app()->user->type != 'student')
		{
			$this->redirect(array(Yii::app()->user->type . '/'));
			return false;
		}
		else if (Yii::app()->user->economyID == UserIdentity::NULL_STATE_VALUE 
			     && $action->id != 'index' && $action->id != 'selectEconomy')
		{
			$this->redirect(array('student/'));
			return false;
		}
		else
		{
			$this->_user = Student::model()->findByPk(Yii::app()->user->getId());
			return true;
		}	
	}
	
	public function actionIndex()
	{
		$this->layout = DoorKeeperController::DEFAULT_LAYOUT;
		$this->render('/doorkeeper/economy-list', array(
			'form' => 'select_economy',
			'economyList' => $this->_user->getEconomyList(),
		));
	}

	public function actionSelectEconomy($id)
	{
		$this->_user->selectEconomy($id);
		$this->_user->selectRole(EntityType::ENTITY_TYPE_WORKER_NAME);
		$this->_user->enteredEconomy();
		$this->redirect(array(strtolower(Yii::app()->user->roleType) . '/'));
	}

	public function actionSelectRole($id)
	{
		$this->_user->selectRole($id);
		$this->redirect(array(strtolower(Yii::app()->user->roleType) . '/'));
	}

	public function actionAccountSettings()
	{
		$this->setPageTitle(Yii::app()->name . ' - Account Settings');
		$this->render('/doorkeeper/account-settings', array(
			'model' => $this->_user,
			'tab' => 'account',
		));
	}

	public function actionUpdateUser()
	{
		if (isset($_POST['User']))
		{
			$firstName = $_POST['User']['firstName'];
			$lastName = $_POST['User']['lastName'];
			$cellPhone = $_POST['User']['cellPhone'];

			User::updateUser($this->_user->getUserID(), $firstName, $lastName, $cellPhone);
		}
		$this->redirect(array('student/account-settings'));
	}

	public function actionUpdatePassword()
	{
		if (isset($_POST['User']))
		{
			$currentPassword = $_POST['User']['currentPassword'];
			$newPassword = $_POST['User']['newPassword'];
			$repeatPassword = $_POST['User']['repeatPassword'];

			$this->_user->updateUserPassword($currentPassword, $newPassword, $repeatPassword);
		}
		$this->setPageTitle(Yii::app()->name . ' - Account Settings');
		$this->render('/doorkeeper/account-settings', array(
			'model' => $this->_user,
			'tab' => 'password',
		));
	}
}
