<?php

/**
 * The SuperController class is there to communicate the Yii framework, and
 * Restful API to the Super class, this includes creating actions to all of the 
 * functions that super user may have.
 *
 * @author   <jsalis@stetson.edu>
 * @since 	 v0.0.0
 */
class SuperController extends AdminController
{
	/**
	 * 	Sets up the model before an action is called.
	 * 	
	 * 	@param  Action    $action 	Represents the action to be called.
	 *  @return boolean           	Whether the action should be called.
	 */
	protected function beforeAction($action)
	{
		if (!Controller::beforeAction($action))
		{
			return false;
		}
		if (Yii::app()->user->type != 'super')
		{
			$this->redirect(array(Yii::app()->user->type . '/'));
			return false;
		}
		else
		{
			$this->_user = Super::model()->findByPk(Yii::app()->user->getId());
			return true;
		}
	}
}
