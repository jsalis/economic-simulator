<?php

/**
 * The AccessorController class is there to communicate the Yii framework, and
 * Restful API to the Accessor class, this includes creating actions to all of the 
 * functions that an Accessor may have.
 *
 * @author   <cmicklis@stetson.edu>, <jsalis@stetson.edu>
 * @since 	 v0.0.0
 */
class AccessorController extends EntityController
{
	public $layout = TeacherController::DEFAULT_LAYOUT;

	/**
	 * 	The AccessorController needs to setup the model before the actions 
	 * 	are called. Also checks if an economy has been selected.
	 * 	
	 * 	@param  Action    $action Represents the action to be called.
	 *  @return boolean           Whether the action should be called.
	 */
	protected function beforeAction($action)
	{
		if (!parent::beforeAction($action))
		{
			return false;
		}
		if (Yii::app()->user->roleType != EntityType::ENTITY_TYPE_ACCESSOR_NAME)
		{
			$this->redirect(array(Yii::app()->user->type . '/'));
			return false;
		}
		else
		{
			if (Yii::app()->user->type == 'admin' || Yii::app()->user->type == 'super')
			{
				$this->layout = AdminController::DEFAULT_LAYOUT;
			}
			$this->_entity = Accessor::model()->findByPk(Yii::app()->user->roleID);
			return true;
		}
	}

	public function actionIndex()
	{
		$this->redirect(array(Yii::app()->user->type . '/student-settings'));
	}

	public function actionGiveCompany()
	{
		if (isset($_POST['Entity']))
		{
			$userEmail = $_POST['Entity']['userEmail'];
			$companyType = $_POST['Entity']['companyType'];

			$this->_entity->giveCompany($userEmail, $companyType, Yii::app()->user->economyID);
		}
		$this->redirect(array(Yii::app()->user->type . '/student-settings'));
	}

	public function actionUpdateEconomyRules()
	{
		if (isset($_POST['Rule']))
		{
			$this->_entity->updateEconomyRules($_POST['Rule']['list']);
		}
		$this->redirect(array(Yii::app()->user->type . '/rule-settings'));
	}

	public function actionCreatePublicGood()
	{
		if (isset($_POST['Item']))
		{
			$itemName = $_POST['Item']['name'];
			$itemType = ItemType::ITEM_TYPE_PUBLIC_GOOD_NAME;
			$itemDescription = $_POST['Item']['description'];
			$monetaryStart = $_POST['Item']['monetary']['start'];
			$monetaryDecreaseRate = $_POST['Item']['monetary']['decrease_rate'];
			$healthStart = $_POST['Item']['health']['start'];
			$healthDecreaseRate = $_POST['Item']['health']['decrease_rate'];
			$efficiencyStart = $_POST['Item']['efficiency']['start'];
			$efficiencyDecreaseRate = $_POST['Item']['efficiency']['decrease_rate'];
			$entityType = $_POST['Item']['entityType'];

			$this->_entity->createPublicGoood($itemName, $itemType, $itemDescription, 
								$monetaryStart, $monetaryDecreaseRate, $healthStart, 
								$healthDecreaseRate, $efficiencyStart, $efficiencyDecreaseRate, 
								$entityType);
		}
		$this->redirect(array(Yii::app()->user->type . '/public-good-settings'));
	}	

	public function actionUpdatePublicGood()
	{
		if (isset($_POST['Item']))
		{
			$itemID = $_POST['Item']['id'];
			$itemName = $_POST['Item']['name'];
			$itemType = ItemType::ITEM_TYPE_PUBLIC_GOOD_NAME;
			$itemDescription = $_POST['Item']['description'];
			$monetaryStart = $_POST['Item']['monetary']['start'];
			$monetaryDecreaseRate = $_POST['Item']['monetary']['decrease_rate'];
			$healthStart = $_POST['Item']['health']['start'];
			$healthDecreaseRate = $_POST['Item']['health']['decrease_rate'];
			$efficiencyStart = $_POST['Item']['efficiency']['start'];
			$efficiencyDecreaseRate = $_POST['Item']['efficiency']['decrease_rate'];
			$entityType = $_POST['Item']['entityType'];

			$this->_entity->updatePublicGood($itemID, $itemName, $itemType, $itemDescription, 
								$monetaryStart, $monetaryDecreaseRate, $healthStart, 
								$healthDecreaseRate, $efficiencyStart, $efficiencyDecreaseRate, 
								$entityType);
		}
		$this->redirect(array(Yii::app()->user->type . '/public-good-settings'));
	}

	public function actionCreateModifier()
	{
		if (isset($_POST['Item']))
		{
			$itemName = $_POST['Item']['name'];
			$itemType = $_POST['Item']['type'];
			$itemDescription = $_POST['Item']['description'];
			$efficiency = $_POST['Item']['efficiency'];
			$price = $_POST['Item']['price'];

			$this->_entity->createModifier($itemName, $itemType, $itemDescription, $efficiency, $price);
		}
		$this->redirect(array(Yii::app()->user->type . '/capital-settings'));
	}

	public function actionUpdateModifier()
	{
		if (isset($_POST['Item']))
		{
			$itemID = $_POST['Item']['id'];
			$itemName = $_POST['Item']['name'];
			$itemType = $_POST['Item']['type'];
			$itemDescription = $_POST['Item']['description'];
			$efficiency = $_POST['Item']['efficiency'];
			$price = $_POST['Item']['price'];

			$this->_entity->updateModifier($itemID, $itemName, $itemType, $itemDescription, $efficiency, $price);
		}
		$this->redirect(array(Yii::app()->user->type . '/capital-settings'));
	}

	public function actionAddItemToMarket()
	{
		if (isset($_POST['Item']))
		{
			$itemID = $_POST['Item']['id'];
			$quantity = $_POST['Item']['quantity'];

			$this->_entity->addItemToMarket($itemID, $quantity);
		}
		$this->redirect(array(Yii::app()->user->type . '/capital-settings'));
	}

	public function actionGiveItemToEntity()
	{
		if (isset($_POST['Item']))
		{
			$itemID = $_POST['Item']['id'];
			$entityID = $_POST['Item']['entityID'];

			$this->_entity->giveItemToEntity($itemID, $entityID);
		}
		$this->redirect(array(Yii::app()->user->type . '/capital-settings'));
	}

	public function actionStartNomination()
	{
		$this->_entity->startNomination();
		$this->redirect(array(Yii::app()->user->type . '/election-settings'));
	}

	public function actionStartElection()
	{
		$this->_entity->startElection();
		$this->redirect(array(Yii::app()->user->type . '/election-settings'));
	}

	public function actionEndElection()
	{
		$this->_entity->endElection();
		$this->redirect(array(Yii::app()->user->type . '/election-settings'));
	}

	public function actionAddCandidate()
	{
		if (isset($_POST['User']))
		{
			$userID = $_POST['User']['id'];
			Economy::addCandidate($userID, Yii::app()->user->economyID);
		}
		$this->redirect(array(Yii::app()->user->type . '/election-settings'));
	}

	public function actionDisplayProfile()
	{
		if (isset($_POST['Entity']))
		{
			$entityID = $_POST['Entity']['id'];

			if (EconomyHasEntity::hasRelation(Yii::app()->user->economyID, $entityID))
			{
				$entity = Entity::getEntityByID($entityID);
				$this->setPageTitle('Profile - ' . $entity->getEntityName());
				$this->render('/profile/profile', array(
					'model' => $this->_entity,
					'entity' => $entity
				));
				return;
			}
		}
		$this->redirect(array(Yii::app()->user->type . '/student-settings'));
	}

	public function actionGiveHealth()
	{
		$amount = $_POST['Entity']['health'];

		$this->_entity->giveHealth($amount);
		$this->redirect(array(Yii::app()->user->type . '/student-settings'));
	}

	public function actionGiveMoney()
	{
		$entityType = $_POST['Entity']['type'];
		$amount = $_POST['Entity']['money'];

		$this->_entity->giveMoney($entityType, $amount);
		$this->redirect(array(Yii::app()->user->type . '/student-settings'));
	}
}
