<?php

/**
 * The GovernmentController class is there to communicate the Yii framework, and
 * Restful API to the Goverment class, this includes creating actions to all of the 
 * functions that an Goverment may have.
 *
 * @author   <cmicklis@stetson.edu>, <jsalis@stetson.edu>
 * @since 	 v0.0.0
 */
class GovernmentController extends EntityController
{
	public $layout = StudentController::DEFAULT_LAYOUT;

	/**
	 * 	Sets up the model before an action is called.
	 * 	
	 * 	@param  Action    $action 	Represents the action to be called.
	 *  @return boolean           	Whether the action should be called.
	 */
	protected function beforeAction($action)
	{
		if (!parent::beforeAction($action))
		{
			return false;
		}

		if (Yii::app()->user->roleType != EntityType::ENTITY_TYPE_GOVERNMENT_NAME)
		{
			$this->redirect(array(Yii::app()->user->type . '/'));
			return false;
		}
		else
		{
			$this->_entity = Government::model()->findByPk(Yii::app()->user->roleID);
			return true;
		}
	}

	public function actionCreateRulePetition()
	{
		if (isset($_POST['Petition']))
		{
			$ruleID = $_POST['Petition']['ruleID'];
			$ruleValue = $_POST['Petition']['ruleValue'];

			$this->_entity->createRulePetition($ruleID, $ruleValue);
		}
		$this->redirect(array(strtolower(Yii::app()->user->roleType) . '/'));
	}

	public function actionCreatePublicGoodPetition()
	{
		if (isset($_POST['Petition']))
		{
			$workerID = $_POST['Petition']['workerID'];

			$this->_entity->createPublicGoodPetition($workerID);
		}
		$this->redirect(array(strtolower(Yii::app()->user->roleType) . '/'));
	}

	public function actionCreateMoneyTransferPetition()
	{
		if (isset($_POST['Petition']))
		{
			$entityID = $_POST['Petition']['entityID'];
			$money = $_POST['Petition']['money'];

			$this->_entity->createMoneyTransferPetition($entityID, $money);
		}
		$this->redirect(array(strtolower(Yii::app()->user->roleType) . '/'));
	}

	public function actionVotePetition()
	{
		if (isset($_POST['Vote']))
		{
			$petitionCreatorID = $_POST['Vote']['petitionCreatorID'];
			$petitionID = $_POST['Vote']['petitionID'];
			$response = $_POST['Vote']['response'];

			$this->_entity->votePetition($petitionCreatorID, $petitionID, $response);
		}
		$this->redirect(array(strtolower(Yii::app()->user->roleType) . '/'));
	}
}
