<?php

/**
 * The AdminController class is there to communicate the Yii framework, and
 * Restful API to the Admin class, this includes creating actions to all of the 
 * functions that an Admin may have.
 *
 * @author   <cmicklis@stetson.edu>, <jsalis@stetson.edu>
 * @since 	 v0.0.0
 */
class AdminController extends TeacherController
{
	const DEFAULT_LAYOUT = 'admin-layout';

	public $layout = AdminController::DEFAULT_LAYOUT;

	/**
	 * 	Sets up the model before an action is called.
	 * 	
	 * 	@param  Action    $action 	Represents the action to be called.
	 *  @return boolean           	Whether the action should be called.
	 */
	protected function beforeAction($action)
	{
		if (!Controller::beforeAction($action))
		{
			return false;
		}
		if (Yii::app()->user->type != 'admin')
		{
			$this->redirect(array(Yii::app()->user->type . '/'));
			return false;
		}
		else
		{
			$this->_user = Admin::model()->findByPk(Yii::app()->user->getId());
			return true;
		}
	}

	public function actionIndex()
	{
		$this->layout = DoorKeeperController::DEFAULT_LAYOUT;
		$this->render('/doorkeeper/economy-list', array(
			'form' => 'select_economy',
			'economyList' => $this->_user->getAllEconomies(),
		));
	}

	public function actionRemoveUser()
	{
		
	}

	public function actionRemoveEconomy()
	{
		
	}
}
