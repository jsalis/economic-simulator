<?php

/**
 * The BankController class is there to communicate the Yii framework, and
 * Restful API to the Bank class, this includes creating actions to all of the 
 * functions that an Bank may have.
 *
 * @author   <cmicklis@stetson.edu>, <jsalis@stetson.edu>
 * @since 	 v0.0.0
 */
class BankController extends EntityController
{
	public $layout = StudentController::DEFAULT_LAYOUT;

	/**
	 * 	Sets up the model before an action is called.
	 * 	
	 * 	@param  Action    $action 	Represents the action to be called.
	 *  @return boolean           	Whether the action should be called.
	 */
	protected function beforeAction($action)
	{
		if (!parent::beforeAction($action))
		{
			return false;
		}

		if (Yii::app()->user->roleType != EntityType::ENTITY_TYPE_BANK_NAME)
		{
			$this->redirect(array(Yii::app()->user->type . '/'));
			return false;
		}
		else
		{
			$this->_entity = Bank::model()->findByPk(Yii::app()->user->roleID);
			return true;
		}
	}

	public function actionCreateLoan()
	{

	}

	public function actionCreateRule()
	{

	}

	public function actionUpdateLoan()
	{

	}

	public function actionUpdateRule()
	{

	}

	public function actionDisplayLoans()
	{

	}

	public function actionSendLoanToEntity()
	{

	} 
}
