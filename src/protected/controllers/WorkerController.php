<?php

/**
 * The WorkerController class is there to communicate the Yii framework, and
 * Restful API to the Worker class, this includes creating actions to all of the 
 * functions that an Worker may have.
 *
 * @author   <cmicklis@stetson.edu>, <jsalis@stetson.edu>
 * @since 	 v0.0.0
 */
class WorkerController extends EntityController
{
	public $layout = StudentController::DEFAULT_LAYOUT;

	/**
	 * 	Sets up the model before an action is called. 
	 * 	
	 * 	@param  Action    $action 	Represents the action to be called.
	 *  @return boolean           	Whether the action should be called.
	 */
	protected function beforeAction($action)
	{
		if (!parent::beforeAction($action))
		{
			return false;
		}

		if (Yii::app()->user->roleType != EntityType::ENTITY_TYPE_WORKER_NAME)
		{
			$this->redirect(array(Yii::app()->user->type . '/'));
			return false;
		}
		else
		{
			$this->_entity = Worker::model()->findByPk(Yii::app()->user->roleID);
			return true;
		}
	}

	public function actionLaborMarket()
	{
		$this->_entity->enteredLaborMarket();
		parent::actionLaborMarket();
	}

	public function actionSendDemandSurvey()
	{
		if (isset($_POST['Item']) && !$this->_entity->hasSentDemandSurvey())
		{
			for ($i = 0; $i < sizeof($_POST['Item']); $i++)
			{
				$price = $_POST['Item'][$i]['price'];
				$itemType = $_POST['Item'][$i]['itemType'];
				$quantity = $_POST['Item'][$i]['quantity'];

				$this->_entity->sendDemandSurvey($price, $itemType, $quantity);
			}
		}
		$this->redirect(array('product-market'));
	}

	public function actionPurchaseItem()
	{
		if (isset($_POST['Item']))
		{
			$itemID = $_POST['Item']['id'];
			$economyID = $_POST['Item']['economyID'];

			$this->_entity->purchaseItem($itemID, $economyID);
		}
		$this->redirect(array('product-market'));
	}

	public function actionCreateRentOffer()
	{
		if (isset($_POST['RentOfferForm']))
		{
			$form = new RentOfferForm('create');
			$form->attributes = $_POST['RentOfferForm'];
			$this->performAjaxValidation($form);

			if ($form->validate())
			{
				$this->_entity->createRentOffer($form->item_id, $form->price);
			}
		}
		$this->redirect(array('rent-market'));
	}

	public function actionReplyRentOffer()
	{
		if (isset($_POST['RentOfferForm']))
		{
			$form = new RentOfferForm('reply');
			$form->attributes = $_POST['RentOfferForm'];
			$this->performAjaxValidation($form);

			if ($form->validate())
			{
				$this->_entity->sendMessage($form->receiver_id, $form->message_type, $form->append);
			}
		}
		$this->render('/market/rent-market', array(
			'model' => $this->_entity,
		));
	}

	public function actionReplyJobOffer()
	{
		if (isset($_POST['JobOfferForm']))
		{
			$form = new JobOfferForm('reply');
			$form->attributes = $_POST['JobOfferForm'];
			$this->performAjaxValidation($form);

			if ($form->validate())
			{
				$this->_entity->sendMessage($form->receiver_id, $form->message_type, $form->append);
			}
		}
		$this->render('/market/labor-market', array(
			'model' => $this->_entity,
		));
	}

	public function actionCreateCompany()
	{

	}
}
