<?php
/**
 *	The BenefitFunction class is there to allow the Econ-Sim to calculate out the benefit 
 *	amount that the entity receives. This is done by a Benefit function that is defined by 
 *	the Teacher of the Econ-Sim and the rules that are associated with the public good function
 *	as they are defined by the teacher when they create the public good.
 *
 *  @author   <cmicklis@stetson.edu>
 *  @since 	  v0.0.0
 */

class BenefitFunction extends CApplicationComponent
{
    const BENEFIT_FUNCTION_LINEAR = "Linear";
    const SMALLEST_BENEFIT_VALUE = 0;

    /**
     *  The calculateBenefit method gets all of the amounts for each entity attribute. Then it
     *  sends out the necessary transfers to the entities that are to receive it. This is defined
     *  by the 
     *  
     *  @param  int     $itemID                     The primary key of the item.
     *  @param  int     $unitsProduced              How much has been produced by the entity.
     *  @param  int     $economyID                  The primary key of the economy.
     */
    public static function calculateBenefit($itemID, $unitsProduced, $economyID)
    {
        $item = Item::model()->findByPk($itemID);
        $healthBenefit = BenefitFunction::amountOfBenefitFromItem($item, $unitsProduced, Entity::ENTITY_ATTRIBUTE_HEALTH, $economyID);
        $monetaryBenefit = BenefitFunction::amountOfBenefitFromItem($item, $unitsProduced, Entity::ENTITY_ATTRIBUTE_MONETARY, $economyID);
        $efficiencyBenefit = BenefitFunction::amountOfBenefitFromItem($item, $unitsProduced, Entity::ENTITY_ATTRIBUTE_EFFICIENCY, $economyID);

        $entityTypeReceiver = $item->getBenefitReceiver();

        if ($monetaryBenefit > BenefitFunction::SMALLEST_BENEFIT_VALUE)
        {
            BenefitFunction::distributeMonetaryBenefit($monetaryBenefit, $entityTypeReceiver, $economyID);
        }

        if ($healthBenefit > BenefitFunction::SMALLEST_BENEFIT_VALUE)
        {
            BenefitFunction::distributeHealthBenefit($healthBenefit, $entityTypeReceiver, $economyID);
        }
        
        if ($efficiencyBenefit > BenefitFunction::SMALLEST_BENEFIT_VALUE)
        {
            BenefitFunction::distributeEfficiencyBenefit($efficiencyBenefit, $entityTypeReceiver, $economyID);
        }
    }

    /**
     *  The distributeMonetaryBenefit method goes and divides up the total amount of money that was
     *  created from the public goods to all of the entities specified, in this case only specified
     *  by type.
     *  
     *  @param  int     $monetaryBenefit            The total amount of money that was created by the 
     *                                              public goods.
     *  @param  String  $entityTypeReceiver         States the type of entity that is to receive the 
     *                                              money.
     *  @param  int     $economyID                  The primary key of the economy.
     */
    public static function distributeMonetaryBenefit($monetaryBenefit, $entityTypeReceiver, $economyID)
    {   
        $publicMessengerID = Economy::getPublicMessengerID($economyID);
        $entityList = Economy::getEntitiesInEconomy($economyID, $entityTypeReceiver);

        $monetarySplit = ceil($monetaryBenefit / sizeof($entityList));

        for ($i = 0; $i < sizeof($entityList); $i++)
        {
            Entity::transferMonetary($publicMessengerID, $entityList[$i]->getEntityID(), $monetarySplit);
        }
    }

    /**
     *  The distributeHealthBenefit method goes and divides up the total amount of health that was
     *  created from the public goods to all of the entities specified, in this case only specified
     *  by type.
     *  
     *  @param  int     $healthBenefit              The total amount of health that was created by the 
     *                                              public goods.
     *  @param  String  $entityTypeReceiver         States the type of entity that is to receive the 
     *                                              health.
     *  @param  int     $economyID                  The primary key of the economy.
     */
    public static function distributeHealthBenefit($healthBenefit, $entityTypeReceiver, $economyID)
    {   
        $publicMessengerID = Economy::getPublicMessengerID($economyID);
        $entityList = Economy::getEntitiesInEconomy($economyID, $entityTypeReceiver);

        $healthSplit = ceil($healthBenefit / sizeof($entityList));

        for ($i = 0; $i < sizeof($entityList); $i++)
        {
            Entity::transferHealth($publicMessengerID, $entityList[$i]->getEntityID(), $healthSplit);
        }
    }

    /**
     *  The distributeEfficiencyBenefit method goes and divides up the total amount of efficiency that was
     *  created from the public goods to all of the entities specified, in this case only specified
     *  by type.
     *  
     *  @param  int     $efficiencyBenefit          The total amount of health that was created by the 
     *                                              public goods.
     *  @param  String  $entityTypeReceiver         States the type of entity that is to receive the 
     *                                              health.
     *  @param  int     $economyID                  The primary key of the economy.
     */
    public static function distributeEfficiencyBenefit($efficiencyBenefit, $entityTypeReceiver, $economyID)
    {   
        $publicMessengerID = Economy::getPublicMessengerID($economyID);
        $entityList = Economy::getEntitiesInEconomy($economyID, $entityTypeReceiver);

        $efficiencySplit = ceil($efficiencyBenefit / sizeof($entityList));

        for ($i = 0; $i < sizeof($entityList); $i++)
        {
            Entity::transferHealth($publicMessengerID, $entityList[$i]->getEntityID(), $efficiencySplit);
        }
    }

    /**
     *  The amountOfBenefitFromItem method checks to see what the current benefit function is
     *  for the economy and then gets the result of that benefit function for the item specified.
     *  
     *  @param  Item    $item                       The item that is to have the benefits associated
     *                                              with it.
     *  @param  int     $unitsProduced              How much has been produced by the entity.
     *  @param  String  $entityAttribute            The attribute of an entity (e.g. monetary, health,
     *                                              ...) that is to receive the benefit.
     *  @param  int     $economyID                  The primary key of the economy.
     *  @return int                                 The amount of benefit that is to be received.
     */
    public static function amountOfBenefitFromItem($item, $unitsProduced, $entityAttribute, $economyID)
    {
    	$functionName = Economy::getEconomyBenefitFunction($economyID);
    	if ($functionName == BenefitFunction::BENEFIT_FUNCTION_LINEAR)
    	{
	    	return BenefitFunction::linearAddition($item, $unitsProduced, $entityAttribute);
    	}
    	return BenefitFunction::SMALLEST_BENEFIT_VALUE;
    }

    /**
     *  The linearAddition method gets the amount of benefit an entity type is to receive after 
     *  a product has been created. It gets all the information relevant for the linear method and
     *  then calls it to get the total benefit.
     *  
     *  @param  Item    $item                       The item that is to have the benefits associated
     *                                              with it.
     *  @param  int     $unitsProduced             How much has been produced by the entity.
     *  @param  String  $entityAttribute            The attribute of an entity (e.g. monetary, health,
     *                                              ...) that is to receive the benefit.
     *  @return int                                 The amount of benefit that is to be received.
     */
    public static function linearAddition($item, $unitsProduced, $entityAttribute)
    {
    	$amountOfItemsCreated = $item->getItemStock() - $unitsProduced;
    	$benefitStart = $item->getBenefitStart($entityAttribute);
    	$benefitDecrease = $item->getBenefitDecreaseRate($entityAttribute);
    	$y = BenefitFunction::linear($amountOfItemsCreated, $benefitStart, $benefitDecrease, $unitsProduced);
    	return $y;
    }

    /**
     *  The linear method goes and gets the total amount of benefit that should be given based
     *  off of where the benefits are currently and where they will end up at depending on how
     *  many items have now been produced.
     *
     *  It does this all simply by getting the the amount of items produced incremented once to 
     *  represent another item being produced, it does this until the incrementor is the same
     *  size as the unitsProduced, and then multiplies that amount by the benefit decrease. The
     *  result of this subtracts by the benefit start.
     *  
     *  @param  int     amountOfItemsCreated        The amount of items that have been created so far.
     *  @param  int     $benefitStart               The start of the benefit curve (where it is at the 
     *                                              x coordinate being 0)
     *  @param  int     $benefitDecrease            How much the benefit decreases when x is incremented 
     *                                              by one (the multiplier of x).
     *  @param  int     $unitsProduced             How much has been produced by the entity.
     *  @return int                                 The amount of benefit that is to be received.
     */
    public static function linear($amountOfItemsCreated, $benefitStart, $benefitDecrease, $unitsProduced)
    {
    	$sum = BenefitFunction::SMALLEST_BENEFIT_VALUE;
    	for ($i = 0; $i < $unitsProduced; $i ++)
    	{
            $benefit = $benefitStart - (($amountOfItemsCreated + $i) * $benefitDecrease);
            if ($benefit > BenefitFunction::SMALLEST_BENEFIT_VALUE)
            {
                $sum += $benefit;   
            }
    	}

    	return $sum;
    }
}
