<?php

/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 *
 * @author   <jsalis@stetson.edu>
 * @since 	 v0.0.0
 * 
 */
class Controller extends CController
{
	protected $_user;

	/**
	 * The default beforeAction redirects to the doorkeeper controller
	 * if the user is not logged in. Any subclass of Controller 
	 * (except DoorKeeperController) that overrides this function 
	 * should call this one first.
	 * 
	 * @param  Action   $action Represents the action to be called.
	 * @return boolean          Whether the action should be called.
	 */
	protected function beforeAction($action)
	{
		if (Yii::app()->user->isGuest)
		{
			Yii::app()->user->loginRequired();
			return false;
		}
		else
		{
			return true;
		}
	}

	/**
	 * Gets the list of economy entities for a user who is logged in.
	 * 
	 * @return array<Entity> 	The list of the user's entities.
	 */
	public function getUserEntityList()
	{
		if (!Yii::app()->user->isGuest && Yii::app()->user->economyID != UserIdentity::NULL_STATE_VALUE)
		{
			return $this->_user->getUserEntitiesInEconomy(Yii::app()->user->economyID);
		}
	}

	/**
	 * Performs validation on the specified form if the request is ajax.
	 * 
	 * @param CFormModel 	$form 	The form to validate.
	 */
	protected function performAjaxValidation($form)
	{
	    if (Yii::app()->request->isAjaxRequest)
	    {
	        echo CActiveForm::validate(array($form));
	        Yii::app()->end();
	    }
	}
}
