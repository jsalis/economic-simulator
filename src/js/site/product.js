jQuery(function($) {
	$('[data-target="#update_product"]').click(function(event) {
		var itemID = $(this).attr('id');
		var form = $('#update_product_form');
		form.find('input[id="ProductForm_item_id"]').val(itemID);
		form.find('input[id="ProductForm_name"]').val(productData[itemID]['name']);
		form.find('textarea[id="ProductForm_description"]').val(productData[itemID]['description']);
		form.find('input[id="ProductForm_price"]').val(productData[itemID]['price']);
		form.find('select[id="ProductForm_economy_id"] option').filter(function() {
			return $(this).text() == productData[itemID]['export'];
		}).attr('selected', true);
		form.find('select[id="ProductForm_economy_id"]').trigger('chosen:updated');
	});
});