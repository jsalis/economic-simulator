jQuery(function($) {
	$('[data-target="#update_public_good"]').click(function(event) {
		var itemID = $(this).attr('id');
		$('#Public_Good_id_update').val(itemID);
		$('#Public_Good_name_update').val($('#'+ itemID +' #name').html());
		$('#Public_Good_description_update').val($('#'+ itemID +' #description').html());
		$('#Public_Good_monetary_start_update').val($('#'+ itemID +' #monetary_start').html());
		$('#Public_Good_monetary_decrease_rate_update').val($('#'+ itemID +' #monetary_decrease_rate').html());
		$('#Public_Good_efficiency_start_update').val($('#'+ itemID +' #efficiency_start').html());
		$('#Public_Good_efficiency_decrease_rate_update').val($('#'+ itemID +' #efficiency_decrease_rate').html());
		$('#Public_Good_health_start_update').val($('#'+ itemID +' #health_start').html());
		$('#Public_Good_health_decrease_rate_update').val($('#'+ itemID +' #health_decrease_rate').html());
		$('#Public_Good_entity_type_update #' + $('#'+ itemID +' #type').html()).attr('selected', true);
	});
});