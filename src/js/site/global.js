
/**
 * The openSidebar method goes and opens the sidebar by simply adding the sidebar-open
 * class to the content-container and the sidebar transition class. This is done only if
 * the menu the button does not have the side-bar clicked method. The button gets both a 
 * sidebar-clicked class associated with it to notify that the button has opened the toggle 
 * as well as a click-in-process class. The click-in-process class is then removed after a 
 * millisecond. 
 * 
 * This class is added to halt the process of the sidebar closing since the user is both
 * clicking in the element, and the body of the content. When the user clicks in the body
 * of the content (away from the sidebar) he/she should be allowed to also close the sidebar.
 */
function openSidebar() 
{

	if (!$('#menu-toggle').hasClass('sidebar-clicked'))
	{
		$('.pusher-overlay').fadeIn(400);
		$('#menu-toggle').addClass("sidebar-clicked");
		$('#menu-toggle').addClass("sidebar-click-in-process");

		$('.content-container').addClass('sidebar-open');
		$('.content-container').addClass('sidebar-transition');

		setTimeout(function() {
			$('#menu-toggle').removeClass("sidebar-click-in-process");
		}, 1);

		$('.pusher').click(function() {
			if ($('.content-container').hasClass('sidebar-open') && !$('#menu-toggle').hasClass('sidebar-click-in-process'))
			{
				closeSidebar();
			}
		});

		$('.pusher-overlay').click(function() {
			if ($('.content-container').hasClass('sidebar-open') && !$('#menu-toggle').hasClass('sidebar-click-in-process'))
			{
				closeSidebar();
			}
		});
	}
}

/**
 * The closeSidebar method goes and removes the sidebar-open class as well as the transition
 * class, but only after the transition has finished (plus a couple extra milliseconds). We 
 * then remove the click function associated with the pusher class and remove the sidebar-clicked
 * class with the toggle button.
 */
function closeSidebar() 
{

	$('.pusher-overlay').fadeOut(400);

	$('.content-container').removeClass('sidebar-open');
	console.log($('.sidebar-transition').css('transition-duration'));

	setTimeout(function() {
		$('.content-container').removeClass('sidebar-transition');
	}, 400);


	$('.pusher').unbind();
	$('.pusher-overlay').unbind();
    $('#menu-toggle').removeClass("sidebar-clicked");

}

jQuery(function($) {

	

	$('#menu-toggle').click(function(){
		openSidebar();
	});

	$('#close-menu').click(function(){
		closeSidebar();
	});

	$('.pusher-overlay').hide();

	/** 
	 * GENERAL BEHAVIOR ACTIVATION
	 */
	$('[data-toggle="tooltip"]').tooltip();
	$('[data-toggle="popover"]').popover();
	$('.switch').bootstrapSwitch({
		size: 'mini',
	});
	$('.phone').mask('(999) 999-9999');
	$('.date').datetimepicker({
		pickTime: false
	});
	$('.time').datetimepicker({
		pickDate: false
	});
	$('.chosen').chosen({
		placeholder_text_single: ' ',
		disable_search_threshold: 8,
		search_contains: true,
		enable_split_word_search: true,
		display_disabled_options: true,
		allow_single_deselect: false,
		width: '100%',
	});
	$('table').footable({
		breakpoints: {
			phone: 500,
			tablet: 650
		}
	});
	$('.money').focusout(function() {
		var regex = /^\d+(?:\.\d\d?)?$/;
		var match = $(this).val().match(regex);
		$(this).val(match);
	});
	$('form').submit(function() {
		$(this).find('button[type="submit"]').not('.download').prop('disabled', true).html('Loading...');
	});

	/**
	 * FIXED TABLE HEADER
	 *
	 * Used to fix table headers to the top of a scrolling container. This behavior is dependent on
	 * the floatThead plugin. Some additional script makes sure the table header is refreshed or 'reflowed'
	 * whenever the DOM structure changes.
	 *
	 * NOTE: Fixed table headers are not compatible with FooTable sorting.
	 * 
	 * @author jsalis@stetson.edu
	 *
	 * @fix-header	The table to apply the fixed header.
	 * @scroller	The container for the table element. Class name must start with @scroller.
	 */
	$('.fix-header').floatThead({
		useAbsolutePositioning: true,
		scrollContainer: function($table) {
			return $table.closest('[class^="scroller"]');
		}
	});
	$('[data-toggle="tab"]').click(function() {
		$('.fix-header').floatThead('reflow');
	});

	/** 
	 * DYNAMIC FORM EDITOR
	 * 
	 * Used to control form submission and editing by showing a button container when the form 
	 * changes state. Inputs without a name are ignored.
	 *
	 * @author jsalis@stetson.edu
	 *
	 * @form			The form tag should contain @form-buttons, @btn-cancel, @btn-edit, and @static-input.
	 * @form-buttons	The container for the form buttons (@btn-cancel, and a submit button).
	 * @btn-cancel		The button that resets the form and hides the form buttons.
	 * @btn-edit		A button that shows @form-buttons and makes the form editable.
	 * @static-input	A container to hold the static representation of an input field. The name of this container
	 *					should be the ID of the input associated with it. When @btn-edit is clicked, the input will
	 *					be populated with the contents of the container, and the container will be hidden.
	 */
	$('input').not('[role="search"]').focus(function(event) {
		var form = $(this).closest('form');
		form.find('.form-buttons').removeClass('hide');
	});
	$('.btn-cancel').click(function() {
		var form = $(this).parents('form');
		form[0].reset();
		$(this).parents('.form-buttons').addClass('hide');
		form.find('.btn-edit').removeClass('hide');
		form.find('.static-input').each(function() {
			$(this).removeClass('hide');
			var name = $(this).attr('name');
			$('#' + name).addClass('hide');
		});
	});
	$('.btn-edit').click(function() {
		$(this).addClass('hide');
		var form = $(this).parents('form');
		form.find('.form-buttons').removeClass('hide');
		form.find('.static-input').each(function() {
			$(this).addClass('hide');
			var name = $(this).attr('name');
			console.log(name);
			$('#' + name).val($(this).html());
			$('#' + name).removeClass('hide');
		});
	});
	// Initial hiding
	$('.form-buttons').addClass('hide');
	$('.static-input').each(function() {
		var name = $(this).attr('name');
		$('#' + name).addClass('hide');
	});

	/** 
	 * CHECKBOX TOGGLE
	 * 
	 * Used to make a container toggle a checkbox that is inside it.
	 *
	 * @author jsalis@stetson.edu
	 *
	 * @checkbox-toggle		The container of the checkbox.
	 */
	$('.checkbox-toggle').click(function(event) {
		var checkbox = $(this).find('input[type=checkbox]');
		if (event.target.id != checkbox.attr('id'))
		{
			checkbox.prop('checked', !checkbox.prop('checked'));
			$(this).parents('form').trigger('change');
		}
	});

	/**
	 * FORM SUBMIT
	 *
	 * Used to make a container submit a form when it is clicked.
	 */
	$('.form-submit').css('cursor', 'pointer');
	$('.form-submit').click(function() {
		$(this).find('form').submit();
	});

	/**
	 * LOAD TAB FROM URL HASHTAG
	 *
	 * Used to extract a hashtag from a url and show a specific navigation tab.
	 * The url hashtag also updates when the active tab switches.
	 */
	var url = document.location.toString();
	if (url.match('#')) {
		var split = url.split('#');
		$('.nav-tabs a[href=#' + split[1] + ']').tab('show');
	}
	$('.nav-tabs a').on('shown.bs.tab', function (e) {
		window.location.hash = e.target.hash;
	});
});
