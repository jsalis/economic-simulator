jQuery(function($) {
	$('[data-target="#view_entities"]').click(function(event) {
		var userID = $(this).attr('id');
		var table = $('#view_entity_table');
		var tbody = table.children('tbody');
		if (typeof userID !== 'undefined') {
			$('#view_entity_title').html(studentData[userID]['name']);
			tbody.find('tr').each(function() {
				var entityID = $(this).attr('id');
				if (studentData[userID][entityID] === undefined) {
					$(this).hide();
				} else {
					$(this).show();
				}
			});
		} else {
			$('#view_entity_title').html('All In Economy');
			tbody.find('tr').each(function() {
				$(this).show();
			});
		}
	});
	$('.dropdown-menu').click(function(event) {
		event.stopPropagation();
	});
	$('[data-target="#give_company"]').click(function(event) {
		var userID = $(this).attr('id');
		$('#give_company_title').html('Give Company to ' + studentData[userID]['name']);
		var form = $('#give_company_form');
		form.find('input[id="Entity_userEmail"]').val(studentData[userID]['email']);
		$('#give_company').modal('show');
	});
});